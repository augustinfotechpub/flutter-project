// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class NotificationModel with _$NotificationModel {
  const factory NotificationModel({
    required List<NotificationsList> results,
  }) = _NotificationModel;

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      _$NotificationModelFromJson(json);
}

@freezed
class NotificationsList with _$NotificationsList {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory NotificationsList({
    required int notificationId,
    required String userId,
    required String notification,
    required DateTime createdTime,
    required String seen,
  }) = _NotificationsList;

  factory NotificationsList.fromJson(Map<String, dynamic> json) =>
      _$NotificationsListFromJson(json);
}




// results NotificationsList