part of 'notification_bloc.dart';

@freezed
class NotificationState with _$NotificationState {
  const factory NotificationState.initial() = _Initial;
  const factory NotificationState.loading() = _Loading;
  const factory NotificationState.success(
      List<NotificationsList> notifications) = _Success;
  const factory NotificationState.error(String error) = _Error;
}
