part of 'notification_bloc.dart';

@freezed
class NotificationEvent with _$NotificationEvent {
  const factory NotificationEvent.getNotification() = _GetNotification;
  const factory NotificationEvent.postNotification({
    required Map<String, dynamic> notification,
  }) = _PostNotification;
  const factory NotificationEvent.deleteNotification(
      {required String id,
      required List<NotificationsList> notificationList}) = _DeleteNotification;
}
