import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/notification/model.dart';
import 'package:shift_alerts/src/utils/hive/hive_keys.dart';
import 'package:shift_alerts/src/utils/hive/hive_utils.dart';

part 'notification_event.dart';
part 'notification_state.dart';
part 'notification_bloc.freezed.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  List<NotificationsList> notifications = [];
  NotificationBloc() : super(const _Initial()) {
    on<NotificationEvent>((event, emit) async {
      if (event is _GetNotification) {
        emit(const _Loading());
        try {
          final response = await apiProvider.getNotification(
            userID: HiveUtils.get(HiveKeys.user).toString(),
          );
          if (response.statusCode == 200) {
            NotificationModel notification =
                NotificationModel.fromJson(jsonDecode(response.body));
            notifications = notification.results;
            emit(_Success(notifications));
          } else {
            emit(_Error(jsonDecode(response.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _PostNotification) {
        emit(const _Loading());
        try {
          final response =
              await apiProvider.postNotification(map: event.notification);
          if (response.statusCode == 200) {
            emit(_Success(notifications));
          } else {
            emit(_Error(jsonDecode(response.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _DeleteNotification) {
        try {
          final response = await apiProvider.deleteNotification(id: event.id);
          if (response.statusCode == 204) {
            emit(_Success(event.notificationList));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
