// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NotificationModel _$$_NotificationModelFromJson(Map<String, dynamic> json) =>
    _$_NotificationModel(
      results: (json['results'] as List<dynamic>)
          .map((e) => NotificationsList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_NotificationModelToJson(
        _$_NotificationModel instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

_$_NotificationsList _$$_NotificationsListFromJson(Map<String, dynamic> json) =>
    _$_NotificationsList(
      notificationId: json['notification_id'] as int,
      userId: json['user_id'] as String,
      notification: json['notification'] as String,
      createdTime: DateTime.parse(json['created_time'] as String),
      seen: json['seen'] as String,
    );

Map<String, dynamic> _$$_NotificationsListToJson(
        _$_NotificationsList instance) =>
    <String, dynamic>{
      'notification_id': instance.notificationId,
      'user_id': instance.userId,
      'notification': instance.notification,
      'created_time': instance.createdTime.toIso8601String(),
      'seen': instance.seen,
    };
