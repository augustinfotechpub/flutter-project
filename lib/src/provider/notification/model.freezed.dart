// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NotificationModel _$NotificationModelFromJson(Map<String, dynamic> json) {
  return _NotificationModel.fromJson(json);
}

/// @nodoc
mixin _$NotificationModel {
  List<NotificationsList> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NotificationModelCopyWith<NotificationModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotificationModelCopyWith<$Res> {
  factory $NotificationModelCopyWith(
          NotificationModel value, $Res Function(NotificationModel) then) =
      _$NotificationModelCopyWithImpl<$Res, NotificationModel>;
  @useResult
  $Res call({List<NotificationsList> results});
}

/// @nodoc
class _$NotificationModelCopyWithImpl<$Res, $Val extends NotificationModel>
    implements $NotificationModelCopyWith<$Res> {
  _$NotificationModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<NotificationsList>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_NotificationModelCopyWith<$Res>
    implements $NotificationModelCopyWith<$Res> {
  factory _$$_NotificationModelCopyWith(_$_NotificationModel value,
          $Res Function(_$_NotificationModel) then) =
      __$$_NotificationModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<NotificationsList> results});
}

/// @nodoc
class __$$_NotificationModelCopyWithImpl<$Res>
    extends _$NotificationModelCopyWithImpl<$Res, _$_NotificationModel>
    implements _$$_NotificationModelCopyWith<$Res> {
  __$$_NotificationModelCopyWithImpl(
      _$_NotificationModel _value, $Res Function(_$_NotificationModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_$_NotificationModel(
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<NotificationsList>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NotificationModel implements _NotificationModel {
  const _$_NotificationModel({required final List<NotificationsList> results})
      : _results = results;

  factory _$_NotificationModel.fromJson(Map<String, dynamic> json) =>
      _$$_NotificationModelFromJson(json);

  final List<NotificationsList> _results;
  @override
  List<NotificationsList> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'NotificationModel(results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_NotificationModel &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_NotificationModelCopyWith<_$_NotificationModel> get copyWith =>
      __$$_NotificationModelCopyWithImpl<_$_NotificationModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NotificationModelToJson(
      this,
    );
  }
}

abstract class _NotificationModel implements NotificationModel {
  const factory _NotificationModel(
      {required final List<NotificationsList> results}) = _$_NotificationModel;

  factory _NotificationModel.fromJson(Map<String, dynamic> json) =
      _$_NotificationModel.fromJson;

  @override
  List<NotificationsList> get results;
  @override
  @JsonKey(ignore: true)
  _$$_NotificationModelCopyWith<_$_NotificationModel> get copyWith =>
      throw _privateConstructorUsedError;
}

NotificationsList _$NotificationsListFromJson(Map<String, dynamic> json) {
  return _NotificationsList.fromJson(json);
}

/// @nodoc
mixin _$NotificationsList {
  int get notificationId => throw _privateConstructorUsedError;
  String get userId => throw _privateConstructorUsedError;
  String get notification => throw _privateConstructorUsedError;
  DateTime get createdTime => throw _privateConstructorUsedError;
  String get seen => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NotificationsListCopyWith<NotificationsList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotificationsListCopyWith<$Res> {
  factory $NotificationsListCopyWith(
          NotificationsList value, $Res Function(NotificationsList) then) =
      _$NotificationsListCopyWithImpl<$Res, NotificationsList>;
  @useResult
  $Res call(
      {int notificationId,
      String userId,
      String notification,
      DateTime createdTime,
      String seen});
}

/// @nodoc
class _$NotificationsListCopyWithImpl<$Res, $Val extends NotificationsList>
    implements $NotificationsListCopyWith<$Res> {
  _$NotificationsListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notificationId = null,
    Object? userId = null,
    Object? notification = null,
    Object? createdTime = null,
    Object? seen = null,
  }) {
    return _then(_value.copyWith(
      notificationId: null == notificationId
          ? _value.notificationId
          : notificationId // ignore: cast_nullable_to_non_nullable
              as int,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      notification: null == notification
          ? _value.notification
          : notification // ignore: cast_nullable_to_non_nullable
              as String,
      createdTime: null == createdTime
          ? _value.createdTime
          : createdTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      seen: null == seen
          ? _value.seen
          : seen // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_NotificationsListCopyWith<$Res>
    implements $NotificationsListCopyWith<$Res> {
  factory _$$_NotificationsListCopyWith(_$_NotificationsList value,
          $Res Function(_$_NotificationsList) then) =
      __$$_NotificationsListCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int notificationId,
      String userId,
      String notification,
      DateTime createdTime,
      String seen});
}

/// @nodoc
class __$$_NotificationsListCopyWithImpl<$Res>
    extends _$NotificationsListCopyWithImpl<$Res, _$_NotificationsList>
    implements _$$_NotificationsListCopyWith<$Res> {
  __$$_NotificationsListCopyWithImpl(
      _$_NotificationsList _value, $Res Function(_$_NotificationsList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notificationId = null,
    Object? userId = null,
    Object? notification = null,
    Object? createdTime = null,
    Object? seen = null,
  }) {
    return _then(_$_NotificationsList(
      notificationId: null == notificationId
          ? _value.notificationId
          : notificationId // ignore: cast_nullable_to_non_nullable
              as int,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      notification: null == notification
          ? _value.notification
          : notification // ignore: cast_nullable_to_non_nullable
              as String,
      createdTime: null == createdTime
          ? _value.createdTime
          : createdTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      seen: null == seen
          ? _value.seen
          : seen // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_NotificationsList implements _NotificationsList {
  const _$_NotificationsList(
      {required this.notificationId,
      required this.userId,
      required this.notification,
      required this.createdTime,
      required this.seen});

  factory _$_NotificationsList.fromJson(Map<String, dynamic> json) =>
      _$$_NotificationsListFromJson(json);

  @override
  final int notificationId;
  @override
  final String userId;
  @override
  final String notification;
  @override
  final DateTime createdTime;
  @override
  final String seen;

  @override
  String toString() {
    return 'NotificationsList(notificationId: $notificationId, userId: $userId, notification: $notification, createdTime: $createdTime, seen: $seen)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_NotificationsList &&
            (identical(other.notificationId, notificationId) ||
                other.notificationId == notificationId) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.notification, notification) ||
                other.notification == notification) &&
            (identical(other.createdTime, createdTime) ||
                other.createdTime == createdTime) &&
            (identical(other.seen, seen) || other.seen == seen));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, notificationId, userId, notification, createdTime, seen);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_NotificationsListCopyWith<_$_NotificationsList> get copyWith =>
      __$$_NotificationsListCopyWithImpl<_$_NotificationsList>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NotificationsListToJson(
      this,
    );
  }
}

abstract class _NotificationsList implements NotificationsList {
  const factory _NotificationsList(
      {required final int notificationId,
      required final String userId,
      required final String notification,
      required final DateTime createdTime,
      required final String seen}) = _$_NotificationsList;

  factory _NotificationsList.fromJson(Map<String, dynamic> json) =
      _$_NotificationsList.fromJson;

  @override
  int get notificationId;
  @override
  String get userId;
  @override
  String get notification;
  @override
  DateTime get createdTime;
  @override
  String get seen;
  @override
  @JsonKey(ignore: true)
  _$$_NotificationsListCopyWith<_$_NotificationsList> get copyWith =>
      throw _privateConstructorUsedError;
}
