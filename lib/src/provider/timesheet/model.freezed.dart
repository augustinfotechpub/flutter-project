// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Timesheet _$TimesheetFromJson(Map<String, dynamic> json) {
  return _Timesheet.fromJson(json);
}

/// @nodoc
mixin _$Timesheet {
  List<TimesheetDetail> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TimesheetCopyWith<Timesheet> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimesheetCopyWith<$Res> {
  factory $TimesheetCopyWith(Timesheet value, $Res Function(Timesheet) then) =
      _$TimesheetCopyWithImpl<$Res, Timesheet>;
  @useResult
  $Res call({List<TimesheetDetail> results});
}

/// @nodoc
class _$TimesheetCopyWithImpl<$Res, $Val extends Timesheet>
    implements $TimesheetCopyWith<$Res> {
  _$TimesheetCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<TimesheetDetail>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TimesheetCopyWith<$Res> implements $TimesheetCopyWith<$Res> {
  factory _$$_TimesheetCopyWith(
          _$_Timesheet value, $Res Function(_$_Timesheet) then) =
      __$$_TimesheetCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<TimesheetDetail> results});
}

/// @nodoc
class __$$_TimesheetCopyWithImpl<$Res>
    extends _$TimesheetCopyWithImpl<$Res, _$_Timesheet>
    implements _$$_TimesheetCopyWith<$Res> {
  __$$_TimesheetCopyWithImpl(
      _$_Timesheet _value, $Res Function(_$_Timesheet) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_$_Timesheet(
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<TimesheetDetail>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Timesheet implements _Timesheet {
  const _$_Timesheet({required final List<TimesheetDetail> results})
      : _results = results;

  factory _$_Timesheet.fromJson(Map<String, dynamic> json) =>
      _$$_TimesheetFromJson(json);

  final List<TimesheetDetail> _results;
  @override
  List<TimesheetDetail> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'Timesheet(results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Timesheet &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TimesheetCopyWith<_$_Timesheet> get copyWith =>
      __$$_TimesheetCopyWithImpl<_$_Timesheet>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TimesheetToJson(
      this,
    );
  }
}

abstract class _Timesheet implements Timesheet {
  const factory _Timesheet({required final List<TimesheetDetail> results}) =
      _$_Timesheet;

  factory _Timesheet.fromJson(Map<String, dynamic> json) =
      _$_Timesheet.fromJson;

  @override
  List<TimesheetDetail> get results;
  @override
  @JsonKey(ignore: true)
  _$$_TimesheetCopyWith<_$_Timesheet> get copyWith =>
      throw _privateConstructorUsedError;
}

TimesheetDetail _$TimesheetDetailFromJson(Map<String, dynamic> json) {
  return _TimesheetDetail.fromJson(json);
}

/// @nodoc
mixin _$TimesheetDetail {
  int get timesheetId => throw _privateConstructorUsedError;
  String get shiftId => throw _privateConstructorUsedError;
  String get clinicianId => throw _privateConstructorUsedError;
  String get clinicianName => throw _privateConstructorUsedError;
  String get facilityName => throw _privateConstructorUsedError;
  String get pocAdmin => throw _privateConstructorUsedError;
  String get shiftColor => throw _privateConstructorUsedError;
  String get feedbackId => throw _privateConstructorUsedError;
  String get clinicianPosition => throw _privateConstructorUsedError;
  String get breakInDate => throw _privateConstructorUsedError;
  String get checkInTime => throw _privateConstructorUsedError;
  String get breakOutDate => throw _privateConstructorUsedError;
  String get checkOutTime => throw _privateConstructorUsedError;
  String get shiftDate => throw _privateConstructorUsedError;
  String get clinicianNote => throw _privateConstructorUsedError;
  String get facilityNote => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  String get ratings => throw _privateConstructorUsedError;
  String get feedback => throw _privateConstructorUsedError;
  String get pocSignature => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TimesheetDetailCopyWith<TimesheetDetail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimesheetDetailCopyWith<$Res> {
  factory $TimesheetDetailCopyWith(
          TimesheetDetail value, $Res Function(TimesheetDetail) then) =
      _$TimesheetDetailCopyWithImpl<$Res, TimesheetDetail>;
  @useResult
  $Res call(
      {int timesheetId,
      String shiftId,
      String clinicianId,
      String clinicianName,
      String facilityName,
      String pocAdmin,
      String shiftColor,
      String feedbackId,
      String clinicianPosition,
      String breakInDate,
      String checkInTime,
      String breakOutDate,
      String checkOutTime,
      String shiftDate,
      String clinicianNote,
      String facilityNote,
      String status,
      String ratings,
      String feedback,
      String pocSignature});
}

/// @nodoc
class _$TimesheetDetailCopyWithImpl<$Res, $Val extends TimesheetDetail>
    implements $TimesheetDetailCopyWith<$Res> {
  _$TimesheetDetailCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? timesheetId = null,
    Object? shiftId = null,
    Object? clinicianId = null,
    Object? clinicianName = null,
    Object? facilityName = null,
    Object? pocAdmin = null,
    Object? shiftColor = null,
    Object? feedbackId = null,
    Object? clinicianPosition = null,
    Object? breakInDate = null,
    Object? checkInTime = null,
    Object? breakOutDate = null,
    Object? checkOutTime = null,
    Object? shiftDate = null,
    Object? clinicianNote = null,
    Object? facilityNote = null,
    Object? status = null,
    Object? ratings = null,
    Object? feedback = null,
    Object? pocSignature = null,
  }) {
    return _then(_value.copyWith(
      timesheetId: null == timesheetId
          ? _value.timesheetId
          : timesheetId // ignore: cast_nullable_to_non_nullable
              as int,
      shiftId: null == shiftId
          ? _value.shiftId
          : shiftId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      facilityName: null == facilityName
          ? _value.facilityName
          : facilityName // ignore: cast_nullable_to_non_nullable
              as String,
      pocAdmin: null == pocAdmin
          ? _value.pocAdmin
          : pocAdmin // ignore: cast_nullable_to_non_nullable
              as String,
      shiftColor: null == shiftColor
          ? _value.shiftColor
          : shiftColor // ignore: cast_nullable_to_non_nullable
              as String,
      feedbackId: null == feedbackId
          ? _value.feedbackId
          : feedbackId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      breakInDate: null == breakInDate
          ? _value.breakInDate
          : breakInDate // ignore: cast_nullable_to_non_nullable
              as String,
      checkInTime: null == checkInTime
          ? _value.checkInTime
          : checkInTime // ignore: cast_nullable_to_non_nullable
              as String,
      breakOutDate: null == breakOutDate
          ? _value.breakOutDate
          : breakOutDate // ignore: cast_nullable_to_non_nullable
              as String,
      checkOutTime: null == checkOutTime
          ? _value.checkOutTime
          : checkOutTime // ignore: cast_nullable_to_non_nullable
              as String,
      shiftDate: null == shiftDate
          ? _value.shiftDate
          : shiftDate // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianNote: null == clinicianNote
          ? _value.clinicianNote
          : clinicianNote // ignore: cast_nullable_to_non_nullable
              as String,
      facilityNote: null == facilityNote
          ? _value.facilityNote
          : facilityNote // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      ratings: null == ratings
          ? _value.ratings
          : ratings // ignore: cast_nullable_to_non_nullable
              as String,
      feedback: null == feedback
          ? _value.feedback
          : feedback // ignore: cast_nullable_to_non_nullable
              as String,
      pocSignature: null == pocSignature
          ? _value.pocSignature
          : pocSignature // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TimesheetDetailCopyWith<$Res>
    implements $TimesheetDetailCopyWith<$Res> {
  factory _$$_TimesheetDetailCopyWith(
          _$_TimesheetDetail value, $Res Function(_$_TimesheetDetail) then) =
      __$$_TimesheetDetailCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int timesheetId,
      String shiftId,
      String clinicianId,
      String clinicianName,
      String facilityName,
      String pocAdmin,
      String shiftColor,
      String feedbackId,
      String clinicianPosition,
      String breakInDate,
      String checkInTime,
      String breakOutDate,
      String checkOutTime,
      String shiftDate,
      String clinicianNote,
      String facilityNote,
      String status,
      String ratings,
      String feedback,
      String pocSignature});
}

/// @nodoc
class __$$_TimesheetDetailCopyWithImpl<$Res>
    extends _$TimesheetDetailCopyWithImpl<$Res, _$_TimesheetDetail>
    implements _$$_TimesheetDetailCopyWith<$Res> {
  __$$_TimesheetDetailCopyWithImpl(
      _$_TimesheetDetail _value, $Res Function(_$_TimesheetDetail) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? timesheetId = null,
    Object? shiftId = null,
    Object? clinicianId = null,
    Object? clinicianName = null,
    Object? facilityName = null,
    Object? pocAdmin = null,
    Object? shiftColor = null,
    Object? feedbackId = null,
    Object? clinicianPosition = null,
    Object? breakInDate = null,
    Object? checkInTime = null,
    Object? breakOutDate = null,
    Object? checkOutTime = null,
    Object? shiftDate = null,
    Object? clinicianNote = null,
    Object? facilityNote = null,
    Object? status = null,
    Object? ratings = null,
    Object? feedback = null,
    Object? pocSignature = null,
  }) {
    return _then(_$_TimesheetDetail(
      timesheetId: null == timesheetId
          ? _value.timesheetId
          : timesheetId // ignore: cast_nullable_to_non_nullable
              as int,
      shiftId: null == shiftId
          ? _value.shiftId
          : shiftId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      facilityName: null == facilityName
          ? _value.facilityName
          : facilityName // ignore: cast_nullable_to_non_nullable
              as String,
      pocAdmin: null == pocAdmin
          ? _value.pocAdmin
          : pocAdmin // ignore: cast_nullable_to_non_nullable
              as String,
      shiftColor: null == shiftColor
          ? _value.shiftColor
          : shiftColor // ignore: cast_nullable_to_non_nullable
              as String,
      feedbackId: null == feedbackId
          ? _value.feedbackId
          : feedbackId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      breakInDate: null == breakInDate
          ? _value.breakInDate
          : breakInDate // ignore: cast_nullable_to_non_nullable
              as String,
      checkInTime: null == checkInTime
          ? _value.checkInTime
          : checkInTime // ignore: cast_nullable_to_non_nullable
              as String,
      breakOutDate: null == breakOutDate
          ? _value.breakOutDate
          : breakOutDate // ignore: cast_nullable_to_non_nullable
              as String,
      checkOutTime: null == checkOutTime
          ? _value.checkOutTime
          : checkOutTime // ignore: cast_nullable_to_non_nullable
              as String,
      shiftDate: null == shiftDate
          ? _value.shiftDate
          : shiftDate // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianNote: null == clinicianNote
          ? _value.clinicianNote
          : clinicianNote // ignore: cast_nullable_to_non_nullable
              as String,
      facilityNote: null == facilityNote
          ? _value.facilityNote
          : facilityNote // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      ratings: null == ratings
          ? _value.ratings
          : ratings // ignore: cast_nullable_to_non_nullable
              as String,
      feedback: null == feedback
          ? _value.feedback
          : feedback // ignore: cast_nullable_to_non_nullable
              as String,
      pocSignature: null == pocSignature
          ? _value.pocSignature
          : pocSignature // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_TimesheetDetail implements _TimesheetDetail {
  const _$_TimesheetDetail(
      {required this.timesheetId,
      required this.shiftId,
      required this.clinicianId,
      required this.clinicianName,
      required this.facilityName,
      required this.pocAdmin,
      required this.shiftColor,
      required this.feedbackId,
      required this.clinicianPosition,
      required this.breakInDate,
      required this.checkInTime,
      required this.breakOutDate,
      required this.checkOutTime,
      required this.shiftDate,
      required this.clinicianNote,
      required this.facilityNote,
      required this.status,
      required this.ratings,
      required this.feedback,
      required this.pocSignature});

  factory _$_TimesheetDetail.fromJson(Map<String, dynamic> json) =>
      _$$_TimesheetDetailFromJson(json);

  @override
  final int timesheetId;
  @override
  final String shiftId;
  @override
  final String clinicianId;
  @override
  final String clinicianName;
  @override
  final String facilityName;
  @override
  final String pocAdmin;
  @override
  final String shiftColor;
  @override
  final String feedbackId;
  @override
  final String clinicianPosition;
  @override
  final String breakInDate;
  @override
  final String checkInTime;
  @override
  final String breakOutDate;
  @override
  final String checkOutTime;
  @override
  final String shiftDate;
  @override
  final String clinicianNote;
  @override
  final String facilityNote;
  @override
  final String status;
  @override
  final String ratings;
  @override
  final String feedback;
  @override
  final String pocSignature;

  @override
  String toString() {
    return 'TimesheetDetail(timesheetId: $timesheetId, shiftId: $shiftId, clinicianId: $clinicianId, clinicianName: $clinicianName, facilityName: $facilityName, pocAdmin: $pocAdmin, shiftColor: $shiftColor, feedbackId: $feedbackId, clinicianPosition: $clinicianPosition, breakInDate: $breakInDate, checkInTime: $checkInTime, breakOutDate: $breakOutDate, checkOutTime: $checkOutTime, shiftDate: $shiftDate, clinicianNote: $clinicianNote, facilityNote: $facilityNote, status: $status, ratings: $ratings, feedback: $feedback, pocSignature: $pocSignature)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TimesheetDetail &&
            (identical(other.timesheetId, timesheetId) ||
                other.timesheetId == timesheetId) &&
            (identical(other.shiftId, shiftId) || other.shiftId == shiftId) &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId) &&
            (identical(other.clinicianName, clinicianName) ||
                other.clinicianName == clinicianName) &&
            (identical(other.facilityName, facilityName) ||
                other.facilityName == facilityName) &&
            (identical(other.pocAdmin, pocAdmin) ||
                other.pocAdmin == pocAdmin) &&
            (identical(other.shiftColor, shiftColor) ||
                other.shiftColor == shiftColor) &&
            (identical(other.feedbackId, feedbackId) ||
                other.feedbackId == feedbackId) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.breakInDate, breakInDate) ||
                other.breakInDate == breakInDate) &&
            (identical(other.checkInTime, checkInTime) ||
                other.checkInTime == checkInTime) &&
            (identical(other.breakOutDate, breakOutDate) ||
                other.breakOutDate == breakOutDate) &&
            (identical(other.checkOutTime, checkOutTime) ||
                other.checkOutTime == checkOutTime) &&
            (identical(other.shiftDate, shiftDate) ||
                other.shiftDate == shiftDate) &&
            (identical(other.clinicianNote, clinicianNote) ||
                other.clinicianNote == clinicianNote) &&
            (identical(other.facilityNote, facilityNote) ||
                other.facilityNote == facilityNote) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.ratings, ratings) || other.ratings == ratings) &&
            (identical(other.feedback, feedback) ||
                other.feedback == feedback) &&
            (identical(other.pocSignature, pocSignature) ||
                other.pocSignature == pocSignature));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        timesheetId,
        shiftId,
        clinicianId,
        clinicianName,
        facilityName,
        pocAdmin,
        shiftColor,
        feedbackId,
        clinicianPosition,
        breakInDate,
        checkInTime,
        breakOutDate,
        checkOutTime,
        shiftDate,
        clinicianNote,
        facilityNote,
        status,
        ratings,
        feedback,
        pocSignature
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TimesheetDetailCopyWith<_$_TimesheetDetail> get copyWith =>
      __$$_TimesheetDetailCopyWithImpl<_$_TimesheetDetail>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TimesheetDetailToJson(
      this,
    );
  }
}

abstract class _TimesheetDetail implements TimesheetDetail {
  const factory _TimesheetDetail(
      {required final int timesheetId,
      required final String shiftId,
      required final String clinicianId,
      required final String clinicianName,
      required final String facilityName,
      required final String pocAdmin,
      required final String shiftColor,
      required final String feedbackId,
      required final String clinicianPosition,
      required final String breakInDate,
      required final String checkInTime,
      required final String breakOutDate,
      required final String checkOutTime,
      required final String shiftDate,
      required final String clinicianNote,
      required final String facilityNote,
      required final String status,
      required final String ratings,
      required final String feedback,
      required final String pocSignature}) = _$_TimesheetDetail;

  factory _TimesheetDetail.fromJson(Map<String, dynamic> json) =
      _$_TimesheetDetail.fromJson;

  @override
  int get timesheetId;
  @override
  String get shiftId;
  @override
  String get clinicianId;
  @override
  String get clinicianName;
  @override
  String get facilityName;
  @override
  String get pocAdmin;
  @override
  String get shiftColor;
  @override
  String get feedbackId;
  @override
  String get clinicianPosition;
  @override
  String get breakInDate;
  @override
  String get checkInTime;
  @override
  String get breakOutDate;
  @override
  String get checkOutTime;
  @override
  String get shiftDate;
  @override
  String get clinicianNote;
  @override
  String get facilityNote;
  @override
  String get status;
  @override
  String get ratings;
  @override
  String get feedback;
  @override
  String get pocSignature;
  @override
  @JsonKey(ignore: true)
  _$$_TimesheetDetailCopyWith<_$_TimesheetDetail> get copyWith =>
      throw _privateConstructorUsedError;
}
