// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Timesheet _$$_TimesheetFromJson(Map<String, dynamic> json) => _$_Timesheet(
      results: (json['results'] as List<dynamic>)
          .map((e) => TimesheetDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TimesheetToJson(_$_Timesheet instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

_$_TimesheetDetail _$$_TimesheetDetailFromJson(Map<String, dynamic> json) =>
    _$_TimesheetDetail(
      timesheetId: json['timesheet_id'] as int,
      shiftId: json['shift_id'] as String,
      clinicianId: json['clinician_id'] as String,
      clinicianName: json['clinician_name'] as String,
      facilityName: json['facility_name'] as String,
      pocAdmin: json['poc_admin'] as String,
      shiftColor: json['shift_color'] as String,
      feedbackId: json['feedback_id'] as String,
      clinicianPosition: json['clinician_position'] as String,
      breakInDate: json['break_in_date'] as String,
      checkInTime: json['check_in_time'] as String,
      breakOutDate: json['break_out_date'] as String,
      checkOutTime: json['check_out_time'] as String,
      shiftDate: json['shift_date'] as String,
      clinicianNote: json['clinician_note'] as String,
      facilityNote: json['facility_note'] as String,
      status: json['status'] as String,
      ratings: json['ratings'] as String,
      feedback: json['feedback'] as String,
      pocSignature: json['poc_signature'] as String,
    );

Map<String, dynamic> _$$_TimesheetDetailToJson(_$_TimesheetDetail instance) =>
    <String, dynamic>{
      'timesheet_id': instance.timesheetId,
      'shift_id': instance.shiftId,
      'clinician_id': instance.clinicianId,
      'clinician_name': instance.clinicianName,
      'facility_name': instance.facilityName,
      'poc_admin': instance.pocAdmin,
      'shift_color': instance.shiftColor,
      'feedback_id': instance.feedbackId,
      'clinician_position': instance.clinicianPosition,
      'break_in_date': instance.breakInDate,
      'check_in_time': instance.checkInTime,
      'break_out_date': instance.breakOutDate,
      'check_out_time': instance.checkOutTime,
      'shift_date': instance.shiftDate,
      'clinician_note': instance.clinicianNote,
      'facility_note': instance.facilityNote,
      'status': instance.status,
      'ratings': instance.ratings,
      'feedback': instance.feedback,
      'poc_signature': instance.pocSignature,
    };
