// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'timesheet_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TimesheetEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            DateTime fromDate, DateTime toDate, DateTime focusDay)
        getTimesheet,
    required TResult Function(DateTime focusday) refreshTimesheet,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult? Function(DateTime focusday)? refreshTimesheet,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult Function(DateTime focusday)? refreshTimesheet,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTimesheet value) getTimesheet,
    required TResult Function(_RefreshTimesheet value) refreshTimesheet,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTimesheet value)? getTimesheet,
    TResult? Function(_RefreshTimesheet value)? refreshTimesheet,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTimesheet value)? getTimesheet,
    TResult Function(_RefreshTimesheet value)? refreshTimesheet,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimesheetEventCopyWith<$Res> {
  factory $TimesheetEventCopyWith(
          TimesheetEvent value, $Res Function(TimesheetEvent) then) =
      _$TimesheetEventCopyWithImpl<$Res, TimesheetEvent>;
}

/// @nodoc
class _$TimesheetEventCopyWithImpl<$Res, $Val extends TimesheetEvent>
    implements $TimesheetEventCopyWith<$Res> {
  _$TimesheetEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetTimesheetCopyWith<$Res> {
  factory _$$_GetTimesheetCopyWith(
          _$_GetTimesheet value, $Res Function(_$_GetTimesheet) then) =
      __$$_GetTimesheetCopyWithImpl<$Res>;
  @useResult
  $Res call({DateTime fromDate, DateTime toDate, DateTime focusDay});
}

/// @nodoc
class __$$_GetTimesheetCopyWithImpl<$Res>
    extends _$TimesheetEventCopyWithImpl<$Res, _$_GetTimesheet>
    implements _$$_GetTimesheetCopyWith<$Res> {
  __$$_GetTimesheetCopyWithImpl(
      _$_GetTimesheet _value, $Res Function(_$_GetTimesheet) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
    Object? focusDay = null,
  }) {
    return _then(_$_GetTimesheet(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      focusDay: null == focusDay
          ? _value.focusDay
          : focusDay // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_GetTimesheet implements _GetTimesheet {
  const _$_GetTimesheet(
      {required this.fromDate, required this.toDate, required this.focusDay});

  @override
  final DateTime fromDate;
  @override
  final DateTime toDate;
  @override
  final DateTime focusDay;

  @override
  String toString() {
    return 'TimesheetEvent.getTimesheet(fromDate: $fromDate, toDate: $toDate, focusDay: $focusDay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetTimesheet &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.focusDay, focusDay) ||
                other.focusDay == focusDay));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate, focusDay);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetTimesheetCopyWith<_$_GetTimesheet> get copyWith =>
      __$$_GetTimesheetCopyWithImpl<_$_GetTimesheet>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            DateTime fromDate, DateTime toDate, DateTime focusDay)
        getTimesheet,
    required TResult Function(DateTime focusday) refreshTimesheet,
  }) {
    return getTimesheet(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult? Function(DateTime focusday)? refreshTimesheet,
  }) {
    return getTimesheet?.call(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult Function(DateTime focusday)? refreshTimesheet,
    required TResult orElse(),
  }) {
    if (getTimesheet != null) {
      return getTimesheet(fromDate, toDate, focusDay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTimesheet value) getTimesheet,
    required TResult Function(_RefreshTimesheet value) refreshTimesheet,
  }) {
    return getTimesheet(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTimesheet value)? getTimesheet,
    TResult? Function(_RefreshTimesheet value)? refreshTimesheet,
  }) {
    return getTimesheet?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTimesheet value)? getTimesheet,
    TResult Function(_RefreshTimesheet value)? refreshTimesheet,
    required TResult orElse(),
  }) {
    if (getTimesheet != null) {
      return getTimesheet(this);
    }
    return orElse();
  }
}

abstract class _GetTimesheet implements TimesheetEvent {
  const factory _GetTimesheet(
      {required final DateTime fromDate,
      required final DateTime toDate,
      required final DateTime focusDay}) = _$_GetTimesheet;

  DateTime get fromDate;
  DateTime get toDate;
  DateTime get focusDay;
  @JsonKey(ignore: true)
  _$$_GetTimesheetCopyWith<_$_GetTimesheet> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshTimesheetCopyWith<$Res> {
  factory _$$_RefreshTimesheetCopyWith(
          _$_RefreshTimesheet value, $Res Function(_$_RefreshTimesheet) then) =
      __$$_RefreshTimesheetCopyWithImpl<$Res>;
  @useResult
  $Res call({DateTime focusday});
}

/// @nodoc
class __$$_RefreshTimesheetCopyWithImpl<$Res>
    extends _$TimesheetEventCopyWithImpl<$Res, _$_RefreshTimesheet>
    implements _$$_RefreshTimesheetCopyWith<$Res> {
  __$$_RefreshTimesheetCopyWithImpl(
      _$_RefreshTimesheet _value, $Res Function(_$_RefreshTimesheet) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? focusday = null,
  }) {
    return _then(_$_RefreshTimesheet(
      focusday: null == focusday
          ? _value.focusday
          : focusday // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_RefreshTimesheet implements _RefreshTimesheet {
  const _$_RefreshTimesheet({required this.focusday});

  @override
  final DateTime focusday;

  @override
  String toString() {
    return 'TimesheetEvent.refreshTimesheet(focusday: $focusday)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshTimesheet &&
            (identical(other.focusday, focusday) ||
                other.focusday == focusday));
  }

  @override
  int get hashCode => Object.hash(runtimeType, focusday);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshTimesheetCopyWith<_$_RefreshTimesheet> get copyWith =>
      __$$_RefreshTimesheetCopyWithImpl<_$_RefreshTimesheet>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            DateTime fromDate, DateTime toDate, DateTime focusDay)
        getTimesheet,
    required TResult Function(DateTime focusday) refreshTimesheet,
  }) {
    return refreshTimesheet(focusday);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult? Function(DateTime focusday)? refreshTimesheet,
  }) {
    return refreshTimesheet?.call(focusday);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(DateTime fromDate, DateTime toDate, DateTime focusDay)?
        getTimesheet,
    TResult Function(DateTime focusday)? refreshTimesheet,
    required TResult orElse(),
  }) {
    if (refreshTimesheet != null) {
      return refreshTimesheet(focusday);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTimesheet value) getTimesheet,
    required TResult Function(_RefreshTimesheet value) refreshTimesheet,
  }) {
    return refreshTimesheet(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTimesheet value)? getTimesheet,
    TResult? Function(_RefreshTimesheet value)? refreshTimesheet,
  }) {
    return refreshTimesheet?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTimesheet value)? getTimesheet,
    TResult Function(_RefreshTimesheet value)? refreshTimesheet,
    required TResult orElse(),
  }) {
    if (refreshTimesheet != null) {
      return refreshTimesheet(this);
    }
    return orElse();
  }
}

abstract class _RefreshTimesheet implements TimesheetEvent {
  const factory _RefreshTimesheet({required final DateTime focusday}) =
      _$_RefreshTimesheet;

  DateTime get focusday;
  @JsonKey(ignore: true)
  _$$_RefreshTimesheetCopyWith<_$_RefreshTimesheet> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TimesheetState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<TimesheetDetail> timesheet, DateTime focusday)
        success,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult? Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimesheetStateCopyWith<$Res> {
  factory $TimesheetStateCopyWith(
          TimesheetState value, $Res Function(TimesheetState) then) =
      _$TimesheetStateCopyWithImpl<$Res, TimesheetState>;
}

/// @nodoc
class _$TimesheetStateCopyWithImpl<$Res, $Val extends TimesheetState>
    implements $TimesheetStateCopyWith<$Res> {
  _$TimesheetStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$TimesheetStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'TimesheetState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<TimesheetDetail> timesheet, DateTime focusday)
        success,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult? Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements TimesheetState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$TimesheetStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'TimesheetState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<TimesheetDetail> timesheet, DateTime focusday)
        success,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult? Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements TimesheetState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<TimesheetDetail> timesheet, DateTime focusday});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$TimesheetStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? timesheet = null,
    Object? focusday = null,
  }) {
    return _then(_$_Success(
      null == timesheet
          ? _value._timesheet
          : timesheet // ignore: cast_nullable_to_non_nullable
              as List<TimesheetDetail>,
      null == focusday
          ? _value.focusday
          : focusday // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<TimesheetDetail> timesheet, this.focusday)
      : _timesheet = timesheet;

  final List<TimesheetDetail> _timesheet;
  @override
  List<TimesheetDetail> get timesheet {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_timesheet);
  }

  @override
  final DateTime focusday;

  @override
  String toString() {
    return 'TimesheetState.success(timesheet: $timesheet, focusday: $focusday)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._timesheet, _timesheet) &&
            (identical(other.focusday, focusday) ||
                other.focusday == focusday));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_timesheet), focusday);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<TimesheetDetail> timesheet, DateTime focusday)
        success,
    required TResult Function(String error) error,
  }) {
    return success(timesheet, focusday);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult? Function(String error)? error,
  }) {
    return success?.call(timesheet, focusday);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(timesheet, focusday);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements TimesheetState {
  const factory _Success(
          final List<TimesheetDetail> timesheet, final DateTime focusday) =
      _$_Success;

  List<TimesheetDetail> get timesheet;
  DateTime get focusday;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$TimesheetStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'TimesheetState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<TimesheetDetail> timesheet, DateTime focusday)
        success,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult? Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TimesheetDetail> timesheet, DateTime focusday)?
        success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements TimesheetState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
