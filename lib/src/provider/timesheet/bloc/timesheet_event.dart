part of 'timesheet_bloc.dart';

@freezed
class TimesheetEvent with _$TimesheetEvent {
  const factory TimesheetEvent.getTimesheet({
    required DateTime fromDate,
    required DateTime toDate,
    required DateTime focusDay,
  }) = _GetTimesheet;
  const factory TimesheetEvent.refreshTimesheet({required DateTime focusday}) =
      _RefreshTimesheet;
}
