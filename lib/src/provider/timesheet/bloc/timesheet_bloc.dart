import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/timesheet/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive_keys.dart';
import 'package:shift_alerts/src/utils/hive/hive_utils.dart';

part 'timesheet_event.dart';
part 'timesheet_state.dart';
part 'timesheet_bloc.freezed.dart';

class TimesheetBloc extends Bloc<TimesheetEvent, TimesheetState> {
  TimesheetBloc() : super(const _Initial()) {
    List<TimesheetDetail> myResult = [];
    on<TimesheetEvent>((event, emit) async {
      if (event is _GetTimesheet) {
        emit(const _Loading());
        try {
          final response = await apiProvider.getTimesheets(
            clinicianID: HiveUtils.get(HiveKeys.clinicianId),
            fromDate: event.fromDate.dmY(),
            toDate: event.toDate.dmY(),
          );
          if (response.statusCode == 200) {
            Timesheet timesheet =
                Timesheet.fromJson({'results': jsonDecode(response.body)});
            myResult = timesheet.results;
            emit(_Success(timesheet.results, event.focusDay));
          } else {
            emit(_Error(jsonDecode(response.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _RefreshTimesheet) {
        emit(_Success(myResult, event.focusday));
      }
    });
  }
}
