part of 'timesheet_bloc.dart';

@freezed
class TimesheetState with _$TimesheetState {
  const factory TimesheetState.initial() = _Initial;
  const factory TimesheetState.loading() = _Loading;
  const factory TimesheetState.success(
      List<TimesheetDetail> timesheet, DateTime focusday) = _Success;
  const factory TimesheetState.error(String error) = _Error;
}
