// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class Timesheet with _$Timesheet {
  const factory Timesheet({
    required List<TimesheetDetail> results,
  }) = _Timesheet;

  factory Timesheet.fromJson(Map<String, dynamic> json) =>
      _$TimesheetFromJson(json);
}

@freezed
class TimesheetDetail with _$TimesheetDetail {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory TimesheetDetail({
    required int timesheetId,
    required String shiftId,
    required String clinicianId,
    required String clinicianName,
    required String facilityName,
    required String pocAdmin,
    required String shiftColor,
    required String feedbackId,
    required String clinicianPosition,
    required String breakInDate,
    required String checkInTime,
    required String breakOutDate,
    required String checkOutTime,
    required String shiftDate,
    required String clinicianNote,
    required String facilityNote,
    required String status,
    required String ratings,
    required String feedback,
    required String pocSignature,
  }) = _TimesheetDetail;

  factory TimesheetDetail.fromJson(Map<String, dynamic> json) =>
      _$TimesheetDetailFromJson(json);
}
