import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';

part 'delete_account_event.dart';
part 'delete_account_state.dart';
part 'delete_account_bloc.freezed.dart';

class DeleteAccountBloc extends Bloc<DeleteAccountEvent, DeleteAccountState> {
  DeleteAccountBloc() : super(const _Initial()) {
    on<DeleteAccountEvent>((event, emit) async {
      if (event is _DeleteAccount) {
        emit(const _Loading());
        try {
          final response = userType == UserType.clinician
              ? await apiProvider.deleteAccount()
              : await adminApiProvider.deleteAccount();
          if (response.statusCode == 200) {
            String message = jsonDecode(response.body)['message'];
            if (message == 'deleted') {
              emit(const _Success());
            } else {
              emit(_Error(message));
            }
          } else {
            emit(const _Error(''));
          }
        } catch (e) {
          emit(const _Error(''));
        }
      }
    });
  }
}
