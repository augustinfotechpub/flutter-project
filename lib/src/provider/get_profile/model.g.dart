// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$$_UserFromJson(Map<String, dynamic> json) => _$_User(
      id: json['id'] as int,
      username: json['username'] as String,
      email: json['email'] as String,
      position: json['position'] as String,
      password: json['password'] as String,
      userId: json['user_id'] as String,
      admin: json['Admin'] as String,
    );

Map<String, dynamic> _$$_UserToJson(_$_User instance) => <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'email': instance.email,
      'position': instance.position,
      'password': instance.password,
      'user_id': instance.userId,
      'Admin': instance.admin,
    };
