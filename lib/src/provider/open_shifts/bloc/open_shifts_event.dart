part of 'open_shifts_bloc.dart';

@freezed
class OpenShiftsEvent with _$OpenShiftsEvent {
  const factory OpenShiftsEvent.getOpenShifts({
    required String fromDate,
    required String toDate,
  }) = _GetOpenShifts;

  const factory OpenShiftsEvent.refreshOpenShifts({
    required String fromDate,
    required String toDate,
  }) = _RefreshOpenShifts;

  const factory OpenShiftsEvent.filterOpenShifts({
    required String date,
  }) = _FilterOpenShifts;

  const factory OpenShiftsEvent.editShift({
    required String id,
    required String status,
    required String facilityId,
    required DateTime selectedDate,
    required List<String> reqUsers,
  }) = _EditShift;
}
