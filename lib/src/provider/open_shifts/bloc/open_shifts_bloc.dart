import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/extension.dart';

part 'open_shifts_event.dart';
part 'open_shifts_state.dart';
part 'open_shifts_bloc.freezed.dart';

List<Shift> _openShifts = [];

class OpenShiftsBloc extends Bloc<OpenShiftsEvent, OpenShiftsState> {
  OpenShiftsBloc() : super(const _Initial()) {
    on<OpenShiftsEvent>((event, emit) async {
      if (event is _GetOpenShifts) {
        emit(const _Loading());
        try {
          final shift = await apiProvider.getOpenShifts(
            position: HiveUtils.get(HiveKeys.position),
            fromDate: event.fromDate,
            toDate: event.toDate,
          );
          if (shift.statusCode == 200) {
            UpcomingShift openShifts =
                UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
            _openShifts = List.from(openShifts.shift);
            try {
              final shift = await apiProvider.getUpcomingShifts(
                  fromDate: event.fromDate,
                  toDate: event.toDate,
                  id: HiveUtils.get(HiveKeys.clinicianId));
              if (shift.statusCode == 200) {
                UpcomingShift pendingShift =
                    UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
                _openShifts.addAll(
                    pendingShift.shift.where((e) => e.status.isPending()));
                emit(_Success(_openShifts));
              } else {
                emit(_Error(jsonDecode(shift.body)));
              }
            } catch (e) {
              emit(_Error(e.toString()));
            }
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }

      if (event is _RefreshOpenShifts) {
        try {
          final shift = await apiProvider.getOpenShifts(
            position: HiveUtils.get(HiveKeys.position),
            fromDate: event.fromDate,
            toDate: event.toDate,
          );
          if (shift.statusCode == 200) {
            UpcomingShift openShifts =
                UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
            _openShifts = List.from(openShifts.shift);
            try {
              final shift = await apiProvider.getUpcomingShifts(
                  fromDate: event.fromDate,
                  toDate: event.toDate,
                  id: HiveUtils.get(HiveKeys.clinicianId));
              if (shift.statusCode == 200) {
                UpcomingShift pendingShift =
                    UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
                _openShifts.addAll(
                    pendingShift.shift.where((e) => e.status.isPending()));
                emit(_Success(_openShifts));
              } else {
                emit(_Error(jsonDecode(shift.body)));
              }
            } catch (e) {
              emit(_Error(e.toString()));
            }
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }

      if (event is _FilterOpenShifts) {
        emit(const _Initial());
        emit(_Success(_openShifts));
      }

      if (event is _EditShift) {
        emit(const _Loading());
        try {
          final shift = await apiProvider.editShift(
            id: event.id,
            data: {'requesting_users': event.reqUsers},
          );
          if (shift.statusCode == 200) {
            await sendNotification(event.facilityId, event.status);
            openShiftsBloc.add(
              OpenShiftsEvent.getOpenShifts(
                fromDate: event.selectedDate
                    .firstDate(event.selectedDate.month)
                    .dmY(),
                toDate: event.selectedDate.lastDate().dmY(),
              ),
            );
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }

  Future<void> sendNotification(String facilityId, String status) async {
    List<String> adminId = [];
    final admins = await apiProvider.getAdmins();
    for (var admin in jsonDecode(admins.body)['results']) {
      admin = Admin.fromJson(admin);
      if (admin.role == 'hmc_admin' && admin.adminId != null) {
        adminId.add(admin.adminId.toString());
      }
      if (admin.facility != null) {
        if (admin.role == 'poc_admin' &&
            admin.facility.toString() == facilityId) {
          adminId.add(admin.adminId.toString());
        }
      }
    }
    for (String id in adminId) {
      await apiProvider.postNotification(map: {
        'user_id': id,
        'notification': status == 'Take'
            ? '${HiveUtils.get(HiveKeys.fullName)} is requested for shift.'
            : '${HiveUtils.get(HiveKeys.fullName)} is canceled for shift.',
        'created_time': '${DateTime.now()}',
        'seen': 'active',
      });
    }
  }
}

final OpenShiftsBloc openShiftsBloc = OpenShiftsBloc();
