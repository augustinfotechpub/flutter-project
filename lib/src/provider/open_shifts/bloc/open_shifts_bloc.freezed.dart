// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'open_shifts_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OpenShiftsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getOpenShifts,
    required TResult Function(String fromDate, String toDate) refreshOpenShifts,
    required TResult Function(String date) filterOpenShifts,
    required TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)
        editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getOpenShifts,
    TResult? Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult? Function(String date)? filterOpenShifts,
    TResult? Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getOpenShifts,
    TResult Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult Function(String date)? filterOpenShifts,
    TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetOpenShifts value) getOpenShifts,
    required TResult Function(_RefreshOpenShifts value) refreshOpenShifts,
    required TResult Function(_FilterOpenShifts value) filterOpenShifts,
    required TResult Function(_EditShift value) editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetOpenShifts value)? getOpenShifts,
    TResult? Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult? Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult? Function(_EditShift value)? editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetOpenShifts value)? getOpenShifts,
    TResult Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenShiftsEventCopyWith<$Res> {
  factory $OpenShiftsEventCopyWith(
          OpenShiftsEvent value, $Res Function(OpenShiftsEvent) then) =
      _$OpenShiftsEventCopyWithImpl<$Res, OpenShiftsEvent>;
}

/// @nodoc
class _$OpenShiftsEventCopyWithImpl<$Res, $Val extends OpenShiftsEvent>
    implements $OpenShiftsEventCopyWith<$Res> {
  _$OpenShiftsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetOpenShiftsCopyWith<$Res> {
  factory _$$_GetOpenShiftsCopyWith(
          _$_GetOpenShifts value, $Res Function(_$_GetOpenShifts) then) =
      __$$_GetOpenShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_GetOpenShiftsCopyWithImpl<$Res>
    extends _$OpenShiftsEventCopyWithImpl<$Res, _$_GetOpenShifts>
    implements _$$_GetOpenShiftsCopyWith<$Res> {
  __$$_GetOpenShiftsCopyWithImpl(
      _$_GetOpenShifts _value, $Res Function(_$_GetOpenShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_GetOpenShifts(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_GetOpenShifts implements _GetOpenShifts {
  const _$_GetOpenShifts({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'OpenShiftsEvent.getOpenShifts(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetOpenShifts &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetOpenShiftsCopyWith<_$_GetOpenShifts> get copyWith =>
      __$$_GetOpenShiftsCopyWithImpl<_$_GetOpenShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getOpenShifts,
    required TResult Function(String fromDate, String toDate) refreshOpenShifts,
    required TResult Function(String date) filterOpenShifts,
    required TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)
        editShift,
  }) {
    return getOpenShifts(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getOpenShifts,
    TResult? Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult? Function(String date)? filterOpenShifts,
    TResult? Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
  }) {
    return getOpenShifts?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getOpenShifts,
    TResult Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult Function(String date)? filterOpenShifts,
    TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
    required TResult orElse(),
  }) {
    if (getOpenShifts != null) {
      return getOpenShifts(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetOpenShifts value) getOpenShifts,
    required TResult Function(_RefreshOpenShifts value) refreshOpenShifts,
    required TResult Function(_FilterOpenShifts value) filterOpenShifts,
    required TResult Function(_EditShift value) editShift,
  }) {
    return getOpenShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetOpenShifts value)? getOpenShifts,
    TResult? Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult? Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return getOpenShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetOpenShifts value)? getOpenShifts,
    TResult Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (getOpenShifts != null) {
      return getOpenShifts(this);
    }
    return orElse();
  }
}

abstract class _GetOpenShifts implements OpenShiftsEvent {
  const factory _GetOpenShifts(
      {required final String fromDate,
      required final String toDate}) = _$_GetOpenShifts;

  String get fromDate;
  String get toDate;
  @JsonKey(ignore: true)
  _$$_GetOpenShiftsCopyWith<_$_GetOpenShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshOpenShiftsCopyWith<$Res> {
  factory _$$_RefreshOpenShiftsCopyWith(_$_RefreshOpenShifts value,
          $Res Function(_$_RefreshOpenShifts) then) =
      __$$_RefreshOpenShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_RefreshOpenShiftsCopyWithImpl<$Res>
    extends _$OpenShiftsEventCopyWithImpl<$Res, _$_RefreshOpenShifts>
    implements _$$_RefreshOpenShiftsCopyWith<$Res> {
  __$$_RefreshOpenShiftsCopyWithImpl(
      _$_RefreshOpenShifts _value, $Res Function(_$_RefreshOpenShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_RefreshOpenShifts(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RefreshOpenShifts implements _RefreshOpenShifts {
  const _$_RefreshOpenShifts({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'OpenShiftsEvent.refreshOpenShifts(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshOpenShifts &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshOpenShiftsCopyWith<_$_RefreshOpenShifts> get copyWith =>
      __$$_RefreshOpenShiftsCopyWithImpl<_$_RefreshOpenShifts>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getOpenShifts,
    required TResult Function(String fromDate, String toDate) refreshOpenShifts,
    required TResult Function(String date) filterOpenShifts,
    required TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)
        editShift,
  }) {
    return refreshOpenShifts(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getOpenShifts,
    TResult? Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult? Function(String date)? filterOpenShifts,
    TResult? Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
  }) {
    return refreshOpenShifts?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getOpenShifts,
    TResult Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult Function(String date)? filterOpenShifts,
    TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
    required TResult orElse(),
  }) {
    if (refreshOpenShifts != null) {
      return refreshOpenShifts(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetOpenShifts value) getOpenShifts,
    required TResult Function(_RefreshOpenShifts value) refreshOpenShifts,
    required TResult Function(_FilterOpenShifts value) filterOpenShifts,
    required TResult Function(_EditShift value) editShift,
  }) {
    return refreshOpenShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetOpenShifts value)? getOpenShifts,
    TResult? Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult? Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return refreshOpenShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetOpenShifts value)? getOpenShifts,
    TResult Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (refreshOpenShifts != null) {
      return refreshOpenShifts(this);
    }
    return orElse();
  }
}

abstract class _RefreshOpenShifts implements OpenShiftsEvent {
  const factory _RefreshOpenShifts(
      {required final String fromDate,
      required final String toDate}) = _$_RefreshOpenShifts;

  String get fromDate;
  String get toDate;
  @JsonKey(ignore: true)
  _$$_RefreshOpenShiftsCopyWith<_$_RefreshOpenShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_FilterOpenShiftsCopyWith<$Res> {
  factory _$$_FilterOpenShiftsCopyWith(
          _$_FilterOpenShifts value, $Res Function(_$_FilterOpenShifts) then) =
      __$$_FilterOpenShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String date});
}

/// @nodoc
class __$$_FilterOpenShiftsCopyWithImpl<$Res>
    extends _$OpenShiftsEventCopyWithImpl<$Res, _$_FilterOpenShifts>
    implements _$$_FilterOpenShiftsCopyWith<$Res> {
  __$$_FilterOpenShiftsCopyWithImpl(
      _$_FilterOpenShifts _value, $Res Function(_$_FilterOpenShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
  }) {
    return _then(_$_FilterOpenShifts(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FilterOpenShifts implements _FilterOpenShifts {
  const _$_FilterOpenShifts({required this.date});

  @override
  final String date;

  @override
  String toString() {
    return 'OpenShiftsEvent.filterOpenShifts(date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FilterOpenShifts &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode => Object.hash(runtimeType, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FilterOpenShiftsCopyWith<_$_FilterOpenShifts> get copyWith =>
      __$$_FilterOpenShiftsCopyWithImpl<_$_FilterOpenShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getOpenShifts,
    required TResult Function(String fromDate, String toDate) refreshOpenShifts,
    required TResult Function(String date) filterOpenShifts,
    required TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)
        editShift,
  }) {
    return filterOpenShifts(date);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getOpenShifts,
    TResult? Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult? Function(String date)? filterOpenShifts,
    TResult? Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
  }) {
    return filterOpenShifts?.call(date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getOpenShifts,
    TResult Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult Function(String date)? filterOpenShifts,
    TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
    required TResult orElse(),
  }) {
    if (filterOpenShifts != null) {
      return filterOpenShifts(date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetOpenShifts value) getOpenShifts,
    required TResult Function(_RefreshOpenShifts value) refreshOpenShifts,
    required TResult Function(_FilterOpenShifts value) filterOpenShifts,
    required TResult Function(_EditShift value) editShift,
  }) {
    return filterOpenShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetOpenShifts value)? getOpenShifts,
    TResult? Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult? Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return filterOpenShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetOpenShifts value)? getOpenShifts,
    TResult Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (filterOpenShifts != null) {
      return filterOpenShifts(this);
    }
    return orElse();
  }
}

abstract class _FilterOpenShifts implements OpenShiftsEvent {
  const factory _FilterOpenShifts({required final String date}) =
      _$_FilterOpenShifts;

  String get date;
  @JsonKey(ignore: true)
  _$$_FilterOpenShiftsCopyWith<_$_FilterOpenShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EditShiftCopyWith<$Res> {
  factory _$$_EditShiftCopyWith(
          _$_EditShift value, $Res Function(_$_EditShift) then) =
      __$$_EditShiftCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String id,
      String status,
      String facilityId,
      DateTime selectedDate,
      List<String> reqUsers});
}

/// @nodoc
class __$$_EditShiftCopyWithImpl<$Res>
    extends _$OpenShiftsEventCopyWithImpl<$Res, _$_EditShift>
    implements _$$_EditShiftCopyWith<$Res> {
  __$$_EditShiftCopyWithImpl(
      _$_EditShift _value, $Res Function(_$_EditShift) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? status = null,
    Object? facilityId = null,
    Object? selectedDate = null,
    Object? reqUsers = null,
  }) {
    return _then(_$_EditShift(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      facilityId: null == facilityId
          ? _value.facilityId
          : facilityId // ignore: cast_nullable_to_non_nullable
              as String,
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      reqUsers: null == reqUsers
          ? _value._reqUsers
          : reqUsers // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$_EditShift implements _EditShift {
  const _$_EditShift(
      {required this.id,
      required this.status,
      required this.facilityId,
      required this.selectedDate,
      required final List<String> reqUsers})
      : _reqUsers = reqUsers;

  @override
  final String id;
  @override
  final String status;
  @override
  final String facilityId;
  @override
  final DateTime selectedDate;
  final List<String> _reqUsers;
  @override
  List<String> get reqUsers {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_reqUsers);
  }

  @override
  String toString() {
    return 'OpenShiftsEvent.editShift(id: $id, status: $status, facilityId: $facilityId, selectedDate: $selectedDate, reqUsers: $reqUsers)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EditShift &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.facilityId, facilityId) ||
                other.facilityId == facilityId) &&
            (identical(other.selectedDate, selectedDate) ||
                other.selectedDate == selectedDate) &&
            const DeepCollectionEquality().equals(other._reqUsers, _reqUsers));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, status, facilityId,
      selectedDate, const DeepCollectionEquality().hash(_reqUsers));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EditShiftCopyWith<_$_EditShift> get copyWith =>
      __$$_EditShiftCopyWithImpl<_$_EditShift>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getOpenShifts,
    required TResult Function(String fromDate, String toDate) refreshOpenShifts,
    required TResult Function(String date) filterOpenShifts,
    required TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)
        editShift,
  }) {
    return editShift(id, status, facilityId, selectedDate, reqUsers);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getOpenShifts,
    TResult? Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult? Function(String date)? filterOpenShifts,
    TResult? Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
  }) {
    return editShift?.call(id, status, facilityId, selectedDate, reqUsers);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getOpenShifts,
    TResult Function(String fromDate, String toDate)? refreshOpenShifts,
    TResult Function(String date)? filterOpenShifts,
    TResult Function(String id, String status, String facilityId,
            DateTime selectedDate, List<String> reqUsers)?
        editShift,
    required TResult orElse(),
  }) {
    if (editShift != null) {
      return editShift(id, status, facilityId, selectedDate, reqUsers);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetOpenShifts value) getOpenShifts,
    required TResult Function(_RefreshOpenShifts value) refreshOpenShifts,
    required TResult Function(_FilterOpenShifts value) filterOpenShifts,
    required TResult Function(_EditShift value) editShift,
  }) {
    return editShift(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetOpenShifts value)? getOpenShifts,
    TResult? Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult? Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return editShift?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetOpenShifts value)? getOpenShifts,
    TResult Function(_RefreshOpenShifts value)? refreshOpenShifts,
    TResult Function(_FilterOpenShifts value)? filterOpenShifts,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (editShift != null) {
      return editShift(this);
    }
    return orElse();
  }
}

abstract class _EditShift implements OpenShiftsEvent {
  const factory _EditShift(
      {required final String id,
      required final String status,
      required final String facilityId,
      required final DateTime selectedDate,
      required final List<String> reqUsers}) = _$_EditShift;

  String get id;
  String get status;
  String get facilityId;
  DateTime get selectedDate;
  List<String> get reqUsers;
  @JsonKey(ignore: true)
  _$$_EditShiftCopyWith<_$_EditShift> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$OpenShiftsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> openShifts) success,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> openShifts)? success,
    TResult? Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> openShifts)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenShiftsStateCopyWith<$Res> {
  factory $OpenShiftsStateCopyWith(
          OpenShiftsState value, $Res Function(OpenShiftsState) then) =
      _$OpenShiftsStateCopyWithImpl<$Res, OpenShiftsState>;
}

/// @nodoc
class _$OpenShiftsStateCopyWithImpl<$Res, $Val extends OpenShiftsState>
    implements $OpenShiftsStateCopyWith<$Res> {
  _$OpenShiftsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$OpenShiftsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'OpenShiftsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> openShifts) success,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> openShifts)? success,
    TResult? Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> openShifts)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements OpenShiftsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$OpenShiftsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'OpenShiftsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> openShifts) success,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> openShifts)? success,
    TResult? Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> openShifts)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements OpenShiftsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Shift> openShifts});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$OpenShiftsStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? openShifts = null,
  }) {
    return _then(_$_Success(
      null == openShifts
          ? _value._openShifts
          : openShifts // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<Shift> openShifts) : _openShifts = openShifts;

  final List<Shift> _openShifts;
  @override
  List<Shift> get openShifts {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_openShifts);
  }

  @override
  String toString() {
    return 'OpenShiftsState.success(openShifts: $openShifts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._openShifts, _openShifts));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_openShifts));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> openShifts) success,
    required TResult Function(String error) error,
  }) {
    return success(openShifts);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> openShifts)? success,
    TResult? Function(String error)? error,
  }) {
    return success?.call(openShifts);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> openShifts)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(openShifts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements OpenShiftsState {
  const factory _Success(final List<Shift> openShifts) = _$_Success;

  List<Shift> get openShifts;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$OpenShiftsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'OpenShiftsState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> openShifts) success,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> openShifts)? success,
    TResult? Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> openShifts)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements OpenShiftsState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
