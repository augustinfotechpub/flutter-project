part of 'open_shifts_bloc.dart';

@freezed
class OpenShiftsState with _$OpenShiftsState {
  const factory OpenShiftsState.initial() = _Initial;
  const factory OpenShiftsState.loading() = _Loading;
  const factory OpenShiftsState.success(List<Shift> openShifts) = _Success;
  const factory OpenShiftsState.error(String error) = _Error;
}
