// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Notes _$$_NotesFromJson(Map<String, dynamic> json) => _$_Notes(
      results: (json['results'] as List<dynamic>)
          .map((e) => Note.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_NotesToJson(_$_Notes instance) => <String, dynamic>{
      'results': instance.results,
    };

_$_Note _$$_NoteFromJson(Map<String, dynamic> json) => _$_Note(
      noteId: json['note_id'] as int,
      userId: json['user_id'] as String,
      note: json['note'] as String,
      createdDate: json['created_date'] as String,
      createdTime: DateTime.parse(json['created_time'] as String),
    );

Map<String, dynamic> _$$_NoteToJson(_$_Note instance) => <String, dynamic>{
      'note_id': instance.noteId,
      'user_id': instance.userId,
      'note': instance.note,
      'created_date': instance.createdDate,
      'created_time': instance.createdTime.toIso8601String(),
    };
