// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class Notes with _$Notes {
  const factory Notes({
    required List<Note> results,
  }) = _Notes;

  factory Notes.fromJson(Map<String, dynamic> json) => _$NotesFromJson(json);
}

@freezed
class Note with _$Note {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Note({
    required int noteId,
    required String userId,
    required String note,
    required String createdDate,
    required DateTime createdTime,
  }) = _Note;

  factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
}
