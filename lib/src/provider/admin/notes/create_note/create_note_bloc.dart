import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';

part 'create_note_event.dart';
part 'create_note_state.dart';
part 'create_note_bloc.freezed.dart';

class CreateNoteBloc extends Bloc<CreateNoteEvent, CreateNoteState> {
  CreateNoteBloc() : super(const _Initial()) {
    on<CreateNoteEvent>((event, emit) async {
      emit(const _Loading());
      if (event.data['note'].toString().trim() != '') {
        try {
          final response = await adminApiProvider.createNote(body: event.data);
          if (response.statusCode == 201) {
            emit(const _Success());
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      } else {
        emit(const _Error('Please fill note'));
      }
    });
  }
}
