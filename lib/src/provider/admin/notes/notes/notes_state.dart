part of 'notes_bloc.dart';

@freezed
class NotesState with _$NotesState {
  const factory NotesState.initial() = _Initial;
  const factory NotesState.loading() = _Loading;
  const factory NotesState.success(List<Note> notes) = _Success;
  const factory NotesState.refreshedList(List<Note> refreshedNotes) =
      _RefreshedNotes;
  const factory NotesState.successDelete() = _SuccessDelete;
  const factory NotesState.error(String error) = _Error;
}
