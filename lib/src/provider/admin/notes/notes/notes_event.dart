part of 'notes_bloc.dart';

@freezed
class NotesEvent with _$NotesEvent {
  const factory NotesEvent.getNotes() = _GetNotes;
  const factory NotesEvent.refreshNotes() = _RefreshNotes;
  const factory NotesEvent.deleteNote(String noteId) = _DeleteNote;
}
