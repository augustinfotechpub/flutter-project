import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/notes/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'notes_event.dart';
part 'notes_state.dart';
part 'notes_bloc.freezed.dart';

class NotesBloc extends Bloc<NotesEvent, NotesState> {
  NotesBloc() : super(const _Initial()) {
    on<NotesEvent>((event, emit) async {
      if (event is _GetNotes) {
        emit(const _Loading());
      }
      if (event is _GetNotes || event is _RefreshNotes) {
        try {
          final response = await adminApiProvider.getNotes(
            id: HiveUtils.get(HiveKeys.user).toString(),
          );
          String body = utf8.decode(response.bodyBytes);
          Notes notes = Notes.fromJson(jsonDecode(body));
          if (event is _RefreshNotes) {
            emit(_RefreshedNotes(notes.results));
          } else {
            emit(_Success(notes.results));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _DeleteNote) {
        emit(const _Loading());
        try {
          final response = await adminApiProvider.deleteNotes(id: event.noteId);
          if (response.statusCode == 204) {
            emit(const _SuccessDelete());
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
