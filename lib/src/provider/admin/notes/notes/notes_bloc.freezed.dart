// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'notes_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$NotesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getNotes,
    required TResult Function() refreshNotes,
    required TResult Function(String noteId) deleteNote,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getNotes,
    TResult? Function()? refreshNotes,
    TResult? Function(String noteId)? deleteNote,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getNotes,
    TResult Function()? refreshNotes,
    TResult Function(String noteId)? deleteNote,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetNotes value) getNotes,
    required TResult Function(_RefreshNotes value) refreshNotes,
    required TResult Function(_DeleteNote value) deleteNote,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetNotes value)? getNotes,
    TResult? Function(_RefreshNotes value)? refreshNotes,
    TResult? Function(_DeleteNote value)? deleteNote,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetNotes value)? getNotes,
    TResult Function(_RefreshNotes value)? refreshNotes,
    TResult Function(_DeleteNote value)? deleteNote,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotesEventCopyWith<$Res> {
  factory $NotesEventCopyWith(
          NotesEvent value, $Res Function(NotesEvent) then) =
      _$NotesEventCopyWithImpl<$Res, NotesEvent>;
}

/// @nodoc
class _$NotesEventCopyWithImpl<$Res, $Val extends NotesEvent>
    implements $NotesEventCopyWith<$Res> {
  _$NotesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetNotesCopyWith<$Res> {
  factory _$$_GetNotesCopyWith(
          _$_GetNotes value, $Res Function(_$_GetNotes) then) =
      __$$_GetNotesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetNotesCopyWithImpl<$Res>
    extends _$NotesEventCopyWithImpl<$Res, _$_GetNotes>
    implements _$$_GetNotesCopyWith<$Res> {
  __$$_GetNotesCopyWithImpl(
      _$_GetNotes _value, $Res Function(_$_GetNotes) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetNotes implements _GetNotes {
  const _$_GetNotes();

  @override
  String toString() {
    return 'NotesEvent.getNotes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetNotes);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getNotes,
    required TResult Function() refreshNotes,
    required TResult Function(String noteId) deleteNote,
  }) {
    return getNotes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getNotes,
    TResult? Function()? refreshNotes,
    TResult? Function(String noteId)? deleteNote,
  }) {
    return getNotes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getNotes,
    TResult Function()? refreshNotes,
    TResult Function(String noteId)? deleteNote,
    required TResult orElse(),
  }) {
    if (getNotes != null) {
      return getNotes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetNotes value) getNotes,
    required TResult Function(_RefreshNotes value) refreshNotes,
    required TResult Function(_DeleteNote value) deleteNote,
  }) {
    return getNotes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetNotes value)? getNotes,
    TResult? Function(_RefreshNotes value)? refreshNotes,
    TResult? Function(_DeleteNote value)? deleteNote,
  }) {
    return getNotes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetNotes value)? getNotes,
    TResult Function(_RefreshNotes value)? refreshNotes,
    TResult Function(_DeleteNote value)? deleteNote,
    required TResult orElse(),
  }) {
    if (getNotes != null) {
      return getNotes(this);
    }
    return orElse();
  }
}

abstract class _GetNotes implements NotesEvent {
  const factory _GetNotes() = _$_GetNotes;
}

/// @nodoc
abstract class _$$_RefreshNotesCopyWith<$Res> {
  factory _$$_RefreshNotesCopyWith(
          _$_RefreshNotes value, $Res Function(_$_RefreshNotes) then) =
      __$$_RefreshNotesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_RefreshNotesCopyWithImpl<$Res>
    extends _$NotesEventCopyWithImpl<$Res, _$_RefreshNotes>
    implements _$$_RefreshNotesCopyWith<$Res> {
  __$$_RefreshNotesCopyWithImpl(
      _$_RefreshNotes _value, $Res Function(_$_RefreshNotes) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_RefreshNotes implements _RefreshNotes {
  const _$_RefreshNotes();

  @override
  String toString() {
    return 'NotesEvent.refreshNotes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_RefreshNotes);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getNotes,
    required TResult Function() refreshNotes,
    required TResult Function(String noteId) deleteNote,
  }) {
    return refreshNotes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getNotes,
    TResult? Function()? refreshNotes,
    TResult? Function(String noteId)? deleteNote,
  }) {
    return refreshNotes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getNotes,
    TResult Function()? refreshNotes,
    TResult Function(String noteId)? deleteNote,
    required TResult orElse(),
  }) {
    if (refreshNotes != null) {
      return refreshNotes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetNotes value) getNotes,
    required TResult Function(_RefreshNotes value) refreshNotes,
    required TResult Function(_DeleteNote value) deleteNote,
  }) {
    return refreshNotes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetNotes value)? getNotes,
    TResult? Function(_RefreshNotes value)? refreshNotes,
    TResult? Function(_DeleteNote value)? deleteNote,
  }) {
    return refreshNotes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetNotes value)? getNotes,
    TResult Function(_RefreshNotes value)? refreshNotes,
    TResult Function(_DeleteNote value)? deleteNote,
    required TResult orElse(),
  }) {
    if (refreshNotes != null) {
      return refreshNotes(this);
    }
    return orElse();
  }
}

abstract class _RefreshNotes implements NotesEvent {
  const factory _RefreshNotes() = _$_RefreshNotes;
}

/// @nodoc
abstract class _$$_DeleteNoteCopyWith<$Res> {
  factory _$$_DeleteNoteCopyWith(
          _$_DeleteNote value, $Res Function(_$_DeleteNote) then) =
      __$$_DeleteNoteCopyWithImpl<$Res>;
  @useResult
  $Res call({String noteId});
}

/// @nodoc
class __$$_DeleteNoteCopyWithImpl<$Res>
    extends _$NotesEventCopyWithImpl<$Res, _$_DeleteNote>
    implements _$$_DeleteNoteCopyWith<$Res> {
  __$$_DeleteNoteCopyWithImpl(
      _$_DeleteNote _value, $Res Function(_$_DeleteNote) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? noteId = null,
  }) {
    return _then(_$_DeleteNote(
      null == noteId
          ? _value.noteId
          : noteId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_DeleteNote implements _DeleteNote {
  const _$_DeleteNote(this.noteId);

  @override
  final String noteId;

  @override
  String toString() {
    return 'NotesEvent.deleteNote(noteId: $noteId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeleteNote &&
            (identical(other.noteId, noteId) || other.noteId == noteId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, noteId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DeleteNoteCopyWith<_$_DeleteNote> get copyWith =>
      __$$_DeleteNoteCopyWithImpl<_$_DeleteNote>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getNotes,
    required TResult Function() refreshNotes,
    required TResult Function(String noteId) deleteNote,
  }) {
    return deleteNote(noteId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getNotes,
    TResult? Function()? refreshNotes,
    TResult? Function(String noteId)? deleteNote,
  }) {
    return deleteNote?.call(noteId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getNotes,
    TResult Function()? refreshNotes,
    TResult Function(String noteId)? deleteNote,
    required TResult orElse(),
  }) {
    if (deleteNote != null) {
      return deleteNote(noteId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetNotes value) getNotes,
    required TResult Function(_RefreshNotes value) refreshNotes,
    required TResult Function(_DeleteNote value) deleteNote,
  }) {
    return deleteNote(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetNotes value)? getNotes,
    TResult? Function(_RefreshNotes value)? refreshNotes,
    TResult? Function(_DeleteNote value)? deleteNote,
  }) {
    return deleteNote?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetNotes value)? getNotes,
    TResult Function(_RefreshNotes value)? refreshNotes,
    TResult Function(_DeleteNote value)? deleteNote,
    required TResult orElse(),
  }) {
    if (deleteNote != null) {
      return deleteNote(this);
    }
    return orElse();
  }
}

abstract class _DeleteNote implements NotesEvent {
  const factory _DeleteNote(final String noteId) = _$_DeleteNote;

  String get noteId;
  @JsonKey(ignore: true)
  _$$_DeleteNoteCopyWith<_$_DeleteNote> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$NotesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotesStateCopyWith<$Res> {
  factory $NotesStateCopyWith(
          NotesState value, $Res Function(NotesState) then) =
      _$NotesStateCopyWithImpl<$Res, NotesState>;
}

/// @nodoc
class _$NotesStateCopyWithImpl<$Res, $Val extends NotesState>
    implements $NotesStateCopyWith<$Res> {
  _$NotesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'NotesState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements NotesState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'NotesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements NotesState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Note> notes});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notes = null,
  }) {
    return _then(_$_Success(
      null == notes
          ? _value._notes
          : notes // ignore: cast_nullable_to_non_nullable
              as List<Note>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<Note> notes) : _notes = notes;

  final List<Note> _notes;
  @override
  List<Note> get notes {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_notes);
  }

  @override
  String toString() {
    return 'NotesState.success(notes: $notes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._notes, _notes));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_notes));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return success(notes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return success?.call(notes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(notes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements NotesState {
  const factory _Success(final List<Note> notes) = _$_Success;

  List<Note> get notes;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshedNotesCopyWith<$Res> {
  factory _$$_RefreshedNotesCopyWith(
          _$_RefreshedNotes value, $Res Function(_$_RefreshedNotes) then) =
      __$$_RefreshedNotesCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Note> refreshedNotes});
}

/// @nodoc
class __$$_RefreshedNotesCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_RefreshedNotes>
    implements _$$_RefreshedNotesCopyWith<$Res> {
  __$$_RefreshedNotesCopyWithImpl(
      _$_RefreshedNotes _value, $Res Function(_$_RefreshedNotes) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? refreshedNotes = null,
  }) {
    return _then(_$_RefreshedNotes(
      null == refreshedNotes
          ? _value._refreshedNotes
          : refreshedNotes // ignore: cast_nullable_to_non_nullable
              as List<Note>,
    ));
  }
}

/// @nodoc

class _$_RefreshedNotes implements _RefreshedNotes {
  const _$_RefreshedNotes(final List<Note> refreshedNotes)
      : _refreshedNotes = refreshedNotes;

  final List<Note> _refreshedNotes;
  @override
  List<Note> get refreshedNotes {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_refreshedNotes);
  }

  @override
  String toString() {
    return 'NotesState.refreshedList(refreshedNotes: $refreshedNotes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshedNotes &&
            const DeepCollectionEquality()
                .equals(other._refreshedNotes, _refreshedNotes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_refreshedNotes));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshedNotesCopyWith<_$_RefreshedNotes> get copyWith =>
      __$$_RefreshedNotesCopyWithImpl<_$_RefreshedNotes>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return refreshedList(refreshedNotes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return refreshedList?.call(refreshedNotes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (refreshedList != null) {
      return refreshedList(refreshedNotes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return refreshedList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return refreshedList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (refreshedList != null) {
      return refreshedList(this);
    }
    return orElse();
  }
}

abstract class _RefreshedNotes implements NotesState {
  const factory _RefreshedNotes(final List<Note> refreshedNotes) =
      _$_RefreshedNotes;

  List<Note> get refreshedNotes;
  @JsonKey(ignore: true)
  _$$_RefreshedNotesCopyWith<_$_RefreshedNotes> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SuccessDeleteCopyWith<$Res> {
  factory _$$_SuccessDeleteCopyWith(
          _$_SuccessDelete value, $Res Function(_$_SuccessDelete) then) =
      __$$_SuccessDeleteCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SuccessDeleteCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_SuccessDelete>
    implements _$$_SuccessDeleteCopyWith<$Res> {
  __$$_SuccessDeleteCopyWithImpl(
      _$_SuccessDelete _value, $Res Function(_$_SuccessDelete) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SuccessDelete implements _SuccessDelete {
  const _$_SuccessDelete();

  @override
  String toString() {
    return 'NotesState.successDelete()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SuccessDelete);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return successDelete();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return successDelete?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (successDelete != null) {
      return successDelete();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return successDelete(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return successDelete?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (successDelete != null) {
      return successDelete(this);
    }
    return orElse();
  }
}

abstract class _SuccessDelete implements NotesState {
  const factory _SuccessDelete() = _$_SuccessDelete;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$NotesStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'NotesState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Note> notes) success,
    required TResult Function(List<Note> refreshedNotes) refreshedList,
    required TResult Function() successDelete,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Note> notes)? success,
    TResult? Function(List<Note> refreshedNotes)? refreshedList,
    TResult? Function()? successDelete,
    TResult? Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Note> notes)? success,
    TResult Function(List<Note> refreshedNotes)? refreshedList,
    TResult Function()? successDelete,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_RefreshedNotes value) refreshedList,
    required TResult Function(_SuccessDelete value) successDelete,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_RefreshedNotes value)? refreshedList,
    TResult? Function(_SuccessDelete value)? successDelete,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_RefreshedNotes value)? refreshedList,
    TResult Function(_SuccessDelete value)? successDelete,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements NotesState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
