import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'admin_data_event.dart';
part 'admin_data_state.dart';
part 'admin_data_bloc.freezed.dart';

class AdminDataBloc extends Bloc<AdminDataEvent, AdminDataState> {
  AdminDataBloc() : super(const _Initial(0)) {
    on<AdminDataEvent>((event, emit) {
      emit(_Initial(event.index));
    });
  }
}


