part of 'admin_data_bloc.dart';

@freezed
class AdminDataState with _$AdminDataState {
  const factory AdminDataState.initial(int index) = _Initial;
}
