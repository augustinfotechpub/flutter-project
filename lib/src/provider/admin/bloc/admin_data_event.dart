part of 'admin_data_bloc.dart';

@freezed
class AdminDataEvent with _$AdminDataEvent {
  const factory AdminDataEvent.changeIndex(int index) = _ChnageIndex;
}