// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class Admin with _$Admin {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Admin({
    required int adminId,
    required String fullname,
    required String firstname,
    required String lastname,
    required String email,
    required String username,
    required String password,
    required String avatarImage,
    required String role,
    required String contactNo,
    required String address,
    required String dateOfBirth,
    required bool allowScheduling,
    required bool isActive,
    int? facility,
    required int user,
  }) = _Admin;

  factory Admin.fromJson(Map<String, dynamic> json) => _$AdminFromJson(json);
}
