// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Admin _$AdminFromJson(Map<String, dynamic> json) {
  return _Admin.fromJson(json);
}

/// @nodoc
mixin _$Admin {
  int get adminId => throw _privateConstructorUsedError;
  String get fullname => throw _privateConstructorUsedError;
  String get firstname => throw _privateConstructorUsedError;
  String get lastname => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  String get avatarImage => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;
  String get contactNo => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  String get dateOfBirth => throw _privateConstructorUsedError;
  bool get allowScheduling => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  int? get facility => throw _privateConstructorUsedError;
  int get user => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AdminCopyWith<Admin> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AdminCopyWith<$Res> {
  factory $AdminCopyWith(Admin value, $Res Function(Admin) then) =
      _$AdminCopyWithImpl<$Res, Admin>;
  @useResult
  $Res call(
      {int adminId,
      String fullname,
      String firstname,
      String lastname,
      String email,
      String username,
      String password,
      String avatarImage,
      String role,
      String contactNo,
      String address,
      String dateOfBirth,
      bool allowScheduling,
      bool isActive,
      int? facility,
      int user});
}

/// @nodoc
class _$AdminCopyWithImpl<$Res, $Val extends Admin>
    implements $AdminCopyWith<$Res> {
  _$AdminCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adminId = null,
    Object? fullname = null,
    Object? firstname = null,
    Object? lastname = null,
    Object? email = null,
    Object? username = null,
    Object? password = null,
    Object? avatarImage = null,
    Object? role = null,
    Object? contactNo = null,
    Object? address = null,
    Object? dateOfBirth = null,
    Object? allowScheduling = null,
    Object? isActive = null,
    Object? facility = freezed,
    Object? user = null,
  }) {
    return _then(_value.copyWith(
      adminId: null == adminId
          ? _value.adminId
          : adminId // ignore: cast_nullable_to_non_nullable
              as int,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      allowScheduling: null == allowScheduling
          ? _value.allowScheduling
          : allowScheduling // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      facility: freezed == facility
          ? _value.facility
          : facility // ignore: cast_nullable_to_non_nullable
              as int?,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AdminCopyWith<$Res> implements $AdminCopyWith<$Res> {
  factory _$$_AdminCopyWith(_$_Admin value, $Res Function(_$_Admin) then) =
      __$$_AdminCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int adminId,
      String fullname,
      String firstname,
      String lastname,
      String email,
      String username,
      String password,
      String avatarImage,
      String role,
      String contactNo,
      String address,
      String dateOfBirth,
      bool allowScheduling,
      bool isActive,
      int? facility,
      int user});
}

/// @nodoc
class __$$_AdminCopyWithImpl<$Res> extends _$AdminCopyWithImpl<$Res, _$_Admin>
    implements _$$_AdminCopyWith<$Res> {
  __$$_AdminCopyWithImpl(_$_Admin _value, $Res Function(_$_Admin) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adminId = null,
    Object? fullname = null,
    Object? firstname = null,
    Object? lastname = null,
    Object? email = null,
    Object? username = null,
    Object? password = null,
    Object? avatarImage = null,
    Object? role = null,
    Object? contactNo = null,
    Object? address = null,
    Object? dateOfBirth = null,
    Object? allowScheduling = null,
    Object? isActive = null,
    Object? facility = freezed,
    Object? user = null,
  }) {
    return _then(_$_Admin(
      adminId: null == adminId
          ? _value.adminId
          : adminId // ignore: cast_nullable_to_non_nullable
              as int,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      allowScheduling: null == allowScheduling
          ? _value.allowScheduling
          : allowScheduling // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      facility: freezed == facility
          ? _value.facility
          : facility // ignore: cast_nullable_to_non_nullable
              as int?,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Admin implements _Admin {
  const _$_Admin(
      {required this.adminId,
      required this.fullname,
      required this.firstname,
      required this.lastname,
      required this.email,
      required this.username,
      required this.password,
      required this.avatarImage,
      required this.role,
      required this.contactNo,
      required this.address,
      required this.dateOfBirth,
      required this.allowScheduling,
      required this.isActive,
      this.facility,
      required this.user});

  factory _$_Admin.fromJson(Map<String, dynamic> json) =>
      _$$_AdminFromJson(json);

  @override
  final int adminId;
  @override
  final String fullname;
  @override
  final String firstname;
  @override
  final String lastname;
  @override
  final String email;
  @override
  final String username;
  @override
  final String password;
  @override
  final String avatarImage;
  @override
  final String role;
  @override
  final String contactNo;
  @override
  final String address;
  @override
  final String dateOfBirth;
  @override
  final bool allowScheduling;
  @override
  final bool isActive;
  @override
  final int? facility;
  @override
  final int user;

  @override
  String toString() {
    return 'Admin(adminId: $adminId, fullname: $fullname, firstname: $firstname, lastname: $lastname, email: $email, username: $username, password: $password, avatarImage: $avatarImage, role: $role, contactNo: $contactNo, address: $address, dateOfBirth: $dateOfBirth, allowScheduling: $allowScheduling, isActive: $isActive, facility: $facility, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Admin &&
            (identical(other.adminId, adminId) || other.adminId == adminId) &&
            (identical(other.fullname, fullname) ||
                other.fullname == fullname) &&
            (identical(other.firstname, firstname) ||
                other.firstname == firstname) &&
            (identical(other.lastname, lastname) ||
                other.lastname == lastname) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.avatarImage, avatarImage) ||
                other.avatarImage == avatarImage) &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.contactNo, contactNo) ||
                other.contactNo == contactNo) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            (identical(other.allowScheduling, allowScheduling) ||
                other.allowScheduling == allowScheduling) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.facility, facility) ||
                other.facility == facility) &&
            (identical(other.user, user) || other.user == user));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      adminId,
      fullname,
      firstname,
      lastname,
      email,
      username,
      password,
      avatarImage,
      role,
      contactNo,
      address,
      dateOfBirth,
      allowScheduling,
      isActive,
      facility,
      user);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AdminCopyWith<_$_Admin> get copyWith =>
      __$$_AdminCopyWithImpl<_$_Admin>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AdminToJson(
      this,
    );
  }
}

abstract class _Admin implements Admin {
  const factory _Admin(
      {required final int adminId,
      required final String fullname,
      required final String firstname,
      required final String lastname,
      required final String email,
      required final String username,
      required final String password,
      required final String avatarImage,
      required final String role,
      required final String contactNo,
      required final String address,
      required final String dateOfBirth,
      required final bool allowScheduling,
      required final bool isActive,
      final int? facility,
      required final int user}) = _$_Admin;

  factory _Admin.fromJson(Map<String, dynamic> json) = _$_Admin.fromJson;

  @override
  int get adminId;
  @override
  String get fullname;
  @override
  String get firstname;
  @override
  String get lastname;
  @override
  String get email;
  @override
  String get username;
  @override
  String get password;
  @override
  String get avatarImage;
  @override
  String get role;
  @override
  String get contactNo;
  @override
  String get address;
  @override
  String get dateOfBirth;
  @override
  bool get allowScheduling;
  @override
  bool get isActive;
  @override
  int? get facility;
  @override
  int get user;
  @override
  @JsonKey(ignore: true)
  _$$_AdminCopyWith<_$_Admin> get copyWith =>
      throw _privateConstructorUsedError;
}
