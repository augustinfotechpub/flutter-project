// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Admin _$$_AdminFromJson(Map<String, dynamic> json) => _$_Admin(
      adminId: json['admin_id'] as int,
      fullname: json['fullname'] as String,
      firstname: json['firstname'] as String,
      lastname: json['lastname'] as String,
      email: json['email'] as String,
      username: json['username'] as String,
      password: json['password'] as String,
      avatarImage: json['avatar_image'] as String,
      role: json['role'] as String,
      contactNo: json['contact_no'] as String,
      address: json['address'] as String,
      dateOfBirth: json['date_of_birth'] as String,
      allowScheduling: json['allow_scheduling'] as bool,
      isActive: json['is_active'] as bool,
      facility: json['facility'] as int?,
      user: json['user'] as int,
    );

Map<String, dynamic> _$$_AdminToJson(_$_Admin instance) => <String, dynamic>{
      'admin_id': instance.adminId,
      'fullname': instance.fullname,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'username': instance.username,
      'password': instance.password,
      'avatar_image': instance.avatarImage,
      'role': instance.role,
      'contact_no': instance.contactNo,
      'address': instance.address,
      'date_of_birth': instance.dateOfBirth,
      'allow_scheduling': instance.allowScheduling,
      'is_active': instance.isActive,
      'facility': instance.facility,
      'user': instance.user,
    };
