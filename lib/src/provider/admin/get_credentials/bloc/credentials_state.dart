part of 'credentials_bloc.dart';

@freezed
class CredentialsState with _$CredentialsState {
  const factory CredentialsState.initial() = _Initial;
  const factory CredentialsState.loading() = _Loading;
  const factory CredentialsState.success({
    required CredentialModel credentialModel,
    required String position,
  }) = _Success;
  const factory CredentialsState.error(String error) = _Error;
}
