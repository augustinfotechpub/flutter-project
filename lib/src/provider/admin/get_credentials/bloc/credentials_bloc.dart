import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/get_credentials/model.dart';

part 'credentials_event.dart';
part 'credentials_state.dart';
part 'credentials_bloc.freezed.dart';

class CredentialsBloc extends Bloc<CredentialsEvent, CredentialsState> {
  CredentialsBloc() : super(const _Initial()) {
    on<CredentialsEvent>((event, emit) async {
      emit(const _Loading());
      try {
        final shift =
            await adminApiProvider.getCredentials(position: event.position);
        if (shift.statusCode == 200) {
          CredentialModel credentils =
              CredentialModel.fromJson(jsonDecode(shift.body));
          emit(_Success(credentialModel: credentils, position: event.position));
        } else {
          emit(_Error(jsonDecode(shift.body)));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
