import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/edit_profile/model.dart';

part 'get_clinician_event.dart';
part 'get_clinician_state.dart';
part 'get_clinician_bloc.freezed.dart';

class GetClinicianBloc extends Bloc<GetClinicianEvent, GetClinicianState> {
  GetClinicianBloc() : super(const _Initial()) {
    on<GetClinicianEvent>((event, emit) async {
      emit(const _Loading());
      try {
        final userResponse = await apiProvider.getClinician(event.id);
        if (userResponse.statusCode == 200) {
          Profile user =
              Profile.fromJson(jsonDecode(userResponse.body)['results'].first);
          emit(_Success(user: user));
        } else {
          emit(_Error(jsonDecode(userResponse.body)));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
