part of 'get_clinician_bloc.dart';

@freezed
class GetClinicianState with _$GetClinicianState {
  const factory GetClinicianState.initial() = _Initial;
  const factory GetClinicianState.loading() = _Loading;
  const factory GetClinicianState.success({
    required Profile user,
  }) = _Success;
  const factory GetClinicianState.error(String error) = _Error;
}
