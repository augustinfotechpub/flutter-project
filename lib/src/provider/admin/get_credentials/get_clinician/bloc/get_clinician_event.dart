part of 'get_clinician_bloc.dart';

@freezed
class GetClinicianEvent with _$GetClinicianEvent {
  const factory GetClinicianEvent.getClinician(String id) = _GetClinician;
}
