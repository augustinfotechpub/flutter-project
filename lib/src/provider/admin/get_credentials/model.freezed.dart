// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CredentialModel _$CredentialModelFromJson(Map<String, dynamic> json) {
  return _CredentialModel.fromJson(json);
}

/// @nodoc
mixin _$CredentialModel {
  List<CredentialDetails> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CredentialModelCopyWith<CredentialModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CredentialModelCopyWith<$Res> {
  factory $CredentialModelCopyWith(
          CredentialModel value, $Res Function(CredentialModel) then) =
      _$CredentialModelCopyWithImpl<$Res, CredentialModel>;
  @useResult
  $Res call({List<CredentialDetails> results});
}

/// @nodoc
class _$CredentialModelCopyWithImpl<$Res, $Val extends CredentialModel>
    implements $CredentialModelCopyWith<$Res> {
  _$CredentialModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<CredentialDetails>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CredentialModelCopyWith<$Res>
    implements $CredentialModelCopyWith<$Res> {
  factory _$$_CredentialModelCopyWith(
          _$_CredentialModel value, $Res Function(_$_CredentialModel) then) =
      __$$_CredentialModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<CredentialDetails> results});
}

/// @nodoc
class __$$_CredentialModelCopyWithImpl<$Res>
    extends _$CredentialModelCopyWithImpl<$Res, _$_CredentialModel>
    implements _$$_CredentialModelCopyWith<$Res> {
  __$$_CredentialModelCopyWithImpl(
      _$_CredentialModel _value, $Res Function(_$_CredentialModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_$_CredentialModel(
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<CredentialDetails>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CredentialModel implements _CredentialModel {
  const _$_CredentialModel({required final List<CredentialDetails> results})
      : _results = results;

  factory _$_CredentialModel.fromJson(Map<String, dynamic> json) =>
      _$$_CredentialModelFromJson(json);

  final List<CredentialDetails> _results;
  @override
  List<CredentialDetails> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'CredentialModel(results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CredentialModel &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CredentialModelCopyWith<_$_CredentialModel> get copyWith =>
      __$$_CredentialModelCopyWithImpl<_$_CredentialModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CredentialModelToJson(
      this,
    );
  }
}

abstract class _CredentialModel implements CredentialModel {
  const factory _CredentialModel(
      {required final List<CredentialDetails> results}) = _$_CredentialModel;

  factory _CredentialModel.fromJson(Map<String, dynamic> json) =
      _$_CredentialModel.fromJson;

  @override
  List<CredentialDetails> get results;
  @override
  @JsonKey(ignore: true)
  _$$_CredentialModelCopyWith<_$_CredentialModel> get copyWith =>
      throw _privateConstructorUsedError;
}

CredentialDetails _$CredentialDetailsFromJson(Map<String, dynamic> json) {
  return _CredentialDetails.fromJson(json);
}

/// @nodoc
mixin _$CredentialDetails {
  int get documentId => throw _privateConstructorUsedError;
  String get clinicianName => throw _privateConstructorUsedError;
  String get clinicianPosition => throw _privateConstructorUsedError;
  String get clinicianAvatar => throw _privateConstructorUsedError;
  String get documentName => throw _privateConstructorUsedError;
  String get documentPath => throw _privateConstructorUsedError;
  DateTime get documentUpdatedDate => throw _privateConstructorUsedError;
  String get documentStatus => throw _privateConstructorUsedError;
  int get clinicianId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CredentialDetailsCopyWith<CredentialDetails> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CredentialDetailsCopyWith<$Res> {
  factory $CredentialDetailsCopyWith(
          CredentialDetails value, $Res Function(CredentialDetails) then) =
      _$CredentialDetailsCopyWithImpl<$Res, CredentialDetails>;
  @useResult
  $Res call(
      {int documentId,
      String clinicianName,
      String clinicianPosition,
      String clinicianAvatar,
      String documentName,
      String documentPath,
      DateTime documentUpdatedDate,
      String documentStatus,
      int clinicianId});
}

/// @nodoc
class _$CredentialDetailsCopyWithImpl<$Res, $Val extends CredentialDetails>
    implements $CredentialDetailsCopyWith<$Res> {
  _$CredentialDetailsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? documentId = null,
    Object? clinicianName = null,
    Object? clinicianPosition = null,
    Object? clinicianAvatar = null,
    Object? documentName = null,
    Object? documentPath = null,
    Object? documentUpdatedDate = null,
    Object? documentStatus = null,
    Object? clinicianId = null,
  }) {
    return _then(_value.copyWith(
      documentId: null == documentId
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianAvatar: null == clinicianAvatar
          ? _value.clinicianAvatar
          : clinicianAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      documentName: null == documentName
          ? _value.documentName
          : documentName // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: null == documentPath
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      documentUpdatedDate: null == documentUpdatedDate
          ? _value.documentUpdatedDate
          : documentUpdatedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      documentStatus: null == documentStatus
          ? _value.documentStatus
          : documentStatus // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CredentialDetailsCopyWith<$Res>
    implements $CredentialDetailsCopyWith<$Res> {
  factory _$$_CredentialDetailsCopyWith(_$_CredentialDetails value,
          $Res Function(_$_CredentialDetails) then) =
      __$$_CredentialDetailsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int documentId,
      String clinicianName,
      String clinicianPosition,
      String clinicianAvatar,
      String documentName,
      String documentPath,
      DateTime documentUpdatedDate,
      String documentStatus,
      int clinicianId});
}

/// @nodoc
class __$$_CredentialDetailsCopyWithImpl<$Res>
    extends _$CredentialDetailsCopyWithImpl<$Res, _$_CredentialDetails>
    implements _$$_CredentialDetailsCopyWith<$Res> {
  __$$_CredentialDetailsCopyWithImpl(
      _$_CredentialDetails _value, $Res Function(_$_CredentialDetails) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? documentId = null,
    Object? clinicianName = null,
    Object? clinicianPosition = null,
    Object? clinicianAvatar = null,
    Object? documentName = null,
    Object? documentPath = null,
    Object? documentUpdatedDate = null,
    Object? documentStatus = null,
    Object? clinicianId = null,
  }) {
    return _then(_$_CredentialDetails(
      documentId: null == documentId
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianAvatar: null == clinicianAvatar
          ? _value.clinicianAvatar
          : clinicianAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      documentName: null == documentName
          ? _value.documentName
          : documentName // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: null == documentPath
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      documentUpdatedDate: null == documentUpdatedDate
          ? _value.documentUpdatedDate
          : documentUpdatedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      documentStatus: null == documentStatus
          ? _value.documentStatus
          : documentStatus // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_CredentialDetails implements _CredentialDetails {
  const _$_CredentialDetails(
      {required this.documentId,
      required this.clinicianName,
      required this.clinicianPosition,
      required this.clinicianAvatar,
      required this.documentName,
      required this.documentPath,
      required this.documentUpdatedDate,
      required this.documentStatus,
      required this.clinicianId});

  factory _$_CredentialDetails.fromJson(Map<String, dynamic> json) =>
      _$$_CredentialDetailsFromJson(json);

  @override
  final int documentId;
  @override
  final String clinicianName;
  @override
  final String clinicianPosition;
  @override
  final String clinicianAvatar;
  @override
  final String documentName;
  @override
  final String documentPath;
  @override
  final DateTime documentUpdatedDate;
  @override
  final String documentStatus;
  @override
  final int clinicianId;

  @override
  String toString() {
    return 'CredentialDetails(documentId: $documentId, clinicianName: $clinicianName, clinicianPosition: $clinicianPosition, clinicianAvatar: $clinicianAvatar, documentName: $documentName, documentPath: $documentPath, documentUpdatedDate: $documentUpdatedDate, documentStatus: $documentStatus, clinicianId: $clinicianId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CredentialDetails &&
            (identical(other.documentId, documentId) ||
                other.documentId == documentId) &&
            (identical(other.clinicianName, clinicianName) ||
                other.clinicianName == clinicianName) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.clinicianAvatar, clinicianAvatar) ||
                other.clinicianAvatar == clinicianAvatar) &&
            (identical(other.documentName, documentName) ||
                other.documentName == documentName) &&
            (identical(other.documentPath, documentPath) ||
                other.documentPath == documentPath) &&
            (identical(other.documentUpdatedDate, documentUpdatedDate) ||
                other.documentUpdatedDate == documentUpdatedDate) &&
            (identical(other.documentStatus, documentStatus) ||
                other.documentStatus == documentStatus) &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      documentId,
      clinicianName,
      clinicianPosition,
      clinicianAvatar,
      documentName,
      documentPath,
      documentUpdatedDate,
      documentStatus,
      clinicianId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CredentialDetailsCopyWith<_$_CredentialDetails> get copyWith =>
      __$$_CredentialDetailsCopyWithImpl<_$_CredentialDetails>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CredentialDetailsToJson(
      this,
    );
  }
}

abstract class _CredentialDetails implements CredentialDetails {
  const factory _CredentialDetails(
      {required final int documentId,
      required final String clinicianName,
      required final String clinicianPosition,
      required final String clinicianAvatar,
      required final String documentName,
      required final String documentPath,
      required final DateTime documentUpdatedDate,
      required final String documentStatus,
      required final int clinicianId}) = _$_CredentialDetails;

  factory _CredentialDetails.fromJson(Map<String, dynamic> json) =
      _$_CredentialDetails.fromJson;

  @override
  int get documentId;
  @override
  String get clinicianName;
  @override
  String get clinicianPosition;
  @override
  String get clinicianAvatar;
  @override
  String get documentName;
  @override
  String get documentPath;
  @override
  DateTime get documentUpdatedDate;
  @override
  String get documentStatus;
  @override
  int get clinicianId;
  @override
  @JsonKey(ignore: true)
  _$$_CredentialDetailsCopyWith<_$_CredentialDetails> get copyWith =>
      throw _privateConstructorUsedError;
}
