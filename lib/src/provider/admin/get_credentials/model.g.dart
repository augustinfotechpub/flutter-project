// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CredentialModel _$$_CredentialModelFromJson(Map<String, dynamic> json) =>
    _$_CredentialModel(
      results: (json['results'] as List<dynamic>)
          .map((e) => CredentialDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_CredentialModelToJson(_$_CredentialModel instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

_$_CredentialDetails _$$_CredentialDetailsFromJson(Map<String, dynamic> json) =>
    _$_CredentialDetails(
      documentId: json['document_id'] as int,
      clinicianName: json['clinician_name'] as String,
      clinicianPosition: json['clinician_position'] as String,
      clinicianAvatar: json['clinician_avatar'] as String,
      documentName: json['document_name'] as String,
      documentPath: json['document_path'] as String,
      documentUpdatedDate:
          DateTime.parse(json['document_updated_date'] as String),
      documentStatus: json['document_status'] as String,
      clinicianId: json['clinician_id'] as int,
    );

Map<String, dynamic> _$$_CredentialDetailsToJson(
        _$_CredentialDetails instance) =>
    <String, dynamic>{
      'document_id': instance.documentId,
      'clinician_name': instance.clinicianName,
      'clinician_position': instance.clinicianPosition,
      'clinician_avatar': instance.clinicianAvatar,
      'document_name': instance.documentName,
      'document_path': instance.documentPath,
      'document_updated_date': instance.documentUpdatedDate.toIso8601String(),
      'document_status': instance.documentStatus,
      'clinician_id': instance.clinicianId,
    };
