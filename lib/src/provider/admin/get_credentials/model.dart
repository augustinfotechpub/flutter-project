// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class CredentialModel with _$CredentialModel {
  const factory CredentialModel({
    required List<CredentialDetails> results,
  }) = _CredentialModel;

  factory CredentialModel.fromJson(Map<String, dynamic> json) =>
      _$CredentialModelFromJson(json);
}

@freezed
class CredentialDetails with _$CredentialDetails {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory CredentialDetails({
    required int documentId,
    required String clinicianName,
    required String clinicianPosition,
    required String clinicianAvatar,
    required String documentName,
    required String documentPath,
    required DateTime documentUpdatedDate,
    required String documentStatus,
    required int clinicianId,
  }) = _CredentialDetails;

  factory CredentialDetails.fromJson(Map<String, dynamic> json) =>
      _$CredentialDetailsFromJson(json);
}
