import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/invoice/model.dart';

part 'invoice_event.dart';
part 'invoice_state.dart';
part 'invoice_bloc.freezed.dart';

class InvoiceBloc extends Bloc<InvoiceEvent, InvoiceState> {
  InvoiceBloc() : super(const _Initial()) {
    on<InvoiceEvent>((event, emit) async {
      if (event is _GetInvoices) {
        emit(const _Loading());
        try {
          final invoice = await adminApiProvider.getInvoices();
          if (invoice.statusCode == 200) {
            final invoiceData = InvoiceData.fromJson(jsonDecode(invoice.body));
            emit(_Success(invoiceData: invoiceData));
          } else {
            emit(_Error(jsonDecode(invoice.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _DeleteInvoice) {
        emit(const _Loading());
        try {
          final invoice = await adminApiProvider.deleteInvoice(event.invoiceId);
          if (invoice.statusCode == 200) {
            final invoiceData = InvoiceData.fromJson(jsonDecode(invoice.body));
            emit(_Success(invoiceData: invoiceData));
          } else {
            emit(_Error(jsonDecode(invoice.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
