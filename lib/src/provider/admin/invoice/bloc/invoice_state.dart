part of 'invoice_bloc.dart';

@freezed
class InvoiceState with _$InvoiceState {
  const factory InvoiceState.initial() = _Initial;
  const factory InvoiceState.loading() = _Loading;
  const factory InvoiceState.success({required InvoiceData invoiceData}) =
      _Success;
  const factory InvoiceState.error(String error) = _Error;
}
