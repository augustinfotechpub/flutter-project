part of 'invoice_bloc.dart';

@freezed
class InvoiceEvent with _$InvoiceEvent {
  const factory InvoiceEvent.getInvoices() = _GetInvoices;
  const factory InvoiceEvent.deleteInvoice(String invoiceId) = _DeleteInvoice;
}