// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class InvoiceData with _$InvoiceData {
  const factory InvoiceData({
    required List<InvoiceDetails> results,
  }) = _InvoiceData;

  factory InvoiceData.fromJson(Map<String, dynamic> json) =>
      _$InvoiceDataFromJson(json);
}

@freezed
class InvoiceDetails with _$InvoiceDetails {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory InvoiceDetails({
    required int invoiceId,
    required String serviceDate,
    required String clinicianName,
    required String position,
    required String hrsWorked,
    required String rate,
    required String amount,
  }) = _InvoiceDetails;

  factory InvoiceDetails.fromJson(Map<String, dynamic> json) =>
      _$InvoiceDetailsFromJson(json);
}
