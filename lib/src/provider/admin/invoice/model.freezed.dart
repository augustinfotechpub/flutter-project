// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

InvoiceData _$InvoiceDataFromJson(Map<String, dynamic> json) {
  return _InvoiceData.fromJson(json);
}

/// @nodoc
mixin _$InvoiceData {
  List<InvoiceDetails> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $InvoiceDataCopyWith<InvoiceData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvoiceDataCopyWith<$Res> {
  factory $InvoiceDataCopyWith(
          InvoiceData value, $Res Function(InvoiceData) then) =
      _$InvoiceDataCopyWithImpl<$Res, InvoiceData>;
  @useResult
  $Res call({List<InvoiceDetails> results});
}

/// @nodoc
class _$InvoiceDataCopyWithImpl<$Res, $Val extends InvoiceData>
    implements $InvoiceDataCopyWith<$Res> {
  _$InvoiceDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<InvoiceDetails>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InvoiceDataCopyWith<$Res>
    implements $InvoiceDataCopyWith<$Res> {
  factory _$$_InvoiceDataCopyWith(
          _$_InvoiceData value, $Res Function(_$_InvoiceData) then) =
      __$$_InvoiceDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<InvoiceDetails> results});
}

/// @nodoc
class __$$_InvoiceDataCopyWithImpl<$Res>
    extends _$InvoiceDataCopyWithImpl<$Res, _$_InvoiceData>
    implements _$$_InvoiceDataCopyWith<$Res> {
  __$$_InvoiceDataCopyWithImpl(
      _$_InvoiceData _value, $Res Function(_$_InvoiceData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_$_InvoiceData(
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<InvoiceDetails>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_InvoiceData implements _InvoiceData {
  const _$_InvoiceData({required final List<InvoiceDetails> results})
      : _results = results;

  factory _$_InvoiceData.fromJson(Map<String, dynamic> json) =>
      _$$_InvoiceDataFromJson(json);

  final List<InvoiceDetails> _results;
  @override
  List<InvoiceDetails> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'InvoiceData(results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvoiceData &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InvoiceDataCopyWith<_$_InvoiceData> get copyWith =>
      __$$_InvoiceDataCopyWithImpl<_$_InvoiceData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_InvoiceDataToJson(
      this,
    );
  }
}

abstract class _InvoiceData implements InvoiceData {
  const factory _InvoiceData({required final List<InvoiceDetails> results}) =
      _$_InvoiceData;

  factory _InvoiceData.fromJson(Map<String, dynamic> json) =
      _$_InvoiceData.fromJson;

  @override
  List<InvoiceDetails> get results;
  @override
  @JsonKey(ignore: true)
  _$$_InvoiceDataCopyWith<_$_InvoiceData> get copyWith =>
      throw _privateConstructorUsedError;
}

InvoiceDetails _$InvoiceDetailsFromJson(Map<String, dynamic> json) {
  return _InvoiceDetails.fromJson(json);
}

/// @nodoc
mixin _$InvoiceDetails {
  int get invoiceId => throw _privateConstructorUsedError;
  String get serviceDate => throw _privateConstructorUsedError;
  String get clinicianName => throw _privateConstructorUsedError;
  String get position => throw _privateConstructorUsedError;
  String get hrsWorked => throw _privateConstructorUsedError;
  String get rate => throw _privateConstructorUsedError;
  String get amount => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $InvoiceDetailsCopyWith<InvoiceDetails> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvoiceDetailsCopyWith<$Res> {
  factory $InvoiceDetailsCopyWith(
          InvoiceDetails value, $Res Function(InvoiceDetails) then) =
      _$InvoiceDetailsCopyWithImpl<$Res, InvoiceDetails>;
  @useResult
  $Res call(
      {int invoiceId,
      String serviceDate,
      String clinicianName,
      String position,
      String hrsWorked,
      String rate,
      String amount});
}

/// @nodoc
class _$InvoiceDetailsCopyWithImpl<$Res, $Val extends InvoiceDetails>
    implements $InvoiceDetailsCopyWith<$Res> {
  _$InvoiceDetailsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? serviceDate = null,
    Object? clinicianName = null,
    Object? position = null,
    Object? hrsWorked = null,
    Object? rate = null,
    Object? amount = null,
  }) {
    return _then(_value.copyWith(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDate: null == serviceDate
          ? _value.serviceDate
          : serviceDate // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      position: null == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as String,
      hrsWorked: null == hrsWorked
          ? _value.hrsWorked
          : hrsWorked // ignore: cast_nullable_to_non_nullable
              as String,
      rate: null == rate
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InvoiceDetailsCopyWith<$Res>
    implements $InvoiceDetailsCopyWith<$Res> {
  factory _$$_InvoiceDetailsCopyWith(
          _$_InvoiceDetails value, $Res Function(_$_InvoiceDetails) then) =
      __$$_InvoiceDetailsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int invoiceId,
      String serviceDate,
      String clinicianName,
      String position,
      String hrsWorked,
      String rate,
      String amount});
}

/// @nodoc
class __$$_InvoiceDetailsCopyWithImpl<$Res>
    extends _$InvoiceDetailsCopyWithImpl<$Res, _$_InvoiceDetails>
    implements _$$_InvoiceDetailsCopyWith<$Res> {
  __$$_InvoiceDetailsCopyWithImpl(
      _$_InvoiceDetails _value, $Res Function(_$_InvoiceDetails) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? invoiceId = null,
    Object? serviceDate = null,
    Object? clinicianName = null,
    Object? position = null,
    Object? hrsWorked = null,
    Object? rate = null,
    Object? amount = null,
  }) {
    return _then(_$_InvoiceDetails(
      invoiceId: null == invoiceId
          ? _value.invoiceId
          : invoiceId // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDate: null == serviceDate
          ? _value.serviceDate
          : serviceDate // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      position: null == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as String,
      hrsWorked: null == hrsWorked
          ? _value.hrsWorked
          : hrsWorked // ignore: cast_nullable_to_non_nullable
              as String,
      rate: null == rate
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_InvoiceDetails implements _InvoiceDetails {
  const _$_InvoiceDetails(
      {required this.invoiceId,
      required this.serviceDate,
      required this.clinicianName,
      required this.position,
      required this.hrsWorked,
      required this.rate,
      required this.amount});

  factory _$_InvoiceDetails.fromJson(Map<String, dynamic> json) =>
      _$$_InvoiceDetailsFromJson(json);

  @override
  final int invoiceId;
  @override
  final String serviceDate;
  @override
  final String clinicianName;
  @override
  final String position;
  @override
  final String hrsWorked;
  @override
  final String rate;
  @override
  final String amount;

  @override
  String toString() {
    return 'InvoiceDetails(invoiceId: $invoiceId, serviceDate: $serviceDate, clinicianName: $clinicianName, position: $position, hrsWorked: $hrsWorked, rate: $rate, amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvoiceDetails &&
            (identical(other.invoiceId, invoiceId) ||
                other.invoiceId == invoiceId) &&
            (identical(other.serviceDate, serviceDate) ||
                other.serviceDate == serviceDate) &&
            (identical(other.clinicianName, clinicianName) ||
                other.clinicianName == clinicianName) &&
            (identical(other.position, position) ||
                other.position == position) &&
            (identical(other.hrsWorked, hrsWorked) ||
                other.hrsWorked == hrsWorked) &&
            (identical(other.rate, rate) || other.rate == rate) &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, invoiceId, serviceDate,
      clinicianName, position, hrsWorked, rate, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InvoiceDetailsCopyWith<_$_InvoiceDetails> get copyWith =>
      __$$_InvoiceDetailsCopyWithImpl<_$_InvoiceDetails>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_InvoiceDetailsToJson(
      this,
    );
  }
}

abstract class _InvoiceDetails implements InvoiceDetails {
  const factory _InvoiceDetails(
      {required final int invoiceId,
      required final String serviceDate,
      required final String clinicianName,
      required final String position,
      required final String hrsWorked,
      required final String rate,
      required final String amount}) = _$_InvoiceDetails;

  factory _InvoiceDetails.fromJson(Map<String, dynamic> json) =
      _$_InvoiceDetails.fromJson;

  @override
  int get invoiceId;
  @override
  String get serviceDate;
  @override
  String get clinicianName;
  @override
  String get position;
  @override
  String get hrsWorked;
  @override
  String get rate;
  @override
  String get amount;
  @override
  @JsonKey(ignore: true)
  _$$_InvoiceDetailsCopyWith<_$_InvoiceDetails> get copyWith =>
      throw _privateConstructorUsedError;
}
