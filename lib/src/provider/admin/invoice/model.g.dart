// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_InvoiceData _$$_InvoiceDataFromJson(Map<String, dynamic> json) =>
    _$_InvoiceData(
      results: (json['results'] as List<dynamic>)
          .map((e) => InvoiceDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_InvoiceDataToJson(_$_InvoiceData instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

_$_InvoiceDetails _$$_InvoiceDetailsFromJson(Map<String, dynamic> json) =>
    _$_InvoiceDetails(
      invoiceId: json['invoice_id'] as int,
      serviceDate: json['service_date'] as String,
      clinicianName: json['clinician_name'] as String,
      position: json['position'] as String,
      hrsWorked: json['hrs_worked'] as String,
      rate: json['rate'] as String,
      amount: json['amount'] as String,
    );

Map<String, dynamic> _$$_InvoiceDetailsToJson(_$_InvoiceDetails instance) =>
    <String, dynamic>{
      'invoice_id': instance.invoiceId,
      'service_date': instance.serviceDate,
      'clinician_name': instance.clinicianName,
      'position': instance.position,
      'hrs_worked': instance.hrsWorked,
      'rate': instance.rate,
      'amount': instance.amount,
    };
