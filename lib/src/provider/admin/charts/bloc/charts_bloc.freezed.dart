// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'charts_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ChartsEvent {
  String get fromDate => throw _privateConstructorUsedError;
  String get toDate => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getData,
    required TResult Function(String fromDate, String toDate) refreshChart,
    required TResult Function(String fromDate, String toDate) getAllData,
    required TResult Function(String fromDate, String toDate) refreshAllChart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getData,
    TResult? Function(String fromDate, String toDate)? refreshChart,
    TResult? Function(String fromDate, String toDate)? getAllData,
    TResult? Function(String fromDate, String toDate)? refreshAllChart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getData,
    TResult Function(String fromDate, String toDate)? refreshChart,
    TResult Function(String fromDate, String toDate)? getAllData,
    TResult Function(String fromDate, String toDate)? refreshAllChart,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetData value) getData,
    required TResult Function(_RefreshChart value) refreshChart,
    required TResult Function(_GetAllData value) getAllData,
    required TResult Function(_RefreshAllChart value) refreshAllChart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetData value)? getData,
    TResult? Function(_RefreshChart value)? refreshChart,
    TResult? Function(_GetAllData value)? getAllData,
    TResult? Function(_RefreshAllChart value)? refreshAllChart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetData value)? getData,
    TResult Function(_RefreshChart value)? refreshChart,
    TResult Function(_GetAllData value)? getAllData,
    TResult Function(_RefreshAllChart value)? refreshAllChart,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChartsEventCopyWith<ChartsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChartsEventCopyWith<$Res> {
  factory $ChartsEventCopyWith(
          ChartsEvent value, $Res Function(ChartsEvent) then) =
      _$ChartsEventCopyWithImpl<$Res, ChartsEvent>;
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class _$ChartsEventCopyWithImpl<$Res, $Val extends ChartsEvent>
    implements $ChartsEventCopyWith<$Res> {
  _$ChartsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_value.copyWith(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GetDataCopyWith<$Res> implements $ChartsEventCopyWith<$Res> {
  factory _$$_GetDataCopyWith(
          _$_GetData value, $Res Function(_$_GetData) then) =
      __$$_GetDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_GetDataCopyWithImpl<$Res>
    extends _$ChartsEventCopyWithImpl<$Res, _$_GetData>
    implements _$$_GetDataCopyWith<$Res> {
  __$$_GetDataCopyWithImpl(_$_GetData _value, $Res Function(_$_GetData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_GetData(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_GetData implements _GetData {
  const _$_GetData({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'ChartsEvent.getData(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetData &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetDataCopyWith<_$_GetData> get copyWith =>
      __$$_GetDataCopyWithImpl<_$_GetData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getData,
    required TResult Function(String fromDate, String toDate) refreshChart,
    required TResult Function(String fromDate, String toDate) getAllData,
    required TResult Function(String fromDate, String toDate) refreshAllChart,
  }) {
    return getData(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getData,
    TResult? Function(String fromDate, String toDate)? refreshChart,
    TResult? Function(String fromDate, String toDate)? getAllData,
    TResult? Function(String fromDate, String toDate)? refreshAllChart,
  }) {
    return getData?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getData,
    TResult Function(String fromDate, String toDate)? refreshChart,
    TResult Function(String fromDate, String toDate)? getAllData,
    TResult Function(String fromDate, String toDate)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (getData != null) {
      return getData(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetData value) getData,
    required TResult Function(_RefreshChart value) refreshChart,
    required TResult Function(_GetAllData value) getAllData,
    required TResult Function(_RefreshAllChart value) refreshAllChart,
  }) {
    return getData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetData value)? getData,
    TResult? Function(_RefreshChart value)? refreshChart,
    TResult? Function(_GetAllData value)? getAllData,
    TResult? Function(_RefreshAllChart value)? refreshAllChart,
  }) {
    return getData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetData value)? getData,
    TResult Function(_RefreshChart value)? refreshChart,
    TResult Function(_GetAllData value)? getAllData,
    TResult Function(_RefreshAllChart value)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (getData != null) {
      return getData(this);
    }
    return orElse();
  }
}

abstract class _GetData implements ChartsEvent {
  const factory _GetData(
      {required final String fromDate,
      required final String toDate}) = _$_GetData;

  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  @JsonKey(ignore: true)
  _$$_GetDataCopyWith<_$_GetData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshChartCopyWith<$Res>
    implements $ChartsEventCopyWith<$Res> {
  factory _$$_RefreshChartCopyWith(
          _$_RefreshChart value, $Res Function(_$_RefreshChart) then) =
      __$$_RefreshChartCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_RefreshChartCopyWithImpl<$Res>
    extends _$ChartsEventCopyWithImpl<$Res, _$_RefreshChart>
    implements _$$_RefreshChartCopyWith<$Res> {
  __$$_RefreshChartCopyWithImpl(
      _$_RefreshChart _value, $Res Function(_$_RefreshChart) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_RefreshChart(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RefreshChart implements _RefreshChart {
  const _$_RefreshChart({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'ChartsEvent.refreshChart(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshChart &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshChartCopyWith<_$_RefreshChart> get copyWith =>
      __$$_RefreshChartCopyWithImpl<_$_RefreshChart>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getData,
    required TResult Function(String fromDate, String toDate) refreshChart,
    required TResult Function(String fromDate, String toDate) getAllData,
    required TResult Function(String fromDate, String toDate) refreshAllChart,
  }) {
    return refreshChart(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getData,
    TResult? Function(String fromDate, String toDate)? refreshChart,
    TResult? Function(String fromDate, String toDate)? getAllData,
    TResult? Function(String fromDate, String toDate)? refreshAllChart,
  }) {
    return refreshChart?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getData,
    TResult Function(String fromDate, String toDate)? refreshChart,
    TResult Function(String fromDate, String toDate)? getAllData,
    TResult Function(String fromDate, String toDate)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (refreshChart != null) {
      return refreshChart(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetData value) getData,
    required TResult Function(_RefreshChart value) refreshChart,
    required TResult Function(_GetAllData value) getAllData,
    required TResult Function(_RefreshAllChart value) refreshAllChart,
  }) {
    return refreshChart(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetData value)? getData,
    TResult? Function(_RefreshChart value)? refreshChart,
    TResult? Function(_GetAllData value)? getAllData,
    TResult? Function(_RefreshAllChart value)? refreshAllChart,
  }) {
    return refreshChart?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetData value)? getData,
    TResult Function(_RefreshChart value)? refreshChart,
    TResult Function(_GetAllData value)? getAllData,
    TResult Function(_RefreshAllChart value)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (refreshChart != null) {
      return refreshChart(this);
    }
    return orElse();
  }
}

abstract class _RefreshChart implements ChartsEvent {
  const factory _RefreshChart(
      {required final String fromDate,
      required final String toDate}) = _$_RefreshChart;

  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  @JsonKey(ignore: true)
  _$$_RefreshChartCopyWith<_$_RefreshChart> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GetAllDataCopyWith<$Res>
    implements $ChartsEventCopyWith<$Res> {
  factory _$$_GetAllDataCopyWith(
          _$_GetAllData value, $Res Function(_$_GetAllData) then) =
      __$$_GetAllDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_GetAllDataCopyWithImpl<$Res>
    extends _$ChartsEventCopyWithImpl<$Res, _$_GetAllData>
    implements _$$_GetAllDataCopyWith<$Res> {
  __$$_GetAllDataCopyWithImpl(
      _$_GetAllData _value, $Res Function(_$_GetAllData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_GetAllData(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_GetAllData implements _GetAllData {
  const _$_GetAllData({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'ChartsEvent.getAllData(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetAllData &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetAllDataCopyWith<_$_GetAllData> get copyWith =>
      __$$_GetAllDataCopyWithImpl<_$_GetAllData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getData,
    required TResult Function(String fromDate, String toDate) refreshChart,
    required TResult Function(String fromDate, String toDate) getAllData,
    required TResult Function(String fromDate, String toDate) refreshAllChart,
  }) {
    return getAllData(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getData,
    TResult? Function(String fromDate, String toDate)? refreshChart,
    TResult? Function(String fromDate, String toDate)? getAllData,
    TResult? Function(String fromDate, String toDate)? refreshAllChart,
  }) {
    return getAllData?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getData,
    TResult Function(String fromDate, String toDate)? refreshChart,
    TResult Function(String fromDate, String toDate)? getAllData,
    TResult Function(String fromDate, String toDate)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (getAllData != null) {
      return getAllData(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetData value) getData,
    required TResult Function(_RefreshChart value) refreshChart,
    required TResult Function(_GetAllData value) getAllData,
    required TResult Function(_RefreshAllChart value) refreshAllChart,
  }) {
    return getAllData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetData value)? getData,
    TResult? Function(_RefreshChart value)? refreshChart,
    TResult? Function(_GetAllData value)? getAllData,
    TResult? Function(_RefreshAllChart value)? refreshAllChart,
  }) {
    return getAllData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetData value)? getData,
    TResult Function(_RefreshChart value)? refreshChart,
    TResult Function(_GetAllData value)? getAllData,
    TResult Function(_RefreshAllChart value)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (getAllData != null) {
      return getAllData(this);
    }
    return orElse();
  }
}

abstract class _GetAllData implements ChartsEvent {
  const factory _GetAllData(
      {required final String fromDate,
      required final String toDate}) = _$_GetAllData;

  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  @JsonKey(ignore: true)
  _$$_GetAllDataCopyWith<_$_GetAllData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshAllChartCopyWith<$Res>
    implements $ChartsEventCopyWith<$Res> {
  factory _$$_RefreshAllChartCopyWith(
          _$_RefreshAllChart value, $Res Function(_$_RefreshAllChart) then) =
      __$$_RefreshAllChartCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDate, String toDate});
}

/// @nodoc
class __$$_RefreshAllChartCopyWithImpl<$Res>
    extends _$ChartsEventCopyWithImpl<$Res, _$_RefreshAllChart>
    implements _$$_RefreshAllChartCopyWith<$Res> {
  __$$_RefreshAllChartCopyWithImpl(
      _$_RefreshAllChart _value, $Res Function(_$_RefreshAllChart) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
  }) {
    return _then(_$_RefreshAllChart(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RefreshAllChart implements _RefreshAllChart {
  const _$_RefreshAllChart({required this.fromDate, required this.toDate});

  @override
  final String fromDate;
  @override
  final String toDate;

  @override
  String toString() {
    return 'ChartsEvent.refreshAllChart(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshAllChart &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshAllChartCopyWith<_$_RefreshAllChart> get copyWith =>
      __$$_RefreshAllChartCopyWithImpl<_$_RefreshAllChart>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate) getData,
    required TResult Function(String fromDate, String toDate) refreshChart,
    required TResult Function(String fromDate, String toDate) getAllData,
    required TResult Function(String fromDate, String toDate) refreshAllChart,
  }) {
    return refreshAllChart(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate)? getData,
    TResult? Function(String fromDate, String toDate)? refreshChart,
    TResult? Function(String fromDate, String toDate)? getAllData,
    TResult? Function(String fromDate, String toDate)? refreshAllChart,
  }) {
    return refreshAllChart?.call(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate)? getData,
    TResult Function(String fromDate, String toDate)? refreshChart,
    TResult Function(String fromDate, String toDate)? getAllData,
    TResult Function(String fromDate, String toDate)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (refreshAllChart != null) {
      return refreshAllChart(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetData value) getData,
    required TResult Function(_RefreshChart value) refreshChart,
    required TResult Function(_GetAllData value) getAllData,
    required TResult Function(_RefreshAllChart value) refreshAllChart,
  }) {
    return refreshAllChart(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetData value)? getData,
    TResult? Function(_RefreshChart value)? refreshChart,
    TResult? Function(_GetAllData value)? getAllData,
    TResult? Function(_RefreshAllChart value)? refreshAllChart,
  }) {
    return refreshAllChart?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetData value)? getData,
    TResult Function(_RefreshChart value)? refreshChart,
    TResult Function(_GetAllData value)? getAllData,
    TResult Function(_RefreshAllChart value)? refreshAllChart,
    required TResult orElse(),
  }) {
    if (refreshAllChart != null) {
      return refreshAllChart(this);
    }
    return orElse();
  }
}

abstract class _RefreshAllChart implements ChartsEvent {
  const factory _RefreshAllChart(
      {required final String fromDate,
      required final String toDate}) = _$_RefreshAllChart;

  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  @JsonKey(ignore: true)
  _$$_RefreshAllChartCopyWith<_$_RefreshAllChart> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ChartsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> shifts, List<Shift>? allShift)
        success,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult? Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChartsStateCopyWith<$Res> {
  factory $ChartsStateCopyWith(
          ChartsState value, $Res Function(ChartsState) then) =
      _$ChartsStateCopyWithImpl<$Res, ChartsState>;
}

/// @nodoc
class _$ChartsStateCopyWithImpl<$Res, $Val extends ChartsState>
    implements $ChartsStateCopyWith<$Res> {
  _$ChartsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ChartsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ChartsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> shifts, List<Shift>? allShift)
        success,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult? Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ChartsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$ChartsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'ChartsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> shifts, List<Shift>? allShift)
        success,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult? Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements ChartsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Shift> shifts, List<Shift>? allShift});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$ChartsStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shifts = null,
    Object? allShift = freezed,
  }) {
    return _then(_$_Success(
      null == shifts
          ? _value._shifts
          : shifts // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
      freezed == allShift
          ? _value._allShift
          : allShift // ignore: cast_nullable_to_non_nullable
              as List<Shift>?,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<Shift> shifts, final List<Shift>? allShift)
      : _shifts = shifts,
        _allShift = allShift;

  final List<Shift> _shifts;
  @override
  List<Shift> get shifts {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_shifts);
  }

  final List<Shift>? _allShift;
  @override
  List<Shift>? get allShift {
    final value = _allShift;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ChartsState.success(shifts: $shifts, allShift: $allShift)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._shifts, _shifts) &&
            const DeepCollectionEquality().equals(other._allShift, _allShift));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_shifts),
      const DeepCollectionEquality().hash(_allShift));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> shifts, List<Shift>? allShift)
        success,
    required TResult Function(String error) error,
  }) {
    return success(shifts, allShift);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult? Function(String error)? error,
  }) {
    return success?.call(shifts, allShift);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(shifts, allShift);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements ChartsState {
  const factory _Success(
      final List<Shift> shifts, final List<Shift>? allShift) = _$_Success;

  List<Shift> get shifts;
  List<Shift>? get allShift;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$ChartsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'ChartsState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Shift> shifts, List<Shift>? allShift)
        success,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult? Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Shift> shifts, List<Shift>? allShift)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements ChartsState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
