part of 'charts_bloc.dart';

@freezed
class ChartsEvent with _$ChartsEvent {
  const factory ChartsEvent.getData(
      {required String fromDate, required String toDate}) = _GetData;

  const factory ChartsEvent.refreshChart(
      {required String fromDate, required String toDate}) = _RefreshChart;

  const factory ChartsEvent.getAllData(
      {required String fromDate, required String toDate}) = _GetAllData;

  const factory ChartsEvent.refreshAllChart(
      {required String fromDate, required String toDate}) = _RefreshAllChart;
}
