import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'charts_event.dart';
part 'charts_state.dart';
part 'charts_bloc.freezed.dart';

class ChartsBloc extends Bloc<ChartsEvent, ChartsState> {
  ChartsBloc() : super(const _Initial()) {
    on<ChartsEvent>((event, emit) async {
      if (event is _GetData || event is _GetAllData) {
        emit(const _Loading());
      }
      try {
        final response = await adminApiProvider.getSchedule(
            fromDate: event.fromDate,
            toDate: event.toDate,
            id: HiveUtils.get(HiveKeys.facilityId));
        if (response.statusCode == 200) {
          final schedule =
              UpcomingShift.fromJson({'shift': jsonDecode(response.body)});
          if (event is _GetAllData || event is _RefreshAllChart) {
            try {
              final res = await adminApiProvider.getSchedule(
                  fromDate: event.fromDate, toDate: event.toDate);
              if (res.statusCode == 200) {
                UpcomingShift allSchedule =
                    UpcomingShift.fromJson({'shift': jsonDecode(res.body)});
                emit(_Success(schedule.shift, allSchedule.shift));
              }
            } catch (e) {
              emit(_Error(e.toString()));
            }
          } else {
            emit(_Success(schedule.shift, null));
          }
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}

ChartsBloc chartsBloc = ChartsBloc();
