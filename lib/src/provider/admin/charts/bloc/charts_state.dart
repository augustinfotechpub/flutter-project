part of 'charts_bloc.dart';

@freezed
class ChartsState with _$ChartsState {
  const factory ChartsState.initial() = _Initial;
  const factory ChartsState.loading() = _Loading;
  const factory ChartsState.success(List<Shift> shifts, List<Shift>? allShift) =
      _Success;
  const factory ChartsState.error(String error) = _Error;
}
