// ignore_for_file: empty_catches

import 'dart:convert';

import 'package:http/http.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/restart_app/restart_app_bloc.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/third_party_login_utils.dart';

class AdminApiProvider {
  // final String _baseUrl = 'http://44.203.161.90/api/';
  final String _baseUrl = 'https://admin.shift-alerts.com/api/';

  final String _adminProfile = 'admin/view/?admin_id=';
  final String _editProfile = 'admin/edit/';
  final String _schedule = 'schedulingfilter/view/';
  final String _getNotes = 'notes/view/?user_id=';
  final String _createNote = 'notes/create/';
  final String _credential = 'document/view/?clinician_position=';
  final String _feedbackByPosition = 'timesheet/view/?clinician_position=';
  final String _feedbackByID = 'timesheet/view/?clinician_id=';
  final String _deleteNotes = 'notes/delete/';
  final String _getInvoices = 'invoice/view/';
  final String _deleteInvoice = 'invoice/delete/';
  final String _createInvoiceEndPoint = 'invoice/create/';
  final String _editInvoiceEndPoint = 'invoice/edit/';
  final String _deleteAccount = 'admin/delete/';
  final String _refreshTokenEndPoint = 'token/refresh/';

  final Map<String, String> _headers = {'Content-Type': 'application/json'};

  Future<Response> getAdminProfile(String id) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_adminProfile$id'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getAdminProfile(id);
    }
    return response;
  }

  Future<Response> updateProfile(
      {required Map<String, dynamic> userData, String? id}) async {
    late Response response;
    if (id == null) {
      response = await patch(
        Uri.parse('$_baseUrl$_editProfile${userData['admin_id']}/'),
        headers: {
          'Authorization': 'Bearer $accessToken',
          'Content-Type': 'application/json',
        },
        body: jsonEncode(userData),
      );
    } else {
      response = await patch(
        Uri.parse('$_baseUrl$_editProfile$id/'),
        headers: {
          'Authorization': 'Bearer $accessToken',
          'Content-Type': 'application/json',
        },
      );
    }
    if (response.statusCode == 403) {
      await refreshToken();
      return updateProfile(userData: userData);
    }
    return response;
  }

  Future<Response> getSchedule(
      {required String fromDate, required String toDate, String? id}) async {
    final Response response = await get(
      Uri.parse(id == null
          ? '$_baseUrl$_schedule?from_date=$fromDate&to_date=$toDate'
          : '$_baseUrl$_schedule?from_date=$fromDate&to_date=$toDate&facility_id=$id'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getSchedule(fromDate: fromDate, toDate: toDate, id: id);
    }
    return response;
  }

  Future<Response> getNotes({required String id}) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getNotes$id'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getNotes(id: id);
    }
    return response;
  }

  Future<Response> createNote({required Map<String, dynamic> body}) async {
    final Response response = await post(
      Uri.parse('$_baseUrl$_createNote'),
      body: body,
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return createNote(body: body);
    }
    return response;
  }

  Future<Response> getCredentials({required String position}) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_credential$position'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getCredentials(position: position);
    }
    return response;
  }

  Future<Response> getFeedback(
      {required String? position, required String? id}) async {
    final Response response = await get(
      Uri.parse(
        id == null
            ? '$_baseUrl$_feedbackByPosition$position'
            : '$_baseUrl$_feedbackByID$id',
      ),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getFeedback(position: position, id: id);
    }
    return response;
  }

  Future<Response> deleteNotes({required String id}) async {
    final Response response = await delete(
      Uri.parse('$_baseUrl$_deleteNotes$id/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return deleteNotes(id: id);
    }
    return response;
  }

  Future<Response> getInvoices() async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getInvoices'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getInvoices();
    }
    return response;
  }

  Future<Response> deleteInvoice(String invoiceID) async {
    final response = await delete(
      Uri.parse('$_baseUrl$_deleteInvoice$invoiceID/'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return deleteInvoice(invoiceID);
    }
    if (response.statusCode == 204) {
      return await adminApiProvider.getInvoices();
    } else {
      return response;
    }
  }

  Future<Response> createInvoice(Map<String, dynamic> map) async {
    final Response response = await post(
      Uri.parse('$_baseUrl$_createInvoiceEndPoint'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: json.encode(map),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return createInvoice(map);
    }
    return response;
  }

  Future<Response> editInvoice(
      Map<String, dynamic> map, String invoiceId) async {
    final Response response = await patch(
      Uri.parse('$_baseUrl$_editInvoiceEndPoint$invoiceId/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: json.encode(map),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return editInvoice(map, invoiceId);
    }
    return response;
  }

  Future<Response> deleteAccount() async {
    final Response response = await delete(
      Uri.parse('$_baseUrl$_deleteAccount${HiveUtils.get(HiveKeys.adminId)}/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return deleteAccount();
    }
    return response;
  }

  Future<void> refreshToken() async {
    final Response response = await post(
      Uri.parse('$_baseUrl$_refreshTokenEndPoint'),
      headers: _headers,
      body: jsonEncode({'refresh': HiveUtils.get(HiveKeys.refreshToken)}),
    );
    if (response.statusCode == 403) {
      try {
        rtmClient.logout();
        if (await loginUtils.googleSignIn.isSignedIn()) {
          loginUtils.googleSignIn.signOut();
        }
        loginUtils.facebookLogin.logOut();
      } catch (e) {}
      apiProvider.updateToken('');
      if (!HiveUtils.get(HiveKeys.rememberMe)) {
        HiveUtils.remove(HiveKeys.email);
        HiveUtils.remove(HiveKeys.password);
      }
      HiveUtils.remove(HiveKeys.userType);
      restartAppBloc.add(const RestartAppEvent.restart());
    } else if (response.statusCode == 200) {
      HiveUtils.set(HiveKeys.accessToken, jsonDecode(response.body)['access']);
      accessToken = jsonDecode(response.body)['access'];
    }
  }
}

final AdminApiProvider adminApiProvider = AdminApiProvider();
