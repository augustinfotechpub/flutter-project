import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/timesheet/model.dart';

part 'feedback_event.dart';
part 'feedback_state.dart';
part 'feedback_bloc.freezed.dart';

class FeedbackBloc extends Bloc<FeedbackEvent, FeedbackState> {
  FeedbackBloc() : super(const _Initial()) {
    on<FeedbackEvent>((event, emit) async {
      emit(const _Loading());
      try {
        final response = await adminApiProvider.getFeedback(
          position: event.position,
          id: event.id,
        );
        if (response.statusCode == 200) {
          Timesheet timesheets = Timesheet.fromJson(jsonDecode(response.body));
          emit(_Success(
            timesheets.results,
            event.position ?? '',
          ));
        } else {
          emit(_Error(response.body));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
