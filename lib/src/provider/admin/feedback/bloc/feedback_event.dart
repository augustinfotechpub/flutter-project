part of 'feedback_bloc.dart';

@freezed
class FeedbackEvent with _$FeedbackEvent {
  const factory FeedbackEvent.getFeedback({required String? position, required String? id}) = _GetFeedback;
}
