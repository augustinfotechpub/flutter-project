part of 'feedback_bloc.dart';

@freezed
class FeedbackState with _$FeedbackState {
  const factory FeedbackState.initial() = _Initial;
  const factory FeedbackState.loading() = _Loading;
  const factory FeedbackState.success(List<TimesheetDetail> feedbacks, String position) =
      _Success;
  const factory FeedbackState.error(String error) = _Error;
}
