import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'my_performance_event.dart';
part 'my_performance_state.dart';
part 'my_performance_bloc.freezed.dart';

class MyPerformanceBloc extends Bloc<MyPerformanceEvent, MyPerformanceState> {
  MyPerformanceBloc() : super(const _Initial()) {
    on<MyPerformanceEvent>((event, emit) async {
      if (event is _GetChartData) {
        emit(const _Loading());
      }
      try {
        final shift = await apiProvider.getUpcomingShifts(
            fromDate: event.fromDate,
            toDate: event.toDate,
            id: HiveUtils.get(HiveKeys.clinicianId));
        if (shift.statusCode == 200) {
          UpcomingShift upcomingShift =
              UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
          emit(_Success(event.dateType, upcomingShift));
        } else {
          emit(_Error(jsonDecode(shift.body)));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
