part of 'my_performance_bloc.dart';

@freezed
class MyPerformanceEvent with _$MyPerformanceEvent {
  const factory MyPerformanceEvent.getChartData({
    required String dateType,
    required String fromDate,
    required String toDate,
  }) = _GetChartData;
}
