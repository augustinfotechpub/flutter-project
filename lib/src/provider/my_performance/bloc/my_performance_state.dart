part of 'my_performance_bloc.dart';

@freezed
class MyPerformanceState with _$MyPerformanceState {
  const factory MyPerformanceState.initial() = _Initial;
  const factory MyPerformanceState.loading() = _Loading;
  const factory MyPerformanceState.success(
      String dateType, UpcomingShift upcomingShift) = _Success;
  const factory MyPerformanceState.error(String error) = _Error;
}
