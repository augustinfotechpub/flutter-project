part of 'onshift_card_bloc.dart';

@freezed
class OnshiftCardState with _$OnshiftCardState {
  const factory OnshiftCardState.initial({
    required bool showBrekinButton,
    required bool disableBrekinButton,
    required String buttonName,
  }) = _Initial;
}
