// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'onshift_card_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OnshiftCardEvent {
  bool get showBrekinButton => throw _privateConstructorUsedError;
  bool get disableBrekinButton => throw _privateConstructorUsedError;
  String get buttonName => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)
        started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        started,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OnshiftCardEventCopyWith<OnshiftCardEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OnshiftCardEventCopyWith<$Res> {
  factory $OnshiftCardEventCopyWith(
          OnshiftCardEvent value, $Res Function(OnshiftCardEvent) then) =
      _$OnshiftCardEventCopyWithImpl<$Res, OnshiftCardEvent>;
  @useResult
  $Res call(
      {bool showBrekinButton, bool disableBrekinButton, String buttonName});
}

/// @nodoc
class _$OnshiftCardEventCopyWithImpl<$Res, $Val extends OnshiftCardEvent>
    implements $OnshiftCardEventCopyWith<$Res> {
  _$OnshiftCardEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showBrekinButton = null,
    Object? disableBrekinButton = null,
    Object? buttonName = null,
  }) {
    return _then(_value.copyWith(
      showBrekinButton: null == showBrekinButton
          ? _value.showBrekinButton
          : showBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      disableBrekinButton: null == disableBrekinButton
          ? _value.disableBrekinButton
          : disableBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      buttonName: null == buttonName
          ? _value.buttonName
          : buttonName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res>
    implements $OnshiftCardEventCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool showBrekinButton, bool disableBrekinButton, String buttonName});
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$OnshiftCardEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showBrekinButton = null,
    Object? disableBrekinButton = null,
    Object? buttonName = null,
  }) {
    return _then(_$_Started(
      showBrekinButton: null == showBrekinButton
          ? _value.showBrekinButton
          : showBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      disableBrekinButton: null == disableBrekinButton
          ? _value.disableBrekinButton
          : disableBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      buttonName: null == buttonName
          ? _value.buttonName
          : buttonName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started(
      {required this.showBrekinButton,
      required this.disableBrekinButton,
      required this.buttonName});

  @override
  final bool showBrekinButton;
  @override
  final bool disableBrekinButton;
  @override
  final String buttonName;

  @override
  String toString() {
    return 'OnshiftCardEvent.started(showBrekinButton: $showBrekinButton, disableBrekinButton: $disableBrekinButton, buttonName: $buttonName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Started &&
            (identical(other.showBrekinButton, showBrekinButton) ||
                other.showBrekinButton == showBrekinButton) &&
            (identical(other.disableBrekinButton, disableBrekinButton) ||
                other.disableBrekinButton == disableBrekinButton) &&
            (identical(other.buttonName, buttonName) ||
                other.buttonName == buttonName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, showBrekinButton, disableBrekinButton, buttonName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      __$$_StartedCopyWithImpl<_$_Started>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)
        started,
  }) {
    return started(showBrekinButton, disableBrekinButton, buttonName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        started,
  }) {
    return started?.call(showBrekinButton, disableBrekinButton, buttonName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        started,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(showBrekinButton, disableBrekinButton, buttonName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements OnshiftCardEvent {
  const factory _Started(
      {required final bool showBrekinButton,
      required final bool disableBrekinButton,
      required final String buttonName}) = _$_Started;

  @override
  bool get showBrekinButton;
  @override
  bool get disableBrekinButton;
  @override
  String get buttonName;
  @override
  @JsonKey(ignore: true)
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$OnshiftCardState {
  bool get showBrekinButton => throw _privateConstructorUsedError;
  bool get disableBrekinButton => throw _privateConstructorUsedError;
  String get buttonName => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OnshiftCardStateCopyWith<OnshiftCardState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OnshiftCardStateCopyWith<$Res> {
  factory $OnshiftCardStateCopyWith(
          OnshiftCardState value, $Res Function(OnshiftCardState) then) =
      _$OnshiftCardStateCopyWithImpl<$Res, OnshiftCardState>;
  @useResult
  $Res call(
      {bool showBrekinButton, bool disableBrekinButton, String buttonName});
}

/// @nodoc
class _$OnshiftCardStateCopyWithImpl<$Res, $Val extends OnshiftCardState>
    implements $OnshiftCardStateCopyWith<$Res> {
  _$OnshiftCardStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showBrekinButton = null,
    Object? disableBrekinButton = null,
    Object? buttonName = null,
  }) {
    return _then(_value.copyWith(
      showBrekinButton: null == showBrekinButton
          ? _value.showBrekinButton
          : showBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      disableBrekinButton: null == disableBrekinButton
          ? _value.disableBrekinButton
          : disableBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      buttonName: null == buttonName
          ? _value.buttonName
          : buttonName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res>
    implements $OnshiftCardStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool showBrekinButton, bool disableBrekinButton, String buttonName});
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$OnshiftCardStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showBrekinButton = null,
    Object? disableBrekinButton = null,
    Object? buttonName = null,
  }) {
    return _then(_$_Initial(
      showBrekinButton: null == showBrekinButton
          ? _value.showBrekinButton
          : showBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      disableBrekinButton: null == disableBrekinButton
          ? _value.disableBrekinButton
          : disableBrekinButton // ignore: cast_nullable_to_non_nullable
              as bool,
      buttonName: null == buttonName
          ? _value.buttonName
          : buttonName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(
      {required this.showBrekinButton,
      required this.disableBrekinButton,
      required this.buttonName});

  @override
  final bool showBrekinButton;
  @override
  final bool disableBrekinButton;
  @override
  final String buttonName;

  @override
  String toString() {
    return 'OnshiftCardState.initial(showBrekinButton: $showBrekinButton, disableBrekinButton: $disableBrekinButton, buttonName: $buttonName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            (identical(other.showBrekinButton, showBrekinButton) ||
                other.showBrekinButton == showBrekinButton) &&
            (identical(other.disableBrekinButton, disableBrekinButton) ||
                other.disableBrekinButton == disableBrekinButton) &&
            (identical(other.buttonName, buttonName) ||
                other.buttonName == buttonName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, showBrekinButton, disableBrekinButton, buttonName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)
        initial,
  }) {
    return initial(showBrekinButton, disableBrekinButton, buttonName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        initial,
  }) {
    return initial?.call(showBrekinButton, disableBrekinButton, buttonName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            bool showBrekinButton, bool disableBrekinButton, String buttonName)?
        initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(showBrekinButton, disableBrekinButton, buttonName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements OnshiftCardState {
  const factory _Initial(
      {required final bool showBrekinButton,
      required final bool disableBrekinButton,
      required final String buttonName}) = _$_Initial;

  @override
  bool get showBrekinButton;
  @override
  bool get disableBrekinButton;
  @override
  String get buttonName;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
