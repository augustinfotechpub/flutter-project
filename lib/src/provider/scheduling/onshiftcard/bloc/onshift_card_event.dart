part of 'onshift_card_bloc.dart';

@freezed
class OnshiftCardEvent with _$OnshiftCardEvent {
  const factory OnshiftCardEvent.started({
    required bool showBrekinButton,
    required bool disableBrekinButton,
    required String buttonName,
  }) = _Started;
}
