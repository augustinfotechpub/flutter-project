import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/constant/string.dart';

part 'onshift_card_event.dart';
part 'onshift_card_state.dart';
part 'onshift_card_bloc.freezed.dart';

class OnshiftCardBloc extends Bloc<OnshiftCardEvent, OnshiftCardState> {
  OnshiftCardBloc()
      : super(_Initial(
          buttonName: SaString.clockIn.split(':')[0],
          disableBrekinButton: false,
          showBrekinButton: false,
        )) {
    on<OnshiftCardEvent>((event, emit) {
      emit(
        _Initial(
          showBrekinButton: event.showBrekinButton,
          disableBrekinButton: event.disableBrekinButton,
          buttonName: event.buttonName,
        ),
      );
    });
  }
}
