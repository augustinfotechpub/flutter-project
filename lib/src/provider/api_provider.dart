// ignore_for_file: empty_catches

import 'dart:convert';

import 'package:http/http.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/provider/restart_app/restart_app_bloc.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/third_party_login_utils.dart';

class ApiProvider {
  // final String _baseUrl = 'http://44.203.161.90/api/';
  final String _baseUrl = 'https://admin.shift-alerts.com/api/';

  final String _loginEndPoint = 'user/login/';
  final String _socialLoginEndPoint = 'user/social-login/';
  final String _appleLoginEndPoint = 'user/apple-login/';
  final String _clinicianEndPoint = 'clinician/view/?clinician_id=';
  final String _profileEndPoint = 'user/profile/';
  final String _editProfileEndPoint = 'clinician/edit/';
  final String _forgotPasswordEndPoint = 'user/sendresetpwd/';
  final String _changePasswordEndPoint = 'user/changepassword/';
  final String _schedulingView = 'schedulingfilter/view/';
  final String _openShiftsEndPoint =
      'OpenShift/view/?open_shift=Open&clinician_position=';
  final String _editShiftEndPoint = 'scheduling/edit/';
  final String _viewDocumentEndPoint = 'document/view/?clinician_id=';
  final String _uploadDocumentEndPoint = 'document/create/';
  final String _deleteDocumentEndPoint = 'document/delete/';
  final String _getTimesheetEndPoint = 'timesheetFilter/view/?clinician_id=';
  final String _submitPOCEndPoint = 'timesheet/create/';
  final String _getFaciltyEndPoint = 'admin/view/?facility=';
  final String _getNotificationEndPoint = 'notification/view/?user_id=';
  final String _postNotificationEndPoint = 'notification/create/';
  final String _deleteNotificationEndPoint = 'notification/delete/';
  final String _getAdminsEndPoint = 'admin/view/';
  final String _getMessageEndPoint = 'chat/view/?channel=';
  final String _createChannelEndPoint = 'chat/history/create/';
  final String _sendMessageEndPoint = 'chat/history/edit/';
  final String _sendFileEndPoint = 's3upload/chatfile/';
  final String _chatUsers = 'chat/users/';
  final String _chatList = 'chat/history/view/?user_id=';
  final String _getTokenEndPoint = 'token/view/?user_id=';
  final String _createTokenEndPoint = 'token/create/';
  final String _updateTokenEndPoint = 'token/edit/';
  final String _latLongEndPoint = 'facility/view/?facility_id=';
  final String _imageUploadEndPoint = 's3upload/image/';
  final String _docUploadEndPoint = 's3upload/document/';
  final String _agoraTokenEndPoint = 'tokengenerator/';
  final String _deleteAccountEndPoint = 'clinician/delete/';
  final String _createActivityEndPoint = 'activities/create/';
  final String _refreshTokenEndPoint = 'token/refresh/';

  final String _fcm = 'https://fcm.googleapis.com/fcm/send';

  final Map<String, String> _headers = {'Content-Type': 'application/json'};

  Future<Response> login(
      {required String email, required String password}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _loginEndPoint),
      headers: _headers,
      body: jsonEncode(
          {'email': email, 'password': base64.encode(utf8.encode(password))}),
    );
    return response;
  }

  Future<Response> socialLogIn({String? email, String? identifier}) async {
    String endPoint =
        identifier == null ? _socialLoginEndPoint : _appleLoginEndPoint;
    Map<String, dynamic> body = identifier == null
        ? {'email': email}
        : {'email': email, 'apple_id': identifier};
    final Response response = await post(
      Uri.parse(_baseUrl + endPoint),
      headers: _headers,
      body: jsonEncode(body),
    );
    return response;
  }

  Future<Response> getClinician(String id) async {
    final Response response = await get(
      Uri.parse(_baseUrl + _clinicianEndPoint + id),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getClinician(id);
    }
    return response;
  }

  Future<Response> getProfile(String token) async {
    final Response response = await get(
      Uri.parse(_baseUrl + _profileEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getProfile(token);
    }
    return response;
  }

  Future<Response> resetPassword({required String email}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _forgotPasswordEndPoint),
      headers: _headers,
      body: jsonEncode({'email': email}),
    );
    return response;
  }

  Future<Response> updateProfile(
      {required Map<String, dynamic> userData, String? id}) async {
    late Response response;
    if (id == null) {
      response = await patch(
        Uri.parse('$_baseUrl$_editProfileEndPoint${userData['clinician_id']}/'),
        headers: {
          'Authorization': 'Bearer $accessToken',
          'Content-Type': 'application/json',
        },
        body: jsonEncode(userData),
      );
      if (response.statusCode == 403) {
        await refreshToken();
        return updateProfile(userData: userData);
      }
    } else {
      response = await patch(
        Uri.parse('$_baseUrl$_editProfileEndPoint$id/'),
        headers: {
          'Authorization': 'Bearer $accessToken',
          'Content-Type': 'application/json',
        },
      );
      if (response.statusCode == 403) {
        await refreshToken();
        return updateProfile(userData: userData);
      }
    }
    return response;
  }

  Future<Response> changePassword({
    required String oldPassword,
    required String newPassword,
    required String confirmPassword,
  }) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _changePasswordEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        "old_password": base64.encode(utf8.encode(oldPassword)),
        "password": base64.encode(utf8.encode(newPassword)),
        "password2": base64.encode(utf8.encode(confirmPassword))
      }),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return changePassword(
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      );
    }
    return response;
  }

  Future<Response> getUpcomingShifts({
    required String fromDate,
    required String toDate,
    required String id,
  }) async {
    final Response response = await get(
      Uri.parse(
          '$_baseUrl$_schedulingView?from_date=$fromDate&to_date=$toDate&clinician_id=$id'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getUpcomingShifts(fromDate: fromDate, toDate: toDate, id: id);
    }
    return response;
  }

  Future<Response> getOnShifts({
    required String date,
    required String id,
  }) async {
    final Response response = await get(
      Uri.parse(
          '$_baseUrl$_schedulingView?from_date=$date&to_date=$date&clinician_id=$id'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );

    if (response.statusCode == 403) {
      await refreshToken();
      return getOnShifts(date: date, id: id);
    }
    return response;
  }

  Future<Response> getOpenShifts({
    required String position,
    required String fromDate,
    required String toDate,
  }) async {
    final Response response = await get(
      Uri.parse(
          '$_baseUrl$_openShiftsEndPoint$position&from_date=$fromDate&to_date=$toDate&clinician_id=${HiveUtils.get(HiveKeys.clinicianId)}'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getOpenShifts(
          position: position, fromDate: fromDate, toDate: toDate);
    }
    return response;
  }

  Future<Response> editShift({
    required String id,
    required Map<String, dynamic> data,
  }) async {
    final response = await patch(
      Uri.parse('$_baseUrl$_editShiftEndPoint$id/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: json.encode(data),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return editShift(id: id, data: data);
    }
    return response;
  }

  Future<Response> getDocuments({required String clinicianID}) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_viewDocumentEndPoint$clinicianID'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getDocuments(clinicianID: clinicianID);
    }
    return response;
  }

  Future<Response> uploadDocument({
    required Map<String, dynamic> doc,
  }) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _uploadDocumentEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode(doc),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return uploadDocument(doc: doc);
    }
    return response;
  }

  Future<Response> deleteDocument({
    required int documentID,
  }) async {
    final Response response = await delete(
      Uri.parse('$_baseUrl$_deleteDocumentEndPoint$documentID/'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      deleteDocument(documentID: documentID);
    }
    return response;
  }

  Future<Response> getTimesheets({
    required String clinicianID,
    required String fromDate,
    required String toDate,
  }) async {
    final Response response = await get(
      Uri.parse(
        '$_baseUrl$_getTimesheetEndPoint$clinicianID&from_date=$fromDate&to_date=$toDate',
      ),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getTimesheets(
          clinicianID: clinicianID, fromDate: fromDate, toDate: toDate);
    }
    return response;
  }

  Future<Response> submitForSignature({
    required Map<String, dynamic> map,
  }) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _submitPOCEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode(map),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return submitForSignature(map: map);
    }
    return response;
  }

  Future<Map<String, dynamic>?> getFacilty({required String id}) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getFaciltyEndPoint$id&role=poc_admin'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 200) {
      final map =
          List<Map<String, dynamic>>.from(jsonDecode(response.body)["results"]);
      if (map.isEmpty) {
        return null;
      }
      return {
        "fullname": map.first['fullname'],
        "address": map.first['address'],
        "role": map.first['role']
      };
    } else if (response.statusCode == 403) {
      await refreshToken();
      return getFacilty(id: id);
    } else {
      return null;
    }
  }

  Future<Response> getNotification({required String userID}) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getNotificationEndPoint$userID'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getNotification(userID: userID);
    }
    return response;
  }

  Future<Response> postNotification({required Map<String, dynamic> map}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _postNotificationEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode(map),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return postNotification(map: map);
    }
    return response;
  }

  Future<Response> deleteNotification({required String id}) async {
    final Response response = await delete(
      Uri.parse('$_baseUrl$_deleteNotificationEndPoint$id/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return deleteNotification(id: id);
    }
    return response;
  }

  Future<Response> getAdmins() async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getAdminsEndPoint'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getAdmins();
    }
    return response;
  }

  Future<Response> getChatUsers() async {
    String url = userType == UserType.admin
        ? '$_baseUrl$_chatUsers?admin_id=${HiveUtils.get(HiveKeys.adminId)}'
        : '$_baseUrl$_chatUsers?clinician_id=${HiveUtils.get(HiveKeys.clinicianId)}';
    final Response response = await get(
      Uri.parse(url),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getChatUsers();
    }
    return response;
  }

  Future<Response> getChatList() async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_chatList${HiveUtils.get(HiveKeys.user)}'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getChatList();
    }
    return response;
  }

  Future<Response> getMessages(String channelId) async {
    final Response response = await get(
      Uri.parse('$_baseUrl$_getMessageEndPoint$channelId'),
      headers: {'Authorization': 'Bearer $accessToken'},
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getMessages(channelId);
    }
    return response;
  }

  Future<Response> updateStatus(String channelId, String peerId) async {
    final Response response = await patch(
      Uri.parse('$_baseUrl$_sendMessageEndPoint$channelId/'),
      headers: {'Authorization': 'Bearer $accessToken'},
      body: {
        'channel': channelId,
        'displayed': 'true',
        'user_id': '${HiveUtils.get(HiveKeys.user)}',
        'peer_id': peerId,
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return updateStatus(channelId, peerId);
    }
    return response;
  }

  Future<Response> sendFile(String file, String type, String name) async {
    final Response response = await post(
      Uri.parse('$_baseUrl$_sendFileEndPoint'),
      headers: {'Authorization': 'Bearer $accessToken'},
      body: {
        'user_id': HiveUtils.get(HiveKeys.user).toString(),
        'data': file,
        'type': type,
        'name': name
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return sendFile(file, type, name);
    }
    return response;
  }

  Future<Response> sendMessages({
    required String channelId,
    required String peerId,
    required String peerName,
    required String peerImage,
    required String message,
    required List<Map<String, dynamic>> chatData,
  }) async {
    Response response;
    Map<String, dynamic> body = {
      'chat_data': jsonEncode(chatData).toString(),
      'channel': channelId,
      'user_id': '${HiveUtils.get(HiveKeys.user)}',
      'peer_id': peerId,
      'user_fullname': '${HiveUtils.get(HiveKeys.fullName)}',
      'peer_fullname': peerName,
      'user_avatar_image': '${HiveUtils.get(HiveKeys.avatarImage)}',
      'peer_avatar_image': peerImage,
      'last_message': message,
      'message_by': '${HiveUtils.get(HiveKeys.user)}',
      'message_to': peerId,
      'message_time': '${DateTime.now()}',
      'displayed': 'false',
    };
    response = await patch(
      Uri.parse('$_baseUrl$_sendMessageEndPoint$channelId/'),
      headers: {'Authorization': 'Bearer $accessToken'},
      body: body,
    );
    if (response.statusCode != 200) {
      response = await post(
        Uri.parse('$_baseUrl$_createChannelEndPoint'),
        headers: {'Authorization': 'Bearer $accessToken'},
        body: body,
      );
    } else {
      if (jsonDecode(response.body)['status'] != 200) {
        response = await post(
          Uri.parse('$_baseUrl$_createChannelEndPoint'),
          headers: {'Authorization': 'Bearer $accessToken'},
          body: body,
        );
      }
    }
    if (response.statusCode == 403) {
      await refreshToken();
      return sendMessages(
          channelId: channelId,
          peerId: peerId,
          peerName: peerName,
          peerImage: peerImage,
          message: message,
          chatData: chatData);
    }
    return response;
  }

  Future<Response> getToken(String id) async {
    Response response = await get(
      Uri.parse('$_baseUrl$_getTokenEndPoint$id'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getToken(id);
    }
    return response;
  }

  Future<Response> createToken(String token) async {
    Response response = await post(
      Uri.parse('$_baseUrl$_createTokenEndPoint'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        'user_id': HiveUtils.get(HiveKeys.user),
        'token': token,
      }),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return createToken(token);
    }
    return response;
  }

  Future<Response> updateToken(String token) async {
    Response response = await patch(
      Uri.parse(
          '$_baseUrl$_updateTokenEndPoint${HiveUtils.get(HiveKeys.user)}/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({'token': token}),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return updateToken(token);
    }
    return response;
  }

  Future<dynamic> sendNotification(
      {required String message,
      required String type,
      required String channelId,
      required String peerId,
      required String image}) async {
    Response res = await getToken(peerId);
    final body = {
      'to': jsonDecode(res.body)['results'].first['token'],
      // 'to': HiveUtils.get(HiveKeys.deviceToken),
      'priority': 'high',
      "mutable_content": true,
      "content_available": true,
      'data': {
        'payload': {
          'peerId': '${HiveUtils.get(HiveKeys.user)}',
          'peerName': '${HiveUtils.get(HiveKeys.fullName)}',
          'channelId': channelId,
          'message': message,
          'message_type': type,
          'image': '${HiveUtils.get(HiveKeys.avatarImage)}'
        },
        'content': {
          'channelKey': 'shift_alerts',
          'title': '${HiveUtils.get(HiveKeys.fullName)}',
          'body': message,
          'largeIcon': '',
          'bigPicture': '',
        }
      },
    };
    Response response = await post(
      Uri.parse(_fcm),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=${SaString.fcmToken}',
      },
      body: jsonEncode(body),
    );
    return response;
  }

  Future<Map<String, dynamic>?> getLatLong(String id) async {
    final response = await get(
      Uri.parse(_baseUrl + _latLongEndPoint + id),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      final String latitude =
          jsonDecode(response.body)['results'][0]['latitude'];
      final String longitude =
          jsonDecode(response.body)['results'][0]['longitude'];
      return {'latitude': latitude, 'longitude': longitude};
    }
    if (response.statusCode == 403) {
      await refreshToken();
      return getLatLong(id);
    }
    return null;
  }

  Future<Response> uploadImage({required Map<String, dynamic> doc}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _imageUploadEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode(doc),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return uploadImage(doc: doc);
    }
    return response;
  }

  Future<Response> uploadDocumentFile(
      {required Map<String, dynamic> doc}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _docUploadEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode(doc),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return uploadDocumentFile(doc: doc);
    }
    return response;
  }

  Future<Response> getAgoraToken({required String channelId}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _agoraTokenEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({'channelName': channelId}),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return getAgoraToken(channelId: channelId);
    }
    return response;
  }

  Future<Response> createActivity(
      {required String title,
      required String date,
      required String facilityId}) async {
    final Response response = await post(
      Uri.parse(_baseUrl + _createActivityEndPoint),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        "activity_title": title,
        "activity_status": "success",
        "acitivity_date": date,
        "category": '${title}s',
        "facility_id": 5,
      }),
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return createActivity(title: title, date: date, facilityId: facilityId);
    }
    return response;
  }

  Future<Response> deleteAccount() async {
    final Response response = await delete(
      Uri.parse(
          '$_baseUrl$_deleteAccountEndPoint${HiveUtils.get(HiveKeys.clinicianId)}/'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 403) {
      await refreshToken();
      return deleteAccount();
    }
    return response;
  }

  Future<void> refreshToken() async {
    final Response response = await post(
      Uri.parse('$_baseUrl$_refreshTokenEndPoint'),
      headers: _headers,
      body: jsonEncode({'refresh': HiveUtils.get(HiveKeys.refreshToken)}),
    );
    if (response.statusCode == 401) {
      try {
        if (await loginUtils.googleSignIn.isSignedIn()) {
          loginUtils.googleSignIn.signOut();
        }
        loginUtils.facebookLogin.logOut();
      } catch (e) {}
      // apiProvider.updateToken('');
      if (!HiveUtils.get(HiveKeys.rememberMe)) {
        HiveUtils.remove(HiveKeys.email);
        HiveUtils.remove(HiveKeys.password);
      }
      HiveUtils.remove(HiveKeys.userType);
      restartAppBloc.add(const RestartAppEvent.restart());
      return;
    } else if (response.statusCode == 200) {
      HiveUtils.set(HiveKeys.accessToken, jsonDecode(response.body)['access']);
      accessToken = jsonDecode(response.body)['access'];
    }
  }
}

final ApiProvider apiProvider = ApiProvider();
