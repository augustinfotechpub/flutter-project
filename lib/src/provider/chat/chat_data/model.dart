// ignore_for_file: invalid_annotation_target

import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class MessageList with _$MessageList {
  const factory MessageList({
    required List<Message> results,
  }) = _MessageList;

  factory MessageList.fromJson(Map<String, dynamic> json) =>
      _$MessageListFromJson(json);
}

@freezed
class Message with _$Message {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Message({
    required int chatId,
    required String channel,
    @JsonKey(fromJson: chatDataFromJson) required List<ChatData> chatData,
    required String lastChat,
    required String lastChatBy,
    required String read,
  }) = _Message;

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);
}

List<ChatData> chatDataFromJson(String chatData) {
  return ChatDataList.fromJson({'chatDataList': jsonDecode(chatData)})
      .chatDataList;
}

@freezed
class ChatDataList with _$ChatDataList {
  const factory ChatDataList({
    required List<ChatData> chatDataList,
  }) = _ChatDataList;

  factory ChatDataList.fromJson(Map<String, dynamic> json) =>
      _$ChatDataListFromJson(json);
}

@freezed
class ChatData with _$ChatData {
  const factory ChatData({
    required String uid,
    required String text,
    required String time,
    required String msgType,
  }) = _ChatData;

  factory ChatData.fromJson(Map<String, dynamic> json) =>
      _$ChatDataFromJson(json);
}
