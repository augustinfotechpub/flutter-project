// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MessageList _$$_MessageListFromJson(Map<String, dynamic> json) =>
    _$_MessageList(
      results: (json['results'] as List<dynamic>)
          .map((e) => Message.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_MessageListToJson(_$_MessageList instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

_$_Message _$$_MessageFromJson(Map<String, dynamic> json) => _$_Message(
      chatId: json['chat_id'] as int,
      channel: json['channel'] as String,
      chatData: chatDataFromJson(json['chat_data'] as String),
      lastChat: json['last_chat'] as String,
      lastChatBy: json['last_chat_by'] as String,
      read: json['read'] as String,
    );

Map<String, dynamic> _$$_MessageToJson(_$_Message instance) =>
    <String, dynamic>{
      'chat_id': instance.chatId,
      'channel': instance.channel,
      'chat_data': instance.chatData,
      'last_chat': instance.lastChat,
      'last_chat_by': instance.lastChatBy,
      'read': instance.read,
    };

_$_ChatDataList _$$_ChatDataListFromJson(Map<String, dynamic> json) =>
    _$_ChatDataList(
      chatDataList: (json['chatDataList'] as List<dynamic>)
          .map((e) => ChatData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ChatDataListToJson(_$_ChatDataList instance) =>
    <String, dynamic>{
      'chatDataList': instance.chatDataList,
    };

_$_ChatData _$$_ChatDataFromJson(Map<String, dynamic> json) => _$_ChatData(
      uid: json['uid'] as String,
      text: json['text'] as String,
      time: json['time'] as String,
      msgType: json['msgType'] as String,
    );

Map<String, dynamic> _$$_ChatDataToJson(_$_ChatData instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'text': instance.text,
      'time': instance.time,
      'msgType': instance.msgType,
    };
