// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MessageList _$MessageListFromJson(Map<String, dynamic> json) {
  return _MessageList.fromJson(json);
}

/// @nodoc
mixin _$MessageList {
  List<Message> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageListCopyWith<MessageList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageListCopyWith<$Res> {
  factory $MessageListCopyWith(
          MessageList value, $Res Function(MessageList) then) =
      _$MessageListCopyWithImpl<$Res, MessageList>;
  @useResult
  $Res call({List<Message> results});
}

/// @nodoc
class _$MessageListCopyWithImpl<$Res, $Val extends MessageList>
    implements $MessageListCopyWith<$Res> {
  _$MessageListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Message>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MessageListCopyWith<$Res>
    implements $MessageListCopyWith<$Res> {
  factory _$$_MessageListCopyWith(
          _$_MessageList value, $Res Function(_$_MessageList) then) =
      __$$_MessageListCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Message> results});
}

/// @nodoc
class __$$_MessageListCopyWithImpl<$Res>
    extends _$MessageListCopyWithImpl<$Res, _$_MessageList>
    implements _$$_MessageListCopyWith<$Res> {
  __$$_MessageListCopyWithImpl(
      _$_MessageList _value, $Res Function(_$_MessageList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? results = null,
  }) {
    return _then(_$_MessageList(
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Message>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MessageList implements _MessageList {
  const _$_MessageList({required final List<Message> results})
      : _results = results;

  factory _$_MessageList.fromJson(Map<String, dynamic> json) =>
      _$$_MessageListFromJson(json);

  final List<Message> _results;
  @override
  List<Message> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'MessageList(results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MessageList &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MessageListCopyWith<_$_MessageList> get copyWith =>
      __$$_MessageListCopyWithImpl<_$_MessageList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MessageListToJson(
      this,
    );
  }
}

abstract class _MessageList implements MessageList {
  const factory _MessageList({required final List<Message> results}) =
      _$_MessageList;

  factory _MessageList.fromJson(Map<String, dynamic> json) =
      _$_MessageList.fromJson;

  @override
  List<Message> get results;
  @override
  @JsonKey(ignore: true)
  _$$_MessageListCopyWith<_$_MessageList> get copyWith =>
      throw _privateConstructorUsedError;
}

Message _$MessageFromJson(Map<String, dynamic> json) {
  return _Message.fromJson(json);
}

/// @nodoc
mixin _$Message {
  int get chatId => throw _privateConstructorUsedError;
  String get channel => throw _privateConstructorUsedError;
  @JsonKey(fromJson: chatDataFromJson)
  List<ChatData> get chatData => throw _privateConstructorUsedError;
  String get lastChat => throw _privateConstructorUsedError;
  String get lastChatBy => throw _privateConstructorUsedError;
  String get read => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageCopyWith<Message> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageCopyWith<$Res> {
  factory $MessageCopyWith(Message value, $Res Function(Message) then) =
      _$MessageCopyWithImpl<$Res, Message>;
  @useResult
  $Res call(
      {int chatId,
      String channel,
      @JsonKey(fromJson: chatDataFromJson) List<ChatData> chatData,
      String lastChat,
      String lastChatBy,
      String read});
}

/// @nodoc
class _$MessageCopyWithImpl<$Res, $Val extends Message>
    implements $MessageCopyWith<$Res> {
  _$MessageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? channel = null,
    Object? chatData = null,
    Object? lastChat = null,
    Object? lastChatBy = null,
    Object? read = null,
  }) {
    return _then(_value.copyWith(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      channel: null == channel
          ? _value.channel
          : channel // ignore: cast_nullable_to_non_nullable
              as String,
      chatData: null == chatData
          ? _value.chatData
          : chatData // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
      lastChat: null == lastChat
          ? _value.lastChat
          : lastChat // ignore: cast_nullable_to_non_nullable
              as String,
      lastChatBy: null == lastChatBy
          ? _value.lastChatBy
          : lastChatBy // ignore: cast_nullable_to_non_nullable
              as String,
      read: null == read
          ? _value.read
          : read // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MessageCopyWith<$Res> implements $MessageCopyWith<$Res> {
  factory _$$_MessageCopyWith(
          _$_Message value, $Res Function(_$_Message) then) =
      __$$_MessageCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int chatId,
      String channel,
      @JsonKey(fromJson: chatDataFromJson) List<ChatData> chatData,
      String lastChat,
      String lastChatBy,
      String read});
}

/// @nodoc
class __$$_MessageCopyWithImpl<$Res>
    extends _$MessageCopyWithImpl<$Res, _$_Message>
    implements _$$_MessageCopyWith<$Res> {
  __$$_MessageCopyWithImpl(_$_Message _value, $Res Function(_$_Message) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? channel = null,
    Object? chatData = null,
    Object? lastChat = null,
    Object? lastChatBy = null,
    Object? read = null,
  }) {
    return _then(_$_Message(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      channel: null == channel
          ? _value.channel
          : channel // ignore: cast_nullable_to_non_nullable
              as String,
      chatData: null == chatData
          ? _value._chatData
          : chatData // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
      lastChat: null == lastChat
          ? _value.lastChat
          : lastChat // ignore: cast_nullable_to_non_nullable
              as String,
      lastChatBy: null == lastChatBy
          ? _value.lastChatBy
          : lastChatBy // ignore: cast_nullable_to_non_nullable
              as String,
      read: null == read
          ? _value.read
          : read // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Message implements _Message {
  const _$_Message(
      {required this.chatId,
      required this.channel,
      @JsonKey(fromJson: chatDataFromJson)
          required final List<ChatData> chatData,
      required this.lastChat,
      required this.lastChatBy,
      required this.read})
      : _chatData = chatData;

  factory _$_Message.fromJson(Map<String, dynamic> json) =>
      _$$_MessageFromJson(json);

  @override
  final int chatId;
  @override
  final String channel;
  final List<ChatData> _chatData;
  @override
  @JsonKey(fromJson: chatDataFromJson)
  List<ChatData> get chatData {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chatData);
  }

  @override
  final String lastChat;
  @override
  final String lastChatBy;
  @override
  final String read;

  @override
  String toString() {
    return 'Message(chatId: $chatId, channel: $channel, chatData: $chatData, lastChat: $lastChat, lastChatBy: $lastChatBy, read: $read)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Message &&
            (identical(other.chatId, chatId) || other.chatId == chatId) &&
            (identical(other.channel, channel) || other.channel == channel) &&
            const DeepCollectionEquality().equals(other._chatData, _chatData) &&
            (identical(other.lastChat, lastChat) ||
                other.lastChat == lastChat) &&
            (identical(other.lastChatBy, lastChatBy) ||
                other.lastChatBy == lastChatBy) &&
            (identical(other.read, read) || other.read == read));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      chatId,
      channel,
      const DeepCollectionEquality().hash(_chatData),
      lastChat,
      lastChatBy,
      read);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MessageCopyWith<_$_Message> get copyWith =>
      __$$_MessageCopyWithImpl<_$_Message>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MessageToJson(
      this,
    );
  }
}

abstract class _Message implements Message {
  const factory _Message(
      {required final int chatId,
      required final String channel,
      @JsonKey(fromJson: chatDataFromJson)
          required final List<ChatData> chatData,
      required final String lastChat,
      required final String lastChatBy,
      required final String read}) = _$_Message;

  factory _Message.fromJson(Map<String, dynamic> json) = _$_Message.fromJson;

  @override
  int get chatId;
  @override
  String get channel;
  @override
  @JsonKey(fromJson: chatDataFromJson)
  List<ChatData> get chatData;
  @override
  String get lastChat;
  @override
  String get lastChatBy;
  @override
  String get read;
  @override
  @JsonKey(ignore: true)
  _$$_MessageCopyWith<_$_Message> get copyWith =>
      throw _privateConstructorUsedError;
}

ChatDataList _$ChatDataListFromJson(Map<String, dynamic> json) {
  return _ChatDataList.fromJson(json);
}

/// @nodoc
mixin _$ChatDataList {
  List<ChatData> get chatDataList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatDataListCopyWith<ChatDataList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatDataListCopyWith<$Res> {
  factory $ChatDataListCopyWith(
          ChatDataList value, $Res Function(ChatDataList) then) =
      _$ChatDataListCopyWithImpl<$Res, ChatDataList>;
  @useResult
  $Res call({List<ChatData> chatDataList});
}

/// @nodoc
class _$ChatDataListCopyWithImpl<$Res, $Val extends ChatDataList>
    implements $ChatDataListCopyWith<$Res> {
  _$ChatDataListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatDataList = null,
  }) {
    return _then(_value.copyWith(
      chatDataList: null == chatDataList
          ? _value.chatDataList
          : chatDataList // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatDataListCopyWith<$Res>
    implements $ChatDataListCopyWith<$Res> {
  factory _$$_ChatDataListCopyWith(
          _$_ChatDataList value, $Res Function(_$_ChatDataList) then) =
      __$$_ChatDataListCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ChatData> chatDataList});
}

/// @nodoc
class __$$_ChatDataListCopyWithImpl<$Res>
    extends _$ChatDataListCopyWithImpl<$Res, _$_ChatDataList>
    implements _$$_ChatDataListCopyWith<$Res> {
  __$$_ChatDataListCopyWithImpl(
      _$_ChatDataList _value, $Res Function(_$_ChatDataList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatDataList = null,
  }) {
    return _then(_$_ChatDataList(
      chatDataList: null == chatDataList
          ? _value._chatDataList
          : chatDataList // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatDataList implements _ChatDataList {
  const _$_ChatDataList({required final List<ChatData> chatDataList})
      : _chatDataList = chatDataList;

  factory _$_ChatDataList.fromJson(Map<String, dynamic> json) =>
      _$$_ChatDataListFromJson(json);

  final List<ChatData> _chatDataList;
  @override
  List<ChatData> get chatDataList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chatDataList);
  }

  @override
  String toString() {
    return 'ChatDataList(chatDataList: $chatDataList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatDataList &&
            const DeepCollectionEquality()
                .equals(other._chatDataList, _chatDataList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_chatDataList));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatDataListCopyWith<_$_ChatDataList> get copyWith =>
      __$$_ChatDataListCopyWithImpl<_$_ChatDataList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatDataListToJson(
      this,
    );
  }
}

abstract class _ChatDataList implements ChatDataList {
  const factory _ChatDataList({required final List<ChatData> chatDataList}) =
      _$_ChatDataList;

  factory _ChatDataList.fromJson(Map<String, dynamic> json) =
      _$_ChatDataList.fromJson;

  @override
  List<ChatData> get chatDataList;
  @override
  @JsonKey(ignore: true)
  _$$_ChatDataListCopyWith<_$_ChatDataList> get copyWith =>
      throw _privateConstructorUsedError;
}

ChatData _$ChatDataFromJson(Map<String, dynamic> json) {
  return _ChatData.fromJson(json);
}

/// @nodoc
mixin _$ChatData {
  String get uid => throw _privateConstructorUsedError;
  String get text => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get msgType => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatDataCopyWith<ChatData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatDataCopyWith<$Res> {
  factory $ChatDataCopyWith(ChatData value, $Res Function(ChatData) then) =
      _$ChatDataCopyWithImpl<$Res, ChatData>;
  @useResult
  $Res call({String uid, String text, String time, String msgType});
}

/// @nodoc
class _$ChatDataCopyWithImpl<$Res, $Val extends ChatData>
    implements $ChatDataCopyWith<$Res> {
  _$ChatDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? text = null,
    Object? time = null,
    Object? msgType = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      msgType: null == msgType
          ? _value.msgType
          : msgType // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatDataCopyWith<$Res> implements $ChatDataCopyWith<$Res> {
  factory _$$_ChatDataCopyWith(
          _$_ChatData value, $Res Function(_$_ChatData) then) =
      __$$_ChatDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String uid, String text, String time, String msgType});
}

/// @nodoc
class __$$_ChatDataCopyWithImpl<$Res>
    extends _$ChatDataCopyWithImpl<$Res, _$_ChatData>
    implements _$$_ChatDataCopyWith<$Res> {
  __$$_ChatDataCopyWithImpl(
      _$_ChatData _value, $Res Function(_$_ChatData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? text = null,
    Object? time = null,
    Object? msgType = null,
  }) {
    return _then(_$_ChatData(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      msgType: null == msgType
          ? _value.msgType
          : msgType // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatData implements _ChatData {
  const _$_ChatData(
      {required this.uid,
      required this.text,
      required this.time,
      required this.msgType});

  factory _$_ChatData.fromJson(Map<String, dynamic> json) =>
      _$$_ChatDataFromJson(json);

  @override
  final String uid;
  @override
  final String text;
  @override
  final String time;
  @override
  final String msgType;

  @override
  String toString() {
    return 'ChatData(uid: $uid, text: $text, time: $time, msgType: $msgType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatData &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.text, text) || other.text == text) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.msgType, msgType) || other.msgType == msgType));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, uid, text, time, msgType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatDataCopyWith<_$_ChatData> get copyWith =>
      __$$_ChatDataCopyWithImpl<_$_ChatData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatDataToJson(
      this,
    );
  }
}

abstract class _ChatData implements ChatData {
  const factory _ChatData(
      {required final String uid,
      required final String text,
      required final String time,
      required final String msgType}) = _$_ChatData;

  factory _ChatData.fromJson(Map<String, dynamic> json) = _$_ChatData.fromJson;

  @override
  String get uid;
  @override
  String get text;
  @override
  String get time;
  @override
  String get msgType;
  @override
  @JsonKey(ignore: true)
  _$$_ChatDataCopyWith<_$_ChatData> get copyWith =>
      throw _privateConstructorUsedError;
}
