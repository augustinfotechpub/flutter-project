// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ChatEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String channelId) getMessage,
    required TResult Function(List<ChatData> chatData) refresh,
    required TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)
        postMessage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String channelId)? getMessage,
    TResult? Function(List<ChatData> chatData)? refresh,
    TResult? Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String channelId)? getMessage,
    TResult Function(List<ChatData> chatData)? refresh,
    TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMessage value) getMessage,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_PostMessage value) postMessage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMessage value)? getMessage,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_PostMessage value)? postMessage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMessage value)? getMessage,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_PostMessage value)? postMessage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatEventCopyWith<$Res> {
  factory $ChatEventCopyWith(ChatEvent value, $Res Function(ChatEvent) then) =
      _$ChatEventCopyWithImpl<$Res, ChatEvent>;
}

/// @nodoc
class _$ChatEventCopyWithImpl<$Res, $Val extends ChatEvent>
    implements $ChatEventCopyWith<$Res> {
  _$ChatEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetMessageCopyWith<$Res> {
  factory _$$_GetMessageCopyWith(
          _$_GetMessage value, $Res Function(_$_GetMessage) then) =
      __$$_GetMessageCopyWithImpl<$Res>;
  @useResult
  $Res call({String channelId});
}

/// @nodoc
class __$$_GetMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res, _$_GetMessage>
    implements _$$_GetMessageCopyWith<$Res> {
  __$$_GetMessageCopyWithImpl(
      _$_GetMessage _value, $Res Function(_$_GetMessage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? channelId = null,
  }) {
    return _then(_$_GetMessage(
      null == channelId
          ? _value.channelId
          : channelId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_GetMessage implements _GetMessage {
  const _$_GetMessage(this.channelId);

  @override
  final String channelId;

  @override
  String toString() {
    return 'ChatEvent.getMessage(channelId: $channelId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetMessage &&
            (identical(other.channelId, channelId) ||
                other.channelId == channelId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, channelId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetMessageCopyWith<_$_GetMessage> get copyWith =>
      __$$_GetMessageCopyWithImpl<_$_GetMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String channelId) getMessage,
    required TResult Function(List<ChatData> chatData) refresh,
    required TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)
        postMessage,
  }) {
    return getMessage(channelId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String channelId)? getMessage,
    TResult? Function(List<ChatData> chatData)? refresh,
    TResult? Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
  }) {
    return getMessage?.call(channelId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String channelId)? getMessage,
    TResult Function(List<ChatData> chatData)? refresh,
    TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
    required TResult orElse(),
  }) {
    if (getMessage != null) {
      return getMessage(channelId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMessage value) getMessage,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_PostMessage value) postMessage,
  }) {
    return getMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMessage value)? getMessage,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_PostMessage value)? postMessage,
  }) {
    return getMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMessage value)? getMessage,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_PostMessage value)? postMessage,
    required TResult orElse(),
  }) {
    if (getMessage != null) {
      return getMessage(this);
    }
    return orElse();
  }
}

abstract class _GetMessage implements ChatEvent {
  const factory _GetMessage(final String channelId) = _$_GetMessage;

  String get channelId;
  @JsonKey(ignore: true)
  _$$_GetMessageCopyWith<_$_GetMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshCopyWith<$Res> {
  factory _$$_RefreshCopyWith(
          _$_Refresh value, $Res Function(_$_Refresh) then) =
      __$$_RefreshCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ChatData> chatData});
}

/// @nodoc
class __$$_RefreshCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res, _$_Refresh>
    implements _$$_RefreshCopyWith<$Res> {
  __$$_RefreshCopyWithImpl(_$_Refresh _value, $Res Function(_$_Refresh) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatData = null,
  }) {
    return _then(_$_Refresh(
      null == chatData
          ? _value._chatData
          : chatData // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
    ));
  }
}

/// @nodoc

class _$_Refresh implements _Refresh {
  const _$_Refresh(final List<ChatData> chatData) : _chatData = chatData;

  final List<ChatData> _chatData;
  @override
  List<ChatData> get chatData {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chatData);
  }

  @override
  String toString() {
    return 'ChatEvent.refresh(chatData: $chatData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Refresh &&
            const DeepCollectionEquality().equals(other._chatData, _chatData));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_chatData));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshCopyWith<_$_Refresh> get copyWith =>
      __$$_RefreshCopyWithImpl<_$_Refresh>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String channelId) getMessage,
    required TResult Function(List<ChatData> chatData) refresh,
    required TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)
        postMessage,
  }) {
    return refresh(chatData);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String channelId)? getMessage,
    TResult? Function(List<ChatData> chatData)? refresh,
    TResult? Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
  }) {
    return refresh?.call(chatData);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String channelId)? getMessage,
    TResult Function(List<ChatData> chatData)? refresh,
    TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh(chatData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMessage value) getMessage,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_PostMessage value) postMessage,
  }) {
    return refresh(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMessage value)? getMessage,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_PostMessage value)? postMessage,
  }) {
    return refresh?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMessage value)? getMessage,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_PostMessage value)? postMessage,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh(this);
    }
    return orElse();
  }
}

abstract class _Refresh implements ChatEvent {
  const factory _Refresh(final List<ChatData> chatData) = _$_Refresh;

  List<ChatData> get chatData;
  @JsonKey(ignore: true)
  _$$_RefreshCopyWith<_$_Refresh> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_PostMessageCopyWith<$Res> {
  factory _$$_PostMessageCopyWith(
          _$_PostMessage value, $Res Function(_$_PostMessage) then) =
      __$$_PostMessageCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String channelId,
      String peerId,
      String peerName,
      String peerImage,
      String message,
      List<Map<String, dynamic>> chatData});
}

/// @nodoc
class __$$_PostMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res, _$_PostMessage>
    implements _$$_PostMessageCopyWith<$Res> {
  __$$_PostMessageCopyWithImpl(
      _$_PostMessage _value, $Res Function(_$_PostMessage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? channelId = null,
    Object? peerId = null,
    Object? peerName = null,
    Object? peerImage = null,
    Object? message = null,
    Object? chatData = null,
  }) {
    return _then(_$_PostMessage(
      channelId: null == channelId
          ? _value.channelId
          : channelId // ignore: cast_nullable_to_non_nullable
              as String,
      peerId: null == peerId
          ? _value.peerId
          : peerId // ignore: cast_nullable_to_non_nullable
              as String,
      peerName: null == peerName
          ? _value.peerName
          : peerName // ignore: cast_nullable_to_non_nullable
              as String,
      peerImage: null == peerImage
          ? _value.peerImage
          : peerImage // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      chatData: null == chatData
          ? _value._chatData
          : chatData // ignore: cast_nullable_to_non_nullable
              as List<Map<String, dynamic>>,
    ));
  }
}

/// @nodoc

class _$_PostMessage implements _PostMessage {
  const _$_PostMessage(
      {required this.channelId,
      required this.peerId,
      required this.peerName,
      required this.peerImage,
      required this.message,
      required final List<Map<String, dynamic>> chatData})
      : _chatData = chatData;

  @override
  final String channelId;
  @override
  final String peerId;
  @override
  final String peerName;
  @override
  final String peerImage;
  @override
  final String message;
  final List<Map<String, dynamic>> _chatData;
  @override
  List<Map<String, dynamic>> get chatData {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chatData);
  }

  @override
  String toString() {
    return 'ChatEvent.postMessage(channelId: $channelId, peerId: $peerId, peerName: $peerName, peerImage: $peerImage, message: $message, chatData: $chatData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PostMessage &&
            (identical(other.channelId, channelId) ||
                other.channelId == channelId) &&
            (identical(other.peerId, peerId) || other.peerId == peerId) &&
            (identical(other.peerName, peerName) ||
                other.peerName == peerName) &&
            (identical(other.peerImage, peerImage) ||
                other.peerImage == peerImage) &&
            (identical(other.message, message) || other.message == message) &&
            const DeepCollectionEquality().equals(other._chatData, _chatData));
  }

  @override
  int get hashCode => Object.hash(runtimeType, channelId, peerId, peerName,
      peerImage, message, const DeepCollectionEquality().hash(_chatData));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PostMessageCopyWith<_$_PostMessage> get copyWith =>
      __$$_PostMessageCopyWithImpl<_$_PostMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String channelId) getMessage,
    required TResult Function(List<ChatData> chatData) refresh,
    required TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)
        postMessage,
  }) {
    return postMessage(
        channelId, peerId, peerName, peerImage, message, chatData);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String channelId)? getMessage,
    TResult? Function(List<ChatData> chatData)? refresh,
    TResult? Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
  }) {
    return postMessage?.call(
        channelId, peerId, peerName, peerImage, message, chatData);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String channelId)? getMessage,
    TResult Function(List<ChatData> chatData)? refresh,
    TResult Function(
            String channelId,
            String peerId,
            String peerName,
            String peerImage,
            String message,
            List<Map<String, dynamic>> chatData)?
        postMessage,
    required TResult orElse(),
  }) {
    if (postMessage != null) {
      return postMessage(
          channelId, peerId, peerName, peerImage, message, chatData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMessage value) getMessage,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_PostMessage value) postMessage,
  }) {
    return postMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMessage value)? getMessage,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_PostMessage value)? postMessage,
  }) {
    return postMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMessage value)? getMessage,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_PostMessage value)? postMessage,
    required TResult orElse(),
  }) {
    if (postMessage != null) {
      return postMessage(this);
    }
    return orElse();
  }
}

abstract class _PostMessage implements ChatEvent {
  const factory _PostMessage(
      {required final String channelId,
      required final String peerId,
      required final String peerName,
      required final String peerImage,
      required final String message,
      required final List<Map<String, dynamic>> chatData}) = _$_PostMessage;

  String get channelId;
  String get peerId;
  String get peerName;
  String get peerImage;
  String get message;
  List<Map<String, dynamic>> get chatData;
  @JsonKey(ignore: true)
  _$$_PostMessageCopyWith<_$_PostMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ChatState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ChatData> messages) success,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ChatData> messages)? success,
    TResult? Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ChatData> messages)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatStateCopyWith<$Res> {
  factory $ChatStateCopyWith(ChatState value, $Res Function(ChatState) then) =
      _$ChatStateCopyWithImpl<$Res, ChatState>;
}

/// @nodoc
class _$ChatStateCopyWithImpl<$Res, $Val extends ChatState>
    implements $ChatStateCopyWith<$Res> {
  _$ChatStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ChatStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ChatState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ChatData> messages) success,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ChatData> messages)? success,
    TResult? Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ChatData> messages)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ChatState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$ChatStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'ChatState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ChatData> messages) success,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ChatData> messages)? success,
    TResult? Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ChatData> messages)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements ChatState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ChatData> messages});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$ChatStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? messages = null,
  }) {
    return _then(_$_Success(
      null == messages
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<ChatData>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<ChatData> messages) : _messages = messages;

  final List<ChatData> _messages;
  @override
  List<ChatData> get messages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_messages);
  }

  @override
  String toString() {
    return 'ChatState.success(messages: $messages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._messages, _messages));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_messages));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ChatData> messages) success,
    required TResult Function(String error) error,
  }) {
    return success(messages);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ChatData> messages)? success,
    TResult? Function(String error)? error,
  }) {
    return success?.call(messages);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ChatData> messages)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(messages);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements ChatState {
  const factory _Success(final List<ChatData> messages) = _$_Success;

  List<ChatData> get messages;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$ChatStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'ChatState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ChatData> messages) success,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ChatData> messages)? success,
    TResult? Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ChatData> messages)? success,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements ChatState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
