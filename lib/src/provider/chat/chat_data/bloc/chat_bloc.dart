import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/chat/chat_data/model.dart';

part 'chat_event.dart';
part 'chat_state.dart';
part 'chat_bloc.freezed.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(const _Initial()) {
    on<ChatEvent>((event, emit) async {
      if (event is _GetMessage) {
        emit(const _Loading());
        try {
          final response = await apiProvider.getMessages(event.channelId);
          if (response.statusCode == 200) {
            if (jsonDecode(response.body)['results'].isEmpty) {
              emit(const _Success([]));
            } else {
              MessageList message =
                  MessageList.fromJson(jsonDecode(response.body));
              emit(_Success(message.results.first.chatData));
            }
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _PostMessage) {
        emit(const _Loading());
        try {
          await apiProvider.sendMessages(
            channelId: event.channelId,
            chatData: event.chatData,
            message: event.message,
            peerId: event.peerId,
            peerImage: event.peerImage,
            peerName: event.peerName,
          );
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _Refresh) {
        emit(_Success(event.chatData));
      }
    });
  }
}

ChatBloc chatBloc = ChatBloc();
