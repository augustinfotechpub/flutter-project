part of 'chat_bloc.dart';

@freezed
class ChatEvent with _$ChatEvent {
  const factory ChatEvent.getMessage(String channelId) = _GetMessage;
  const factory ChatEvent.refresh(List<ChatData> chatData) = _Refresh;
  const factory ChatEvent.postMessage({
    required String channelId,
    required String peerId,
    required String peerName,
    required String peerImage,
    required String message,
    required List<Map<String, dynamic>> chatData,
  }) = _PostMessage;
}
