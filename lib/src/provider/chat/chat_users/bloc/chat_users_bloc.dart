import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/chat/chat_users/model.dart';

part 'chat_users_event.dart';
part 'chat_users_state.dart';
part 'chat_users_bloc.freezed.dart';

class ChatUsersBloc extends Bloc<ChatUsersEvent, ChatUsersState> {
  ChatUsersBloc() : super(const _Initial()) {
    on<ChatUsersEvent>((event, emit) async {
      if (event is _GetChatUsers) {
        emit(const _Loading());
        try {
          final response = await apiProvider.getChatUsers();
          if (response.statusCode == 200) {
            ChatUserList chatUserList = ChatUserList.fromJson(
                {'payload': jsonDecode(response.body)['payload']});
            emit(_Success(chatUserList.payload));
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
