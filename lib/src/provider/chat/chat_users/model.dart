// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class ChatUserList with _$ChatUserList {
  const factory ChatUserList({
    required List<Chatuser> payload,
  }) = _ChatUserList;

  factory ChatUserList.fromJson(Map<String, dynamic> json) =>
      _$ChatUserListFromJson(json);
}

@freezed
class Chatuser with _$Chatuser {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Chatuser({
    int? adminId,
    String? fullname,
    required String firstname,
    required String lastname,
    required String email,
    required String username,
    required String password,
    required String avatarImage,
    required String role,
    required String contactNo,
    required String address,
    required DateTime dateOfBirth,
    bool? allowScheduling,
    required bool isActive,
    int? facilityId,
    required int userId,
    int? clinicianId,
    List<String>? jobSites,
    String? clinicianPosition,
    bool? applyShift,
  }) = _Chatuser;

  factory Chatuser.fromJson(Map<String, dynamic> json) =>
      _$ChatuserFromJson(json);
}
