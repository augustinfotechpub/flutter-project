// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatUserList _$ChatUserListFromJson(Map<String, dynamic> json) {
  return _ChatUserList.fromJson(json);
}

/// @nodoc
mixin _$ChatUserList {
  List<Chatuser> get payload => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatUserListCopyWith<ChatUserList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatUserListCopyWith<$Res> {
  factory $ChatUserListCopyWith(
          ChatUserList value, $Res Function(ChatUserList) then) =
      _$ChatUserListCopyWithImpl<$Res, ChatUserList>;
  @useResult
  $Res call({List<Chatuser> payload});
}

/// @nodoc
class _$ChatUserListCopyWithImpl<$Res, $Val extends ChatUserList>
    implements $ChatUserListCopyWith<$Res> {
  _$ChatUserListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? payload = null,
  }) {
    return _then(_value.copyWith(
      payload: null == payload
          ? _value.payload
          : payload // ignore: cast_nullable_to_non_nullable
              as List<Chatuser>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatUserListCopyWith<$Res>
    implements $ChatUserListCopyWith<$Res> {
  factory _$$_ChatUserListCopyWith(
          _$_ChatUserList value, $Res Function(_$_ChatUserList) then) =
      __$$_ChatUserListCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Chatuser> payload});
}

/// @nodoc
class __$$_ChatUserListCopyWithImpl<$Res>
    extends _$ChatUserListCopyWithImpl<$Res, _$_ChatUserList>
    implements _$$_ChatUserListCopyWith<$Res> {
  __$$_ChatUserListCopyWithImpl(
      _$_ChatUserList _value, $Res Function(_$_ChatUserList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? payload = null,
  }) {
    return _then(_$_ChatUserList(
      payload: null == payload
          ? _value._payload
          : payload // ignore: cast_nullable_to_non_nullable
              as List<Chatuser>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatUserList implements _ChatUserList {
  const _$_ChatUserList({required final List<Chatuser> payload})
      : _payload = payload;

  factory _$_ChatUserList.fromJson(Map<String, dynamic> json) =>
      _$$_ChatUserListFromJson(json);

  final List<Chatuser> _payload;
  @override
  List<Chatuser> get payload {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_payload);
  }

  @override
  String toString() {
    return 'ChatUserList(payload: $payload)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatUserList &&
            const DeepCollectionEquality().equals(other._payload, _payload));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_payload));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatUserListCopyWith<_$_ChatUserList> get copyWith =>
      __$$_ChatUserListCopyWithImpl<_$_ChatUserList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatUserListToJson(
      this,
    );
  }
}

abstract class _ChatUserList implements ChatUserList {
  const factory _ChatUserList({required final List<Chatuser> payload}) =
      _$_ChatUserList;

  factory _ChatUserList.fromJson(Map<String, dynamic> json) =
      _$_ChatUserList.fromJson;

  @override
  List<Chatuser> get payload;
  @override
  @JsonKey(ignore: true)
  _$$_ChatUserListCopyWith<_$_ChatUserList> get copyWith =>
      throw _privateConstructorUsedError;
}

Chatuser _$ChatuserFromJson(Map<String, dynamic> json) {
  return _Chatuser.fromJson(json);
}

/// @nodoc
mixin _$Chatuser {
  int? get adminId => throw _privateConstructorUsedError;
  String? get fullname => throw _privateConstructorUsedError;
  String get firstname => throw _privateConstructorUsedError;
  String get lastname => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  String get avatarImage => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;
  String get contactNo => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  DateTime get dateOfBirth => throw _privateConstructorUsedError;
  bool? get allowScheduling => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  int? get facilityId => throw _privateConstructorUsedError;
  int get userId => throw _privateConstructorUsedError;
  int? get clinicianId => throw _privateConstructorUsedError;
  List<String>? get jobSites => throw _privateConstructorUsedError;
  String? get clinicianPosition => throw _privateConstructorUsedError;
  bool? get applyShift => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatuserCopyWith<Chatuser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatuserCopyWith<$Res> {
  factory $ChatuserCopyWith(Chatuser value, $Res Function(Chatuser) then) =
      _$ChatuserCopyWithImpl<$Res, Chatuser>;
  @useResult
  $Res call(
      {int? adminId,
      String? fullname,
      String firstname,
      String lastname,
      String email,
      String username,
      String password,
      String avatarImage,
      String role,
      String contactNo,
      String address,
      DateTime dateOfBirth,
      bool? allowScheduling,
      bool isActive,
      int? facilityId,
      int userId,
      int? clinicianId,
      List<String>? jobSites,
      String? clinicianPosition,
      bool? applyShift});
}

/// @nodoc
class _$ChatuserCopyWithImpl<$Res, $Val extends Chatuser>
    implements $ChatuserCopyWith<$Res> {
  _$ChatuserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adminId = freezed,
    Object? fullname = freezed,
    Object? firstname = null,
    Object? lastname = null,
    Object? email = null,
    Object? username = null,
    Object? password = null,
    Object? avatarImage = null,
    Object? role = null,
    Object? contactNo = null,
    Object? address = null,
    Object? dateOfBirth = null,
    Object? allowScheduling = freezed,
    Object? isActive = null,
    Object? facilityId = freezed,
    Object? userId = null,
    Object? clinicianId = freezed,
    Object? jobSites = freezed,
    Object? clinicianPosition = freezed,
    Object? applyShift = freezed,
  }) {
    return _then(_value.copyWith(
      adminId: freezed == adminId
          ? _value.adminId
          : adminId // ignore: cast_nullable_to_non_nullable
              as int?,
      fullname: freezed == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String?,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as DateTime,
      allowScheduling: freezed == allowScheduling
          ? _value.allowScheduling
          : allowScheduling // ignore: cast_nullable_to_non_nullable
              as bool?,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      facilityId: freezed == facilityId
          ? _value.facilityId
          : facilityId // ignore: cast_nullable_to_non_nullable
              as int?,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianId: freezed == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int?,
      jobSites: freezed == jobSites
          ? _value.jobSites
          : jobSites // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      clinicianPosition: freezed == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String?,
      applyShift: freezed == applyShift
          ? _value.applyShift
          : applyShift // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatuserCopyWith<$Res> implements $ChatuserCopyWith<$Res> {
  factory _$$_ChatuserCopyWith(
          _$_Chatuser value, $Res Function(_$_Chatuser) then) =
      __$$_ChatuserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? adminId,
      String? fullname,
      String firstname,
      String lastname,
      String email,
      String username,
      String password,
      String avatarImage,
      String role,
      String contactNo,
      String address,
      DateTime dateOfBirth,
      bool? allowScheduling,
      bool isActive,
      int? facilityId,
      int userId,
      int? clinicianId,
      List<String>? jobSites,
      String? clinicianPosition,
      bool? applyShift});
}

/// @nodoc
class __$$_ChatuserCopyWithImpl<$Res>
    extends _$ChatuserCopyWithImpl<$Res, _$_Chatuser>
    implements _$$_ChatuserCopyWith<$Res> {
  __$$_ChatuserCopyWithImpl(
      _$_Chatuser _value, $Res Function(_$_Chatuser) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adminId = freezed,
    Object? fullname = freezed,
    Object? firstname = null,
    Object? lastname = null,
    Object? email = null,
    Object? username = null,
    Object? password = null,
    Object? avatarImage = null,
    Object? role = null,
    Object? contactNo = null,
    Object? address = null,
    Object? dateOfBirth = null,
    Object? allowScheduling = freezed,
    Object? isActive = null,
    Object? facilityId = freezed,
    Object? userId = null,
    Object? clinicianId = freezed,
    Object? jobSites = freezed,
    Object? clinicianPosition = freezed,
    Object? applyShift = freezed,
  }) {
    return _then(_$_Chatuser(
      adminId: freezed == adminId
          ? _value.adminId
          : adminId // ignore: cast_nullable_to_non_nullable
              as int?,
      fullname: freezed == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as String?,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as DateTime,
      allowScheduling: freezed == allowScheduling
          ? _value.allowScheduling
          : allowScheduling // ignore: cast_nullable_to_non_nullable
              as bool?,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      facilityId: freezed == facilityId
          ? _value.facilityId
          : facilityId // ignore: cast_nullable_to_non_nullable
              as int?,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianId: freezed == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int?,
      jobSites: freezed == jobSites
          ? _value._jobSites
          : jobSites // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      clinicianPosition: freezed == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String?,
      applyShift: freezed == applyShift
          ? _value.applyShift
          : applyShift // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Chatuser implements _Chatuser {
  const _$_Chatuser(
      {this.adminId,
      this.fullname,
      required this.firstname,
      required this.lastname,
      required this.email,
      required this.username,
      required this.password,
      required this.avatarImage,
      required this.role,
      required this.contactNo,
      required this.address,
      required this.dateOfBirth,
      this.allowScheduling,
      required this.isActive,
      this.facilityId,
      required this.userId,
      this.clinicianId,
      final List<String>? jobSites,
      this.clinicianPosition,
      this.applyShift})
      : _jobSites = jobSites;

  factory _$_Chatuser.fromJson(Map<String, dynamic> json) =>
      _$$_ChatuserFromJson(json);

  @override
  final int? adminId;
  @override
  final String? fullname;
  @override
  final String firstname;
  @override
  final String lastname;
  @override
  final String email;
  @override
  final String username;
  @override
  final String password;
  @override
  final String avatarImage;
  @override
  final String role;
  @override
  final String contactNo;
  @override
  final String address;
  @override
  final DateTime dateOfBirth;
  @override
  final bool? allowScheduling;
  @override
  final bool isActive;
  @override
  final int? facilityId;
  @override
  final int userId;
  @override
  final int? clinicianId;
  final List<String>? _jobSites;
  @override
  List<String>? get jobSites {
    final value = _jobSites;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final String? clinicianPosition;
  @override
  final bool? applyShift;

  @override
  String toString() {
    return 'Chatuser(adminId: $adminId, fullname: $fullname, firstname: $firstname, lastname: $lastname, email: $email, username: $username, password: $password, avatarImage: $avatarImage, role: $role, contactNo: $contactNo, address: $address, dateOfBirth: $dateOfBirth, allowScheduling: $allowScheduling, isActive: $isActive, facilityId: $facilityId, userId: $userId, clinicianId: $clinicianId, jobSites: $jobSites, clinicianPosition: $clinicianPosition, applyShift: $applyShift)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Chatuser &&
            (identical(other.adminId, adminId) || other.adminId == adminId) &&
            (identical(other.fullname, fullname) ||
                other.fullname == fullname) &&
            (identical(other.firstname, firstname) ||
                other.firstname == firstname) &&
            (identical(other.lastname, lastname) ||
                other.lastname == lastname) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.avatarImage, avatarImage) ||
                other.avatarImage == avatarImage) &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.contactNo, contactNo) ||
                other.contactNo == contactNo) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            (identical(other.allowScheduling, allowScheduling) ||
                other.allowScheduling == allowScheduling) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.facilityId, facilityId) ||
                other.facilityId == facilityId) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId) &&
            const DeepCollectionEquality().equals(other._jobSites, _jobSites) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.applyShift, applyShift) ||
                other.applyShift == applyShift));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        adminId,
        fullname,
        firstname,
        lastname,
        email,
        username,
        password,
        avatarImage,
        role,
        contactNo,
        address,
        dateOfBirth,
        allowScheduling,
        isActive,
        facilityId,
        userId,
        clinicianId,
        const DeepCollectionEquality().hash(_jobSites),
        clinicianPosition,
        applyShift
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatuserCopyWith<_$_Chatuser> get copyWith =>
      __$$_ChatuserCopyWithImpl<_$_Chatuser>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatuserToJson(
      this,
    );
  }
}

abstract class _Chatuser implements Chatuser {
  const factory _Chatuser(
      {final int? adminId,
      final String? fullname,
      required final String firstname,
      required final String lastname,
      required final String email,
      required final String username,
      required final String password,
      required final String avatarImage,
      required final String role,
      required final String contactNo,
      required final String address,
      required final DateTime dateOfBirth,
      final bool? allowScheduling,
      required final bool isActive,
      final int? facilityId,
      required final int userId,
      final int? clinicianId,
      final List<String>? jobSites,
      final String? clinicianPosition,
      final bool? applyShift}) = _$_Chatuser;

  factory _Chatuser.fromJson(Map<String, dynamic> json) = _$_Chatuser.fromJson;

  @override
  int? get adminId;
  @override
  String? get fullname;
  @override
  String get firstname;
  @override
  String get lastname;
  @override
  String get email;
  @override
  String get username;
  @override
  String get password;
  @override
  String get avatarImage;
  @override
  String get role;
  @override
  String get contactNo;
  @override
  String get address;
  @override
  DateTime get dateOfBirth;
  @override
  bool? get allowScheduling;
  @override
  bool get isActive;
  @override
  int? get facilityId;
  @override
  int get userId;
  @override
  int? get clinicianId;
  @override
  List<String>? get jobSites;
  @override
  String? get clinicianPosition;
  @override
  bool? get applyShift;
  @override
  @JsonKey(ignore: true)
  _$$_ChatuserCopyWith<_$_Chatuser> get copyWith =>
      throw _privateConstructorUsedError;
}
