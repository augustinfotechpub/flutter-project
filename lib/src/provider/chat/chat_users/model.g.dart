// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatUserList _$$_ChatUserListFromJson(Map<String, dynamic> json) =>
    _$_ChatUserList(
      payload: (json['payload'] as List<dynamic>)
          .map((e) => Chatuser.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ChatUserListToJson(_$_ChatUserList instance) =>
    <String, dynamic>{
      'payload': instance.payload,
    };

_$_Chatuser _$$_ChatuserFromJson(Map<String, dynamic> json) => _$_Chatuser(
      adminId: json['admin_id'] as int?,
      fullname: json['fullname'] as String?,
      firstname: json['firstname'] as String,
      lastname: json['lastname'] as String,
      email: json['email'] as String,
      username: json['username'] as String,
      password: json['password'] as String,
      avatarImage: json['avatar_image'] as String,
      role: json['role'] as String,
      contactNo: json['contact_no'] as String,
      address: json['address'] as String,
      dateOfBirth: DateTime.parse(json['date_of_birth'] as String),
      allowScheduling: json['allow_scheduling'] as bool?,
      isActive: json['is_active'] as bool,
      facilityId: json['facility_id'] as int?,
      userId: json['user_id'] as int,
      clinicianId: json['clinician_id'] as int?,
      jobSites: (json['job_sites'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      clinicianPosition: json['clinician_position'] as String?,
      applyShift: json['apply_shift'] as bool?,
    );

Map<String, dynamic> _$$_ChatuserToJson(_$_Chatuser instance) =>
    <String, dynamic>{
      'admin_id': instance.adminId,
      'fullname': instance.fullname,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'username': instance.username,
      'password': instance.password,
      'avatar_image': instance.avatarImage,
      'role': instance.role,
      'contact_no': instance.contactNo,
      'address': instance.address,
      'date_of_birth': instance.dateOfBirth.toIso8601String(),
      'allow_scheduling': instance.allowScheduling,
      'is_active': instance.isActive,
      'facility_id': instance.facilityId,
      'user_id': instance.userId,
      'clinician_id': instance.clinicianId,
      'job_sites': instance.jobSites,
      'clinician_position': instance.clinicianPosition,
      'apply_shift': instance.applyShift,
    };
