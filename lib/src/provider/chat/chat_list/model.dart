// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class ChatList with _$ChatList {
  const factory ChatList({
    required List<LastMessage> payload,
  }) = _ChatList;

  factory ChatList.fromJson(Map<String, dynamic> json) =>
      _$ChatListFromJson(json);
}

@freezed
class LastMessage with _$LastMessage {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory LastMessage({
    required int chatHisoryId,
    required String channel,
    required String userId,
    required String peerId,
    required String userFullname,
    required String peerFullname,
    required String userAvatarImage,
    required String peerAvatarImage,
    required String lastMessage,
    required String messageBy,
    required String messageTo,
    required String messageTime,
    required String displayed,
  }) = _LastMessage;

  factory LastMessage.fromJson(Map<String, dynamic> json) =>
      _$LastMessageFromJson(json);
}
