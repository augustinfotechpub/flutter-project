// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatList _$$_ChatListFromJson(Map<String, dynamic> json) => _$_ChatList(
      payload: (json['payload'] as List<dynamic>)
          .map((e) => LastMessage.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ChatListToJson(_$_ChatList instance) =>
    <String, dynamic>{
      'payload': instance.payload,
    };

_$_LastMessage _$$_LastMessageFromJson(Map<String, dynamic> json) =>
    _$_LastMessage(
      chatHisoryId: json['chat_hisory_id'] as int,
      channel: json['channel'] as String,
      userId: json['user_id'] as String,
      peerId: json['peer_id'] as String,
      userFullname: json['user_fullname'] as String,
      peerFullname: json['peer_fullname'] as String,
      userAvatarImage: json['user_avatar_image'] as String,
      peerAvatarImage: json['peer_avatar_image'] as String,
      lastMessage: json['last_message'] as String,
      messageBy: json['message_by'] as String,
      messageTo: json['message_to'] as String,
      messageTime: json['message_time'] as String,
      displayed: json['displayed'] as String,
    );

Map<String, dynamic> _$$_LastMessageToJson(_$_LastMessage instance) =>
    <String, dynamic>{
      'chat_hisory_id': instance.chatHisoryId,
      'channel': instance.channel,
      'user_id': instance.userId,
      'peer_id': instance.peerId,
      'user_fullname': instance.userFullname,
      'peer_fullname': instance.peerFullname,
      'user_avatar_image': instance.userAvatarImage,
      'peer_avatar_image': instance.peerAvatarImage,
      'last_message': instance.lastMessage,
      'message_by': instance.messageBy,
      'message_to': instance.messageTo,
      'message_time': instance.messageTime,
      'displayed': instance.displayed,
    };
