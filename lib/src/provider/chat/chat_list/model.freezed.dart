// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatList _$ChatListFromJson(Map<String, dynamic> json) {
  return _ChatList.fromJson(json);
}

/// @nodoc
mixin _$ChatList {
  List<LastMessage> get payload => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatListCopyWith<ChatList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatListCopyWith<$Res> {
  factory $ChatListCopyWith(ChatList value, $Res Function(ChatList) then) =
      _$ChatListCopyWithImpl<$Res, ChatList>;
  @useResult
  $Res call({List<LastMessage> payload});
}

/// @nodoc
class _$ChatListCopyWithImpl<$Res, $Val extends ChatList>
    implements $ChatListCopyWith<$Res> {
  _$ChatListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? payload = null,
  }) {
    return _then(_value.copyWith(
      payload: null == payload
          ? _value.payload
          : payload // ignore: cast_nullable_to_non_nullable
              as List<LastMessage>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatListCopyWith<$Res> implements $ChatListCopyWith<$Res> {
  factory _$$_ChatListCopyWith(
          _$_ChatList value, $Res Function(_$_ChatList) then) =
      __$$_ChatListCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<LastMessage> payload});
}

/// @nodoc
class __$$_ChatListCopyWithImpl<$Res>
    extends _$ChatListCopyWithImpl<$Res, _$_ChatList>
    implements _$$_ChatListCopyWith<$Res> {
  __$$_ChatListCopyWithImpl(
      _$_ChatList _value, $Res Function(_$_ChatList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? payload = null,
  }) {
    return _then(_$_ChatList(
      payload: null == payload
          ? _value._payload
          : payload // ignore: cast_nullable_to_non_nullable
              as List<LastMessage>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatList implements _ChatList {
  const _$_ChatList({required final List<LastMessage> payload})
      : _payload = payload;

  factory _$_ChatList.fromJson(Map<String, dynamic> json) =>
      _$$_ChatListFromJson(json);

  final List<LastMessage> _payload;
  @override
  List<LastMessage> get payload {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_payload);
  }

  @override
  String toString() {
    return 'ChatList(payload: $payload)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatList &&
            const DeepCollectionEquality().equals(other._payload, _payload));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_payload));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatListCopyWith<_$_ChatList> get copyWith =>
      __$$_ChatListCopyWithImpl<_$_ChatList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatListToJson(
      this,
    );
  }
}

abstract class _ChatList implements ChatList {
  const factory _ChatList({required final List<LastMessage> payload}) =
      _$_ChatList;

  factory _ChatList.fromJson(Map<String, dynamic> json) = _$_ChatList.fromJson;

  @override
  List<LastMessage> get payload;
  @override
  @JsonKey(ignore: true)
  _$$_ChatListCopyWith<_$_ChatList> get copyWith =>
      throw _privateConstructorUsedError;
}

LastMessage _$LastMessageFromJson(Map<String, dynamic> json) {
  return _LastMessage.fromJson(json);
}

/// @nodoc
mixin _$LastMessage {
  int get chatHisoryId => throw _privateConstructorUsedError;
  String get channel => throw _privateConstructorUsedError;
  String get userId => throw _privateConstructorUsedError;
  String get peerId => throw _privateConstructorUsedError;
  String get userFullname => throw _privateConstructorUsedError;
  String get peerFullname => throw _privateConstructorUsedError;
  String get userAvatarImage => throw _privateConstructorUsedError;
  String get peerAvatarImage => throw _privateConstructorUsedError;
  String get lastMessage => throw _privateConstructorUsedError;
  String get messageBy => throw _privateConstructorUsedError;
  String get messageTo => throw _privateConstructorUsedError;
  String get messageTime => throw _privateConstructorUsedError;
  String get displayed => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LastMessageCopyWith<LastMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LastMessageCopyWith<$Res> {
  factory $LastMessageCopyWith(
          LastMessage value, $Res Function(LastMessage) then) =
      _$LastMessageCopyWithImpl<$Res, LastMessage>;
  @useResult
  $Res call(
      {int chatHisoryId,
      String channel,
      String userId,
      String peerId,
      String userFullname,
      String peerFullname,
      String userAvatarImage,
      String peerAvatarImage,
      String lastMessage,
      String messageBy,
      String messageTo,
      String messageTime,
      String displayed});
}

/// @nodoc
class _$LastMessageCopyWithImpl<$Res, $Val extends LastMessage>
    implements $LastMessageCopyWith<$Res> {
  _$LastMessageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatHisoryId = null,
    Object? channel = null,
    Object? userId = null,
    Object? peerId = null,
    Object? userFullname = null,
    Object? peerFullname = null,
    Object? userAvatarImage = null,
    Object? peerAvatarImage = null,
    Object? lastMessage = null,
    Object? messageBy = null,
    Object? messageTo = null,
    Object? messageTime = null,
    Object? displayed = null,
  }) {
    return _then(_value.copyWith(
      chatHisoryId: null == chatHisoryId
          ? _value.chatHisoryId
          : chatHisoryId // ignore: cast_nullable_to_non_nullable
              as int,
      channel: null == channel
          ? _value.channel
          : channel // ignore: cast_nullable_to_non_nullable
              as String,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      peerId: null == peerId
          ? _value.peerId
          : peerId // ignore: cast_nullable_to_non_nullable
              as String,
      userFullname: null == userFullname
          ? _value.userFullname
          : userFullname // ignore: cast_nullable_to_non_nullable
              as String,
      peerFullname: null == peerFullname
          ? _value.peerFullname
          : peerFullname // ignore: cast_nullable_to_non_nullable
              as String,
      userAvatarImage: null == userAvatarImage
          ? _value.userAvatarImage
          : userAvatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      peerAvatarImage: null == peerAvatarImage
          ? _value.peerAvatarImage
          : peerAvatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      messageBy: null == messageBy
          ? _value.messageBy
          : messageBy // ignore: cast_nullable_to_non_nullable
              as String,
      messageTo: null == messageTo
          ? _value.messageTo
          : messageTo // ignore: cast_nullable_to_non_nullable
              as String,
      messageTime: null == messageTime
          ? _value.messageTime
          : messageTime // ignore: cast_nullable_to_non_nullable
              as String,
      displayed: null == displayed
          ? _value.displayed
          : displayed // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LastMessageCopyWith<$Res>
    implements $LastMessageCopyWith<$Res> {
  factory _$$_LastMessageCopyWith(
          _$_LastMessage value, $Res Function(_$_LastMessage) then) =
      __$$_LastMessageCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int chatHisoryId,
      String channel,
      String userId,
      String peerId,
      String userFullname,
      String peerFullname,
      String userAvatarImage,
      String peerAvatarImage,
      String lastMessage,
      String messageBy,
      String messageTo,
      String messageTime,
      String displayed});
}

/// @nodoc
class __$$_LastMessageCopyWithImpl<$Res>
    extends _$LastMessageCopyWithImpl<$Res, _$_LastMessage>
    implements _$$_LastMessageCopyWith<$Res> {
  __$$_LastMessageCopyWithImpl(
      _$_LastMessage _value, $Res Function(_$_LastMessage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatHisoryId = null,
    Object? channel = null,
    Object? userId = null,
    Object? peerId = null,
    Object? userFullname = null,
    Object? peerFullname = null,
    Object? userAvatarImage = null,
    Object? peerAvatarImage = null,
    Object? lastMessage = null,
    Object? messageBy = null,
    Object? messageTo = null,
    Object? messageTime = null,
    Object? displayed = null,
  }) {
    return _then(_$_LastMessage(
      chatHisoryId: null == chatHisoryId
          ? _value.chatHisoryId
          : chatHisoryId // ignore: cast_nullable_to_non_nullable
              as int,
      channel: null == channel
          ? _value.channel
          : channel // ignore: cast_nullable_to_non_nullable
              as String,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      peerId: null == peerId
          ? _value.peerId
          : peerId // ignore: cast_nullable_to_non_nullable
              as String,
      userFullname: null == userFullname
          ? _value.userFullname
          : userFullname // ignore: cast_nullable_to_non_nullable
              as String,
      peerFullname: null == peerFullname
          ? _value.peerFullname
          : peerFullname // ignore: cast_nullable_to_non_nullable
              as String,
      userAvatarImage: null == userAvatarImage
          ? _value.userAvatarImage
          : userAvatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      peerAvatarImage: null == peerAvatarImage
          ? _value.peerAvatarImage
          : peerAvatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      messageBy: null == messageBy
          ? _value.messageBy
          : messageBy // ignore: cast_nullable_to_non_nullable
              as String,
      messageTo: null == messageTo
          ? _value.messageTo
          : messageTo // ignore: cast_nullable_to_non_nullable
              as String,
      messageTime: null == messageTime
          ? _value.messageTime
          : messageTime // ignore: cast_nullable_to_non_nullable
              as String,
      displayed: null == displayed
          ? _value.displayed
          : displayed // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_LastMessage implements _LastMessage {
  const _$_LastMessage(
      {required this.chatHisoryId,
      required this.channel,
      required this.userId,
      required this.peerId,
      required this.userFullname,
      required this.peerFullname,
      required this.userAvatarImage,
      required this.peerAvatarImage,
      required this.lastMessage,
      required this.messageBy,
      required this.messageTo,
      required this.messageTime,
      required this.displayed});

  factory _$_LastMessage.fromJson(Map<String, dynamic> json) =>
      _$$_LastMessageFromJson(json);

  @override
  final int chatHisoryId;
  @override
  final String channel;
  @override
  final String userId;
  @override
  final String peerId;
  @override
  final String userFullname;
  @override
  final String peerFullname;
  @override
  final String userAvatarImage;
  @override
  final String peerAvatarImage;
  @override
  final String lastMessage;
  @override
  final String messageBy;
  @override
  final String messageTo;
  @override
  final String messageTime;
  @override
  final String displayed;

  @override
  String toString() {
    return 'LastMessage(chatHisoryId: $chatHisoryId, channel: $channel, userId: $userId, peerId: $peerId, userFullname: $userFullname, peerFullname: $peerFullname, userAvatarImage: $userAvatarImage, peerAvatarImage: $peerAvatarImage, lastMessage: $lastMessage, messageBy: $messageBy, messageTo: $messageTo, messageTime: $messageTime, displayed: $displayed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LastMessage &&
            (identical(other.chatHisoryId, chatHisoryId) ||
                other.chatHisoryId == chatHisoryId) &&
            (identical(other.channel, channel) || other.channel == channel) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.peerId, peerId) || other.peerId == peerId) &&
            (identical(other.userFullname, userFullname) ||
                other.userFullname == userFullname) &&
            (identical(other.peerFullname, peerFullname) ||
                other.peerFullname == peerFullname) &&
            (identical(other.userAvatarImage, userAvatarImage) ||
                other.userAvatarImage == userAvatarImage) &&
            (identical(other.peerAvatarImage, peerAvatarImage) ||
                other.peerAvatarImage == peerAvatarImage) &&
            (identical(other.lastMessage, lastMessage) ||
                other.lastMessage == lastMessage) &&
            (identical(other.messageBy, messageBy) ||
                other.messageBy == messageBy) &&
            (identical(other.messageTo, messageTo) ||
                other.messageTo == messageTo) &&
            (identical(other.messageTime, messageTime) ||
                other.messageTime == messageTime) &&
            (identical(other.displayed, displayed) ||
                other.displayed == displayed));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      chatHisoryId,
      channel,
      userId,
      peerId,
      userFullname,
      peerFullname,
      userAvatarImage,
      peerAvatarImage,
      lastMessage,
      messageBy,
      messageTo,
      messageTime,
      displayed);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LastMessageCopyWith<_$_LastMessage> get copyWith =>
      __$$_LastMessageCopyWithImpl<_$_LastMessage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LastMessageToJson(
      this,
    );
  }
}

abstract class _LastMessage implements LastMessage {
  const factory _LastMessage(
      {required final int chatHisoryId,
      required final String channel,
      required final String userId,
      required final String peerId,
      required final String userFullname,
      required final String peerFullname,
      required final String userAvatarImage,
      required final String peerAvatarImage,
      required final String lastMessage,
      required final String messageBy,
      required final String messageTo,
      required final String messageTime,
      required final String displayed}) = _$_LastMessage;

  factory _LastMessage.fromJson(Map<String, dynamic> json) =
      _$_LastMessage.fromJson;

  @override
  int get chatHisoryId;
  @override
  String get channel;
  @override
  String get userId;
  @override
  String get peerId;
  @override
  String get userFullname;
  @override
  String get peerFullname;
  @override
  String get userAvatarImage;
  @override
  String get peerAvatarImage;
  @override
  String get lastMessage;
  @override
  String get messageBy;
  @override
  String get messageTo;
  @override
  String get messageTime;
  @override
  String get displayed;
  @override
  @JsonKey(ignore: true)
  _$$_LastMessageCopyWith<_$_LastMessage> get copyWith =>
      throw _privateConstructorUsedError;
}
