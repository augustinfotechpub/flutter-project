part of 'chat_list_bloc.dart';

@freezed
class ChatListEvent with _$ChatListEvent {
  const factory ChatListEvent.getChatList() = _GetChatList;
  const factory ChatListEvent.refresh() = _Refresh;
}