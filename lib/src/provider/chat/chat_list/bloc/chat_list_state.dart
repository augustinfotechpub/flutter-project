part of 'chat_list_bloc.dart';

@freezed
class ChatListState with _$ChatListState {
  const factory ChatListState.initial() = _Initial;
  const factory ChatListState.loading() = _Loading;
  const factory ChatListState.success(List<LastMessage> users) = _Success;
  const factory ChatListState.error(String error) = _Error;
}
