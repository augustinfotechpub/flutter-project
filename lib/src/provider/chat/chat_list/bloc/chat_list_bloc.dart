import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/chat/chat_list/model.dart';

part 'chat_list_event.dart';
part 'chat_list_state.dart';
part 'chat_list_bloc.freezed.dart';

class ChatListBloc extends Bloc<ChatListEvent, ChatListState> {
  ChatListBloc() : super(const _Initial()) {
    on<ChatListEvent>((event, emit) async {
      if (event is _GetChatList) {
        emit(const _Loading());
        try {
          final response = await apiProvider.getChatList();
          if (response.statusCode == 200) {
            ChatList chatList =
                ChatList.fromJson({'payload': jsonDecode(response.body)});
            emit(_Success(chatList.payload));
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _Refresh) {
        try {
          final response = await apiProvider.getChatList();
          if (response.statusCode == 200) {
            ChatList chatList =
                ChatList.fromJson({'payload': jsonDecode(response.body)});
            emit(_Success(chatList.payload));
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
