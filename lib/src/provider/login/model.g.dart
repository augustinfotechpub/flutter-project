// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Login _$$_LoginFromJson(Map<String, dynamic> json) => _$_Login(
      msg: json['msg'] as String,
      token: Token.fromJson(json['token'] as Map<String, dynamic>),
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_LoginToJson(_$_Login instance) => <String, dynamic>{
      'msg': instance.msg,
      'token': instance.token,
      'data': instance.data,
    };

_$_Token _$$_TokenFromJson(Map<String, dynamic> json) => _$_Token(
      refresh: json['refresh'] as String,
      access: json['access'] as String,
    );

Map<String, dynamic> _$$_TokenToJson(_$_Token instance) => <String, dynamic>{
      'refresh': instance.refresh,
      'access': instance.access,
    };

_$_Data _$$_DataFromJson(Map<String, dynamic> json) => _$_Data(
      email: json['email'] as String,
      password: json['password'] as String?,
      adminId: json['admin_id'] as String,
      userId: json['user_id'] as String,
      position: json['position'] as String,
    );

Map<String, dynamic> _$$_DataToJson(_$_Data instance) => <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'admin_id': instance.adminId,
      'user_id': instance.userId,
      'position': instance.position,
    };
