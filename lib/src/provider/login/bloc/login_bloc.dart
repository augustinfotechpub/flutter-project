// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/login/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const _Initial()) {
    on<LoginEvent>((event, emit) async {
      if (event is _Login || event is _SocialLogin) {
        emit(const _Loading());
      }
      try {
        var loginResponse;
        if (event is _Login) {
          loginResponse = await apiProvider.login(
              email: event.email, password: event.password);
        }
        if (event is _SocialLogin) {
          loginResponse = await apiProvider.socialLogIn(
              email: event.email, identifier: event.identifier);
        }
        if (loginResponse.statusCode == 200) {
          Map<String, dynamic> json = jsonDecode(loginResponse.body);
          if (json.containsKey('payload')) {
            emit(_Error(json['payload']));
          } else {
            Login login = Login.fromJson(jsonDecode(loginResponse.body));
            accessToken = login.token.access;
            HiveUtils.set(HiveKeys.accessToken, login.token.access);
            HiveUtils.set(HiveKeys.refreshToken, login.token.refresh);
            HiveUtils.set(HiveKeys.password, login.data.password);
            HiveUtils.set(HiveKeys.email, login.data.email);
            if (login.data.adminId == '') {
              HiveUtils.set(HiveKeys.clinicianId, login.data.userId);
              try {
                final userResponse =
                    await apiProvider.getClinician(login.data.userId);
                if (userResponse.statusCode == 200) {
                  Profile user = Profile.fromJson(
                      jsonDecode(userResponse.body)['results'].first);
                  HiveUtils.set(HiveKeys.user, user.user);
                  HiveUtils.set(HiveKeys.userName, user.username);
                  HiveUtils.set(HiveKeys.position, user.clinicianPosition);
                  HiveUtils.set(HiveKeys.avatarImage, user.avatarImage);
                  HiveUtils.set(HiveKeys.fullName,
                      '${user.firstname.capitalize()} ${user.lastname.capitalize()}');
                  emit(const _Success(false));
                } else {
                  emit(const _Error('Token is invalid or expired'));
                }
              } catch (e) {
                emit(_Error(e.toString()));
              }
            } else {
              HiveUtils.set(HiveKeys.adminId, login.data.adminId);
              try {
                final adminResponse =
                    await adminApiProvider.getAdminProfile(login.data.adminId);
                if (adminResponse.statusCode == 200) {
                  Admin admin = Admin.fromJson(
                      jsonDecode(adminResponse.body)['results'].first);
                  HiveUtils.set(HiveKeys.user, admin.user);
                  HiveUtils.set(HiveKeys.email, admin.email);
                  HiveUtils.set(HiveKeys.userName, admin.username);
                  HiveUtils.set(HiveKeys.avatarImage, admin.avatarImage);
                  HiveUtils.set(HiveKeys.fullName,
                      '${admin.firstname.capitalize()} ${admin.lastname.capitalize()}');
                  HiveUtils.set(HiveKeys.facilityId, admin.facility.toString());
                  emit(const _Success(true));
                } else {
                  emit(const _Error('Token is invalid or expired'));
                }
              } catch (e) {
                emit(_Error(e.toString()));
              }
            }
          }
        } else {
          emit(_Error(jsonDecode(loginResponse.body)['email'].first));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
