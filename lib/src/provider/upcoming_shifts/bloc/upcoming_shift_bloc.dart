import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'upcoming_shift_event.dart';
part 'upcoming_shift_state.dart';
part 'upcoming_shift_bloc.freezed.dart';

class UpcomingShiftBloc extends Bloc<UpcomingShiftEvent, UpcomingShiftState> {
  UpcomingShiftBloc() : super(const _Initial()) {
    on<UpcomingShiftEvent>((event, emit) async {
      if (event is _Started) {
        emit(const _Loading());
      }
      try {
        final shift = await apiProvider.getUpcomingShifts(
            fromDate: event.fromDate,
            toDate: event.toDate,
            id: HiveUtils.get(HiveKeys.clinicianId));
        if (shift.statusCode == 200) {
          UpcomingShift upcomingShift =
              UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
          emit(_Success(event.dateType, upcomingShift));
        } else {
          emit(_Error(jsonDecode(shift.body)));
        }
      } catch (e) {
        emit(_Error(e.toString()));
      }
    });
  }
}
