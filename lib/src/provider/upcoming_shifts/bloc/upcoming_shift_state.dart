part of 'upcoming_shift_bloc.dart';

@freezed
class UpcomingShiftState with _$UpcomingShiftState {
  const factory UpcomingShiftState.initial() = _Initial;
  const factory UpcomingShiftState.loading() = _Loading;
  const factory UpcomingShiftState.success(
      String dateType, UpcomingShift upcomingShift) = _Success;
  const factory UpcomingShiftState.error(String error) = _Error;
}
