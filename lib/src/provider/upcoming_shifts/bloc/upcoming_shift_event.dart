part of 'upcoming_shift_bloc.dart';

@freezed
class UpcomingShiftEvent with _$UpcomingShiftEvent {
  const factory UpcomingShiftEvent.getShift({
    required String dateType,
    required String fromDate,
    required String toDate,
  }) = _Started;
}
