// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UpcomingShift _$UpcomingShiftFromJson(Map<String, dynamic> json) {
  return _UpcomingShift.fromJson(json);
}

/// @nodoc
mixin _$UpcomingShift {
  List<Shift> get shift => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UpcomingShiftCopyWith<UpcomingShift> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpcomingShiftCopyWith<$Res> {
  factory $UpcomingShiftCopyWith(
          UpcomingShift value, $Res Function(UpcomingShift) then) =
      _$UpcomingShiftCopyWithImpl<$Res, UpcomingShift>;
  @useResult
  $Res call({List<Shift> shift});
}

/// @nodoc
class _$UpcomingShiftCopyWithImpl<$Res, $Val extends UpcomingShift>
    implements $UpcomingShiftCopyWith<$Res> {
  _$UpcomingShiftCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shift = null,
  }) {
    return _then(_value.copyWith(
      shift: null == shift
          ? _value.shift
          : shift // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UpcomingShiftCopyWith<$Res>
    implements $UpcomingShiftCopyWith<$Res> {
  factory _$$_UpcomingShiftCopyWith(
          _$_UpcomingShift value, $Res Function(_$_UpcomingShift) then) =
      __$$_UpcomingShiftCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Shift> shift});
}

/// @nodoc
class __$$_UpcomingShiftCopyWithImpl<$Res>
    extends _$UpcomingShiftCopyWithImpl<$Res, _$_UpcomingShift>
    implements _$$_UpcomingShiftCopyWith<$Res> {
  __$$_UpcomingShiftCopyWithImpl(
      _$_UpcomingShift _value, $Res Function(_$_UpcomingShift) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shift = null,
  }) {
    return _then(_$_UpcomingShift(
      shift: null == shift
          ? _value._shift
          : shift // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UpcomingShift implements _UpcomingShift {
  const _$_UpcomingShift({required final List<Shift> shift}) : _shift = shift;

  factory _$_UpcomingShift.fromJson(Map<String, dynamic> json) =>
      _$$_UpcomingShiftFromJson(json);

  final List<Shift> _shift;
  @override
  List<Shift> get shift {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_shift);
  }

  @override
  String toString() {
    return 'UpcomingShift(shift: $shift)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpcomingShift &&
            const DeepCollectionEquality().equals(other._shift, _shift));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_shift));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpcomingShiftCopyWith<_$_UpcomingShift> get copyWith =>
      __$$_UpcomingShiftCopyWithImpl<_$_UpcomingShift>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UpcomingShiftToJson(
      this,
    );
  }
}

abstract class _UpcomingShift implements UpcomingShift {
  const factory _UpcomingShift({required final List<Shift> shift}) =
      _$_UpcomingShift;

  factory _UpcomingShift.fromJson(Map<String, dynamic> json) =
      _$_UpcomingShift.fromJson;

  @override
  List<Shift> get shift;
  @override
  @JsonKey(ignore: true)
  _$$_UpcomingShiftCopyWith<_$_UpcomingShift> get copyWith =>
      throw _privateConstructorUsedError;
}

Shift _$ShiftFromJson(Map<String, dynamic> json) {
  return _Shift.fromJson(json);
}

/// @nodoc
mixin _$Shift {
  int get scheduleId => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get facilityId => throw _privateConstructorUsedError;
  String get facilityName => throw _privateConstructorUsedError;
  String get clinicianId => throw _privateConstructorUsedError;
  String get avatarImage => throw _privateConstructorUsedError;
  String get shiftTemplateId => throw _privateConstructorUsedError;
  String get clinicianName => throw _privateConstructorUsedError;
  String get shiftColor => throw _privateConstructorUsedError;
  String get clinicianPosition => throw _privateConstructorUsedError;
  String get startDate => throw _privateConstructorUsedError;
  String get startTime => throw _privateConstructorUsedError;
  String get endDate => throw _privateConstructorUsedError;
  String get endTime => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  String get declinedBy => throw _privateConstructorUsedError;
  String get note => throw _privateConstructorUsedError;
  String get timesheetId => throw _privateConstructorUsedError;
  String get checkIn => throw _privateConstructorUsedError;
  String get checkOut => throw _privateConstructorUsedError;
  String get breakIn => throw _privateConstructorUsedError;
  String get breakOut => throw _privateConstructorUsedError;
  String get unit => throw _privateConstructorUsedError;
  String get noMealBreak => throw _privateConstructorUsedError;
  List<String>? get requestingUsers => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ShiftCopyWith<Shift> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShiftCopyWith<$Res> {
  factory $ShiftCopyWith(Shift value, $Res Function(Shift) then) =
      _$ShiftCopyWithImpl<$Res, Shift>;
  @useResult
  $Res call(
      {int scheduleId,
      String title,
      String facilityId,
      String facilityName,
      String clinicianId,
      String avatarImage,
      String shiftTemplateId,
      String clinicianName,
      String shiftColor,
      String clinicianPosition,
      String startDate,
      String startTime,
      String endDate,
      String endTime,
      String status,
      String declinedBy,
      String note,
      String timesheetId,
      String checkIn,
      String checkOut,
      String breakIn,
      String breakOut,
      String unit,
      String noMealBreak,
      List<String>? requestingUsers});
}

/// @nodoc
class _$ShiftCopyWithImpl<$Res, $Val extends Shift>
    implements $ShiftCopyWith<$Res> {
  _$ShiftCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? scheduleId = null,
    Object? title = null,
    Object? facilityId = null,
    Object? facilityName = null,
    Object? clinicianId = null,
    Object? avatarImage = null,
    Object? shiftTemplateId = null,
    Object? clinicianName = null,
    Object? shiftColor = null,
    Object? clinicianPosition = null,
    Object? startDate = null,
    Object? startTime = null,
    Object? endDate = null,
    Object? endTime = null,
    Object? status = null,
    Object? declinedBy = null,
    Object? note = null,
    Object? timesheetId = null,
    Object? checkIn = null,
    Object? checkOut = null,
    Object? breakIn = null,
    Object? breakOut = null,
    Object? unit = null,
    Object? noMealBreak = null,
    Object? requestingUsers = freezed,
  }) {
    return _then(_value.copyWith(
      scheduleId: null == scheduleId
          ? _value.scheduleId
          : scheduleId // ignore: cast_nullable_to_non_nullable
              as int,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      facilityId: null == facilityId
          ? _value.facilityId
          : facilityId // ignore: cast_nullable_to_non_nullable
              as String,
      facilityName: null == facilityName
          ? _value.facilityName
          : facilityName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      shiftTemplateId: null == shiftTemplateId
          ? _value.shiftTemplateId
          : shiftTemplateId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      shiftColor: null == shiftColor
          ? _value.shiftColor
          : shiftColor // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      startDate: null == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String,
      startTime: null == startTime
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as String,
      endDate: null == endDate
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String,
      endTime: null == endTime
          ? _value.endTime
          : endTime // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      declinedBy: null == declinedBy
          ? _value.declinedBy
          : declinedBy // ignore: cast_nullable_to_non_nullable
              as String,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      timesheetId: null == timesheetId
          ? _value.timesheetId
          : timesheetId // ignore: cast_nullable_to_non_nullable
              as String,
      checkIn: null == checkIn
          ? _value.checkIn
          : checkIn // ignore: cast_nullable_to_non_nullable
              as String,
      checkOut: null == checkOut
          ? _value.checkOut
          : checkOut // ignore: cast_nullable_to_non_nullable
              as String,
      breakIn: null == breakIn
          ? _value.breakIn
          : breakIn // ignore: cast_nullable_to_non_nullable
              as String,
      breakOut: null == breakOut
          ? _value.breakOut
          : breakOut // ignore: cast_nullable_to_non_nullable
              as String,
      unit: null == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String,
      noMealBreak: null == noMealBreak
          ? _value.noMealBreak
          : noMealBreak // ignore: cast_nullable_to_non_nullable
              as String,
      requestingUsers: freezed == requestingUsers
          ? _value.requestingUsers
          : requestingUsers // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ShiftCopyWith<$Res> implements $ShiftCopyWith<$Res> {
  factory _$$_ShiftCopyWith(_$_Shift value, $Res Function(_$_Shift) then) =
      __$$_ShiftCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int scheduleId,
      String title,
      String facilityId,
      String facilityName,
      String clinicianId,
      String avatarImage,
      String shiftTemplateId,
      String clinicianName,
      String shiftColor,
      String clinicianPosition,
      String startDate,
      String startTime,
      String endDate,
      String endTime,
      String status,
      String declinedBy,
      String note,
      String timesheetId,
      String checkIn,
      String checkOut,
      String breakIn,
      String breakOut,
      String unit,
      String noMealBreak,
      List<String>? requestingUsers});
}

/// @nodoc
class __$$_ShiftCopyWithImpl<$Res> extends _$ShiftCopyWithImpl<$Res, _$_Shift>
    implements _$$_ShiftCopyWith<$Res> {
  __$$_ShiftCopyWithImpl(_$_Shift _value, $Res Function(_$_Shift) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? scheduleId = null,
    Object? title = null,
    Object? facilityId = null,
    Object? facilityName = null,
    Object? clinicianId = null,
    Object? avatarImage = null,
    Object? shiftTemplateId = null,
    Object? clinicianName = null,
    Object? shiftColor = null,
    Object? clinicianPosition = null,
    Object? startDate = null,
    Object? startTime = null,
    Object? endDate = null,
    Object? endTime = null,
    Object? status = null,
    Object? declinedBy = null,
    Object? note = null,
    Object? timesheetId = null,
    Object? checkIn = null,
    Object? checkOut = null,
    Object? breakIn = null,
    Object? breakOut = null,
    Object? unit = null,
    Object? noMealBreak = null,
    Object? requestingUsers = freezed,
  }) {
    return _then(_$_Shift(
      scheduleId: null == scheduleId
          ? _value.scheduleId
          : scheduleId // ignore: cast_nullable_to_non_nullable
              as int,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      facilityId: null == facilityId
          ? _value.facilityId
          : facilityId // ignore: cast_nullable_to_non_nullable
              as String,
      facilityName: null == facilityName
          ? _value.facilityName
          : facilityName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      shiftTemplateId: null == shiftTemplateId
          ? _value.shiftTemplateId
          : shiftTemplateId // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      shiftColor: null == shiftColor
          ? _value.shiftColor
          : shiftColor // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      startDate: null == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String,
      startTime: null == startTime
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as String,
      endDate: null == endDate
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String,
      endTime: null == endTime
          ? _value.endTime
          : endTime // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      declinedBy: null == declinedBy
          ? _value.declinedBy
          : declinedBy // ignore: cast_nullable_to_non_nullable
              as String,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      timesheetId: null == timesheetId
          ? _value.timesheetId
          : timesheetId // ignore: cast_nullable_to_non_nullable
              as String,
      checkIn: null == checkIn
          ? _value.checkIn
          : checkIn // ignore: cast_nullable_to_non_nullable
              as String,
      checkOut: null == checkOut
          ? _value.checkOut
          : checkOut // ignore: cast_nullable_to_non_nullable
              as String,
      breakIn: null == breakIn
          ? _value.breakIn
          : breakIn // ignore: cast_nullable_to_non_nullable
              as String,
      breakOut: null == breakOut
          ? _value.breakOut
          : breakOut // ignore: cast_nullable_to_non_nullable
              as String,
      unit: null == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String,
      noMealBreak: null == noMealBreak
          ? _value.noMealBreak
          : noMealBreak // ignore: cast_nullable_to_non_nullable
              as String,
      requestingUsers: freezed == requestingUsers
          ? _value._requestingUsers
          : requestingUsers // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Shift implements _Shift {
  const _$_Shift(
      {required this.scheduleId,
      required this.title,
      required this.facilityId,
      required this.facilityName,
      required this.clinicianId,
      required this.avatarImage,
      required this.shiftTemplateId,
      required this.clinicianName,
      required this.shiftColor,
      required this.clinicianPosition,
      required this.startDate,
      required this.startTime,
      required this.endDate,
      required this.endTime,
      required this.status,
      required this.declinedBy,
      required this.note,
      required this.timesheetId,
      required this.checkIn,
      required this.checkOut,
      required this.breakIn,
      required this.breakOut,
      required this.unit,
      required this.noMealBreak,
      final List<String>? requestingUsers})
      : _requestingUsers = requestingUsers;

  factory _$_Shift.fromJson(Map<String, dynamic> json) =>
      _$$_ShiftFromJson(json);

  @override
  final int scheduleId;
  @override
  final String title;
  @override
  final String facilityId;
  @override
  final String facilityName;
  @override
  final String clinicianId;
  @override
  final String avatarImage;
  @override
  final String shiftTemplateId;
  @override
  final String clinicianName;
  @override
  final String shiftColor;
  @override
  final String clinicianPosition;
  @override
  final String startDate;
  @override
  final String startTime;
  @override
  final String endDate;
  @override
  final String endTime;
  @override
  final String status;
  @override
  final String declinedBy;
  @override
  final String note;
  @override
  final String timesheetId;
  @override
  final String checkIn;
  @override
  final String checkOut;
  @override
  final String breakIn;
  @override
  final String breakOut;
  @override
  final String unit;
  @override
  final String noMealBreak;
  final List<String>? _requestingUsers;
  @override
  List<String>? get requestingUsers {
    final value = _requestingUsers;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'Shift(scheduleId: $scheduleId, title: $title, facilityId: $facilityId, facilityName: $facilityName, clinicianId: $clinicianId, avatarImage: $avatarImage, shiftTemplateId: $shiftTemplateId, clinicianName: $clinicianName, shiftColor: $shiftColor, clinicianPosition: $clinicianPosition, startDate: $startDate, startTime: $startTime, endDate: $endDate, endTime: $endTime, status: $status, declinedBy: $declinedBy, note: $note, timesheetId: $timesheetId, checkIn: $checkIn, checkOut: $checkOut, breakIn: $breakIn, breakOut: $breakOut, unit: $unit, noMealBreak: $noMealBreak, requestingUsers: $requestingUsers)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Shift &&
            (identical(other.scheduleId, scheduleId) ||
                other.scheduleId == scheduleId) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.facilityId, facilityId) ||
                other.facilityId == facilityId) &&
            (identical(other.facilityName, facilityName) ||
                other.facilityName == facilityName) &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId) &&
            (identical(other.avatarImage, avatarImage) ||
                other.avatarImage == avatarImage) &&
            (identical(other.shiftTemplateId, shiftTemplateId) ||
                other.shiftTemplateId == shiftTemplateId) &&
            (identical(other.clinicianName, clinicianName) ||
                other.clinicianName == clinicianName) &&
            (identical(other.shiftColor, shiftColor) ||
                other.shiftColor == shiftColor) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.startTime, startTime) ||
                other.startTime == startTime) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.endTime, endTime) || other.endTime == endTime) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.declinedBy, declinedBy) ||
                other.declinedBy == declinedBy) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.timesheetId, timesheetId) ||
                other.timesheetId == timesheetId) &&
            (identical(other.checkIn, checkIn) || other.checkIn == checkIn) &&
            (identical(other.checkOut, checkOut) ||
                other.checkOut == checkOut) &&
            (identical(other.breakIn, breakIn) || other.breakIn == breakIn) &&
            (identical(other.breakOut, breakOut) ||
                other.breakOut == breakOut) &&
            (identical(other.unit, unit) || other.unit == unit) &&
            (identical(other.noMealBreak, noMealBreak) ||
                other.noMealBreak == noMealBreak) &&
            const DeepCollectionEquality()
                .equals(other._requestingUsers, _requestingUsers));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        scheduleId,
        title,
        facilityId,
        facilityName,
        clinicianId,
        avatarImage,
        shiftTemplateId,
        clinicianName,
        shiftColor,
        clinicianPosition,
        startDate,
        startTime,
        endDate,
        endTime,
        status,
        declinedBy,
        note,
        timesheetId,
        checkIn,
        checkOut,
        breakIn,
        breakOut,
        unit,
        noMealBreak,
        const DeepCollectionEquality().hash(_requestingUsers)
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ShiftCopyWith<_$_Shift> get copyWith =>
      __$$_ShiftCopyWithImpl<_$_Shift>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ShiftToJson(
      this,
    );
  }
}

abstract class _Shift implements Shift {
  const factory _Shift(
      {required final int scheduleId,
      required final String title,
      required final String facilityId,
      required final String facilityName,
      required final String clinicianId,
      required final String avatarImage,
      required final String shiftTemplateId,
      required final String clinicianName,
      required final String shiftColor,
      required final String clinicianPosition,
      required final String startDate,
      required final String startTime,
      required final String endDate,
      required final String endTime,
      required final String status,
      required final String declinedBy,
      required final String note,
      required final String timesheetId,
      required final String checkIn,
      required final String checkOut,
      required final String breakIn,
      required final String breakOut,
      required final String unit,
      required final String noMealBreak,
      final List<String>? requestingUsers}) = _$_Shift;

  factory _Shift.fromJson(Map<String, dynamic> json) = _$_Shift.fromJson;

  @override
  int get scheduleId;
  @override
  String get title;
  @override
  String get facilityId;
  @override
  String get facilityName;
  @override
  String get clinicianId;
  @override
  String get avatarImage;
  @override
  String get shiftTemplateId;
  @override
  String get clinicianName;
  @override
  String get shiftColor;
  @override
  String get clinicianPosition;
  @override
  String get startDate;
  @override
  String get startTime;
  @override
  String get endDate;
  @override
  String get endTime;
  @override
  String get status;
  @override
  String get declinedBy;
  @override
  String get note;
  @override
  String get timesheetId;
  @override
  String get checkIn;
  @override
  String get checkOut;
  @override
  String get breakIn;
  @override
  String get breakOut;
  @override
  String get unit;
  @override
  String get noMealBreak;
  @override
  List<String>? get requestingUsers;
  @override
  @JsonKey(ignore: true)
  _$$_ShiftCopyWith<_$_Shift> get copyWith =>
      throw _privateConstructorUsedError;
}
