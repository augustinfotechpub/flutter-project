// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UpcomingShift _$$_UpcomingShiftFromJson(Map<String, dynamic> json) =>
    _$_UpcomingShift(
      shift: (json['shift'] as List<dynamic>)
          .map((e) => Shift.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_UpcomingShiftToJson(_$_UpcomingShift instance) =>
    <String, dynamic>{
      'shift': instance.shift,
    };

_$_Shift _$$_ShiftFromJson(Map<String, dynamic> json) => _$_Shift(
      scheduleId: json['schedule_id'] as int,
      title: json['title'] as String,
      facilityId: json['facility_id'] as String,
      facilityName: json['facility_name'] as String,
      clinicianId: json['clinician_id'] as String,
      avatarImage: json['avatar_image'] as String,
      shiftTemplateId: json['shift_template_id'] as String,
      clinicianName: json['clinician_name'] as String,
      shiftColor: json['shift_color'] as String,
      clinicianPosition: json['clinician_position'] as String,
      startDate: json['start_date'] as String,
      startTime: json['start_time'] as String,
      endDate: json['end_date'] as String,
      endTime: json['end_time'] as String,
      status: json['status'] as String,
      declinedBy: json['declined_by'] as String,
      note: json['note'] as String,
      timesheetId: json['timesheet_id'] as String,
      checkIn: json['check_in'] as String,
      checkOut: json['check_out'] as String,
      breakIn: json['break_in'] as String,
      breakOut: json['break_out'] as String,
      unit: json['unit'] as String,
      noMealBreak: json['no_meal_break'] as String,
      requestingUsers: (json['requesting_users'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$$_ShiftToJson(_$_Shift instance) => <String, dynamic>{
      'schedule_id': instance.scheduleId,
      'title': instance.title,
      'facility_id': instance.facilityId,
      'facility_name': instance.facilityName,
      'clinician_id': instance.clinicianId,
      'avatar_image': instance.avatarImage,
      'shift_template_id': instance.shiftTemplateId,
      'clinician_name': instance.clinicianName,
      'shift_color': instance.shiftColor,
      'clinician_position': instance.clinicianPosition,
      'start_date': instance.startDate,
      'start_time': instance.startTime,
      'end_date': instance.endDate,
      'end_time': instance.endTime,
      'status': instance.status,
      'declined_by': instance.declinedBy,
      'note': instance.note,
      'timesheet_id': instance.timesheetId,
      'check_in': instance.checkIn,
      'check_out': instance.checkOut,
      'break_in': instance.breakIn,
      'break_out': instance.breakOut,
      'unit': instance.unit,
      'no_meal_break': instance.noMealBreak,
      'requesting_users': instance.requestingUsers,
    };
