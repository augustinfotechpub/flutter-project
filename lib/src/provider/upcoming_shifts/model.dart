// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class UpcomingShift with _$UpcomingShift {
  const factory UpcomingShift({
    required List<Shift> shift,
  }) = _UpcomingShift;
  factory UpcomingShift.fromJson(Map<String, dynamic> json) =>
      _$UpcomingShiftFromJson(json);
}

@freezed
class Shift with _$Shift {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Shift({
    required int scheduleId,
    required String title,
    required String facilityId,
    required String facilityName,
    required String clinicianId,
    required String avatarImage,
    required String shiftTemplateId,
    required String clinicianName,
    required String shiftColor,
    required String clinicianPosition,
    required String startDate,
    required String startTime,
    required String endDate,
    required String endTime,
    required String status,
    required String declinedBy,
    required String note,
    required String timesheetId,
    required String checkIn,
    required String checkOut,
    required String breakIn,
    required String breakOut,
    required String unit,
    required String noMealBreak,
    List<String>? requestingUsers,
  }) = _Shift;

  factory Shift.fromJson(Map<String, dynamic> json) => _$ShiftFromJson(json);
}
