// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Profile _$ProfileFromJson(Map<String, dynamic> json) {
  return _Profile.fromJson(json);
}

/// @nodoc
mixin _$Profile {
  int get clinicianId => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get firstname => throw _privateConstructorUsedError;
  String get lastname => throw _privateConstructorUsedError;
  String get avatarImage => throw _privateConstructorUsedError;
  String get contactNo => throw _privateConstructorUsedError;
  String get dateOfBirth => throw _privateConstructorUsedError;
  List<String> get jobSites => throw _privateConstructorUsedError;
  String get clinicianPosition => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  bool get applyShift => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;
  int get user => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProfileCopyWith<Profile> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileCopyWith<$Res> {
  factory $ProfileCopyWith(Profile value, $Res Function(Profile) then) =
      _$ProfileCopyWithImpl<$Res, Profile>;
  @useResult
  $Res call(
      {int clinicianId,
      String username,
      String password,
      String email,
      String firstname,
      String lastname,
      String avatarImage,
      String contactNo,
      String dateOfBirth,
      List<String> jobSites,
      String clinicianPosition,
      bool isActive,
      bool applyShift,
      String role,
      int user});
}

/// @nodoc
class _$ProfileCopyWithImpl<$Res, $Val extends Profile>
    implements $ProfileCopyWith<$Res> {
  _$ProfileCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? clinicianId = null,
    Object? username = null,
    Object? password = null,
    Object? email = null,
    Object? firstname = null,
    Object? lastname = null,
    Object? avatarImage = null,
    Object? contactNo = null,
    Object? dateOfBirth = null,
    Object? jobSites = null,
    Object? clinicianPosition = null,
    Object? isActive = null,
    Object? applyShift = null,
    Object? role = null,
    Object? user = null,
  }) {
    return _then(_value.copyWith(
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      jobSites: null == jobSites
          ? _value.jobSites
          : jobSites // ignore: cast_nullable_to_non_nullable
              as List<String>,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      applyShift: null == applyShift
          ? _value.applyShift
          : applyShift // ignore: cast_nullable_to_non_nullable
              as bool,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProfileCopyWith<$Res> implements $ProfileCopyWith<$Res> {
  factory _$$_ProfileCopyWith(
          _$_Profile value, $Res Function(_$_Profile) then) =
      __$$_ProfileCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int clinicianId,
      String username,
      String password,
      String email,
      String firstname,
      String lastname,
      String avatarImage,
      String contactNo,
      String dateOfBirth,
      List<String> jobSites,
      String clinicianPosition,
      bool isActive,
      bool applyShift,
      String role,
      int user});
}

/// @nodoc
class __$$_ProfileCopyWithImpl<$Res>
    extends _$ProfileCopyWithImpl<$Res, _$_Profile>
    implements _$$_ProfileCopyWith<$Res> {
  __$$_ProfileCopyWithImpl(_$_Profile _value, $Res Function(_$_Profile) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? clinicianId = null,
    Object? username = null,
    Object? password = null,
    Object? email = null,
    Object? firstname = null,
    Object? lastname = null,
    Object? avatarImage = null,
    Object? contactNo = null,
    Object? dateOfBirth = null,
    Object? jobSites = null,
    Object? clinicianPosition = null,
    Object? isActive = null,
    Object? applyShift = null,
    Object? role = null,
    Object? user = null,
  }) {
    return _then(_$_Profile(
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      firstname: null == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String,
      lastname: null == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String,
      avatarImage: null == avatarImage
          ? _value.avatarImage
          : avatarImage // ignore: cast_nullable_to_non_nullable
              as String,
      contactNo: null == contactNo
          ? _value.contactNo
          : contactNo // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      jobSites: null == jobSites
          ? _value._jobSites
          : jobSites // ignore: cast_nullable_to_non_nullable
              as List<String>,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      applyShift: null == applyShift
          ? _value.applyShift
          : applyShift // ignore: cast_nullable_to_non_nullable
              as bool,
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Profile implements _Profile {
  const _$_Profile(
      {required this.clinicianId,
      required this.username,
      required this.password,
      required this.email,
      required this.firstname,
      required this.lastname,
      required this.avatarImage,
      required this.contactNo,
      required this.dateOfBirth,
      required final List<String> jobSites,
      required this.clinicianPosition,
      required this.isActive,
      required this.applyShift,
      required this.role,
      required this.user})
      : _jobSites = jobSites;

  factory _$_Profile.fromJson(Map<String, dynamic> json) =>
      _$$_ProfileFromJson(json);

  @override
  final int clinicianId;
  @override
  final String username;
  @override
  final String password;
  @override
  final String email;
  @override
  final String firstname;
  @override
  final String lastname;
  @override
  final String avatarImage;
  @override
  final String contactNo;
  @override
  final String dateOfBirth;
  final List<String> _jobSites;
  @override
  List<String> get jobSites {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_jobSites);
  }

  @override
  final String clinicianPosition;
  @override
  final bool isActive;
  @override
  final bool applyShift;
  @override
  final String role;
  @override
  final int user;

  @override
  String toString() {
    return 'Profile(clinicianId: $clinicianId, username: $username, password: $password, email: $email, firstname: $firstname, lastname: $lastname, avatarImage: $avatarImage, contactNo: $contactNo, dateOfBirth: $dateOfBirth, jobSites: $jobSites, clinicianPosition: $clinicianPosition, isActive: $isActive, applyShift: $applyShift, role: $role, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Profile &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.firstname, firstname) ||
                other.firstname == firstname) &&
            (identical(other.lastname, lastname) ||
                other.lastname == lastname) &&
            (identical(other.avatarImage, avatarImage) ||
                other.avatarImage == avatarImage) &&
            (identical(other.contactNo, contactNo) ||
                other.contactNo == contactNo) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            const DeepCollectionEquality().equals(other._jobSites, _jobSites) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.applyShift, applyShift) ||
                other.applyShift == applyShift) &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.user, user) || other.user == user));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      clinicianId,
      username,
      password,
      email,
      firstname,
      lastname,
      avatarImage,
      contactNo,
      dateOfBirth,
      const DeepCollectionEquality().hash(_jobSites),
      clinicianPosition,
      isActive,
      applyShift,
      role,
      user);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProfileCopyWith<_$_Profile> get copyWith =>
      __$$_ProfileCopyWithImpl<_$_Profile>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProfileToJson(
      this,
    );
  }
}

abstract class _Profile implements Profile {
  const factory _Profile(
      {required final int clinicianId,
      required final String username,
      required final String password,
      required final String email,
      required final String firstname,
      required final String lastname,
      required final String avatarImage,
      required final String contactNo,
      required final String dateOfBirth,
      required final List<String> jobSites,
      required final String clinicianPosition,
      required final bool isActive,
      required final bool applyShift,
      required final String role,
      required final int user}) = _$_Profile;

  factory _Profile.fromJson(Map<String, dynamic> json) = _$_Profile.fromJson;

  @override
  int get clinicianId;
  @override
  String get username;
  @override
  String get password;
  @override
  String get email;
  @override
  String get firstname;
  @override
  String get lastname;
  @override
  String get avatarImage;
  @override
  String get contactNo;
  @override
  String get dateOfBirth;
  @override
  List<String> get jobSites;
  @override
  String get clinicianPosition;
  @override
  bool get isActive;
  @override
  bool get applyShift;
  @override
  String get role;
  @override
  int get user;
  @override
  @JsonKey(ignore: true)
  _$$_ProfileCopyWith<_$_Profile> get copyWith =>
      throw _privateConstructorUsedError;
}
