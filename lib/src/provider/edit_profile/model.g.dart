// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Profile _$$_ProfileFromJson(Map<String, dynamic> json) => _$_Profile(
      clinicianId: json['clinician_id'] as int,
      username: json['username'] as String,
      password: json['password'] as String,
      email: json['email'] as String,
      firstname: json['firstname'] as String,
      lastname: json['lastname'] as String,
      avatarImage: json['avatar_image'] as String,
      contactNo: json['contact_no'] as String,
      dateOfBirth: json['date_of_birth'] as String,
      jobSites:
          (json['job_sites'] as List<dynamic>).map((e) => e as String).toList(),
      clinicianPosition: json['clinician_position'] as String,
      isActive: json['is_active'] as bool,
      applyShift: json['apply_shift'] as bool,
      role: json['role'] as String,
      user: json['user'] as int,
    );

Map<String, dynamic> _$$_ProfileToJson(_$_Profile instance) =>
    <String, dynamic>{
      'clinician_id': instance.clinicianId,
      'username': instance.username,
      'password': instance.password,
      'email': instance.email,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'avatar_image': instance.avatarImage,
      'contact_no': instance.contactNo,
      'date_of_birth': instance.dateOfBirth,
      'job_sites': instance.jobSites,
      'clinician_position': instance.clinicianPosition,
      'is_active': instance.isActive,
      'apply_shift': instance.applyShift,
      'role': instance.role,
      'user': instance.user,
    };
