// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class Profile with _$Profile {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Profile({
    required int clinicianId,
    required String username,
    required String password,
    required String email,
    required String firstname,
    required String lastname,
    required String avatarImage,
    required String contactNo,
    required String dateOfBirth,
    required List<String> jobSites,
    required String clinicianPosition,
    required bool isActive,
    required bool applyShift,
    required String role,
    required int user,
  }) = _Profile;

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);
}
