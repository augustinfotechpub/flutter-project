import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/edit_profile/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'edit_profile_event.dart';
part 'edit_profile_state.dart';
part 'edit_profile_bloc.freezed.dart';

Profile? _login;
Admin? _admin;

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  EditProfileBloc() : super(const _Loading()) {
    on<EditProfileEvent>((event, emit) async {
      if (event is _SaveChanges) {
        emit(const _Loading());
        try {
          final response = event.isAdmin
              ? await adminApiProvider.updateProfile(
                  userData: event.user.toJson())
              : await apiProvider.updateProfile(userData: event.user.toJson());
          if (response.statusCode == 200) {
            await CachedNetworkImage.evictFromCache(
                HiveUtils.get(HiveKeys.avatarImage));
            if (event.isAdmin) {
              _admin = Admin.fromJson(jsonDecode(response.body)['payload']);
              HiveUtils.set(HiveKeys.email, _admin!.email);
              HiveUtils.set(HiveKeys.user, _admin!.adminId);
              HiveUtils.set(HiveKeys.userName, _admin!.username);
              HiveUtils.set(HiveKeys.avatarImage, _admin!.avatarImage);
              HiveUtils.set(HiveKeys.fullName,
                  '${_admin!.firstname.capitalize()} ${_admin!.lastname.capitalize()}');
              HiveUtils.set(HiveKeys.facilityId, _admin!.facility.toString());
              emit(_Success(false, admin: _admin));
            } else {
              _login = Profile.fromJson(jsonDecode(response.body)['payload']);
              HiveUtils.set(HiveKeys.email, _login!.email);
              HiveUtils.set(HiveKeys.user, _login!.user);
              HiveUtils.set(HiveKeys.userName, _login!.username);
              HiveUtils.set(HiveKeys.position, _login!.clinicianPosition);
              HiveUtils.set(HiveKeys.avatarImage, _login!.avatarImage);
              HiveUtils.set(HiveKeys.fullName,
                  '${_login!.firstname.capitalize()} ${_login!.lastname.capitalize()}');
              emit(_Success(false, response: _login));
            }
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _EditProfile) {
        emit(const _Loading());
        if (event.value) {
          if (event.isAdmin) {
            emit(_Success(true, admin: _admin));
          } else {
            emit(_Success(true, response: _login));
          }
        } else {
          try {
            final response = event.isAdmin
                ? await adminApiProvider.updateProfile(
                    userData: {}, id: HiveUtils.get(HiveKeys.adminId))
                : await apiProvider.updateProfile(
                    id: HiveUtils.get(HiveKeys.clinicianId), userData: {});
            if (response.statusCode == 200) {
              if (event.isAdmin) {
                _admin = Admin.fromJson(jsonDecode(response.body)['payload']);
                emit(_Success(false, admin: _admin));
              } else {
                _login = Profile.fromJson(jsonDecode(response.body)['payload']);
                emit(_Success(false, response: _login));
              }
            } else {
              emit(_Error(response.body));
            }
          } catch (e) {
            emit(_Error(e.toString()));
          }
        }
      }
    });
  }
}
