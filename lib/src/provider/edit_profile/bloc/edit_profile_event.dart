part of 'edit_profile_bloc.dart';

@freezed
class EditProfileEvent with _$EditProfileEvent {
  const factory EditProfileEvent.editProfile(bool value,{required bool isAdmin}) = _EditProfile;
  const factory EditProfileEvent.saveChanges(dynamic user, {required bool isAdmin}) = _SaveChanges;
}
