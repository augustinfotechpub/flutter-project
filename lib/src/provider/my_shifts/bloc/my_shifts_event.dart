part of 'my_shifts_bloc.dart';

@freezed
class MyShiftsEvent with _$MyShiftsEvent {
  const factory MyShiftsEvent.getMyShifts(
      {required String fromDate,
      required String toDate,
      required DateTime focusDay}) = _GetMyShifts;
  const factory MyShiftsEvent.refreshMyShifts(
      {required String fromDate,
      required String toDate,
      required DateTime focusDay}) = _RefreshMyShifts;
  const factory MyShiftsEvent.filterMyShifts({required String date}) =
      _FilterMyShifts;
  const factory MyShiftsEvent.getOnShifts(String date) = _GetOnShifts;
  const factory MyShiftsEvent.refresh() = _Refresh;
  const factory MyShiftsEvent.editShift(
      {required String id, required Map<String, dynamic> data}) = _EditShift;
}
