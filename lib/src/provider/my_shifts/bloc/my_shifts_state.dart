part of 'my_shifts_bloc.dart';

@freezed
class MyShiftsState with _$MyShiftsState {
  const factory MyShiftsState.initial() = _Initial;

  const factory MyShiftsState.loading() = _Loading;

  const factory MyShiftsState.filtered(String date) = _Filtered;

  const factory MyShiftsState.success(
    List<Shift> shifts,
    String date,
  ) = _Success;

  const factory MyShiftsState.onShiftSuccess(
    List<Shift> shifts,
  ) = _OnShiftSuccess;
  const factory MyShiftsState.error(String error) = _Error;

  const factory MyShiftsState.refreshList({
    required List<Shift> onShift,
    required List<Shift> upcomingShift,
    required int random,
  }) = _RefreshList;
}
