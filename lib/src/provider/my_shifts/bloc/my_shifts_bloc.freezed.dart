// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'my_shifts_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MyShiftsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyShiftsEventCopyWith<$Res> {
  factory $MyShiftsEventCopyWith(
          MyShiftsEvent value, $Res Function(MyShiftsEvent) then) =
      _$MyShiftsEventCopyWithImpl<$Res, MyShiftsEvent>;
}

/// @nodoc
class _$MyShiftsEventCopyWithImpl<$Res, $Val extends MyShiftsEvent>
    implements $MyShiftsEventCopyWith<$Res> {
  _$MyShiftsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetMyShiftsCopyWith<$Res> {
  factory _$$_GetMyShiftsCopyWith(
          _$_GetMyShifts value, $Res Function(_$_GetMyShifts) then) =
      __$$_GetMyShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String fromDate, String toDate, DateTime focusDay});
}

/// @nodoc
class __$$_GetMyShiftsCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_GetMyShifts>
    implements _$$_GetMyShiftsCopyWith<$Res> {
  __$$_GetMyShiftsCopyWithImpl(
      _$_GetMyShifts _value, $Res Function(_$_GetMyShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
    Object? focusDay = null,
  }) {
    return _then(_$_GetMyShifts(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      focusDay: null == focusDay
          ? _value.focusDay
          : focusDay // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_GetMyShifts implements _GetMyShifts {
  const _$_GetMyShifts(
      {required this.fromDate, required this.toDate, required this.focusDay});

  @override
  final String fromDate;
  @override
  final String toDate;
  @override
  final DateTime focusDay;

  @override
  String toString() {
    return 'MyShiftsEvent.getMyShifts(fromDate: $fromDate, toDate: $toDate, focusDay: $focusDay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetMyShifts &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.focusDay, focusDay) ||
                other.focusDay == focusDay));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate, focusDay);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetMyShiftsCopyWith<_$_GetMyShifts> get copyWith =>
      __$$_GetMyShiftsCopyWithImpl<_$_GetMyShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return getMyShifts(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return getMyShifts?.call(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (getMyShifts != null) {
      return getMyShifts(fromDate, toDate, focusDay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return getMyShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return getMyShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (getMyShifts != null) {
      return getMyShifts(this);
    }
    return orElse();
  }
}

abstract class _GetMyShifts implements MyShiftsEvent {
  const factory _GetMyShifts(
      {required final String fromDate,
      required final String toDate,
      required final DateTime focusDay}) = _$_GetMyShifts;

  String get fromDate;
  String get toDate;
  DateTime get focusDay;
  @JsonKey(ignore: true)
  _$$_GetMyShiftsCopyWith<_$_GetMyShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshMyShiftsCopyWith<$Res> {
  factory _$$_RefreshMyShiftsCopyWith(
          _$_RefreshMyShifts value, $Res Function(_$_RefreshMyShifts) then) =
      __$$_RefreshMyShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String fromDate, String toDate, DateTime focusDay});
}

/// @nodoc
class __$$_RefreshMyShiftsCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_RefreshMyShifts>
    implements _$$_RefreshMyShiftsCopyWith<$Res> {
  __$$_RefreshMyShiftsCopyWithImpl(
      _$_RefreshMyShifts _value, $Res Function(_$_RefreshMyShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDate = null,
    Object? toDate = null,
    Object? focusDay = null,
  }) {
    return _then(_$_RefreshMyShifts(
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      focusDay: null == focusDay
          ? _value.focusDay
          : focusDay // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_RefreshMyShifts implements _RefreshMyShifts {
  const _$_RefreshMyShifts(
      {required this.fromDate, required this.toDate, required this.focusDay});

  @override
  final String fromDate;
  @override
  final String toDate;
  @override
  final DateTime focusDay;

  @override
  String toString() {
    return 'MyShiftsEvent.refreshMyShifts(fromDate: $fromDate, toDate: $toDate, focusDay: $focusDay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshMyShifts &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.focusDay, focusDay) ||
                other.focusDay == focusDay));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDate, toDate, focusDay);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshMyShiftsCopyWith<_$_RefreshMyShifts> get copyWith =>
      __$$_RefreshMyShiftsCopyWithImpl<_$_RefreshMyShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return refreshMyShifts(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return refreshMyShifts?.call(fromDate, toDate, focusDay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (refreshMyShifts != null) {
      return refreshMyShifts(fromDate, toDate, focusDay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return refreshMyShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return refreshMyShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (refreshMyShifts != null) {
      return refreshMyShifts(this);
    }
    return orElse();
  }
}

abstract class _RefreshMyShifts implements MyShiftsEvent {
  const factory _RefreshMyShifts(
      {required final String fromDate,
      required final String toDate,
      required final DateTime focusDay}) = _$_RefreshMyShifts;

  String get fromDate;
  String get toDate;
  DateTime get focusDay;
  @JsonKey(ignore: true)
  _$$_RefreshMyShiftsCopyWith<_$_RefreshMyShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_FilterMyShiftsCopyWith<$Res> {
  factory _$$_FilterMyShiftsCopyWith(
          _$_FilterMyShifts value, $Res Function(_$_FilterMyShifts) then) =
      __$$_FilterMyShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String date});
}

/// @nodoc
class __$$_FilterMyShiftsCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_FilterMyShifts>
    implements _$$_FilterMyShiftsCopyWith<$Res> {
  __$$_FilterMyShiftsCopyWithImpl(
      _$_FilterMyShifts _value, $Res Function(_$_FilterMyShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
  }) {
    return _then(_$_FilterMyShifts(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FilterMyShifts implements _FilterMyShifts {
  const _$_FilterMyShifts({required this.date});

  @override
  final String date;

  @override
  String toString() {
    return 'MyShiftsEvent.filterMyShifts(date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FilterMyShifts &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode => Object.hash(runtimeType, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FilterMyShiftsCopyWith<_$_FilterMyShifts> get copyWith =>
      __$$_FilterMyShiftsCopyWithImpl<_$_FilterMyShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return filterMyShifts(date);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return filterMyShifts?.call(date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (filterMyShifts != null) {
      return filterMyShifts(date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return filterMyShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return filterMyShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (filterMyShifts != null) {
      return filterMyShifts(this);
    }
    return orElse();
  }
}

abstract class _FilterMyShifts implements MyShiftsEvent {
  const factory _FilterMyShifts({required final String date}) =
      _$_FilterMyShifts;

  String get date;
  @JsonKey(ignore: true)
  _$$_FilterMyShiftsCopyWith<_$_FilterMyShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GetOnShiftsCopyWith<$Res> {
  factory _$$_GetOnShiftsCopyWith(
          _$_GetOnShifts value, $Res Function(_$_GetOnShifts) then) =
      __$$_GetOnShiftsCopyWithImpl<$Res>;
  @useResult
  $Res call({String date});
}

/// @nodoc
class __$$_GetOnShiftsCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_GetOnShifts>
    implements _$$_GetOnShiftsCopyWith<$Res> {
  __$$_GetOnShiftsCopyWithImpl(
      _$_GetOnShifts _value, $Res Function(_$_GetOnShifts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
  }) {
    return _then(_$_GetOnShifts(
      null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_GetOnShifts implements _GetOnShifts {
  const _$_GetOnShifts(this.date);

  @override
  final String date;

  @override
  String toString() {
    return 'MyShiftsEvent.getOnShifts(date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetOnShifts &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode => Object.hash(runtimeType, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetOnShiftsCopyWith<_$_GetOnShifts> get copyWith =>
      __$$_GetOnShiftsCopyWithImpl<_$_GetOnShifts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return getOnShifts(date);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return getOnShifts?.call(date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (getOnShifts != null) {
      return getOnShifts(date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return getOnShifts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return getOnShifts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (getOnShifts != null) {
      return getOnShifts(this);
    }
    return orElse();
  }
}

abstract class _GetOnShifts implements MyShiftsEvent {
  const factory _GetOnShifts(final String date) = _$_GetOnShifts;

  String get date;
  @JsonKey(ignore: true)
  _$$_GetOnShiftsCopyWith<_$_GetOnShifts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshCopyWith<$Res> {
  factory _$$_RefreshCopyWith(
          _$_Refresh value, $Res Function(_$_Refresh) then) =
      __$$_RefreshCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_RefreshCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_Refresh>
    implements _$$_RefreshCopyWith<$Res> {
  __$$_RefreshCopyWithImpl(_$_Refresh _value, $Res Function(_$_Refresh) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Refresh implements _Refresh {
  const _$_Refresh();

  @override
  String toString() {
    return 'MyShiftsEvent.refresh()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Refresh);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return refresh();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return refresh?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return refresh(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return refresh?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh(this);
    }
    return orElse();
  }
}

abstract class _Refresh implements MyShiftsEvent {
  const factory _Refresh() = _$_Refresh;
}

/// @nodoc
abstract class _$$_EditShiftCopyWith<$Res> {
  factory _$$_EditShiftCopyWith(
          _$_EditShift value, $Res Function(_$_EditShift) then) =
      __$$_EditShiftCopyWithImpl<$Res>;
  @useResult
  $Res call({String id, Map<String, dynamic> data});
}

/// @nodoc
class __$$_EditShiftCopyWithImpl<$Res>
    extends _$MyShiftsEventCopyWithImpl<$Res, _$_EditShift>
    implements _$$_EditShiftCopyWith<$Res> {
  __$$_EditShiftCopyWithImpl(
      _$_EditShift _value, $Res Function(_$_EditShift) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? data = null,
  }) {
    return _then(_$_EditShift(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      data: null == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc

class _$_EditShift implements _EditShift {
  const _$_EditShift(
      {required this.id, required final Map<String, dynamic> data})
      : _data = data;

  @override
  final String id;
  final Map<String, dynamic> _data;
  @override
  Map<String, dynamic> get data {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_data);
  }

  @override
  String toString() {
    return 'MyShiftsEvent.editShift(id: $id, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EditShift &&
            (identical(other.id, id) || other.id == id) &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, id, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EditShiftCopyWith<_$_EditShift> get copyWith =>
      __$$_EditShiftCopyWithImpl<_$_EditShift>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        getMyShifts,
    required TResult Function(String fromDate, String toDate, DateTime focusDay)
        refreshMyShifts,
    required TResult Function(String date) filterMyShifts,
    required TResult Function(String date) getOnShifts,
    required TResult Function() refresh,
    required TResult Function(String id, Map<String, dynamic> data) editShift,
  }) {
    return editShift(id, data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult? Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult? Function(String date)? filterMyShifts,
    TResult? Function(String date)? getOnShifts,
    TResult? Function()? refresh,
    TResult? Function(String id, Map<String, dynamic> data)? editShift,
  }) {
    return editShift?.call(id, data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        getMyShifts,
    TResult Function(String fromDate, String toDate, DateTime focusDay)?
        refreshMyShifts,
    TResult Function(String date)? filterMyShifts,
    TResult Function(String date)? getOnShifts,
    TResult Function()? refresh,
    TResult Function(String id, Map<String, dynamic> data)? editShift,
    required TResult orElse(),
  }) {
    if (editShift != null) {
      return editShift(id, data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetMyShifts value) getMyShifts,
    required TResult Function(_RefreshMyShifts value) refreshMyShifts,
    required TResult Function(_FilterMyShifts value) filterMyShifts,
    required TResult Function(_GetOnShifts value) getOnShifts,
    required TResult Function(_Refresh value) refresh,
    required TResult Function(_EditShift value) editShift,
  }) {
    return editShift(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetMyShifts value)? getMyShifts,
    TResult? Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult? Function(_FilterMyShifts value)? filterMyShifts,
    TResult? Function(_GetOnShifts value)? getOnShifts,
    TResult? Function(_Refresh value)? refresh,
    TResult? Function(_EditShift value)? editShift,
  }) {
    return editShift?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetMyShifts value)? getMyShifts,
    TResult Function(_RefreshMyShifts value)? refreshMyShifts,
    TResult Function(_FilterMyShifts value)? filterMyShifts,
    TResult Function(_GetOnShifts value)? getOnShifts,
    TResult Function(_Refresh value)? refresh,
    TResult Function(_EditShift value)? editShift,
    required TResult orElse(),
  }) {
    if (editShift != null) {
      return editShift(this);
    }
    return orElse();
  }
}

abstract class _EditShift implements MyShiftsEvent {
  const factory _EditShift(
      {required final String id,
      required final Map<String, dynamic> data}) = _$_EditShift;

  String get id;
  Map<String, dynamic> get data;
  @JsonKey(ignore: true)
  _$$_EditShiftCopyWith<_$_EditShift> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$MyShiftsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyShiftsStateCopyWith<$Res> {
  factory $MyShiftsStateCopyWith(
          MyShiftsState value, $Res Function(MyShiftsState) then) =
      _$MyShiftsStateCopyWithImpl<$Res, MyShiftsState>;
}

/// @nodoc
class _$MyShiftsStateCopyWithImpl<$Res, $Val extends MyShiftsState>
    implements $MyShiftsStateCopyWith<$Res> {
  _$MyShiftsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'MyShiftsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MyShiftsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'MyShiftsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements MyShiftsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_FilteredCopyWith<$Res> {
  factory _$$_FilteredCopyWith(
          _$_Filtered value, $Res Function(_$_Filtered) then) =
      __$$_FilteredCopyWithImpl<$Res>;
  @useResult
  $Res call({String date});
}

/// @nodoc
class __$$_FilteredCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_Filtered>
    implements _$$_FilteredCopyWith<$Res> {
  __$$_FilteredCopyWithImpl(
      _$_Filtered _value, $Res Function(_$_Filtered) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
  }) {
    return _then(_$_Filtered(
      null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Filtered implements _Filtered {
  const _$_Filtered(this.date);

  @override
  final String date;

  @override
  String toString() {
    return 'MyShiftsState.filtered(date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Filtered &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode => Object.hash(runtimeType, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FilteredCopyWith<_$_Filtered> get copyWith =>
      __$$_FilteredCopyWithImpl<_$_Filtered>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return filtered(date);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return filtered?.call(date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (filtered != null) {
      return filtered(date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return filtered(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return filtered?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (filtered != null) {
      return filtered(this);
    }
    return orElse();
  }
}

abstract class _Filtered implements MyShiftsState {
  const factory _Filtered(final String date) = _$_Filtered;

  String get date;
  @JsonKey(ignore: true)
  _$$_FilteredCopyWith<_$_Filtered> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Shift> shifts, String date});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shifts = null,
    Object? date = null,
  }) {
    return _then(_$_Success(
      null == shifts
          ? _value._shifts
          : shifts // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
      null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(final List<Shift> shifts, this.date) : _shifts = shifts;

  final List<Shift> _shifts;
  @override
  List<Shift> get shifts {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_shifts);
  }

  @override
  final String date;

  @override
  String toString() {
    return 'MyShiftsState.success(shifts: $shifts, date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._shifts, _shifts) &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_shifts), date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return success(shifts, date);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return success?.call(shifts, date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(shifts, date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements MyShiftsState {
  const factory _Success(final List<Shift> shifts, final String date) =
      _$_Success;

  List<Shift> get shifts;
  String get date;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_OnShiftSuccessCopyWith<$Res> {
  factory _$$_OnShiftSuccessCopyWith(
          _$_OnShiftSuccess value, $Res Function(_$_OnShiftSuccess) then) =
      __$$_OnShiftSuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Shift> shifts});
}

/// @nodoc
class __$$_OnShiftSuccessCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_OnShiftSuccess>
    implements _$$_OnShiftSuccessCopyWith<$Res> {
  __$$_OnShiftSuccessCopyWithImpl(
      _$_OnShiftSuccess _value, $Res Function(_$_OnShiftSuccess) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shifts = null,
  }) {
    return _then(_$_OnShiftSuccess(
      null == shifts
          ? _value._shifts
          : shifts // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
    ));
  }
}

/// @nodoc

class _$_OnShiftSuccess implements _OnShiftSuccess {
  const _$_OnShiftSuccess(final List<Shift> shifts) : _shifts = shifts;

  final List<Shift> _shifts;
  @override
  List<Shift> get shifts {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_shifts);
  }

  @override
  String toString() {
    return 'MyShiftsState.onShiftSuccess(shifts: $shifts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OnShiftSuccess &&
            const DeepCollectionEquality().equals(other._shifts, _shifts));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_shifts));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_OnShiftSuccessCopyWith<_$_OnShiftSuccess> get copyWith =>
      __$$_OnShiftSuccessCopyWithImpl<_$_OnShiftSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return onShiftSuccess(shifts);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return onShiftSuccess?.call(shifts);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (onShiftSuccess != null) {
      return onShiftSuccess(shifts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return onShiftSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return onShiftSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (onShiftSuccess != null) {
      return onShiftSuccess(this);
    }
    return orElse();
  }
}

abstract class _OnShiftSuccess implements MyShiftsState {
  const factory _OnShiftSuccess(final List<Shift> shifts) = _$_OnShiftSuccess;

  List<Shift> get shifts;
  @JsonKey(ignore: true)
  _$$_OnShiftSuccessCopyWith<_$_OnShiftSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'MyShiftsState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements MyShiftsState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RefreshListCopyWith<$Res> {
  factory _$$_RefreshListCopyWith(
          _$_RefreshList value, $Res Function(_$_RefreshList) then) =
      __$$_RefreshListCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Shift> onShift, List<Shift> upcomingShift, int random});
}

/// @nodoc
class __$$_RefreshListCopyWithImpl<$Res>
    extends _$MyShiftsStateCopyWithImpl<$Res, _$_RefreshList>
    implements _$$_RefreshListCopyWith<$Res> {
  __$$_RefreshListCopyWithImpl(
      _$_RefreshList _value, $Res Function(_$_RefreshList) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? onShift = null,
    Object? upcomingShift = null,
    Object? random = null,
  }) {
    return _then(_$_RefreshList(
      onShift: null == onShift
          ? _value._onShift
          : onShift // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
      upcomingShift: null == upcomingShift
          ? _value._upcomingShift
          : upcomingShift // ignore: cast_nullable_to_non_nullable
              as List<Shift>,
      random: null == random
          ? _value.random
          : random // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_RefreshList implements _RefreshList {
  const _$_RefreshList(
      {required final List<Shift> onShift,
      required final List<Shift> upcomingShift,
      required this.random})
      : _onShift = onShift,
        _upcomingShift = upcomingShift;

  final List<Shift> _onShift;
  @override
  List<Shift> get onShift {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_onShift);
  }

  final List<Shift> _upcomingShift;
  @override
  List<Shift> get upcomingShift {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_upcomingShift);
  }

  @override
  final int random;

  @override
  String toString() {
    return 'MyShiftsState.refreshList(onShift: $onShift, upcomingShift: $upcomingShift, random: $random)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RefreshList &&
            const DeepCollectionEquality().equals(other._onShift, _onShift) &&
            const DeepCollectionEquality()
                .equals(other._upcomingShift, _upcomingShift) &&
            (identical(other.random, random) || other.random == random));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_onShift),
      const DeepCollectionEquality().hash(_upcomingShift),
      random);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RefreshListCopyWith<_$_RefreshList> get copyWith =>
      __$$_RefreshListCopyWithImpl<_$_RefreshList>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String date) filtered,
    required TResult Function(List<Shift> shifts, String date) success,
    required TResult Function(List<Shift> shifts) onShiftSuccess,
    required TResult Function(String error) error,
    required TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)
        refreshList,
  }) {
    return refreshList(onShift, upcomingShift, random);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String date)? filtered,
    TResult? Function(List<Shift> shifts, String date)? success,
    TResult? Function(List<Shift> shifts)? onShiftSuccess,
    TResult? Function(String error)? error,
    TResult? Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
  }) {
    return refreshList?.call(onShift, upcomingShift, random);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String date)? filtered,
    TResult Function(List<Shift> shifts, String date)? success,
    TResult Function(List<Shift> shifts)? onShiftSuccess,
    TResult Function(String error)? error,
    TResult Function(
            List<Shift> onShift, List<Shift> upcomingShift, int random)?
        refreshList,
    required TResult orElse(),
  }) {
    if (refreshList != null) {
      return refreshList(onShift, upcomingShift, random);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Filtered value) filtered,
    required TResult Function(_Success value) success,
    required TResult Function(_OnShiftSuccess value) onShiftSuccess,
    required TResult Function(_Error value) error,
    required TResult Function(_RefreshList value) refreshList,
  }) {
    return refreshList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Filtered value)? filtered,
    TResult? Function(_Success value)? success,
    TResult? Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult? Function(_Error value)? error,
    TResult? Function(_RefreshList value)? refreshList,
  }) {
    return refreshList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Filtered value)? filtered,
    TResult Function(_Success value)? success,
    TResult Function(_OnShiftSuccess value)? onShiftSuccess,
    TResult Function(_Error value)? error,
    TResult Function(_RefreshList value)? refreshList,
    required TResult orElse(),
  }) {
    if (refreshList != null) {
      return refreshList(this);
    }
    return orElse();
  }
}

abstract class _RefreshList implements MyShiftsState {
  const factory _RefreshList(
      {required final List<Shift> onShift,
      required final List<Shift> upcomingShift,
      required final int random}) = _$_RefreshList;

  List<Shift> get onShift;
  List<Shift> get upcomingShift;
  int get random;
  @JsonKey(ignore: true)
  _$$_RefreshListCopyWith<_$_RefreshList> get copyWith =>
      throw _privateConstructorUsedError;
}
