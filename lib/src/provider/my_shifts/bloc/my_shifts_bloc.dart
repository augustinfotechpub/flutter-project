import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/extension.dart';

part 'my_shifts_event.dart';
part 'my_shifts_state.dart';
part 'my_shifts_bloc.freezed.dart';

List<Shift> _myShifts = [];
List<Shift> _onShifts = [];

class MyShiftsBloc extends Bloc<MyShiftsEvent, MyShiftsState> {
  MyShiftsBloc() : super(const _Initial()) {
    on<MyShiftsEvent>((event, emit) async {
      if (event is _GetMyShifts) {
        emit(const _Loading());
        try {
          final shift = await apiProvider.getUpcomingShifts(
              fromDate: event.fromDate,
              toDate: event.toDate,
              id: HiveUtils.get(HiveKeys.clinicianId));
          if (shift.statusCode == 200) {
            UpcomingShift upcomingShift =
                UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
            _myShifts = List.from(upcomingShift.shift);
            _myShifts
                .removeWhere((e) => e.status.isPending() || e.status.isOpen());
            emit(_Success(_myShifts, event.focusDay.dmY()));
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _RefreshMyShifts) {
        try {
          final shift = await apiProvider.getUpcomingShifts(
              fromDate: event.fromDate,
              toDate: event.toDate,
              id: HiveUtils.get(HiveKeys.clinicianId));
          if (shift.statusCode == 200) {
            UpcomingShift upcomingShift =
                UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
            _myShifts = List.from(upcomingShift.shift);
            _myShifts
                .removeWhere((e) => e.status.isPending() || e.status.isOpen());
            emit(_Success(_myShifts, event.focusDay.dmY()));
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _FilterMyShifts) {
        emit(_Filtered(event.date));
      }
      if (event is _Refresh) {
        await Future.delayed(const Duration(seconds: 1));
        emit(
          _RefreshList(
            onShift: _onShifts,
            upcomingShift: _myShifts,
            random: randomNumber(),
          ),
        );
      }
      if (event is _GetOnShifts) {
        emit(const _Loading());
        try {
          final shift = await apiProvider.getOnShifts(
              date: event.date, id: HiveUtils.get(HiveKeys.clinicianId));
          if (shift.statusCode == 200) {
            UpcomingShift upcomingShift =
                UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
            _onShifts = List.from(upcomingShift.shift);
            _onShifts
                .removeWhere((e) => e.status.isPending() || e.status.isOpen());
            emit(_OnShiftSuccess(_onShifts));
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
      if (event is _EditShift) {
        try {
          final shift =
              await apiProvider.editShift(id: event.id, data: event.data);
          if (shift.statusCode == 200) {
            final abc = jsonDecode(shift.body);
            if (abc is Map) {
              UpcomingShift onShift = UpcomingShift.fromJson({
                'shift': [abc]
              });
              emit(_OnShiftSuccess(onShift.shift));
            } else {
              UpcomingShift onShift =
                  UpcomingShift.fromJson({'shift': jsonDecode(shift.body)});
              emit(_OnShiftSuccess(onShift.shift));
            }
          } else {
            emit(_Error(jsonDecode(shift.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}

MyShiftsBloc myShiftsBloc = MyShiftsBloc();
