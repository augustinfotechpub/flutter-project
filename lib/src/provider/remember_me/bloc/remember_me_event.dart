part of 'remember_me_bloc.dart';

@freezed
class RememberMeEvent with _$RememberMeEvent {
  const factory RememberMeEvent.rememberMe(bool value) = _RememberMe;
}