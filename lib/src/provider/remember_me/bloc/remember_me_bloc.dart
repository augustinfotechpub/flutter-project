import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'remember_me_event.dart';
part 'remember_me_state.dart';
part 'remember_me_bloc.freezed.dart';

class RememberMeBloc extends Bloc<RememberMeEvent, RememberMeState> {
  RememberMeBloc() : super(const _Initial(false)) {
    on<RememberMeEvent>((event, emit) {
      if (event is _RememberMe) {
        HiveUtils.set(HiveKeys.rememberMe, event.value);
        emit(_Initial(event.value));
      }
    });
  }
}
