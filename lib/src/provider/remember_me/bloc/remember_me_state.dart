part of 'remember_me_bloc.dart';

@freezed
class RememberMeState with _$RememberMeState {
  const factory RememberMeState.initial(bool value) = _Initial;
}
