import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';

part 'forgot_password_event.dart';
part 'forgot_password_state.dart';
part 'forgot_password_bloc.freezed.dart';

class ForgotPasswordBloc
    extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  ForgotPasswordBloc() : super(const _Initial()) {
    on<ForgotPasswordEvent>((event, emit) async {
      if (event is _ResetPassword) {
        emit(const _Loading());
      }
      if (event.email != '') {
        try {
          final response = await apiProvider.resetPassword(email: event.email);
          if (response.statusCode == 200) {
            emit(_Success(jsonDecode(response.body)['msg']));
          } else {
            emit(const _Error('Please enter valid email'));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      } else {
        emit(const _Error('Please enter email address'));
      }
    });
  }
}
