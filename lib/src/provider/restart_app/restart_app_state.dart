part of 'restart_app_bloc.dart';

@freezed
class RestartAppState with _$RestartAppState {
  const factory RestartAppState.initial() = _Initial;
  const factory RestartAppState.loading() = _Loading;
  const factory RestartAppState.success() = _Success;
}
