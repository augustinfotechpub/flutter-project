part of 'restart_app_bloc.dart';

@freezed
class RestartAppEvent with _$RestartAppEvent {
  const factory RestartAppEvent.restart() = _Restart;
}