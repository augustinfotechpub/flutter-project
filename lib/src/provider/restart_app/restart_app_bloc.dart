import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'restart_app_event.dart';
part 'restart_app_state.dart';
part 'restart_app_bloc.freezed.dart';

class RestartAppBloc extends Bloc<RestartAppEvent, RestartAppState> {
  RestartAppBloc() : super(const _Initial()) {
    on<RestartAppEvent>((event, emit) {
      emit(const _Loading());
      if (event is _Restart) {
        emit(const _Success());
      }
    });
  }
}

final RestartAppBloc restartAppBloc = RestartAppBloc();
