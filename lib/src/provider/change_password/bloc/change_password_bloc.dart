import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'change_password_event.dart';
part 'change_password_state.dart';
part 'change_password_bloc.freezed.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  ChangePasswordBloc() : super(const _Initial()) {
    on<ChangePasswordEvent>((event, emit) async {
      if (event is _ChangePassword) {
        emit(const _Loading());
      }
      if (event.oldPassword != '' &&
          event.newPassword != '' &&
          event.confirmPassword != '') {
        if (event.oldPassword == HiveUtils.get(HiveKeys.password)) {
          if (event.newPassword == event.confirmPassword) {
            try {
              final response = await apiProvider.changePassword(
                  oldPassword: event.oldPassword,
                  newPassword: event.newPassword,
                  confirmPassword: event.confirmPassword);
              if (response.statusCode == 200) {
                emit(_Success(jsonDecode(response.body)['msg']));
              } else {
                emit(_Error(response.body));
              }
            } catch (e) {
              emit(_Error(e.toString()));
            }
          } else {
            emit(const _Error('Password doesn\'t match'));
          }
        } else {
          emit(const _Error('Current password is wrong'));
        }
      } else {
        emit(const _Error('Please fill all fileds'));
      }
    });
  }
}
