part of 'upload_document_bloc.dart';

@freezed
class UploadDocumentEvent with _$UploadDocumentEvent {
  const factory UploadDocumentEvent.upload({
    required Map<String, dynamic> doc,
    required String base64string,
  }) = _Upload;
}
