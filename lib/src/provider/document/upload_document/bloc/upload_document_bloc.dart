import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';

part 'upload_document_event.dart';
part 'upload_document_state.dart';
part 'upload_document_bloc.freezed.dart';

class UploadDocumentBloc
    extends Bloc<UploadDocumentEvent, UploadDocumentState> {
  UploadDocumentBloc() : super(const _Initial()) {
    on<UploadDocumentEvent>((event, emit) async {
      if (event is _Upload) {
        emit(const _Loading());
        try {
          final location = await apiProvider.uploadDocumentFile(doc: {
            "user_id": "${HiveUtils.get(HiveKeys.user)}",
            "data": event.base64string
          });
          if (location.statusCode == 200) {
            final map = Map<String, dynamic>.from(event.doc);
            map.addEntries(
              {"document_path": jsonDecode(location.body)["location"]}.entries,
            );
            try {
              final response = await apiProvider.uploadDocument(doc: map);
              if (response.statusCode == 201) {
                emit(const _Success());
              } else {
                emit(_Error(response.body));
              }
            } catch (e) {
              emit(_Error(e.toString()));
            }
          } else {
            emit(_Error(location.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
  Future<void> sendNotification() async {
    List<String> adminId = [];
    final admins = await apiProvider.getAdmins();
    for (var admin in jsonDecode(admins.body)['results']) {
      admin = Admin.fromJson(admin);
      if (admin.role == 'hmc_admin' || admin.role == 'poc_admin') {
        adminId.add(admin.adminId.toString());
      }
    }
    for (String id in adminId) {
      await apiProvider.postNotification(map: {
        'user_id': id,
        'notification':
            '${HiveUtils.get(HiveKeys.fullName)} submitted for poc request.',
        'created_time': '${DateTime.now()}',
        'seen': 'active',
      });
    }
  }
}
