part of 'view_document_bloc.dart';

@freezed
class ViewDocumentState with _$ViewDocumentState {
  const factory ViewDocumentState.initial() = _Initial;
  const factory ViewDocumentState.loading() = _Loading;
  const factory ViewDocumentState.success(ViewDocument viewdocs) = _Success;
  const factory ViewDocumentState.error(String error) = _Error;
}
