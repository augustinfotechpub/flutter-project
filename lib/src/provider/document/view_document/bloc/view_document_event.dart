part of 'view_document_bloc.dart';

@freezed
class ViewDocumentEvent with _$ViewDocumentEvent {
  const factory ViewDocumentEvent.getDocument() = _GetDocument;
  const factory ViewDocumentEvent.deleteDocument({required int id}) = _DeleteDocument;
}