import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/document/view_document/model.dart';
import 'package:shift_alerts/src/utils/hive/hive_keys.dart';
import 'package:shift_alerts/src/utils/hive/hive_utils.dart';

part 'view_document_event.dart';
part 'view_document_state.dart';
part 'view_document_bloc.freezed.dart';

class ViewDocumentBloc extends Bloc<ViewDocumentEvent, ViewDocumentState> {
  ViewDocumentBloc() : super(const _Initial()) {
    on<ViewDocumentEvent>((event, emit) async {
      Future<void> httpProcess() async {
        try {
          final docs = await apiProvider.getDocuments(
            clinicianID: HiveUtils.get(HiveKeys.clinicianId),
          );
          if (docs.statusCode == 200) {
            ViewDocument document =
                ViewDocument.fromJson(jsonDecode(docs.body));
            emit(_Success(document));
          } else {
            emit(_Error(jsonDecode(docs.body)));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }

      if (event is _GetDocument) {
        emit(const _Loading());
        await httpProcess();
      }

      
      if (event is _DeleteDocument) {
        emit(const _Loading());
        try {
          final response =
              await apiProvider.deleteDocument(documentID: event.id);
          if (response.statusCode == 204) {
            await httpProcess();
          } else {
            emit(_Error(response.body));
          }
        } catch (e) {
          emit(_Error(e.toString()));
        }
      }
    });
  }
}
