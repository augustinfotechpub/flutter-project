// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ViewDocument _$$_ViewDocumentFromJson(Map<String, dynamic> json) =>
    _$_ViewDocument(
      count: json['count'] as int,
      results: (json['results'] as List<dynamic>)
          .map((e) => Document.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ViewDocumentToJson(_$_ViewDocument instance) =>
    <String, dynamic>{
      'count': instance.count,
      'results': instance.results,
    };

_$_Document _$$_DocumentFromJson(Map<String, dynamic> json) => _$_Document(
      documentId: json['document_id'] as int,
      clinicianName: json['clinician_name'] as String,
      clinicianPosition: json['clinician_position'] as String,
      clinicianAvatar: json['clinician_avatar'] as String,
      documentName: json['document_name'] as String,
      documentPath: json['document_path'] as String,
      documentUpdatedDate:
          DateTime.parse(json['document_updated_date'] as String),
      documentStatus: json['document_status'] as String,
      clinicianId: json['clinician_id'] as int,
    );

Map<String, dynamic> _$$_DocumentToJson(_$_Document instance) =>
    <String, dynamic>{
      'document_id': instance.documentId,
      'clinician_name': instance.clinicianName,
      'clinician_position': instance.clinicianPosition,
      'clinician_avatar': instance.clinicianAvatar,
      'document_name': instance.documentName,
      'document_path': instance.documentPath,
      'document_updated_date': instance.documentUpdatedDate.toIso8601String(),
      'document_status': instance.documentStatus,
      'clinician_id': instance.clinicianId,
    };
