// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';
part 'model.g.dart';

@freezed
class ViewDocument with _$ViewDocument {
  const factory ViewDocument({
    required int count,
    required List<Document> results,
  }) = _ViewDocument;

  factory ViewDocument.fromJson(Map<String, dynamic> json) =>
      _$ViewDocumentFromJson(json);
}

@freezed
class Document with _$Document {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Document({
    required int documentId,
    required String clinicianName,
    required String clinicianPosition,
    required String clinicianAvatar,
    required String documentName,
    required String documentPath,
    required DateTime documentUpdatedDate,
    required String documentStatus,
    required int clinicianId,
  }) = _Document;

  factory Document.fromJson(Map<String, dynamic> json) =>
      _$DocumentFromJson(json);
}
