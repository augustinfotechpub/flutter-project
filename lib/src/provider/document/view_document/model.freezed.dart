// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ViewDocument _$ViewDocumentFromJson(Map<String, dynamic> json) {
  return _ViewDocument.fromJson(json);
}

/// @nodoc
mixin _$ViewDocument {
  int get count => throw _privateConstructorUsedError;
  List<Document> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ViewDocumentCopyWith<ViewDocument> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ViewDocumentCopyWith<$Res> {
  factory $ViewDocumentCopyWith(
          ViewDocument value, $Res Function(ViewDocument) then) =
      _$ViewDocumentCopyWithImpl<$Res, ViewDocument>;
  @useResult
  $Res call({int count, List<Document> results});
}

/// @nodoc
class _$ViewDocumentCopyWithImpl<$Res, $Val extends ViewDocument>
    implements $ViewDocumentCopyWith<$Res> {
  _$ViewDocumentCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? count = null,
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ViewDocumentCopyWith<$Res>
    implements $ViewDocumentCopyWith<$Res> {
  factory _$$_ViewDocumentCopyWith(
          _$_ViewDocument value, $Res Function(_$_ViewDocument) then) =
      __$$_ViewDocumentCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int count, List<Document> results});
}

/// @nodoc
class __$$_ViewDocumentCopyWithImpl<$Res>
    extends _$ViewDocumentCopyWithImpl<$Res, _$_ViewDocument>
    implements _$$_ViewDocumentCopyWith<$Res> {
  __$$_ViewDocumentCopyWithImpl(
      _$_ViewDocument _value, $Res Function(_$_ViewDocument) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? count = null,
    Object? results = null,
  }) {
    return _then(_$_ViewDocument(
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ViewDocument implements _ViewDocument {
  const _$_ViewDocument(
      {required this.count, required final List<Document> results})
      : _results = results;

  factory _$_ViewDocument.fromJson(Map<String, dynamic> json) =>
      _$$_ViewDocumentFromJson(json);

  @override
  final int count;
  final List<Document> _results;
  @override
  List<Document> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'ViewDocument(count: $count, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ViewDocument &&
            (identical(other.count, count) || other.count == count) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, count, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ViewDocumentCopyWith<_$_ViewDocument> get copyWith =>
      __$$_ViewDocumentCopyWithImpl<_$_ViewDocument>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ViewDocumentToJson(
      this,
    );
  }
}

abstract class _ViewDocument implements ViewDocument {
  const factory _ViewDocument(
      {required final int count,
      required final List<Document> results}) = _$_ViewDocument;

  factory _ViewDocument.fromJson(Map<String, dynamic> json) =
      _$_ViewDocument.fromJson;

  @override
  int get count;
  @override
  List<Document> get results;
  @override
  @JsonKey(ignore: true)
  _$$_ViewDocumentCopyWith<_$_ViewDocument> get copyWith =>
      throw _privateConstructorUsedError;
}

Document _$DocumentFromJson(Map<String, dynamic> json) {
  return _Document.fromJson(json);
}

/// @nodoc
mixin _$Document {
  int get documentId => throw _privateConstructorUsedError;
  String get clinicianName => throw _privateConstructorUsedError;
  String get clinicianPosition => throw _privateConstructorUsedError;
  String get clinicianAvatar => throw _privateConstructorUsedError;
  String get documentName => throw _privateConstructorUsedError;
  String get documentPath => throw _privateConstructorUsedError;
  DateTime get documentUpdatedDate => throw _privateConstructorUsedError;
  String get documentStatus => throw _privateConstructorUsedError;
  int get clinicianId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DocumentCopyWith<Document> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocumentCopyWith<$Res> {
  factory $DocumentCopyWith(Document value, $Res Function(Document) then) =
      _$DocumentCopyWithImpl<$Res, Document>;
  @useResult
  $Res call(
      {int documentId,
      String clinicianName,
      String clinicianPosition,
      String clinicianAvatar,
      String documentName,
      String documentPath,
      DateTime documentUpdatedDate,
      String documentStatus,
      int clinicianId});
}

/// @nodoc
class _$DocumentCopyWithImpl<$Res, $Val extends Document>
    implements $DocumentCopyWith<$Res> {
  _$DocumentCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? documentId = null,
    Object? clinicianName = null,
    Object? clinicianPosition = null,
    Object? clinicianAvatar = null,
    Object? documentName = null,
    Object? documentPath = null,
    Object? documentUpdatedDate = null,
    Object? documentStatus = null,
    Object? clinicianId = null,
  }) {
    return _then(_value.copyWith(
      documentId: null == documentId
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianAvatar: null == clinicianAvatar
          ? _value.clinicianAvatar
          : clinicianAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      documentName: null == documentName
          ? _value.documentName
          : documentName // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: null == documentPath
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      documentUpdatedDate: null == documentUpdatedDate
          ? _value.documentUpdatedDate
          : documentUpdatedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      documentStatus: null == documentStatus
          ? _value.documentStatus
          : documentStatus // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DocumentCopyWith<$Res> implements $DocumentCopyWith<$Res> {
  factory _$$_DocumentCopyWith(
          _$_Document value, $Res Function(_$_Document) then) =
      __$$_DocumentCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int documentId,
      String clinicianName,
      String clinicianPosition,
      String clinicianAvatar,
      String documentName,
      String documentPath,
      DateTime documentUpdatedDate,
      String documentStatus,
      int clinicianId});
}

/// @nodoc
class __$$_DocumentCopyWithImpl<$Res>
    extends _$DocumentCopyWithImpl<$Res, _$_Document>
    implements _$$_DocumentCopyWith<$Res> {
  __$$_DocumentCopyWithImpl(
      _$_Document _value, $Res Function(_$_Document) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? documentId = null,
    Object? clinicianName = null,
    Object? clinicianPosition = null,
    Object? clinicianAvatar = null,
    Object? documentName = null,
    Object? documentPath = null,
    Object? documentUpdatedDate = null,
    Object? documentStatus = null,
    Object? clinicianId = null,
  }) {
    return _then(_$_Document(
      documentId: null == documentId
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as int,
      clinicianName: null == clinicianName
          ? _value.clinicianName
          : clinicianName // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianPosition: null == clinicianPosition
          ? _value.clinicianPosition
          : clinicianPosition // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianAvatar: null == clinicianAvatar
          ? _value.clinicianAvatar
          : clinicianAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      documentName: null == documentName
          ? _value.documentName
          : documentName // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: null == documentPath
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      documentUpdatedDate: null == documentUpdatedDate
          ? _value.documentUpdatedDate
          : documentUpdatedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      documentStatus: null == documentStatus
          ? _value.documentStatus
          : documentStatus // ignore: cast_nullable_to_non_nullable
              as String,
      clinicianId: null == clinicianId
          ? _value.clinicianId
          : clinicianId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Document implements _Document {
  const _$_Document(
      {required this.documentId,
      required this.clinicianName,
      required this.clinicianPosition,
      required this.clinicianAvatar,
      required this.documentName,
      required this.documentPath,
      required this.documentUpdatedDate,
      required this.documentStatus,
      required this.clinicianId});

  factory _$_Document.fromJson(Map<String, dynamic> json) =>
      _$$_DocumentFromJson(json);

  @override
  final int documentId;
  @override
  final String clinicianName;
  @override
  final String clinicianPosition;
  @override
  final String clinicianAvatar;
  @override
  final String documentName;
  @override
  final String documentPath;
  @override
  final DateTime documentUpdatedDate;
  @override
  final String documentStatus;
  @override
  final int clinicianId;

  @override
  String toString() {
    return 'Document(documentId: $documentId, clinicianName: $clinicianName, clinicianPosition: $clinicianPosition, clinicianAvatar: $clinicianAvatar, documentName: $documentName, documentPath: $documentPath, documentUpdatedDate: $documentUpdatedDate, documentStatus: $documentStatus, clinicianId: $clinicianId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Document &&
            (identical(other.documentId, documentId) ||
                other.documentId == documentId) &&
            (identical(other.clinicianName, clinicianName) ||
                other.clinicianName == clinicianName) &&
            (identical(other.clinicianPosition, clinicianPosition) ||
                other.clinicianPosition == clinicianPosition) &&
            (identical(other.clinicianAvatar, clinicianAvatar) ||
                other.clinicianAvatar == clinicianAvatar) &&
            (identical(other.documentName, documentName) ||
                other.documentName == documentName) &&
            (identical(other.documentPath, documentPath) ||
                other.documentPath == documentPath) &&
            (identical(other.documentUpdatedDate, documentUpdatedDate) ||
                other.documentUpdatedDate == documentUpdatedDate) &&
            (identical(other.documentStatus, documentStatus) ||
                other.documentStatus == documentStatus) &&
            (identical(other.clinicianId, clinicianId) ||
                other.clinicianId == clinicianId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      documentId,
      clinicianName,
      clinicianPosition,
      clinicianAvatar,
      documentName,
      documentPath,
      documentUpdatedDate,
      documentStatus,
      clinicianId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DocumentCopyWith<_$_Document> get copyWith =>
      __$$_DocumentCopyWithImpl<_$_Document>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DocumentToJson(
      this,
    );
  }
}

abstract class _Document implements Document {
  const factory _Document(
      {required final int documentId,
      required final String clinicianName,
      required final String clinicianPosition,
      required final String clinicianAvatar,
      required final String documentName,
      required final String documentPath,
      required final DateTime documentUpdatedDate,
      required final String documentStatus,
      required final int clinicianId}) = _$_Document;

  factory _Document.fromJson(Map<String, dynamic> json) = _$_Document.fromJson;

  @override
  int get documentId;
  @override
  String get clinicianName;
  @override
  String get clinicianPosition;
  @override
  String get clinicianAvatar;
  @override
  String get documentName;
  @override
  String get documentPath;
  @override
  DateTime get documentUpdatedDate;
  @override
  String get documentStatus;
  @override
  int get clinicianId;
  @override
  @JsonKey(ignore: true)
  _$$_DocumentCopyWith<_$_Document> get copyWith =>
      throw _privateConstructorUsedError;
}
