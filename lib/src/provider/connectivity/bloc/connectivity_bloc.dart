import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'connectivity_event.dart';
part 'connectivity_state.dart';
part 'connectivity_bloc.freezed.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, ConnectivityState> {
  ConnectivityBloc() : super(const _Connection(false)) {
    on<ConnectivityEvent>((event, emit) async {
      if (event is _Connected) {
        if (event.isConnected) {
          emit(const ConnectivityState.connection(true));
        } else {
          emit(const ConnectivityState.connection(false));
        }
      }
    });
  }
}

ConnectivityBloc connectivityBloc = ConnectivityBloc();

class NetworkHelper {
  Future<void> observeNetwork() async {
    Connectivity connectivity = Connectivity();
    final result = await connectivity.checkConnectivity();

    Future<bool> haveConnection() async {
      try {
        final result = await InternetAddress.lookup('example.com');
        return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
      } on SocketException catch (_) {
        return false;
      }
    }

    bool connectivityResult() => (result == ConnectivityResult.wifi ||
        result == ConnectivityResult.mobile);

    // print('haveConnection == ${await haveConnection()} ----- connectivityResult == ${connectivityResult()}');

    if (connectivityResult() == false && await haveConnection() == false) {
      connectivityBloc.add(const ConnectivityEvent.connected(false));
    }

    connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (connectivityResult()) {
        connectivityBloc.add(const ConnectivityEvent.connected(true));
      } else {
        connectivityBloc.add(const ConnectivityEvent.connected(false));
      }
    });
  }
}
