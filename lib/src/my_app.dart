import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/connectivity/bloc/connectivity_bloc.dart';
import 'package:shift_alerts/src/provider/restart_app/restart_app_bloc.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/view/admin/admin_portal.dart';
import 'package:shift_alerts/src/view/bottombar.dart';
import 'package:shift_alerts/src/view/login.dart';
import 'package:shift_alerts/src/widget/no_internet.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_MyAppState>()?.restartApp();
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Key key = UniqueKey();
  bool firstToast = true;

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  void initState() {
    NetworkHelper().observeNetwork();
    init();
    super.initState();
  }

  Future<void> init() async {
    if (HiveUtils.isContainKey(HiveKeys.userType)) {
      userType = HiveUtils.get(HiveKeys.userType) == 'admin'
          ? UserType.admin
          : UserType.clinician;
    }
    if (HiveUtils.isContainKey(HiveKeys.refreshToken)) {
      accessToken = HiveUtils.get(HiveKeys.refreshToken);
    }
  }

  @override
  void dispose() {
    connectivityBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: BlocProvider(
        create: (context) => restartAppBloc,
        child: ScreenUtilInit(
          minTextAdapt: true,
          splitScreenMode: true,
          designSize: const Size(375, 812),
          builder: (context, child) {
            return BlocProvider(
              create: (context) => connectivityBloc,
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Shift Alerts',
                themeMode: ThemeMode.light,
                theme: ThemeData(
                  fontFamily: 'Poppins',
                  colorSchemeSeed: SaColors.primary,
                  scaffoldBackgroundColor: SaColors.scaffold,
                  appBarTheme: const AppBarTheme(
                    systemOverlayStyle: SystemUiOverlayStyle(
                      statusBarBrightness: Brightness.light,
                      statusBarColor: SaColors.transparent,
                      statusBarIconBrightness: Brightness.dark,
                    ),
                    backgroundColor: SaColors.transparent,
                    elevation: 0.0,
                    shadowColor: SaColors.transparent,
                    toolbarHeight: 0.0,
                  ),
                  cardTheme: const CardTheme(
                    elevation: 0.0,
                    margin: EdgeInsets.zero,
                  ),
                  textTheme: const TextTheme(
                    button: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                home: BlocConsumer<ConnectivityBloc, ConnectivityState>(
                  listener: (context, state) {
                    state.when(connection: (isConnected) {
                      if (!isConnected) {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            fullscreenDialog: true,
                            builder: (BuildContext context) {
                              return const Dialog(
                                backgroundColor: SaColors.black,
                                insetPadding: EdgeInsets.zero,
                                child: NoInternet(),
                              );
                            },
                          ),
                        );
                      } else if (isConnected) {
                        if (firstToast) {
                          firstToast = false;
                        } else {
                          Toast.success(
                              context, 'Internet connection retrieved');
                        }
                      }
                    });
                  },
                  builder: (context, state) {
                    return _routes();
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _routes() {
    if (HiveUtils.isContainKey(HiveKeys.userType)) {
      if (HiveUtils.get(HiveKeys.userType) == 'admin') {
        return const AdminPortal();
      } else {
        return const BottomBar();
      }
    } else {
      return const LogIn();
    }
  }
}
