class SaIcons {
  static const String _path = 'assets/icons/';

  static const String drawer = '${_path}drawer.svg';
  static const String home = '${_path}home.svg';
  static const String notification = '${_path}notification.svg';
  static const String homeFill = '${_path}home-fill.svg';
  static const String schedule = '${_path}schedule.svg';
  static const String scheduleFill = '${_path}schedule-fill.svg';
  static const String timesheet = '${_path}timesheet.svg';
  static const String timesheetFill = '${_path}timesheet-fill.svg';
  static const String credential = '${_path}credential.svg';
  static const String credentialFill = '${_path}credential-fill.svg';
  static const String arrowBack = '${_path}arrow-back.svg';
  static const String chat = '${_path}chat.svg';
  static const String facebook = '${_path}facebook.svg';
  static const String google = '${_path}google.svg';
  static const String logout = '${_path}logout.svg';
  static const String tick = '${_path}tick.svg';
  static const String search = '${_path}search.svg';
  static const String clip = '${_path}clip.svg';
  static const String send = '${_path}send.svg';
  static const String trash = '${_path}trash.svg';
  static const String facility = '${_path}facility.svg';
  static const String time = '${_path}time.svg';
  static const String unit = '${_path}unit.svg';
  static const String arrowBackward = '${_path}arrowbackward.svg';
  static const String arrowForward = '${_path}arrowForward.svg';
  static const String rating = '${_path}rating.svg';
  static const String hollowRating = '${_path}hollow-rating.svg';
  static const String close = '${_path}close.svg';
  static const String user = '${_path}user.svg';
  static const String card = '${_path}card.svg';
  static const String fire = '${_path}fire.svg';
  static const String team = '${_path}team.svg';

  static const String person = '${_path}person.svg';
  static const String star = '${_path}star.svg';
  static const String chatBubble = '${_path}chatBubble.svg';
  static const String checked = '${_path}checked.svg';
  static const String payCard = '${_path}payCard.svg';
  //admin
  static const String dollar = '${_path}dollar.svg';
  static const String chart = '${_path}chart.svg';
  static const String notes = '${_path}notes.svg';
  static const String calculator = '${_path}calculator.svg';

  // admin Credentials
  static const String mail = '${_path}mail.svg';
  static const String location = '${_path}location.svg';
  static const String phone = '${_path}phone.svg';

  // no data
  static const String nofile = '${_path}noFiles.svg';
}
