class SaString {
  // login
  static const String emailAddress = 'Email address';
  static const String yourPassword = 'Your password';
  static const String rememberMe = 'Remember me';
  static const String forgotPassword = 'Forgot password?';
  static const String logIn = 'Log in';
  static const String continueWith = '- or continue with -';
  static const String continueWithGoogle = 'Continue with Google';
  static const String continueWithFacebook = 'Continue with Facebook';
  static const String continueWithApple = 'Continue with Apple';

  // edit profile
  static const String changeAvatar = 'Change avatar';
  static const String firstName = 'Firstname';
  static const String lastName = 'Lastname';
  static const String dateOfBirth = 'Date of Birth';
  static const String contactNumber = 'Contact number';
  static const String username = 'Username';
  static const String email = 'Email';
  static const String password = 'Password';
  static const String jobSites = 'Job sites';
  static const String employeeId = 'Employee ID';
  static const String saveChange = 'Save change';
  static const String changePassword = 'Change password';

  // change password
  static const String currentPassword = 'Current password';
  static const String newPassword = 'New password';
  static const String confirmNewpassword = 'Confirm new password';
  static const String passwordHint = '••••••';

  // forgot password
  static const String forgotPasswordTitle = 'Forgot password';
  static const String resetPassword = 'Reset passowrd';

  // notification
  static const String notification = 'Notification';

  // chat
  static const String message = 'Message';
  static const String allUsers = 'All Users';
  static const String search = 'Search';
  static const String chatHere = 'Chat here';

  // credential
  static const String uploadCredential = 'Upload Credential';
  static const String uploadDocument = 'Click to upload document';
  static const String nameDocument = 'Name document';
  static const String enterName = 'Enter name document';
  static const String upload = 'Upload';

  // drawer
  static const String editProfile = 'Edit profile';
  static const String addPaycard = 'Add paycard';
  static const String logOut = 'Log out';
  static const String financialSummary = 'Financial Summary';
  static const String facilityStatistic = 'Facility Statistic';
  static const String yourDocuments = 'Your Documents';
  static const String feedbacksRatings = 'Feedbacks & Ratings';
  static const String notes = 'Notes';
  static const String workforceCalculator = 'Workforce Calculator';
  static const String deleteAccount = 'Delete Account';

  // home
  static const String hello = 'Hello';
  static const String myPerformance = 'My performance';
  static const String ratings = 'Ratings';
  static const String feedbakcs = 'feedbacks';

  // scheduling
  static const String noShiftsFound = 'No Shifts Found';
  static const String myShift = 'My Shifts';
  static const String openShift = 'Open Shifts';
  static const String facility = 'Facility:';
  static const String unit = 'Unit:';
  static const String time = 'Time:';
  static const String date = 'Date:';
  static const String onShift = 'On shift';
  static const String upcomingShifts = 'Upcoming shifts';
  static const String breakIn = 'Break in';
  static const String breakOut = 'Break out';
  static const String noMealBreak = 'No meal break';
  static const String submit = 'Submit for signature';
  static const String rn = 'RN';
  static const String rnRegistered = 'RN (Registered nurse) ';
  static const String pending = 'Pending';
  static const String approved = 'Approved';
  static const String confirmTakingShift = 'Confirm taking shift';
  static const String cancelTakingShift = 'Cancel taking shift';
  static const String nameOfPOC = 'Name of POC';
  static const String position = 'Position';
  static const String note = 'Note';
  static const String yes = 'Yes';
  static const String no = 'No';
  static const String cancel = 'Cancel';
  static const String takeSchedule = 'Take schedule';

  // timesheets
  static const String myTimeSheets = 'My timesheets';
  static const String clockIn = 'Clock in:';
  static const String clockOut = 'Clock out:';
  static const String supervisor = 'Supervisor:';
  static const String ratingsTitle = 'Ratings:';
  static const String feebacks = 'Feedbacks: ';

  // Pay Card
  static const String addPayCard = 'Add paycard';
  static const String getStarted = 'Get Started';
  static const String para1 = '''
Throw out your old leather wallet. Introducing a new way to spend, right on your phone!

No setup fee. No minimum balance. No monthly fees.

Up to 5% back, wherever Debit Mastercard is accepted.''';
  static const String para2 = '''

Our card has great features that are designed for your busy lifestyle.

Our web [or mobile app] allows you to check your balance on the go, top up the card free from any U.S. bank account, and manage your card and spending limits to suit your needs.''';
  static const String banefit1 = 'Setup in 5-Minutes or Less';
  static const String banefit2 = 'Earn up to 5% Cash Back';
  static const String banefit3 = 'Advanced Card Security';
  static const String banefit4 = 'Physical or Digital Cards';
  static const String banefit5 = 'FDIC Insured up to \$250,000';
  static const String banefit6 = 'Set Your Own Spending Limits';

  // admin Credentials
  static const String documents = 'Documents';
  static const String allPosition = 'All position';
  static const String clincian = 'Clinician';
  static const String ondekContracts = 'Ondek Contracts';
  static const String credentials = 'Credentials';
  static const String lastUpdated = 'Last updated';
  static const String status = 'Status';
  static const String declined = 'Declined';

  // note
  static const String newNote = 'Add new note';

  // Feed back
  static const String feedbackRatings = 'Feedbacks & Ratings';

  // work force calculator
  static const String instructions = 'Instructions:';
  static const String instructionsDetails =
      'Enter the known costs below. Any inputs that are not entered will be calculated based on national averages. *';
  static const String facilityCost = 'Facility Cost';
  static const String shiftAlertCost = 'ShiftAlerts Cost';
  static const String hourlyPayRate = 'Hourly pay rate';
  static const String markup = 'Markup';
  static const String burden = 'Burden';
  static const String employeeBenefits = 'Employee Benefits';
  static const String hRResources = 'HR Resources';
  static const String calculateResults = 'Calculate Results';
  static const String totalCostPerHour = 'Total cost per hour';
  static const String savingsPerHour = 'Savings (Per hour)';
  static const String costSavingsPerHour = 'Cost savings % (Per hour)';
  static const String potentialSavings = 'Total potential annual savings';
  static const String annualSavingInstruction =
      'To calculate the annual savings impact of a variable workforce, enter the number of employees to be converted below.';

  static const String facilityReqRate = 'Facility Request Rate';
  static const String nureseReqRate = 'Nurse Tier Request Rate';

  // static const String agoraAppId = '96ca921a6f57441e96ca556198634d8a';
  static const String agoraAppId = 'c501ed932b7b4571b68af340322001e0';

  static const String fcmToken =
      'AAAA5eQNw_g:APA91bHGqgcX4tSOrE-LQ_gS5Mt0_4iRbdeqBCY2U5zxwtwlH6ItR5lIVa_ivhL87e_JanHlP23Y90ILMZi01_414mYqpFhYoVsrd-wUnfGdkmIWzSWUrtAC_jPMlR0RC7LiI9bVgN95';
}
