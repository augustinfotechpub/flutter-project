import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtm/agora_rtm.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

GlobalKey<ScaffoldState> drawerKey = GlobalKey<ScaffoldState>();

enum UserType { clinician, admin }

String accessToken = '';

late UserType userType;

late AgoraRtmClient rtmClient;

late RtcEngine rtcEngine;

String currentChannel = '';

final callStatus = ValueNotifier<String>('Calling...');
final isJoin = ValueNotifier<bool>(false);

int randomNumber() => math.Random().nextInt(100000) + 1;

String getWeekDay(int index) {
  if (index == 1) {
    return 'Mon';
  } else if (index == 2) {
    return 'Tue';
  } else if (index == 3) {
    return 'Wed';
  } else if (index == 4) {
    return 'Thu';
  } else if (index == 5) {
    return 'Fri';
  } else if (index == 6) {
    return 'Sat';
  } else {
    return 'Sun';
  }
}

Timer? timer;

void callDuration() {
  timer = Timer.periodic(const Duration(seconds: 1), (time) {
    if (timer?.isActive == false) {
      time.cancel();
    } else {
      String duration =
          Duration(seconds: time.tick).toString().split('.')[0].padLeft(8, '0');
      if (int.parse(duration.split(':')[0]) <= 0) {
        callStatus.value = duration.substring(3);
      } else {
        callStatus.value = duration;
      }
    }
  });
}

Timer? openShiftTimer;
Timer? myShiftTimer;
String callerDeviceType = '';
