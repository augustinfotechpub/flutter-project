class SaImages {
  static const String fireWork = 'assets/images/fire-work.png';
  static const String team = 'assets/images/team.png';
  static const String nurseProfile = 'assets/images/nurse-profile.png';
  static const String payCard = 'assets/images/pay-card.png';
  static const String noInternet = 'assets/images/no_intrenet.png';
}
