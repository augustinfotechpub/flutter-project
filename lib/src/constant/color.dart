import 'package:flutter/material.dart';

class SaColors {
  static const Color primary = Color(0xff2753b8);
  static const Color scaffold = Color(0xfff2f5fc);
  static const Color hintColor = Color(0xff989ba5);
  static const Color lightHintColor = Color(0xffc9cacf);
  static const Color red = Color(0xffe43429);
  static const Color green = Color(0xff0ea80f);
  static const Color lightGreen = Color(0xFFE7F6E7);
  static const Color orange = Color(0xffff9429);
  static const Color lightOrange = Color(0xfffff0e0);
  static const Color white = Color(0xffffffff);
  static const Color gray = Color(0xff545761);
  static const Color black = Color(0xff272b36);
  static const Color lightPink = Color(0xfffce8ee);
  static const Color transparent = Color(0x00000000);
  static const Color pink = Color(0xffe42961);
  static const Color purple = Color(0xff893deb);
  static const Color yellow = Color(0xffF5CA32);
}
