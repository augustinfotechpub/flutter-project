import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';

class SaTextStyles {
  static const TextStyle heading = TextStyle(
    fontSize: 20.0,
    color: SaColors.black,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle title = TextStyle(
    fontSize: 16.0,
    color: SaColors.black,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle subtitleLarge = TextStyle(
    fontSize: 14.0,
    color: SaColors.black,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle subtitleSmall = TextStyle(
    fontSize: 12.0,
    color: SaColors.black,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle caption = TextStyle(
    fontSize: 10.0,
    color: SaColors.gray,
    fontWeight: FontWeight.w400,
  );
}
