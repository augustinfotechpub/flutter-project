import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';

class ShowRangeDatePicker {
  Future<DateTimeRange?> rangePicker({required BuildContext ctx}) async {
    final value = await showDateRangePicker(
      context: ctx,
      firstDate: DateTime(DateTime.now().year, DateTime.now().month - 6, 1),
      lastDate: DateTime(DateTime.now().year, DateTime.now().month + 7, 0),
      currentDate: DateTime.now(),
      initialEntryMode: DatePickerEntryMode.calendar,
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            colorSchemeSeed: SaColors.primary,
            fontFamily: 'Poppins',
          ),
          child: child!,
        );
      },
    );
    if (value != null) {
      return value;
    } else {
      return null;
    }
  }

  Future<DateTime?> datePicker({required BuildContext ctx}) async {
    final value = await showDatePicker(
      context: ctx,
      firstDate: DateTime(DateTime.now().year, DateTime.now().month - 6, 1),
      lastDate: DateTime(DateTime.now().year, DateTime.now().month + 7, 0),
      initialDate: DateTime.now(),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            colorSchemeSeed: SaColors.primary,
            fontFamily: 'Poppins',
          ),
          child: child!,
        );
      },
    );
    if (value != null) {
      return value;
    } else {
      return null;
    }
  }
}
