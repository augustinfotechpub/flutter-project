// ignore_for_file: empty_catches, use_build_context_synchronously

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/bloc/admin_data_bloc.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/delete_account/bloc/delete_account_bloc.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/third_party_login_utils.dart';
import 'package:shift_alerts/src/view/login.dart';
import 'package:shift_alerts/src/view/pay_card.dart';
import 'package:shift_alerts/src/view/profile/edit_profile.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class SaDrawer extends StatelessWidget {
  SaDrawer({super.key, this.adminDataBloc});
  final AdminDataBloc? adminDataBloc;
  final DeleteAccountBloc _deleteAccountBloc = DeleteAccountBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _deleteAccountBloc,
      child: Drawer(
        backgroundColor: SaColors.primary,
        elevation: 0.0,
        width: 264.0,
        child: SafeArea(
          bottom: false,
          child: Column(
            children: [
              const SizedBox(height: 16.0),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(width: 24.0),
                  Expanded(
                    child: InkWell(
                      onTap: () => NavigatorUtils.push(context,
                          EditProfile(isAdmin: userType == UserType.admin)),
                      child: ValueListenableBuilder<Box>(
                        valueListenable:
                            Hive.box(HiveKeys.boxName).listenable(),
                        builder: (context, box, widget) {
                          return Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(72.0),
                                child: CachedNetworkImage(
                                  placeholder: (context, url) =>
                                      const ColoredBox(
                                          color: SaColors.scaffold),
                                  imageUrl: HiveUtils.get(HiveKeys.avatarImage),
                                  height: 48.0,
                                  width: 48.0,
                                  fit: BoxFit.cover,
                                  errorWidget: (context, url, error) =>
                                      const Icon(
                                    Icons.error_outline_rounded,
                                    color: SaColors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(width: 12.0),
                              Expanded(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      SaString.hello,
                                      style:
                                          SaTextStyles.subtitleLarge.copyWith(
                                        color: SaColors.lightHintColor,
                                      ),
                                    ),
                                    const SizedBox(height: 4.0),
                                    Text(
                                      '${HiveUtils.get(HiveKeys.fullName)}',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: SaTextStyles.title
                                          .copyWith(color: SaColors.white),
                                    ),
                                  ],
                                ),
                              ),
                              const Spacer(),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: SvgPicture.asset(SaIcons.close),
                  ),
                  const SizedBox(width: 24.0),
                ],
              ),
              const SizedBox(height: 32.0),
              userType == UserType.clinician
                  ? _listTile(
                      SaIcons.user,
                      SaString.editProfile,
                      () {
                        NavigatorUtils.pop(context);
                        NavigatorUtils.push(context, const EditProfile());
                      },
                    )
                  : _listTile(
                      SaIcons.dollar,
                      SaString.financialSummary,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(0));
                      },
                    ),
              const SizedBox(height: 4.0),
              userType == UserType.clinician
                  ? _listTile(SaIcons.card, SaString.addPaycard, () {
                      NavigatorUtils.pop(context);
                      NavigatorUtils.push(context, const AddPayCard());
                    })
                  : _listTile(
                      SaIcons.chart,
                      SaString.facilityStatistic,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(1));
                      },
                    ),
              SizedBox(height: userType == UserType.clinician ? 0.0 : 4.0),
              userType == UserType.clinician
                  ? const SizedBox.shrink()
                  : _listTile(
                      SaIcons.credential,
                      SaString.yourDocuments,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(2));
                      },
                    ),
              SizedBox(height: userType == UserType.clinician ? 0.0 : 4.0),
              userType == UserType.clinician
                  ? const SizedBox.shrink()
                  : _listTile(
                      SaIcons.star,
                      SaString.feedbacksRatings,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(3));
                      },
                    ),
              SizedBox(height: userType == UserType.clinician ? 0.0 : 4.0),
              userType == UserType.clinician
                  ? const SizedBox.shrink()
                  : _listTile(
                      SaIcons.notes,
                      SaString.notes,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(4));
                      },
                    ),
              SizedBox(height: userType == UserType.clinician ? 0.0 : 4.0),
              userType == UserType.clinician
                  ? const SizedBox.shrink()
                  : _listTile(
                      SaIcons.calculator,
                      SaString.workforceCalculator,
                      () {
                        NavigatorUtils.pop(context);
                        adminDataBloc!.add(const AdminDataEvent.changeIndex(5));
                      },
                    ),
              const SizedBox(height: 4.0),
              BlocListener<DeleteAccountBloc, DeleteAccountState>(
                listener: (context, state) {
                  state.when(
                    initial: () {},
                    loading: () async {
                      Navigator.pop(context);
                      Loader.show(context);
                    },
                    success: () async {
                      try {
                        rtmClient.logout();
                        if (await loginUtils.googleSignIn.isSignedIn()) {
                          loginUtils.googleSignIn.signOut();
                        }
                        loginUtils.facebookLogin.logOut();
                      } catch (e) {}
                      apiProvider.updateToken('');
                      HiveUtils.remove(HiveKeys.email);
                      HiveUtils.remove(HiveKeys.password);
                      HiveUtils.remove(HiveKeys.rememberMe);
                      HiveUtils.remove(HiveKeys.userType);
                      Loader.hide(context);
                      NavigatorUtils.pop(context);
                      NavigatorUtils.pushRemoveUntil(context, const LogIn());
                    },
                    error: (error) {
                      Loader.hide(context);
                      Toast.error(context,
                          error == '' ? 'something went wrong' : error);
                    },
                  );
                },
                child: _listTile(
                  SaIcons.trash,
                  SaString.deleteAccount,
                  () {
                    ShowAlertDialog().showAlertDialog(
                      context: context,
                      title: SaString.deleteAccount.capitalize(),
                      content: 'Are you sure you want to delete your account ?',
                      showCloseIcon: false,
                      onYesPressed: () async {
                        _deleteAccountBloc
                            .add(const DeleteAccountEvent.deleteAccount());
                      },
                    );
                  },
                ),
              ),
              const Spacer(),
              InkWell(
                onTap: () {
                  ShowAlertDialog().showAlertDialog(
                    context: context,
                    title: SaString.logOut,
                    content: 'Are you sure, do you want to logout ?',
                    showCloseIcon: false,
                    onYesPressed: () async {
                      try {
                        rtmClient.logout();
                        if (await loginUtils.googleSignIn.isSignedIn()) {
                          loginUtils.googleSignIn.signOut();
                        }
                        loginUtils.facebookLogin.logOut();
                      } catch (e) {}
                      apiProvider.updateToken('');
                      if (!HiveUtils.get(HiveKeys.rememberMe)) {
                        HiveUtils.remove(HiveKeys.email);
                        HiveUtils.remove(HiveKeys.password);
                      }
                      HiveUtils.remove(HiveKeys.userType);
                      NavigatorUtils.pop(context);
                      NavigatorUtils.pop(context);
                      NavigatorUtils.pushRemoveUntil(context, const LogIn());
                    },
                  );
                },
                child: Row(
                  children: [
                    const SizedBox(width: 20.0),
                    Container(
                      height: 44.0,
                      width: 44.0,
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        color: SaColors.white,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: SvgPicture.asset(SaIcons.logout),
                    ),
                    const SizedBox(width: 16.0),
                    Text(
                      SaString.logOut,
                      style:
                          SaTextStyles.heading.copyWith(color: SaColors.white),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 32.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _listTile(String icon, String title, void Function()? onTap) {
    return ListTile(
      onTap: onTap,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
      minVerticalPadding: 20.0,
      minLeadingWidth: 20,
      leading: Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: SvgPicture.asset(
          icon,
          color: SaColors.white,
          height: title == SaString.deleteAccount ? 16.0 : null,
        ),
      ),
      title: Text(
        title,
        style: SaTextStyles.subtitleLarge.copyWith(color: SaColors.white),
      ),
    );
  }
}
