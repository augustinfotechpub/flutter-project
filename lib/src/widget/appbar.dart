import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/view/chat/chat_list.dart';
import 'package:shift_alerts/src/view/notification.dart';
import 'package:shift_alerts/src/widget/bouncing_widget.dart';

PreferredSizeWidget appBar(BuildContext context, {required String title}) {
  return AppBar(
    title: Text(
      title,
      style: SaTextStyles.title.copyWith(fontSize: 18.0),
    ),
    leading: Padding(
      padding: const EdgeInsets.only(left: 24.0),
      child: BouncingWidget(
        onPressed: () => NavigatorUtils.pop(context),
        width: 24.0,
        height: 24.0,
        icon: SaIcons.arrowBack,
      ),
    ),
    leadingWidth: 48.0,
    automaticallyImplyLeading: false,
    centerTitle: true,
    toolbarHeight: 60.0,
  );
}

PreferredSizeWidget adminAppBar(context) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 0.0,
    titleSpacing: 24.0,
    // title: Image.asset(SaImages.adminLogo, height: 32.0),
    actions: [
      const SizedBox(width: 16.0),
      Padding(
        padding: const EdgeInsets.only(top: 2.0),
        child: InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () => NavigatorUtils.push(context, const Notifications()),
          child: const Icon(
            CupertinoIcons.bell_fill,
            color: SaColors.black,
            size: 22.0,
          ),
        ),
      ),
      // BouncingWidget(
      //   onPressed: () => NavigatorUtils.push(context, const Notifications()),
      //   icon: SaIcons.notification,
      //   // color: SaColors.black,
      // ),
      const SizedBox(width: 16.0),
      InkWell(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () => NavigatorUtils.push(context, const ChatList()),
        child: SvgPicture.asset(
          SaIcons.chat,
          height: 20.0,
          width: 20.0,
        ),
      ),
      // BouncingWidget(
      //   onPressed: () => NavigatorUtils.push(context, const ChatList()),
      //   icon: SaIcons.chat,
      //   height: 18,
      //   width: 20,
      //   color: SaColors.black,
      // ),
      const SizedBox(width: 16.0),
      InkWell(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () => drawerKey.currentState!.openDrawer(),
        child: SvgPicture.asset(
          SaIcons.drawer,
          height: 16.0,
          width: 16.0,
        ),
      ),
      // BouncingWidget(
      //   onPressed: () => drawerKey.currentState!.openDrawer(),
      //   icon: SaIcons.drawer,
      //   color: SaColors.black,
      //   height: 14,
      //   width: 20,
      // ),
      const SizedBox(width: 24.0),
    ],
    centerTitle: false,
    toolbarHeight: 60.0,
  );
}
