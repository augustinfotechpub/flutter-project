import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class SaTextField extends StatelessWidget {
  final TextEditingController? controller;
  final EdgeInsetsGeometry? contentPadding;
  final void Function()? onTap;
  final String? hintText;
  final Widget? suffixIcon;
  final bool obscureText;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final Widget? prefixIcon;
  final Color? fillColor;
  final TextStyle? hintStyle;
  final bool enabled;
  final bool readOnly;
  final TextCapitalization textCapitalization;
  final int maxLine;
  final int? maxLength;
  final BorderSide borderSide;
  final void Function(String)? onChanged;
  const SaTextField({
    super.key,
    this.controller,
    this.contentPadding,
    this.hintText,
    this.suffixIcon,
    this.obscureText = false,
    this.onTap,
    this.keyboardType,
    this.inputFormatters,
    this.prefixIcon,
    this.fillColor = SaColors.white,
    this.hintStyle,
    this.enabled = true,
    this.textCapitalization = TextCapitalization.none,
    this.maxLine = 1,
    this.maxLength,
    this.borderSide = BorderSide.none,
    this.readOnly = false,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      minLines: maxLine,
      decoration: InputDecoration(
        enabled: enabled,
        isCollapsed: true,
        isDense: true,
        border: OutlineInputBorder(
          borderSide: borderSide,
          borderRadius: BorderRadius.circular(12.0),
        ),
        filled: true,
        fillColor: fillColor,
        hintStyle: hintStyle ??
            SaTextStyles.subtitleLarge.copyWith(color: SaColors.hintColor),
        contentPadding: contentPadding ??
            const EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 12.0,
            ),
        hintText: hintText,
        suffixIcon: suffixIcon,
        suffixIconConstraints:
            const BoxConstraints(maxHeight: 16.0, maxWidth: 32.0),
        prefixIcon: prefixIcon,
        prefixIconConstraints:
            const BoxConstraints(maxHeight: 16.0, maxWidth: 44.0),
        counterText: '',
      ),
      obscureText: obscureText,
      validator: (value) {
        return null;
      },
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      textCapitalization: textCapitalization,
      style: enabled
          ? SaTextStyles.subtitleLarge.copyWith()
          : SaTextStyles.subtitleLarge.copyWith(color: SaColors.hintColor),
      maxLines: maxLine,
      maxLength: maxLength,
      onChanged: onChanged,
      onTap: onTap,
    );
  }
}
