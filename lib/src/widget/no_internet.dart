import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/image.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';

class NoInternet extends StatelessWidget {
  const NoInternet({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(25.0),
          child: Image.asset(
            SaImages.noInternet,
            fit: BoxFit.contain,
          ),
        ),
        Text(
          "Whoops!!",
          style: SaTextStyles.title.copyWith(color: SaColors.white),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            "No Internet connection found.\nCheck your connection or try again.",
            textAlign: TextAlign.center,
            style: SaTextStyles.subtitleLarge.copyWith(color: SaColors.white),
          ),
        ),
        const SizedBox(height: 40),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              minimumSize: const Size(180, 42),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(25.0),
                ),
              )),
          onPressed: () => NavigatorUtils.pop(context),
          child: Text(
            'Try again',
            style: SaTextStyles.subtitleLarge.copyWith(
              color: SaColors.white,
            ),
          ),
        ),
      ],
    );
  }
}
