import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';

class SaButton extends StatelessWidget {
  final void Function()? onPressed;
  final double height;
  final String text;
  final double radius;
  final TextStyle? textStyle;
  const SaButton({
    super.key,
    required this.onPressed,
    required this.text,
    this.height = 36.0,
    this.radius = 16.0,
    this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          // minimumSize: Size(double.infinity, height),
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
          ),
          shadowColor: SaColors.transparent,
        ),
        child: Text(
          text,
          style: textStyle,
        ),
      ),
    );
  }
}

class SaOutlineButton extends StatelessWidget {
  final void Function()? onPressed;
  final double height;
  final double width;
  final Widget text;
  final Widget? icon;
  final Color? borderColor;
  final double? borderWidth;
  final double borderRadius;

  const SaOutlineButton({
    super.key,
    required this.onPressed,
    required this.text,
    this.height = 36,
    this.width = double.infinity,
    this.icon,
    this.borderColor,
    this.borderWidth,
    this.borderRadius = 12.0,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: OutlinedButton(
        onPressed: onPressed,
        style: OutlinedButton.styleFrom(
          minimumSize: Size(width, height),
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          side: BorderSide(
            color: borderColor ?? SaColors.primary,
            width: borderWidth ?? 2.0,
          ),
          shadowColor: SaColors.transparent,
          padding: EdgeInsets.zero,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon ?? const SizedBox.shrink(),
            SizedBox(width: icon == null ? 0.0 : 4.0),
            text,
            SizedBox(width: icon == null ? 0.0 : 4.0),
          ],
        ),
      ),
    );
  }
}
