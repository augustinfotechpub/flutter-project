import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class Toast {
  static success(BuildContext context, String message) => showTopSnackBar(
        context,
        CustomSnackBar.success(
          message: message,
          maxLines: 2,
          backgroundColor: SaColors.green,
          textStyle: const TextStyle(
            fontSize: 14.0,
            color: SaColors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      );

  static error(BuildContext context, String message) => showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: message,
          maxLines: 2,
          backgroundColor: SaColors.red,
          textStyle: const TextStyle(
            fontSize: 14.0,
            color: SaColors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      );
}
