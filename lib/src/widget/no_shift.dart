import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class Noshift extends StatelessWidget {
  const Noshift({
    super.key,
    this.message = SaString.noShiftsFound,
  });
  final String message;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 32),
          SvgPicture.asset(SaIcons.nofile, height: 104.0),
          const SizedBox(height: 16),
          Text(
            message,
            style: SaTextStyles.title.copyWith(color: SaColors.gray),
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
