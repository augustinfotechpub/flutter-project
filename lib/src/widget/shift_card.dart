import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';

class ShiftCard extends StatelessWidget {
  final Shift result;
  const ShiftCard({
    super.key,
    required this.result,
  });

  @override
  Widget build(BuildContext context) {
    final time =
        '${result.startTime.parseDate().hm()} - ${result.endTime.parseDate().hm()}';

    return IntrinsicHeight(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: 4.0,
            margin: const EdgeInsets.symmetric(vertical: 22.0),
            decoration: BoxDecoration(
              color: Color(
                  int.parse("0xFF${result.shiftColor.replaceFirst('#', '')}")),
              borderRadius: BorderRadius.circular(1.0),
            ),
          ),
          const SizedBox(width: 16.0),
          Expanded(
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
              decoration: BoxDecoration(
                color: SaColors.white,
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    result.clinicianPosition,
                    style: SaTextStyles.title,
                  ),
                  const SizedBox(height: 12.0),
                  _details(
                    SaIcons.schedule,
                    SaString.date,
                    result.startDate.parseDate().MMdY(),
                  ),
                  const SizedBox(height: 8.0),
                  _details(SaIcons.time, SaString.time, time),
                  const SizedBox(height: 8.0),
                  _details(
                    SaIcons.facility,
                    SaString.facility,
                    result.facilityName,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _details(String icon, String title, String data) {
    return Row(
      children: [
        SvgPicture.asset(
          icon,
          height: 16.0,
          width: 16.0,
          color: SaColors.lightHintColor,
        ),
        const SizedBox(width: 8.0),
        Text(
          title,
          style: SaTextStyles.subtitleSmall.copyWith(color: SaColors.hintColor),
        ),
        const SizedBox(width: 4.0),
        Text(
          data,
          style: const TextStyle(
            fontSize: 12.0,
            color: SaColors.gray,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
