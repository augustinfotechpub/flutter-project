import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BouncingWidget extends StatefulWidget {
  const BouncingWidget({
    Key? key,
    required this.onPressed,
    required this.icon,
    this.height = 24,
    this.width = 24,
    this.color,
  }) : super(key: key);
  final VoidCallback onPressed;
  final String icon;
  final double height;
  final double width;
  final Color? color;
  @override
  BouncingWidgetState createState() => BouncingWidgetState();
}

class BouncingWidgetState extends State<BouncingWidget>
    with SingleTickerProviderStateMixin {
  late final Animation<double> _scale = Tween<double>(begin: 1.0, end: 0.9)
      .animate(CurvedAnimation(parent: _controller, curve: Curves.ease));
  late final AnimationController _controller = AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 300),
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (PointerDownEvent event) {
        _controller.forward();
      },
      onPointerUp: (PointerUpEvent event) {
        _controller.reverse();
        widget.onPressed();
      },
      child: ScaleTransition(
        scale: _scale,
        child: SvgPicture.asset(
          widget.icon,
          color: widget.color,
          height: widget.height,
          width: widget.width,
        ),
      ),
    );
  }
}
