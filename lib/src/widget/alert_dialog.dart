import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/widget/button.dart';

class ShowAlertDialog {
  Future<dynamic> showAlertDialog({
    required BuildContext context,
    required String title,
    required String content,
    required bool showCloseIcon,
    required void Function()? onYesPressed,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          backgroundColor: SaColors.white,
          contentPadding: const EdgeInsets.all(16),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: SaTextStyles.title.copyWith(fontSize: 18),
                  ),
                  showCloseIcon
                      ? IconButton(
                          constraints:
                              const BoxConstraints(maxHeight: 20, maxWidth: 25),
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context, false),
                          icon: const Icon(
                            Icons.close,
                            color: SaColors.black,
                            size: 25,
                          ),
                        )
                      : const SizedBox.shrink()
                ],
              ),
              const SizedBox(height: 15),
              Text(
                content,
                style: SaTextStyles.subtitleLarge
                    .copyWith(color: SaColors.hintColor),
              ),
              const SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: SaOutlineButton(
                      text: Text(
                        SaString.no,
                        style: SaTextStyles.title.copyWith(color: SaColors.red),
                      ),
                      borderRadius: 12,
                      borderColor: SaColors.red,
                      borderWidth: 2,
                      onPressed: () => Navigator.pop(context, false),
                    ),
                  ),
                  const SizedBox(width: 12),
                  Expanded(
                    child: SaButton(
                      text: SaString.yes,
                      textStyle:
                          SaTextStyles.title.copyWith(color: SaColors.white),
                      radius: 12,
                      onPressed: onYesPressed,
                    ),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
