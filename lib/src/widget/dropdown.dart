import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class SaDropDown extends StatelessWidget {
  final String? value;
  final List<String> valuesList;
  final Color borderColor;
  final Color iconColor;
  final Widget? hintText;
  final double itemHeight;
  final double borderWidth;
  final Widget? icon;
  final bool isExpanded;
  final void Function(String?)? onChanged;
  final double buttonHeight;
  final Color buttonColor;

  const SaDropDown({
    super.key,
    this.value,
    required this.valuesList,
    this.buttonHeight = 32.0,
    this.onChanged,
    this.hintText,
    this.icon,
    this.itemHeight = 30,
    this.borderWidth = 1.0,
    this.isExpanded = false,
    this.borderColor = SaColors.primary,
    this.iconColor = SaColors.primary,
    this.buttonColor = SaColors.transparent,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButton2(
      underline: const SizedBox.shrink(),
      isDense: true,
      isExpanded: isExpanded,
      buttonHeight: buttonHeight,
      buttonPadding: const EdgeInsets.symmetric(horizontal: 12.0),
      itemPadding: const EdgeInsets.symmetric(horizontal: 12.0),
      buttonDecoration: BoxDecoration(
        color: buttonColor,
        border: Border.all(color: borderColor, width: borderWidth),
        borderRadius: BorderRadius.circular(12.0),
      ),
      style: SaTextStyles.subtitleSmall.copyWith(color: SaColors.primary),
      itemHeight: itemHeight,
      dropdownMaxHeight: 200,
      dropdownPadding: null,
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: SaColors.white,
      ),
      hint: hintText,
      dropdownElevation: 1,
      icon: icon ??
          Icon(
            Icons.arrow_drop_down_rounded,
            color: iconColor,
            size: 28,
          ),
      value: value,
      onChanged: onChanged,
      items: valuesList
          .map((e) => DropdownMenuItem(
                value: e,
                child: Text(
                  e,
                  style: SaTextStyles.subtitleLarge.copyWith(
                    color: e == value ? SaColors.primary : null,
                    fontWeight: e == value ? FontWeight.w600 : null,
                  ),
                ),
              ))
          .toList(),
    );
  }
}
