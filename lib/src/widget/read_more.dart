import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class ExpandableText extends StatefulWidget {
  const ExpandableText({
    super.key,
    required this.text,
    required this.onMoreorLessTap,
  });
  final String text;
  final void Function() onMoreorLessTap;

  @override
  State<ExpandableText> createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText> {
  @override
  Widget build(BuildContext context) {
    return ReadMoreText(
      widget.text,
      trimLines: 2,
      onMoreorLessTap: () => widget.onMoreorLessTap(),
      clickableTextStyle: SaTextStyles.subtitleSmall.copyWith(
        fontWeight: FontWeight.w500,
        color: SaColors.primary,
      ),
      style: SaTextStyles.subtitleSmall.copyWith(
        fontWeight: FontWeight.w500,
        color: SaColors.gray,
      ),
    );
  }
}

class ReadMoreText extends StatefulWidget {
  const ReadMoreText(
    this.data, {
    Key? key,
    this.trimLines = 2,
    required this.onMoreorLessTap,
    required this.style,
    required this.clickableTextStyle,
  }) : super(key: key);

  final String data;
  final int trimLines;
  final TextStyle style;
  final TextStyle clickableTextStyle;
  final void Function() onMoreorLessTap;

  @override
  ReadMoreTextState createState() => ReadMoreTextState();
}

class ReadMoreTextState extends State<ReadMoreText> {
  final String _kEllipsis = '\u2026';

  final String _kLineSeparator = '\u2028';
  bool _readMore = true;

  void _ontap() async {
    setState(() => _readMore = !_readMore);
    await Future.delayed(const Duration(milliseconds: 10))
        .then((value) => widget.onMoreorLessTap());
  }

  @override
  Widget build(BuildContext context) {
    TextStyle? effectiveTextStyle = (widget.style.inherit)
        ? DefaultTextStyle.of(context).style.merge(widget.style)
        : widget.style;

    TextSpan moreorLessText = TextSpan(
        text: _readMore ? 'more' : 'less',
        style: widget.clickableTextStyle,
        recognizer: TapGestureRecognizer()..onTap = () => _ontap());

    TextSpan delimiter = TextSpan(
        text: _readMore ? '$_kEllipsis ' : '  ',
        style: widget.style,
        recognizer: TapGestureRecognizer()..onTap = () => _ontap());

    Widget result = LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final maxWidth = constraints.maxWidth;

        final text = TextSpan(text: widget.data, style: effectiveTextStyle);

        TextPainter textPainter = TextPainter(
          text: moreorLessText,
          textAlign: TextAlign.start,
          textDirection: Directionality.of(context),
          textScaleFactor: MediaQuery.textScaleFactorOf(context),
          maxLines: widget.trimLines,
        );

        // read more or less textSize
        textPainter.layout(minWidth: 0, maxWidth: maxWidth);
        final moreorLessTextSize = textPainter.size;

        // delimiter textSize
        textPainter.text = delimiter;
        textPainter.layout(minWidth: 0, maxWidth: maxWidth);
        final delimiterSize = textPainter.size;

        // main text textSize
        textPainter.text = text;
        textPainter.layout(minWidth: constraints.minWidth, maxWidth: maxWidth);
        final textSize = textPainter.size;

        bool linkLongerThanLine = false;
        int endIndex;

        if (moreorLessTextSize.width < maxWidth) {
          final readMoreSize = moreorLessTextSize.width + delimiterSize.width;
          final pos = textPainter.getPositionForOffset(
              Offset(((textSize.width - 60) - readMoreSize), textSize.height));
          endIndex = textPainter.getOffsetBefore(pos.offset) ?? 0;
        } else {
          TextPosition pos = textPainter
              .getPositionForOffset(textSize.bottomLeft(Offset.zero));
          endIndex = pos.offset;
          linkLongerThanLine = true;
        }

        TextSpan textSpan = textPainter.didExceedMaxLines
            ? TextSpan(
                style: effectiveTextStyle,
                text: _readMore
                    ? widget.data.substring(0, endIndex) +
                        (linkLongerThanLine ? _kLineSeparator : '')
                    : widget.data,
                children: <TextSpan>[delimiter, moreorLessText],
              )
            : TextSpan(style: effectiveTextStyle, text: widget.data);

        return Text.rich(
          textAlign: TextAlign.start,
          textDirection: Directionality.of(context),
          softWrap: true,
          overflow: TextOverflow.clip,
          TextSpan(
            children: [
              TextSpan(
                text: SaString.feebacks,
                style: SaTextStyles.subtitleSmall.copyWith(
                  color: SaColors.hintColor,
                ),
              ),
              textSpan,
            ],
          ),
        );
      },
    );
    return result;
  }
}
