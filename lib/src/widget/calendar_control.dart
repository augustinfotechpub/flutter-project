import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/bouncing_widget.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';

class CalendarControl extends StatelessWidget {
  final bool isonOpenshift;
  final DateTime focusDate;
  final String? value;
  final void Function() onBackPress;
  final void Function() onNextPress;
  final void Function(String?)? onDropChanged;
  const CalendarControl({
    required this.isonOpenshift,
    required this.focusDate,
    super.key,
    this.value,
    required this.onBackPress,
    required this.onNextPress,
    this.onDropChanged,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          Text(
            "${focusDate.MM()} ${focusDate.year}",
            style: SaTextStyles.subtitleLarge
                .copyWith(fontWeight: FontWeight.w500),
          ),
          isonOpenshift
              ? const SizedBox(
                  width: 8,
                )
              : const Spacer(),
          BouncingWidget(
            onPressed: onBackPress,
            height: 16,
            width: 16,
            icon: SaIcons.arrowBackward,
            color: SaColors.hintColor,
          ),
          const SizedBox(width: 32),
          BouncingWidget(
            onPressed: onNextPress,
            height: 17,
            width: 17,
            icon: SaIcons.arrowForward,
            color: SaColors.hintColor,
          ),
          if (isonOpenshift) const Spacer(),
          if (isonOpenshift)
            SizedBox(
              width: 100,
              child: SaDropDown(
                value: value,
                valuesList: const ['Month', 'Week', 'Day'],
                onChanged: onDropChanged!,
              ),
            )
        ],
      ),
    );
  }
}
