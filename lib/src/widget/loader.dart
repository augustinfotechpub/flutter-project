import 'dart:math' as math show sin, pi;

import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:shift_alerts/src/constant/color.dart';

// class SaLoader {
//   static OverlayEntry? _overlayEntry;
//   static void show(context) => Overlay.of(context)!.insert(OverlayEntry(
//         builder: (context) {
//           return Loader(color: SaColors.primary);
//         },
//       ));
// }

class Loader extends StatefulWidget {
  final Color? color;
  final Color? backgroundColor;
  final double size;
  final IndexedWidgetBuilder? itemBuilder;
  final Duration duration;
  final AnimationController? controller;

  static OverlayEntry? _overlayEntry;

  const Loader({
    super.key,
    this.color,
    this.backgroundColor,
    this.size = 50.0,
    this.itemBuilder,
    this.duration = const Duration(milliseconds: 1200),
    this.controller,
  });

  Loader.show(
    context, {
    super.key,
    this.color,
    this.backgroundColor,
    this.size = 50.0,
    this.itemBuilder,
    this.duration = const Duration(milliseconds: 1200),
    this.controller,
  }) {
    _overlayEntry = OverlayEntry(
      builder: (context) {
        return Container(
          color: backgroundColor ?? Colors.black.withOpacity(0.25),
          child: const Loader(),
        );
      },
    );
    Overlay.of(context)!.insert(_overlayEntry!);
  }

  Loader.hide(context, {super.key})
      : color = null,
        backgroundColor = null,
        size = 0.0,
        itemBuilder = null,
        duration = const Duration(),
        controller = null {
    // if (_overlayEntry != null) {
    _overlayEntry?.remove();
    // }
  }

  @override
  State<Loader> createState() => _LoaderState();
}

class _LoaderState extends State<Loader> with SingleTickerProviderStateMixin {
  final List<double> delays = [
    .0,
    -1.1,
    -1.0,
    -0.9,
    -0.8,
    -0.7,
    -0.6,
    -0.5,
    -0.4,
    -0.3,
    -0.2,
    -0.1
  ];
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = (widget.controller ??
        AnimationController(vsync: this, duration: widget.duration))
      ..repeat();
  }

  @override
  void dispose() {
    if (widget.controller == null) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox.fromSize(
        size: Size.square(widget.size),
        child: Stack(
          children: List.generate(12, (i) {
            final position = widget.size * .5;
            return Positioned.fill(
              left: position,
              top: position,
              child: Transform(
                transform: Matrix4.rotationZ(30.0 * i * 0.0174533),
                child: Align(
                  alignment: Alignment.center,
                  child: FadeTransition(
                    opacity: DelayTween(begin: 0.0, end: 1.0, delay: delays[i])
                        .animate(_controller),
                    child: SizedBox.fromSize(
                        size: Size.square(widget.size * 0.15),
                        child: _itemBuilder(i)),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget _itemBuilder(int index) => widget.itemBuilder != null
      ? widget.itemBuilder!(context, index)
      : DecoratedBox(
          decoration: BoxDecoration(
              color: widget.color ?? Theme.of(context).primaryColor,
              shape: BoxShape.circle));
}

class DelayTween extends Tween<double> {
  DelayTween({double? begin, double? end, required this.delay})
      : super(begin: begin, end: end);

  final double delay;

  @override
  double lerp(double t) =>
      super.lerp((math.sin((t - delay) * 2 * math.pi) + 1) / 2);

  @override
  double evaluate(Animation<double> animation) => lerp(animation.value);
}
