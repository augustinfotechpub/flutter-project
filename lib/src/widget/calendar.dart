import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/bouncing_widget.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:table_calendar/table_calendar.dart';

List<String> _weekDay = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

class SaCalendar extends StatelessWidget {
  final DateTime focusDate;
  final CalendarFormat calendarFormat;
  final Map<CalendarFormat, String> availableCalendarFormats;
  final void Function(DateTime, DateTime)? onDaySelected;
  final void Function(CalendarFormat)? onFormatChanged;
  final void Function(PageController)? onCalendarCreated;
  final void Function() onPrevious;
  final void Function() onNext;
  final List<Shift> shifts;
  final String? value;
  final void Function(String?)? onDropChanged;

  const SaCalendar({
    super.key,
    required this.focusDate,
    required this.shifts,
    required this.onPrevious,
    required this.onNext,
    this.calendarFormat = CalendarFormat.week,
    this.availableCalendarFormats = const {
      CalendarFormat.month: 'Month',
      CalendarFormat.twoWeeks: '2 weeks',
      CalendarFormat.week: 'Week'
    },
    this.onDaySelected,
    this.onFormatChanged,
    this.onCalendarCreated,
    this.value,
    this.onDropChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TableCalendar(
      firstDay: DateTime.utc(2015, 01, 01),
      lastDay: DateTime.utc(2100, 12, 31),
      focusedDay: focusDate,
      selectedDayPredicate: (day) => focusDate.isSameDay(day),
      daysOfWeekHeight: calendarFormat == CalendarFormat.month ? 25 : 35,
      rowHeight: 45,
      calendarFormat: calendarFormat,
      availableCalendarFormats: availableCalendarFormats,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: const CalendarStyle(
        isTodayHighlighted: false,
        outsideDaysVisible: true,
        cellMargin: EdgeInsets.zero,
      ),
      onFormatChanged: onFormatChanged,
      onDaySelected: onDaySelected,
      headerStyle: const HeaderStyle(
        formatButtonVisible: false,
        rightChevronVisible: false,
        leftChevronVisible: false,
      ),
      onCalendarCreated: onCalendarCreated,
      calendarBuilders: CalendarBuilders(
        defaultBuilder: (context, day, focusedDay) {
          List<Shift> result =
              shifts.where((e) => e.startDate == day.dmY().ydM()).toList();
          return Stack(
            alignment: Alignment.center,
            children: [
              Center(
                child: Text(
                  day.day.toString(),
                  style: SaTextStyles.title.copyWith(
                    color: day.isSameDay(DateTime.now())
                        ? calendarFormat == CalendarFormat.month
                            ? SaColors.green
                            : SaColors.black
                        : !day.isAfter(DateTime.now())
                            ? SaColors.hintColor
                            : SaColors.black,
                  ),
                ),
              ),
              _marker(result, true)
            ],
          );
        },
        selectedBuilder: (context, day, focusedDay) {
          List<Shift> result =
              shifts.where((e) => e.startDate == day.dmY().ydM()).toList();
          return Stack(
            alignment: Alignment.center,
            children: [
              Column(
                children: [
                  Container(
                    height: calendarFormat == CalendarFormat.month ? 35 : 45,
                    width: calendarFormat == CalendarFormat.month ? 35 : null,
                    decoration: calendarFormat == CalendarFormat.month
                        ? BoxDecoration(
                            border: Border.all(color: SaColors.green),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(180)),
                            color: SaColors.transparent,
                          )
                        : const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(16),
                              bottomRight: Radius.circular(16),
                            ),
                            color: SaColors.green,
                          ),
                    child: Center(
                      child: Text(
                        day.day.toString(),
                        style: SaTextStyles.title.copyWith(
                          color: calendarFormat == CalendarFormat.month
                              ? SaColors.green
                              : SaColors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              _marker(result, false),
            ],
          );
        },
        dowBuilder: (context, day) => DecoratedBox(
          decoration: day.dmY() == focusDate.dmY() &&
                  calendarFormat == CalendarFormat.week
              ? const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16),
                  ),
                  color: SaColors.green,
                )
              : const BoxDecoration(),
          child: Center(
            child: Text(
              calendarFormat == CalendarFormat.week
                  ? _weekDay.elementAt(day.weekday - 1)
                  : _weekDay.elementAt(day.weekday - 1).substring(0, 2),
              style: SaTextStyles.subtitleSmall.copyWith(
                color: day.dmY() == focusDate.dmY() &&
                        calendarFormat == CalendarFormat.week
                    ? SaColors.white
                    : SaColors.hintColor,
              ),
            ),
          ),
        ),
        headerTitleBuilder: (context, day) => onFormatChanged != null
            ? _header(day: day)
            : SizedBox(
                height: 24.0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${day.MM()} ${day.year}",
                      style: SaTextStyles.subtitleLarge.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    BouncingWidget(
                      onPressed: onPrevious,
                      height: 16,
                      width: 16,
                      icon: SaIcons.arrowBackward,
                      color: SaColors.hintColor,
                    ),
                    const SizedBox(width: 32),
                    BouncingWidget(
                      onPressed: onNext,
                      height: 17,
                      width: 17,
                      icon: SaIcons.arrowForward,
                      color: SaColors.hintColor,
                    ),
                    const SizedBox(width: 5),
                  ],
                ),
              ),
      ),
    );
  }

  Widget _marker(List<Shift> result, bool defaltView) {
    return calendarFormat == CalendarFormat.month
        ? Positioned(
            bottom: defaltView ? 0 : 1,
            child: result.isEmpty
                ? const SizedBox.shrink()
                : Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: CircleAvatar(
                      maxRadius: 2.0,
                      backgroundColor: Color(int.parse(
                          "0xFF${result.first.shiftColor.replaceFirst('#', '')}")),
                    ),
                  ),
          )
        : const SizedBox.shrink();
  }

  Widget _header({required DateTime day}) {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          Text(
            "${day.MM()} ${day.year}",
            style: SaTextStyles.subtitleLarge
                .copyWith(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: 8.0),
          BouncingWidget(
            onPressed: onPrevious,
            height: 16,
            width: 16,
            icon: SaIcons.arrowBackward,
            color: SaColors.hintColor,
          ),
          const SizedBox(width: 32),
          BouncingWidget(
            onPressed: onNext,
            height: 17,
            width: 17,
            icon: SaIcons.arrowForward,
            color: SaColors.hintColor,
          ),
          const Spacer(),
          SizedBox(
            width: 100,
            child: SaDropDown(
              value: value,
              valuesList: const ['Month', 'Week', 'Day'],
              onChanged: onDropChanged!,
            ),
          ),
        ],
      ),
    );
  }
}
