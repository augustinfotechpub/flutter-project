// ignore_for_file: use_build_context_synchronously, empty_catches

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/my_app.dart';
import 'package:shift_alerts/src/provider/restart_app/restart_app_bloc.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/view/chat/call.dart';
import 'package:shift_alerts/src/view/credential/credentials.dart';
import 'package:shift_alerts/src/view/home/home.dart';
import 'package:shift_alerts/src/view/scheduling/scheduling.dart';
import 'package:shift_alerts/src/view/timesheets.dart';
import 'package:shift_alerts/src/widget/drawer.dart';

class BottomBar extends StatefulWidget {
  const BottomBar({super.key});

  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> with WidgetsBindingObserver {
  final _index = ValueNotifier<int>(0);
  List<Widget> body = const [Home(), Scheduling(), TimeSheets(), Credentials()];

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    init();
    // rtcEngineUtils.callHandler(context);
    super.initState();
  }

  init() async {
    notificationsUtils.generateToken();
    notificationsUtils.messageListener();
    try {
      await rtmClient.login(
          null, HiveUtils.get(HiveKeys.user).toString().padLeft(4, '0'));
    } catch (e) {}
    rtmClient.onLocalInvitationAccepted = (invite) {
      // callStatus.value = '00:00';
      callDuration();
    };
    rtmClient.onLocalInvitationReceivedByPeer = (invite) {
      callStatus.value = 'Ringing';
    };
    rtmClient.onLocalInvitationCanceled = (invite) {
      // print('--- Local Cancel');
    };
    rtmClient.onLocalInvitationRefused = (invite) {
      // print('--- Local Refuse');
      rtcEngine.leaveChannel();
      callStatus.value = 'Call Decline';
      Future.delayed(
        const Duration(seconds: 1),
        () => NavigatorUtils.pop(context),
      );
    };
    rtmClient.onRemoteInvitationAccepted = (invite) {
      // callStatus.value = '00:00';
      callDuration();
    };
    rtmClient.onRemoteInvitationCanceled = (invite) {
      rtcEngine.leaveChannel();
      timer?.cancel();
      callStatus.value = 'Call Ended';
      Future.delayed(
        const Duration(seconds: 1),
        () => Navigator.pop(context),
      );
    };
    rtmClient.onRemoteInvitationRefused = (invite) {
      // print('----- Remote Refuse');
      NavigatorUtils.pop(context);
    };
    rtmClient.onRemoteInvitationReceivedByPeer = (invite) {
      if (callerDeviceType == '') {
        callStatus.value = 'Calling';
        NavigatorUtils.push(
            context,
            VoiceCall(
              joined: false,
              channelId: invite.channelId!,
              peerId: invite.callerId,
              peerName: jsonDecode(invite.content!)['name'],
              image: jsonDecode(invite.content!)['image'],
            ));
      }
    };
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.resumed:
        try {
          await rtmClient.login(
              null, HiveUtils.get(HiveKeys.user).toString().padLeft(4, '0'));
        } catch (error) {}
        break;
      case AppLifecycleState.paused:
        try {
          rtmClient.logout();
        } catch (error) {}
        break;
      case AppLifecycleState.detached:
        try {
          rtmClient.logout();
        } catch (error) {}
        break;
    }
  }

  @override
  void dispose() {
    _index.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RestartAppBloc, RestartAppState>(
      listener: (context, state) {
        state.when(
          initial: () {},
          loading: () {},
          success: () {
            MyApp.restartApp(context);
          },
        );
      },
      child: WillPopScope(
        onWillPop: () async {
          if (_index.value == 0) {
            return true;
          } else {
            _index.value = 0;
            return false;
          }
        },
        child: ValueListenableBuilder(
          valueListenable: _index,
          builder: (context, value, child) {
            myShiftTimer?.cancel();
            openShiftTimer?.cancel();
            return Scaffold(
              drawerEnableOpenDragGesture: false,
              key: drawerKey,
              drawer: SaDrawer(),
              appBar: AppBar(),
              body: IndexedStack(
                index: value,
                children: body,
              ),
              bottomNavigationBar: ClipRRect(
                borderRadius: const BorderRadius.horizontal(
                  left: Radius.circular(20.0),
                  right: Radius.circular(20.0),
                ),
                child: BottomNavigationBar(
                  backgroundColor: SaColors.white,
                  elevation: 0.0,
                  iconSize: 20.0,
                  type: BottomNavigationBarType.fixed,
                  selectedLabelStyle: const TextStyle(
                    fontSize: 10.0,
                    fontWeight: FontWeight.w600,
                  ),
                  unselectedLabelStyle: const TextStyle(
                    fontSize: 10.0,
                    fontWeight: FontWeight.w400,
                  ),
                  currentIndex: value,
                  onTap: (value) {
                    _index.value = value;
                  },
                  items: [
                    BottomNavigationBarItem(
                      icon: SvgPicture.asset(SaIcons.home),
                      activeIcon: SvgPicture.asset(SaIcons.homeFill),
                      label: 'Home',
                    ),
                    BottomNavigationBarItem(
                      icon: SvgPicture.asset(SaIcons.schedule),
                      activeIcon: SvgPicture.asset(SaIcons.scheduleFill),
                      label: 'Scheduling',
                    ),
                    BottomNavigationBarItem(
                      icon: SvgPicture.asset(SaIcons.timesheet),
                      activeIcon: SvgPicture.asset(SaIcons.timesheetFill),
                      label: 'Timesheets',
                    ),
                    BottomNavigationBarItem(
                      icon: SvgPicture.asset(SaIcons.credential),
                      activeIcon: SvgPicture.asset(SaIcons.credentialFill),
                      label: 'Credentials',
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
