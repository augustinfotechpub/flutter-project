import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/feedback/bloc/feedback_bloc.dart';
import 'package:shift_alerts/src/provider/admin/get_credentials/get_clinician/bloc/get_clinician_bloc.dart';
import 'package:shift_alerts/src/provider/admin/get_credentials/model.dart';
import 'package:shift_alerts/src/provider/edit_profile/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/chat/chat.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class AdminCredentialsDetails extends StatefulWidget {
  const AdminCredentialsDetails({
    super.key,
    required this.credentialList,
  });
  final List<CredentialDetails> credentialList;

  @override
  State<AdminCredentialsDetails> createState() =>
      _AdminCredentialsDetailsState();
}

class _AdminCredentialsDetailsState extends State<AdminCredentialsDetails> {
  final GetClinicianBloc _getClinicianBloc = GetClinicianBloc();

  final FeedbackBloc _feedbackBloc = FeedbackBloc();

  @override
  void initState() {
    _feedbackBloc.add(FeedbackEvent.getFeedback(
      position: null,
      id: widget.credentialList.first.clinicianId.toString(),
    ));
    _getClinicianBloc.add(GetClinicianEvent.getClinician(
      widget.credentialList.first.clinicianId.toString(),
    ));

    super.initState();
  }

  @override
  void dispose() {
    _getClinicianBloc.close();
    _feedbackBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => _feedbackBloc),
        BlocProvider(create: (context) => _getClinicianBloc)
      ],
      child: Scaffold(
        appBar: appBar(
          context,
          title: widget.credentialList.first.clinicianName.capitalize(),
        ),
        body: BlocBuilder<GetClinicianBloc, GetClinicianState>(
          builder: (context, state) {
            return state.when(
              initial: () => const SizedBox.shrink(),
              loading: () {
                return SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: const Center(
                    child: Loader(),
                  ),
                );
              },
              success: (user) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(height: 12),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _profilePart(user: user),
                            const SizedBox(height: 24),
                            _detailsCard(
                              SaIcons.schedule,
                              'DOB: ${user.dateOfBirth.split('-').reversed.join("/")}',
                            ),
                            const SizedBox(height: 24),
                            _detailsCard(
                              SaIcons.phone,
                              user.contactNo,
                            ),
                            const SizedBox(height: 24),
                            _detailsCard(
                              SaIcons.location,
                              'San Fransico, USA',
                            ),
                            const SizedBox(height: 24),
                            _detailsCard(
                              SaIcons.mail,
                              user.email,
                            ),
                            const SizedBox(height: 32),
                            Text(
                              SaString.credentials,
                              style:
                                  SaTextStyles.heading.copyWith(fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 24),
                      _table(),
                      const SizedBox(height: 24),
                    ],
                  ),
                );
              },
              error: (error) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: Center(
                    child: Text(
                      error,
                      style: SaTextStyles.title,
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }

  Widget _profilePart({required Profile user}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(16.0),
          child: CachedNetworkImage(
            placeholder: (context, url) =>
                const ColoredBox(color: SaColors.lightPink),
            imageUrl: user.avatarImage,
            height: 140.0,
            width: 140.0,
            fit: BoxFit.cover,
            errorWidget: (context, url, error) => const Icon(
              Icons.error_outline_rounded,
              color: SaColors.red,
            ),
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${user.firstname.capitalize()} ${user.lastname.capitalize()}',
                overflow: TextOverflow.ellipsis,
                style: SaTextStyles.heading.copyWith(fontSize: 18),
              ),
              const SizedBox(height: 4),
              Column(
                children: [
                  Text(
                    '${user.clinicianPosition}, ${user.jobSites.first}',
                    overflow: TextOverflow.ellipsis,
                    style: SaTextStyles.subtitleLarge
                        .copyWith(color: SaColors.hintColor),
                  ),
                ],
              ),
              const SizedBox(height: 4),
              BlocBuilder<FeedbackBloc, FeedbackState>(
                builder: (context, state) {
                  return state.when(
                    initial: () => const SizedBox.shrink(),
                    loading: () => const Loader(),
                    success: (feedbacks, position) {
                      double rating = 0.0;
                      int feedBackCount = 0;
                      for (final feed in feedbacks) {
                        rating += int.parse(
                            feed.ratings.isEmpty ? '0' : feed.ratings);

                        if (feed.feedback.isNotEmpty) {
                          feedBackCount += 1;
                        }
                      }
                      rating = rating / feedbacks.length;

                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(SaIcons.rating),
                          const SizedBox(width: 4),
                          Text(
                            '${rating.toStringAsFixed(1)} ',
                            style: SaTextStyles.heading.copyWith(fontSize: 18),
                          ),
                          Expanded(
                            child: AutoSizeText(
                              '($feedBackCount feedbacks)',
                              style: SaTextStyles.subtitleLarge.copyWith(
                                color: SaColors.hintColor,
                              ),
                              maxLines: 1,
                              minFontSize: 8,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      );
                    },
                    error: (error) => const SizedBox.shrink(),
                  );
                },
              ),
              const SizedBox(height: 20),
              SaOutlineButton(
                onPressed: () {
                  int userId = HiveUtils.get(HiveKeys.user);
                  int peerId = user.user;
                  String channelId;
                  if (userId > peerId) {
                    channelId = '${userId}_$peerId';
                  } else {
                    channelId = '${peerId}_$userId';
                  }
                  NavigatorUtils.push(
                      context,
                      Chat(
                        image: user.avatarImage,
                        name:
                            '${user.firstname.capitalize()} ${user.lastname.capitalize()}',
                        channelId: channelId,
                        peerId: user.user.toString(),
                      )).then((value) {
                    currentChannel = '';
                  });
                },
                width: 120,
                height: 44,
                borderRadius: 12,
                borderColor: SaColors.primary,
                text: const Text(
                  SaString.message,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500,
                    color: SaColors.primary,
                  ),
                ),
                icon: SvgPicture.asset(
                  SaIcons.chat,
                  color: SaColors.primary,
                  height: 24,
                  width: 24,
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _detailsCard(String image, String text) {
    return Row(
      children: [
        SvgPicture.asset(image, color: SaColors.gray, height: 24, width: 24),
        const SizedBox(width: 8),
        Text(text, style: SaTextStyles.subtitleLarge),
      ],
    );
  }

  Widget _table() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          const SizedBox(width: 24.0),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                children: [
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 950),
                    child: Card(
                      color: SaColors.scaffold,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      margin: EdgeInsets.zero,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SaString.credentials,
                          SaString.lastUpdated,
                          SaString.status,
                        ]
                            .map((e) => Container(
                                  width: 850 / 3,
                                  height: 52,
                                  padding: const EdgeInsets.only(left: 16.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    e,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                      color: SaColors.black,
                                    ),
                                  ),
                                ))
                            .toList()
                          ..add(Container(width: 100)),
                      ),
                    ),
                  ),
                  for (final credential in widget.credentialList)
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: _tableRow(
                        credetials: credential.documentName,
                        date: credential.documentUpdatedDate.dmY(),
                        status: credential.documentStatus,
                        url: credential.documentPath,
                      ),
                    ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 24.0),
        ],
      ),
    );
  }

  Widget _tableRow({
    required String credetials,
    required String date,
    required String status,
    required String url,
  }) {
    Color chipTextColors() {
      if (status.isPending()) {
        return SaColors.orange;
      } else if (status.isApproved()) {
        return SaColors.green;
      } else if (status.isDeclined()) {
        return SaColors.red;
      } else {
        return SaColors.black;
      }
    }

    Color chipChipColors() {
      if (status.isPending()) {
        return SaColors.lightOrange;
      } else if (status.isApproved()) {
        return SaColors.lightGreen;
      } else if (status.isDeclined()) {
        return SaColors.lightPink;
      } else {
        return SaColors.white;
      }
    }

    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 950, minHeight: 40),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 850 / 3,
            child: Row(
              children: [
                SvgPicture.asset(
                  SaIcons.credential,
                  color: SaColors.hintColor,
                  height: 24,
                  width: 24,
                ),
                const SizedBox(width: 8),
                Text(
                  credetials,
                  style: const TextStyle(
                    fontSize: 16,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w400,
                    color: SaColors.black,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 850 / 3,
            child: Text(
              date,
              style: SaTextStyles.subtitleLarge.copyWith(fontSize: 16),
            ),
          ),
          SizedBox(
            width: 850 / 3,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Chip(
                backgroundColor: chipChipColors(),
                label: Text(
                  status,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: chipTextColors(),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 100,
            child: IconButton(
              onPressed: () async {
                Loader.show(context);
                await downloadFileandNotification(url, credetials)
                    .then((_) => Loader.hide(context));
              },
              icon: const Icon(Icons.download),
            ),
          )
        ],
      ),
    );
  }

  Future<void> downloadFileandNotification(String url, String name) async {
    try {
      final directory = Platform.isIOS
          ? await getApplicationSupportDirectory()
          : Directory('storage/emulated/0/Download/Shift Alert');
      if (Platform.isAndroid && !(await directory.exists())) {
        await directory.create();
      }
      File file = Platform.isIOS
          ? File('${directory.path}/$name.pdf')
          : File('${directory.path}/$name');

      await _downloadPDF(url, name, file).then(
        (newFile) async {
          if (newFile != null) {
            final savedFile = await newFile
                .copy('${directory.path}/${name}_${randomNumber()}.pdf');
            await file.delete();
            await notificationsUtils
                .localNotification(payload: savedFile.path, title: '$name.pdf')
                .then((_) {
              Toast.success(context, "Download successfully");
            });
          }
        },
      );
    } catch (e) {
      Toast.error(context, 'Storage Permission Denied');
    }
  }

  Future<File?> _downloadPDF(String url, String name, File file) async {
    try {
      final request = await HttpClient().getUrl(Uri.parse(url));
      final response = await request.close();
      if (response.statusCode == 200) {
        final bytes = await consolidateHttpClientResponseBytes(response);
        return await file.writeAsBytes(bytes);
      }
    } catch (e) {
      Toast.success(context, "something went't wrong");
    }
    return null;
  }
}
