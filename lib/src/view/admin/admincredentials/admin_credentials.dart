import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/get_credentials/bloc/credentials_bloc.dart';
import 'package:shift_alerts/src/provider/admin/get_credentials/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/view/admin/admincredentials/admin_cred_details.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class AdminCreddentials extends StatefulWidget {
  const AdminCreddentials({super.key});

  @override
  State<AdminCreddentials> createState() => _AdminCreddentialsState();
}

class _AdminCreddentialsState extends State<AdminCreddentials>
    with TickerProviderStateMixin {
  final CredentialsBloc _credentialsBloc = CredentialsBloc();

  final _searchController = TextEditingController();

  Map<int, List<CredentialDetails>>? gropedModel;
  Map<int, List<CredentialDetails>> _searchModel = {};

  final searchStart = ValueNotifier<int>(0);

  String _dropValue = 'All position';

  final List<String> _list = [
    'All position',
    'CNA',
    'RN',
    'STNA',
    'LVN',
    'LPN',
    'CMA',
  ];

  @override
  void initState() {
    _credentialsBloc.add(const CredentialsEvent.getCredential(position: ''));
    super.initState();
  }

  @override
  void dispose() {
    searchStart.dispose();
    _searchController.dispose();
    _credentialsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _credentialsBloc,
      child: Column(
        children: [
          BlocBuilder<CredentialsBloc, CredentialsState>(
            builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 16),
                    _title(),
                    const SizedBox(height: 16),
                    _searchField(),
                  ],
                ),
              );
            },
          ),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                _credentialsBloc
                    .add(const CredentialsEvent.getCredential(position: ''));
                await Future.delayed(const Duration(milliseconds: 500));
              },
              child: ListView(
                children: [
                  GestureDetector(
                    onTap: () =>
                        FocusScope.of(context).requestFocus(FocusNode()),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      child: BlocConsumer<CredentialsBloc, CredentialsState>(
                        listener: (context, state) {
                          state.when(
                            initial: () {},
                            loading: () => Loader.show(context),
                            success: (credentialModel, position) {
                              Loader.hide(context);
                              _dropValue =
                                  position == '' ? 'All position' : position;

                              gropedModel = credentialModel.results
                                  .groupBy((element) => element.clinicianId);
                            },
                            error: (error) {
                              Loader.hide(context);
                              Toast.error(context, error);
                            },
                          );
                        },
                        builder: (context, state) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(height: 24.0),
                              gropedModel == null
                                  ? const SizedBox.shrink()
                                  : gropedModel!.isEmpty
                                      ? const Noshift(
                                          message: "No Credentials Found")
                                      : ValueListenableBuilder(
                                          valueListenable: searchStart,
                                          builder: (context, value, child) {
                                            if (value != 0) {
                                              if (_searchModel.keys.isEmpty) {
                                                return const Noshift(
                                                  message: 'No document Found',
                                                );
                                              }
                                              return ListView.separated(
                                                shrinkWrap: true,
                                                itemCount:
                                                    _searchModel.keys.length,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                separatorBuilder: (context,
                                                        index) =>
                                                    const SizedBox(height: 16),
                                                itemBuilder: (context, index) {
                                                  return clincianCard(
                                                    _searchModel.values
                                                        .elementAt(index),
                                                  );
                                                },
                                              );
                                            } else {
                                              return ListView.separated(
                                                shrinkWrap: true,
                                                itemCount:
                                                    gropedModel!.keys.length,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                separatorBuilder: (context,
                                                        index) =>
                                                    const SizedBox(height: 16),
                                                itemBuilder: (context, index) {
                                                  return clincianCard(
                                                    gropedModel!.values
                                                        .elementAt(index),
                                                  );
                                                },
                                              );
                                            }
                                          },
                                        ),
                              const SizedBox(height: 24.0),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _title() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(
          SaString.documents,
          style: SaTextStyles.heading,
        ),
        SaDropDown(
          value: _dropValue,
          valuesList: _list,
          onChanged: (value) {
            if (value != null && _dropValue != value) {
              _credentialsBloc.add(
                CredentialsEvent.getCredential(
                  position: value == 'All position' ? '' : value,
                ),
              );
            }
          },
        ),
      ],
    );
  }

  Widget _searchField() {
    return SaTextField(
      controller: _searchController,
      hintText: SaString.search,
      fillColor: SaColors.white,
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 12,
      ),
      onChanged: (p0) {
        if (p0.trim().isNotEmpty && gropedModel != null) {
          final valueList = gropedModel!.values
              .where((element) => element.any((e) =>
                  e.clinicianName.toLowerCase().contains(p0.toLowerCase())))
              .toList();

          if (valueList.isNotEmpty) {
            List<int> keyList = [];
            for (final value in valueList) {
              keyList.add(value.first.clinicianId.toInt());
            }
            for (int i = 0; i < keyList.length; i++) {
              _searchModel[keyList[i]] = gropedModel![keyList[i]]!;
            }
            searchStart.value = randomNumber();
          } else {
            _searchModel = {};
            searchStart.value = randomNumber();
          }
        } else {
          searchStart.value = 0;
        }
      },
      hintStyle: const TextStyle(
        fontSize: 14,
        fontFamily: 'Poppins',
        color: SaColors.hintColor,
      ),
      prefixIcon: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 15.0,
        ),
        child: SvgPicture.asset(
          SaIcons.search,
          color: SaColors.hintColor,
          height: 16,
          width: 16,
        ),
      ),
    );
  }

  Widget clincianCard(List<CredentialDetails> credentialList) {
    return GestureDetector(
      onTap: () => NavigatorUtils.push(
          context,
          AdminCredentialsDetails(
            credentialList: credentialList,
          )),
      child: Card(
        color: SaColors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(72.0),
                    child: CachedNetworkImage(
                      placeholder: (context, url) =>
                          const ColoredBox(color: SaColors.lightPink),
                      imageUrl: credentialList.first.clinicianAvatar,
                      height: 48.0,
                      width: 48.0,
                      fit: BoxFit.cover,
                      errorWidget: (context, url, error) => const Icon(
                        Icons.error_outline_rounded,
                        color: SaColors.red,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Text(
                    credentialList.first.clinicianName.capitalize(),
                    style: SaTextStyles.subtitleLarge
                        .copyWith(fontWeight: FontWeight.w600),
                  )
                ],
              ),
              const SizedBox(height: 12),
              Row(
                children: [
                  SvgPicture.asset(
                    SaIcons.unit,
                    color: SaColors.hintColor,
                    height: 16,
                    width: 20,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    credentialList.first.clinicianPosition,
                    style: SaTextStyles.subtitleLarge.copyWith(
                      fontSize: 16,
                    ),
                  )
                ],
              ),
              const SizedBox(height: 12),
              Row(
                children: [
                  SvgPicture.asset(
                    SaIcons.credential,
                    color: SaColors.hintColor,
                    height: 20,
                    width: 20,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    '${credentialList.where((element) => element.documentStatus.isPending()).toList().length} pending',
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: SaColors.yellow,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
