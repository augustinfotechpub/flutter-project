import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';

class SaPieChart extends StatelessWidget {
  final bool showBadge;
  final int length;
  const SaPieChart({
    super.key,
    this.showBadge = true,
    this.length = 4,
  });

  @override
  Widget build(BuildContext context) {
    return PieChart(
      PieChartData(
        borderData: FlBorderData(show: false),
        sectionsSpace: 2.0,
        sections: List.generate(
          length,
          (index) => PieChartSectionData(
            color: SaColors.pink,
            value: 40,
            showTitle: false,
            radius: 16.0,
            badgeWidget: showBadge ? const _Badge('40%') : null,
            badgePositionPercentageOffset: .98,
          ),
        ),
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(length, (i) {
      const radius = 16.0;

      switch (i) {
        case 0:
          return PieChartSectionData(
            color: SaColors.pink,
            value: 40,
            showTitle: false,
            radius: radius,
            badgeWidget: showBadge ? const _Badge('40%') : null,
            badgePositionPercentageOffset: .98,
          );
        case 1:
          return PieChartSectionData(
            color: SaColors.orange,
            value: 30,
            showTitle: false,
            radius: radius,
            badgeWidget: showBadge ? const _Badge('30%') : null,
            badgePositionPercentageOffset: .98,
          );
        case 2:
          return PieChartSectionData(
            color: SaColors.primary,
            value: 16,
            showTitle: false,
            radius: radius,
            badgeWidget: showBadge ? const _Badge('16%') : null,
            badgePositionPercentageOffset: .98,
          );
        case 3:
          return PieChartSectionData(
            color: SaColors.green,
            value: 15,
            showTitle: false,
            radius: radius,
            badgeWidget: showBadge ? const _Badge('15%') : null,
            badgePositionPercentageOffset: .98,
          );
        default:
          throw 'Oh no';
      }
    });
  }
}

class _Badge extends StatelessWidget {
  final String text;

  const _Badge(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 20.0,
      height: 20.0,
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
      alignment: Alignment.center,
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 7,
          color: SaColors.black,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;

  const Indicator({
    Key? key,
    required this.color,
    required this.text,
    required this.isSquare,
    this.size = 16,
    this.textColor = const Color(0xff505050),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
            color: color,
          ),
        ),
        const SizedBox(
          width: 4,
        ),
        Text(
          text,
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: textColor),
        )
      ],
    );
  }
}
