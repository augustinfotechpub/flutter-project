import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/image.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/feedback/bloc/feedback_bloc.dart';
import 'package:shift_alerts/src/provider/timesheet/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/read_more.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class AdminFeedBack extends StatefulWidget {
  const AdminFeedBack({super.key});

  @override
  State<AdminFeedBack> createState() => _AdminFeedBackState();
}

class _AdminFeedBackState extends State<AdminFeedBack> {
  final FeedbackBloc _feedbackBloc = FeedbackBloc();
  final _searchController = TextEditingController();

  TextStyle getTextStyle(bool lightText) => TextStyle(
        fontSize: 12,
        fontFamily: 'Poppins',
        fontWeight: lightText ? FontWeight.w400 : FontWeight.w500,
        color: lightText ? SaColors.hintColor : SaColors.gray,
      );

  String _dropValue = 'All position';
  List<TimesheetDetail>? _feedbacks;
  List<TimesheetDetail> _searchedFeedbacks = [];

  final searchStart = ValueNotifier<int>(0);

  final List<String> _list = [
    'All position',
    'CNA',
    'RN',
    'STNA',
    'LVN',
    'LPN',
    'CMA',
  ];

  @override
  void initState() {
    _feedbackBloc.add(const FeedbackEvent.getFeedback(position: '', id: null));
    super.initState();
  }

  @override
  void dispose() {
    searchStart.dispose();
    _searchController.dispose();
    _feedbackBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _feedbackBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Column(
          children: [
            BlocBuilder<FeedbackBloc, FeedbackState>(
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        SaString.feedbackRatings,
                        style: SaTextStyles.heading,
                      ),
                      const SizedBox(height: 16),
                      _filter(),
                      const SizedBox(height: 24),
                    ],
                  ),
                );
              },
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () async {
                  _feedbackBloc.add(
                      const FeedbackEvent.getFeedback(position: '', id: null));
                  await Future.delayed(const Duration(milliseconds: 500));
                },
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      child: BlocConsumer<FeedbackBloc, FeedbackState>(
                        listener: (context, state) {
                          state.when(
                            initial: () {},
                            loading: () => Loader.show(context),
                            success: (feedbacks, position) {
                              if (position == '') {
                                _dropValue = 'All position';
                              } else {
                                _dropValue = position;
                              }
                              _feedbacks = feedbacks;
                              Loader.hide(context);
                            },
                            error: (error) {
                              Loader.hide(context);
                              Toast.error(context, error);
                            },
                          );
                        },
                        builder: (context, state) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _feedbacks == null
                                  ? const SizedBox.shrink()
                                  : _feedbacks!.isEmpty
                                      ? SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              1.5,
                                          child: Center(
                                            child: Noshift(
                                              message:
                                                  "No Feedback available For $_dropValue",
                                            ),
                                          ),
                                        )
                                      : ValueListenableBuilder(
                                          valueListenable: searchStart,
                                          builder: (context, value, child) {
                                            if (value != 0) {
                                              if (_searchedFeedbacks.isEmpty) {
                                                return const Noshift(
                                                    message:
                                                        'No feedback Found');
                                              }
                                              return ListView.builder(
                                                itemCount:
                                                    _searchedFeedbacks.length,
                                                shrinkWrap: true,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                itemBuilder: (_, index) {
                                                  return _eventCard(
                                                    ctx: context,
                                                    feedbackDetail:
                                                        _searchedFeedbacks[
                                                            index],
                                                    feedbacks:
                                                        _searchedFeedbacks[
                                                                index]
                                                            .feedback,
                                                    position:
                                                        _searchedFeedbacks[
                                                                index]
                                                            .clinicianPosition,
                                                    rating: _searchedFeedbacks[
                                                                    index]
                                                                .ratings ==
                                                            ''
                                                        ? 0
                                                        : int.parse(
                                                            _searchedFeedbacks[
                                                                    index]
                                                                .ratings),
                                                  );
                                                },
                                              );
                                            } else {
                                              return ListView.builder(
                                                itemCount: _feedbacks!.length,
                                                shrinkWrap: true,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                itemBuilder: (_, index) {
                                                  return _eventCard(
                                                    ctx: context,
                                                    feedbackDetail:
                                                        _feedbacks![index],
                                                    feedbacks:
                                                        _feedbacks![index]
                                                            .feedback,
                                                    position: _feedbacks![index]
                                                        .clinicianPosition,
                                                    rating: _feedbacks![index]
                                                                .ratings ==
                                                            ''
                                                        ? 0
                                                        : int.parse(
                                                            _feedbacks![index]
                                                                .ratings),
                                                  );
                                                },
                                              );
                                            }
                                          },
                                        )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _filter() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 140,
          height: 44,
          child: SaTextField(
            controller: _searchController,
            hintText: SaString.search,
            fillColor: SaColors.white,
            onChanged: (p0) {
              if (p0.trim().isNotEmpty && _feedbacks != null) {
                final list = _feedbacks!
                    .where((element) => element.clinicianName
                        .toLowerCase()
                        .contains(p0.toLowerCase()))
                    .toList();

                if (list.isNotEmpty) {
                  _searchedFeedbacks = list;
                  searchStart.value = randomNumber();
                } else {
                  _searchedFeedbacks = [];
                  searchStart.value = randomNumber();
                }
              } else {
                searchStart.value = 0;
              }
            },
            hintStyle:
                SaTextStyles.subtitleLarge.copyWith(color: SaColors.hintColor),
            prefixIcon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: SvgPicture.asset(
                SaIcons.search,
                color: SaColors.hintColor,
              ),
            ),
          ),
        ),
        SaDropDown(
          value: _dropValue,
          valuesList: _list,
          onChanged: (p0) {
            if (p0 != null && p0 != _dropValue) {
              _feedbackBloc.add(
                FeedbackEvent.getFeedback(
                  position: p0 == 'All position' ? '' : p0,
                  id: null,
                ),
              );
            }
          },
          buttonHeight: 44,
        ),
      ],
    );
  }

  Widget _eventCard({
    required BuildContext ctx,
    required String position,
    required int rating,
    required String feedbacks,
    required TimesheetDetail feedbackDetail,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Card(
        color: SaColors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const CircleAvatar(
                    maxRadius: 18,
                    backgroundImage: AssetImage(SaImages.nurseProfile),
                  ),
                  const SizedBox(width: 8),
                  Text(
                    feedbackDetail.clinicianName.capitalize(),
                    style: SaTextStyles.subtitleLarge
                        .copyWith(fontWeight: FontWeight.w600),
                  )
                ],
              ),
              const SizedBox(height: 12),
              Row(
                children: [
                  _cardImages(SaIcons.person, false),
                  const SizedBox(width: 8),
                  Text(
                    '${SaString.position}:',
                    style: getTextStyle(true),
                  ),
                  const SizedBox(width: 8),
                  Text(
                    feedbackDetail.clinicianPosition.toUpperCase(),
                    style: getTextStyle(false),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Row(children: [
                  _cardImages(SaIcons.star, false),
                  const SizedBox(width: 8),
                  Text(
                    '${SaString.ratings}:',
                    style: getTextStyle(true),
                  ),
                  Row(
                    children: [
                      for (int i = 0; i < rating; i++)
                        _cardImages(SaIcons.rating, true),
                      for (int j = 0; j < 5 - rating; j++)
                        _cardImages(SaIcons.hollowRating, true),
                    ],
                  )
                ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _cardImages(SaIcons.chatBubble, false),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ExpandableText(
                          text: feedbackDetail.feedback,
                          onMoreorLessTap: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardImages(String image, bool isRatingImage) => Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: SvgPicture.asset(
          image,
          color: isRatingImage ? null : SaColors.hintColor,
          height: 16,
        ),
      );
}
