import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/textfield.dart';

class WorkForceCalculator extends StatefulWidget {
  const WorkForceCalculator({super.key});

  @override
  State<WorkForceCalculator> createState() => _WorkForceCalculatorState();
}

class _WorkForceCalculatorState extends State<WorkForceCalculator> {
  final TextEditingController _hourlyPayRate = TextEditingController();
  final TextEditingController _markup = TextEditingController();
  final TextEditingController _burden = TextEditingController();
  final TextEditingController _employeeBenefits = TextEditingController();
  final TextEditingController _hrResources = TextEditingController();
  final TextEditingController _totalCostPerHour = TextEditingController();
  final TextEditingController _totalCostPerHour2 = TextEditingController();
  final TextEditingController _savingPerHour = TextEditingController();
  final TextEditingController _costSaving = TextEditingController();
  final TextEditingController _numberOfEmployee = TextEditingController();
  final TextEditingController _totalSaving = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _hourlyPayRate.dispose();
    _markup.dispose();
    _burden.dispose();
    _employeeBenefits.dispose();
    _hrResources.dispose();
    _totalCostPerHour.dispose();
    _totalCostPerHour2.dispose();
    _savingPerHour.dispose();
    _costSaving.dispose();
    _numberOfEmployee.dispose();
    _totalSaving.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              const Text(
                SaString.workforceCalculator,
                style: SaTextStyles.heading,
              ),
              const SizedBox(height: 16),
              _instructionCard(),
              const SizedBox(height: 20),
              _textFieldsname(),
              const SizedBox(height: 8),
              const Divider(
                color: SaColors.white,
                height: 1,
                thickness: 2,
              ),
              const SizedBox(height: 16),
              _calculator(),
              const SizedBox(height: 24),
              SaButton(
                onPressed: () {
                  _totalCostPerHour.text =
                      '\$${(double.parse(_hourlyPayRate.text.replaceAll('\$', '')) + double.parse(_burden.text.replaceAll('\$', '')) + double.parse(_employeeBenefits.text.replaceAll('\$', '')) + double.parse(_hrResources.text.replaceAll('\$', ''))).toStringAsFixed(2)}';

                  _totalCostPerHour2.text =
                      '\$${(((double.parse(_hourlyPayRate.text.replaceAll('\$', '')) * double.parse(_markup.text.replaceAll('%', '') == '' ? '0' : _markup.text.replaceAll('%', ''))) / 100) + double.parse(_hourlyPayRate.text.replaceAll('\$', ''))).toStringAsFixed(2)}';

                  _savingPerHour.text =
                      '\$${(double.parse(_totalCostPerHour.text.replaceAll('\$', '')) - double.parse(_totalCostPerHour2.text.replaceAll('\$', ''))).toStringAsFixed(2)}';

                  _costSaving.text =
                      '${((double.parse(_savingPerHour.text.replaceAll('\$', '')) / double.parse(_totalCostPerHour.text.replaceAll('\$', ''))) * 100.1).toStringAsFixed(2)}%';
                },
                radius: 16,
                height: 48,
                text: SaString.calculateResults,
                textStyle: SaTextStyles.title.copyWith(
                  color: SaColors.white,
                ),
              ),
              const SizedBox(height: 32),
              _textFieldsname(),
              const SizedBox(height: 8),
              const Divider(
                color: SaColors.white,
                height: 1,
                thickness: 2,
              ),
              const SizedBox(height: 16),
              _calculator2(),
              const SizedBox(height: 48),
            ],
          ),
        ),
      ),
    );
  }

  Widget _instructionCard() {
    return Card(
      color: SaColors.lightGreen,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              SaString.instructions,
              style: SaTextStyles.subtitleSmall
                  .copyWith(fontWeight: FontWeight.w500, color: SaColors.green),
            ),
            const SizedBox(height: 8),
            Text(
              SaString.instructionsDetails,
              style: SaTextStyles.subtitleSmall
                  .copyWith(fontWeight: FontWeight.w500, color: SaColors.green),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textFieldsname() {
    return SizedBox(
      height: 32,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Expanded(child: SizedBox.shrink()),
          SizedBox(
            width: 100,
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                SaString.facilityCost,
                style: SaTextStyles.subtitleSmall
                    .copyWith(fontWeight: FontWeight.w500),
              ),
            ),
          ),
          const SizedBox(width: 20),
          SizedBox(
            width: 100,
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                SaString.shiftAlertCost,
                textAlign: TextAlign.center,
                style: SaTextStyles.subtitleSmall
                    .copyWith(fontWeight: FontWeight.w500),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _calculator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _calCulatorText(SaString.hourlyPayRate),
              const SizedBox(height: 20),
              _calCulatorText(SaString.markup),
              const SizedBox(height: 20),
              _calCulatorText(SaString.burden),
              const SizedBox(height: 20),
              _calCulatorText(SaString.employeeBenefits),
              const SizedBox(height: 20),
              _calCulatorText(SaString.hRResources),
            ],
          ),
        ),
        Column(
          children: [
            _textField(
              controller: _hourlyPayRate,
              hintText: '\$15.00',
              onChanged: (value) {
                if (value.replaceAll('\$', '') != '') {
                  fieldText(_hourlyPayRate);

                  _burden.text =
                      ((double.parse(value.replaceAll('\$', '')) * 13.51) / 100)
                          .toStringAsFixed(2);
                  _employeeBenefits.text =
                      ((double.parse(value.replaceAll('\$', '')) * 37) / 100)
                          .toStringAsFixed(2);
                  _hrResources.text =
                      ((double.parse(value.replaceAll('\$', '')) * 10) / 100)
                          .toStringAsFixed(2);
                  fieldText(_burden);
                  fieldText(_employeeBenefits);
                  fieldText(_hrResources);
                } else {
                  fieldText(_hourlyPayRate);
                  _burden.text = '';
                  _employeeBenefits.text = '';
                  _hrResources.text = '';
                }
              },
            ),
            const SizedBox(height: 80),
            _textField(
              controller: _burden,
              hintText: '\$2.03',
              onChanged: (value) {
                if (value.replaceAll('\$', '') != '') {
                  fieldText(_burden);
                } else {
                  fieldText(_burden);
                }
              },
            ),
            const SizedBox(height: 20),
            _textField(
              controller: _employeeBenefits,
              hintText: '\$5.55',
              onChanged: (value) {
                if (value.replaceAll('\$', '') != '') {
                  fieldText(_employeeBenefits);
                } else {
                  fieldText(_employeeBenefits);
                }
              },
            ),
            const SizedBox(height: 20),
            _textField(
              controller: _hrResources,
              hintText: '\$1.50',
              onChanged: (value) {
                if (value.replaceAll('\$', '') != '') {
                  fieldText(_hrResources);
                } else {
                  fieldText(_hrResources);
                }
              },
            ),
          ],
        ),
        const SizedBox(width: 16),
        Column(
          children: [
            _textField(
              controller: _hourlyPayRate,
              hintText: '',
              readOnly: true,
            ),
            const SizedBox(height: 20),
            _textField(
              controller: _markup,
              hintText: '0',
              onChanged: (value) {
                if (value.replaceAll('%', '') != '') {
                  String text = value.replaceAll('%', '');
                  _markup.text = '$text%';
                  _markup.selection = TextSelection.fromPosition(
                      TextPosition(offset: _markup.text.length - 1));
                } else {
                  _markup.text = '';
                  _markup.selection = TextSelection.fromPosition(
                      TextPosition(offset: _markup.text.length));
                }
              },
            ),
            const SizedBox(height: 20),
            _textField(
              controller: TextEditingController(),
              hintText: '\$0',
              readOnly: true,
            ),
            const SizedBox(height: 20),
            _textField(
              controller: TextEditingController(),
              hintText: '\$0',
              readOnly: true,
            ),
            const SizedBox(height: 20),
            _textField(
              controller: TextEditingController(),
              hintText: '\$0',
              readOnly: true,
            ),
          ],
        ),
      ],
    );
  }

  void fieldText(TextEditingController controller) {
    String text = controller.text.replaceAll('\$', '');
    if (text == '') {
      controller.text = '';
    } else {
      controller.text = '\$$text';
    }
    controller.selection = TextSelection.fromPosition(
        TextPosition(offset: controller.text.length));
  }

  Widget _calCulatorText(String text) {
    return SizedBox(
      height: 40,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style:
              SaTextStyles.subtitleSmall.copyWith(fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  Widget _textField({
    required TextEditingController controller,
    required String hintText,
    bool isSecondCalculator = false,
    bool readOnly = false,
    void Function(String)? onChanged,
  }) {
    return SizedBox(
      width: 100,
      height: isSecondCalculator ? 56 : 40,
      child: SaTextField(
        keyboardType: const TextInputType.numberWithOptions(decimal: true),
        controller: controller,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 12,
          vertical: 10,
        ),
        fillColor: SaColors.white,
        hintText: hintText,
        hintStyle: SaTextStyles.subtitleLarge.copyWith(
          color: SaColors.hintColor,
        ),
        readOnly: readOnly,
        onChanged: onChanged,
      ),
    );
  }

  Widget _calculator2() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: _calCulatorText(SaString.totalCostPerHour)),
            _textField(
              controller: _totalCostPerHour,
              hintText: '',
              isSecondCalculator: true,
              readOnly: true,
            ),
            const SizedBox(width: 16),
            _textField(
              controller: _totalCostPerHour2,
              hintText: '',
              isSecondCalculator: true,
              readOnly: true,
            ),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: _calCulatorText(SaString.savingsPerHour)),
            SizedBox(
              width: 216,
              height: 56,
              child: SaTextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _savingPerHour,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 16,
                ),
                fillColor: SaColors.lightGreen,
                hintText: '',
                readOnly: true,
              ),
            )
          ],
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: _calCulatorText(SaString.costSavingsPerHour)),
            SizedBox(
              width: 216,
              height: 56,
              child: SaTextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _costSaving,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 16,
                ),
                fillColor: SaColors.white,
                hintText: '',
                readOnly: true,
              ),
            ),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.end,
          children: const [
            SizedBox(
              width: 216,
              height: 56,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  SaString.annualSavingInstruction,
                  textAlign: TextAlign.center,
                  style: SaTextStyles.caption,
                ),
              ),
            )
          ],
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              width: 216,
              height: 56,
              child: SaTextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _numberOfEmployee,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 16,
                ),
                fillColor: SaColors.white,
                hintText: '100',
                hintStyle: SaTextStyles.subtitleLarge.copyWith(
                  color: SaColors.hintColor,
                ),
                onChanged: (value) {
                  if (value != '') {
                    _totalSaving.text =
                        '\$${((double.parse(_numberOfEmployee.text) * (double.parse(_totalCostPerHour.text.replaceAll('\$', '')) * 40) * 52.03236) - (double.parse(_numberOfEmployee.text) * (double.parse(_totalCostPerHour2.text.replaceAll('\$', '')) * 40) * 52.03236)).toStringAsFixed(0)}';
                  } else {
                    _totalSaving.text = '';
                  }
                },
              ),
            )
          ],
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: _calCulatorText(SaString.potentialSavings)),
            SizedBox(
              width: 216,
              height: 56,
              child: SaTextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _totalSaving,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 16,
                ),
                fillColor: SaColors.lightGreen,
                hintText: '',
                readOnly: true,
              ),
            )
          ],
        ),
      ],
    );
  }
}
