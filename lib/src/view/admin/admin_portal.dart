// ignore_for_file: empty_catches

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/admin/bloc/admin_data_bloc.dart';
import 'package:shift_alerts/src/provider/admin/charts/bloc/charts_bloc.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/admin/admincredentials/admin_credentials.dart';
import 'package:shift_alerts/src/view/admin/feedback.dart';
import 'package:shift_alerts/src/view/admin/fill_rate.dart';
import 'package:shift_alerts/src/view/admin/financial_summary.dart';
import 'package:shift_alerts/src/view/admin/notes/notes.dart';
import 'package:shift_alerts/src/view/admin/workforce_calculator.dart';
import 'package:shift_alerts/src/view/chat/call.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/drawer.dart';

class AdminPortal extends StatefulWidget {
  const AdminPortal({super.key});

  @override
  State<AdminPortal> createState() => _AdminPortalState();
}

class _AdminPortalState extends State<AdminPortal> with WidgetsBindingObserver {
  final AdminDataBloc adminDataBloc = AdminDataBloc();

  final List<Widget> _list = [
    const FinancialSummary(),
    const FillRate(),
    const AdminCreddentials(),
    const AdminFeedBack(),
    const NotesScreen(),
    const WorkForceCalculator(),
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    init();
    if (!chartsBloc.isClosed) {
      chartsBloc.add(ChartsEvent.getData(
        fromDate: DateTime.now().firstDate(DateTime.now().month).dmY(),
        toDate: DateTime.now().lastDate().dmY(),
      ));
    } else {
      chartsBloc = ChartsBloc();
      chartsBloc.add(ChartsEvent.getData(
        fromDate: DateTime.now().firstDate(DateTime.now().month).dmY(),
        toDate: DateTime.now().lastDate().dmY(),
      ));
    }
  }

  init() async {
    notificationsUtils.generateToken();
    notificationsUtils.messageListener();
    await rtmClient.login(
        null, HiveUtils.get(HiveKeys.user).toString().padLeft(4, '0'));
    rtmClient.onLocalInvitationAccepted = (invite) {
      // callStatus.value = '00:00';
      callDuration();
    };
    rtmClient.onLocalInvitationReceivedByPeer = (invite) {
      callStatus.value = 'Ringing';
    };
    rtmClient.onLocalInvitationCanceled = (invite) {
      // print('--- Local Cancel');
    };
    rtmClient.onLocalInvitationRefused = (invite) {
      // print('--- Local Refuse');
      rtcEngine.leaveChannel();
      callStatus.value = 'Call Decline';
      Future.delayed(
        const Duration(seconds: 1),
        () => NavigatorUtils.pop(context),
      );
    };
    rtmClient.onRemoteInvitationAccepted = (invite) {
      // callStatus.value = '00:00';
      callDuration();
    };
    rtmClient.onRemoteInvitationCanceled = (invite) {
      rtcEngine.leaveChannel();
      timer?.cancel();
      callStatus.value = 'Call Ended';
      Future.delayed(
        const Duration(seconds: 1),
        () => Navigator.pop(context),
      );
    };
    rtmClient.onRemoteInvitationRefused = (invite) {
      NavigatorUtils.pop(context);
    };
    rtmClient.onRemoteInvitationReceivedByPeer = (invite) {
      if (callerDeviceType == '') {
        callStatus.value = 'Calling';
        NavigatorUtils.push(
            context,
            VoiceCall(
              joined: false,
              channelId: invite.channelId!,
              peerId: invite.callerId,
              peerName: jsonDecode(invite.content!)['name'],
              image: jsonDecode(invite.content!)['image'],
            ));
      }
    };
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.resumed:
        try {
          await rtmClient.login(
              null, HiveUtils.get(HiveKeys.user).toString().padLeft(4, '0'));
        } catch (error) {}
        break;
      case AppLifecycleState.paused:
        try {
          rtmClient.logout();
        } catch (error) {}
        break;
      case AppLifecycleState.detached:
        try {
          rtmClient.logout();
        } catch (error) {}
        break;
    }
  }

  @override
  void dispose() {
    chartsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => adminDataBloc),
        BlocProvider(create: (_) => chartsBloc),
      ],
      child: Scaffold(
        drawerEnableOpenDragGesture: false,
        key: drawerKey,
        drawer: SaDrawer(adminDataBloc: adminDataBloc),
        appBar: adminAppBar(context),
        body: BlocBuilder<AdminDataBloc, AdminDataState>(
          builder: (context, state) {
            return state.when(
              initial: (index) {
                return WillPopScope(
                  onWillPop: () async {
                    if (index == 0) {
                      return true;
                    } else {
                      adminDataBloc.add(const AdminDataEvent.changeIndex(0));
                      return false;
                    }
                  },
                  child: _list[index],
                );
              },
            );
          },
        ),
      ),
    );
  }
}
