import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class SaBarChart extends StatefulWidget {
  final List<Map<String, dynamic>> list;
  const SaBarChart({Key? key, required this.list}) : super(key: key);

  @override
  State<SaBarChart> createState() => _SaBarChartState();
}

class _SaBarChartState extends State<SaBarChart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 500,
          child: BarChart(
            BarChartData(
              groupsSpace: 8,
              titlesData: FlTitlesData(
                rightTitles:
                    AxisTitles(sideTitles: SideTitles(showTitles: false)),
                leftTitles: AxisTitles(
                  sideTitles: SideTitles(
                    getTitlesWidget: (value, meta) => SideTitleWidget(
                      axisSide: meta.axisSide,
                      space: 4.0,
                      child: Text(
                        meta.formattedValue,
                        maxLines: 1,
                        style: SaTextStyles.caption,
                      ),
                    ),
                    showTitles: true,
                  ),
                ),
                bottomTitles: AxisTitles(
                  sideTitles: SideTitles(
                    getTitlesWidget: (value, meta) => SideTitleWidget(
                      axisSide: meta.axisSide,
                      space: 4,
                      child: Text(
                        widget.list[value.toInt()]['x'],
                        style: SaTextStyles.caption,
                      ),
                    ),
                    showTitles: true,
                  ),
                ),
                topTitles:
                    AxisTitles(sideTitles: SideTitles(showTitles: false)),
              ),
              borderData: FlBorderData(
                border: const Border(
                  left: BorderSide(color: SaColors.black),
                  bottom: BorderSide(color: SaColors.black),
                ),
              ),
              gridData: FlGridData(
                drawHorizontalLine: false,
                drawVerticalLine: false,
              ),
              barGroups: List.generate(
                widget.list.length,
                (index) => BarChartGroupData(
                  x: index,
                  barsSpace: 0.0,
                  barRods: [
                    BarChartRodData(
                      toY: widget.list[index]['requested'].toDouble(),
                      width: 12,
                      color: SaColors.primary,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(4.0),
                        topRight: Radius.circular(4.0),
                      ),
                    ),
                    BarChartRodData(
                      toY: widget.list[index]['completed'].toDouble(),
                      width: 12,
                      color: SaColors.green,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(4.0),
                        topRight: Radius.circular(4.0),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
