import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/charts/bloc/charts_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/admin/bar_chart.dart';
import 'package:shift_alerts/src/view/admin/circle_chart.dart';
import 'package:shift_alerts/src/view/admin/facilityRequest/facility_req_rate.dart';
import 'package:shift_alerts/src/view/admin/nurse_tier_request_rate.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/date_picker.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/percent_graph.dart';

class FillRate extends StatefulWidget {
  const FillRate({super.key});

  @override
  State<FillRate> createState() => _FillRateState();
}

class _FillRateState extends State<FillRate> {
  final _rangeNotifier = ValueNotifier<DateTimeRange>(DateTimeRange(
      start: DateTime.now().weekFirstDay(), end: DateTime.now().weekLastDay()));

  final List<Color> _colorList = [
    SaColors.primary,
    SaColors.pink,
    SaColors.purple,
    SaColors.orange,
    SaColors.green,
    SaColors.yellow,
  ];

  @override
  void initState() {
    super.initState();
    chartsBloc.add(ChartsEvent.getAllData(
      fromDate: _rangeNotifier.value.start.dmY(),
      toDate: _rangeNotifier.value.end.dmY(),
    ));
  }

  @override
  void dispose() {
    _rangeNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return RefreshIndicator(
      onRefresh: () async {
        chartsBloc.add(ChartsEvent.getAllData(
          fromDate: _rangeNotifier.value.start.dmY(),
          toDate: _rangeNotifier.value.end.dmY(),
        ));
        await Future.delayed(const Duration(seconds: 1));
      },
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    'Fill rates',
                    style: SaTextStyles.heading,
                  ),
                  const Spacer(),
                  SaOutlineButton(
                    width: 217,
                    height: 40,
                    onPressed: () async {
                      await ShowRangeDatePicker()
                          .rangePicker(ctx: context)
                          .then(
                        (value) {
                          if (value != null) {
                            _rangeNotifier.value = value;
                            _rangeNotifier.value = value;
                            chartsBloc.add(ChartsEvent.getAllData(
                              fromDate: _rangeNotifier.value.start.dmY(),
                              toDate: _rangeNotifier.value.end.dmY(),
                            ));
                          }
                        },
                      );
                    },
                    text: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: ValueListenableBuilder(
                            valueListenable: _rangeNotifier,
                            builder: (context, value, child) {
                              return Text(
                                '${value.start.MdY()} - ${value.end.MdY()}',
                                style: SaTextStyles.subtitleSmall.copyWith(
                                  fontWeight: FontWeight.w500,
                                ),
                              );
                            },
                          ),
                        ),
                        const SizedBox(width: 4.0),
                        SvgPicture.asset(
                          SaIcons.schedule,
                          color: SaColors.black,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Center(
                child: BlocBuilder<ChartsBloc, ChartsState>(
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => SizedBox(
                        height: (size.width / 2) - 48,
                        child: const Center(
                          child: Loader(),
                        ),
                      ),
                      success: (shifts, allShift) {
                        List<Shift> shiftList = List.from(shifts);
                        List<Shift> allShiftList = List.from(allShift ?? []);

                        shiftList.removeWhere((element) =>
                            element.startDate.parseDate().day <
                                _rangeNotifier.value.start.day &&
                            element.startDate.parseDate().month ==
                                _rangeNotifier.value.start.month);
                        allShiftList.removeWhere((element) =>
                            element.startDate.parseDate().day <
                                _rangeNotifier.value.start.day &&
                            element.startDate.parseDate().month ==
                                _rangeNotifier.value.start.month);

                        int fillRate = 0;
                        int totalRequest = 0;

                        for (Shift shift in shiftList) {
                          if (shift.status.isOpen() ||
                              shift.status.isDeclined() ||
                              shift.status.isApproved() ||
                              shift.status.isCompleted()) {
                            fillRate++;
                          }
                        }
                        for (Shift shift in allShiftList) {
                          if (shift.status.isOpen() ||
                              shift.status.isDeclined() ||
                              shift.status.isApproved() ||
                              shift.status.isCompleted()) {
                            totalRequest++;
                          }
                        }

                        return Stack(
                          alignment: AlignmentDirectional.center,
                          children: [
                            CircularPercentIndicator(
                              radius: (size.width / 2) - 48,
                              lineWidth: 30,
                              percent: 1,
                              progressColor: SaColors.white,
                              circularStrokeCap: CircularStrokeCap.square,
                              arcType: ArcType.HALF,
                            ),
                            CircularPercentIndicator(
                              radius: (size.width / 2) - 48,
                              lineWidth: 30,
                              percent: fillRate == 0 || totalRequest == 0
                                  ? 0
                                  : fillRate / totalRequest,
                              backgroundColor: SaColors.white,
                              progressColor: SaColors.primary,
                              circularStrokeCap: CircularStrokeCap.square,
                              arcType: ArcType.HALF,
                            ),
                            Positioned(
                              bottom: (((size.width / 2) - 48) / 2) + 100,
                              child: Text(
                                fillRate != 0
                                    ? '${((fillRate / totalRequest) * 100).toStringAsFixed(2)} %'
                                    : '0 %',
                                style: SaTextStyles.title,
                              ),
                            ),
                            Positioned(
                              bottom: (((size.width / 2) - 48) / 2) + 60,
                              child: SizedBox(
                                width: 70,
                                child: Text(
                                  'Total Request',
                                  textAlign: TextAlign.center,
                                  style: SaTextStyles.caption.copyWith(
                                    color: SaColors.hintColor,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: (((size.width / 2) - 48) / 2) + 30,
                              child: Text(
                                '$totalRequest',
                                textAlign: TextAlign.center,
                                style: SaTextStyles.title.copyWith(
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            Positioned(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 72.0, left: 35.0),
                                    child: Text(
                                      '0',
                                      style: SaTextStyles.subtitleSmall
                                          .copyWith(color: SaColors.gray),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 72.0, right: 35.0),
                                    child: Text(
                                      '$fillRate',
                                      style: SaTextStyles.subtitleSmall
                                          .copyWith(color: SaColors.gray),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: 20.0,
                              child: Row(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: SaColors.primary,
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        height: 4,
                                        width: 24,
                                      ),
                                      const SizedBox(width: 8),
                                      const Text(
                                        'Total Requests',
                                        style: SaTextStyles.subtitleSmall,
                                      ),
                                    ],
                                  ),
                                  const SizedBox(width: 30.0),
                                  Row(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: SaColors.green,
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        height: 4,
                                        width: 24,
                                      ),
                                      const SizedBox(width: 8),
                                      const Text(
                                        'Completed',
                                        style: SaTextStyles.subtitleSmall,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                      error: (error) => const SizedBox.shrink(),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 305,
                width: 405,
                child: BlocBuilder<ChartsBloc, ChartsState>(
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => const SizedBox(
                        height: 500,
                        child: Center(
                          child: Loader(),
                        ),
                      ),
                      success: (shifts, allShift) {
                        Map<String, dynamic> mapData;
                        List<int> totalRequestList = [];
                        List<int> completedList = [];
                        List<String> xList = [];
                        Duration difference = _rangeNotifier.value.end
                            .difference(_rangeNotifier.value.start);
                        if (difference.inDays > 10) {
                          mapData = shifts
                              .groupBy((m) => m.startDate.parseDate().M());
                          List.generate(
                              (_rangeNotifier.value.end.month -
                                      _rangeNotifier.value.start.month) +
                                  1,
                              (index) => xList.add(_rangeNotifier.value.start
                                  .firstDate(
                                      _rangeNotifier.value.start.month + index)
                                  .M()));

                          for (String key in xList) {
                            int totalRequest = 0;
                            int completed = 0;
                            if (mapData.containsKey(key)) {
                              for (Shift shift in mapData[key]) {
                                if (shift.status.isOpen() ||
                                    shift.status.isDeclined() ||
                                    shift.status.isApproved() ||
                                    shift.status.isCompleted()) {
                                  totalRequest++;
                                }
                                if (shift.status.isCompleted()) {
                                  completed++;
                                }
                              }
                            }
                            totalRequestList.add(totalRequest);
                            completedList.add(completed);
                          }
                        } else {
                          mapData = shifts
                              .groupBy((m) => m.startDate.parseDate().Md());
                          List.generate(
                              difference.inDays + 1,
                              (index) => xList.add(_rangeNotifier.value.start
                                  .add(Duration(days: index))
                                  .Md()));

                          for (String key in xList) {
                            int totalRequest = 0;
                            int completed = 0;
                            if (mapData.containsKey(key)) {
                              for (Shift shift in mapData[key]) {
                                if (shift.status.isOpen() ||
                                    shift.status.isDeclined() ||
                                    shift.status.isApproved() ||
                                    shift.status.isCompleted()) {
                                  totalRequest++;
                                }
                                if (shift.status.isCompleted()) {
                                  completed++;
                                }
                              }
                            }
                            totalRequestList.add(totalRequest);
                            completedList.add(completed);
                          }
                        }

                        return SaBarChart(
                          list: List.generate(
                            xList.length,
                            (index) => {
                              'x': xList[index],
                              'requested': totalRequestList[index],
                              'completed': completedList[index]
                            },
                          ),
                        );
                      },
                      error: (error) => const SizedBox.shrink(),
                    );
                  },
                ),
              ),
              const SizedBox(height: 32),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 24.0,
                ),
                decoration: BoxDecoration(
                  color: SaColors.white,
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Nurse Tier Request Rate',
                      style: SaTextStyles.title,
                    ),
                    const SizedBox(height: 6.0),
                    InkWell(
                      onTap: () {
                        NavigatorUtils.push(
                          context,
                          const NurseTieRRequestRate(),
                        );
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'View detail',
                            style: SaTextStyles.subtitleLarge.copyWith(
                              color: SaColors.primary,
                            ),
                          ),
                          const SizedBox(width: 10.0),
                          SvgPicture.asset(
                            SaIcons.arrowForward,
                            height: 12.0,
                            color: SaColors.primary,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    BlocBuilder<ChartsBloc, ChartsState>(
                      builder: (context, state) {
                        return state.when(
                          initial: () => const SizedBox.shrink(),
                          loading: () => const SizedBox(
                            height: 186.0,
                            child: Loader(),
                          ),
                          success: (shifts, allShift) {
                            List<Shift> shift = List.from(shifts);
                            shift.removeWhere((element) =>
                                element.startDate.parseDate().day <
                                    _rangeNotifier.value.start.day &&
                                element.startDate.parseDate().month ==
                                    _rangeNotifier.value.start.month);
                            Map<String, dynamic> dataMap =
                                shift.groupBy((m) => m.clinicianPosition);
                            return dataMap.isEmpty
                                ? Container(
                                    height: 186.0,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    child: Text(
                                      'No data available',
                                      style: SaTextStyles.title
                                          .copyWith(color: SaColors.gray),
                                    ),
                                  )
                                : _chart(
                                    list: List.generate(
                                      dataMap.length,
                                      (i) => {
                                        'name': dataMap.keys.elementAt(i),
                                        'color': _colorList[i],
                                        'value': dataMap.values
                                                    .elementAt(i)
                                                    .length ==
                                                0
                                            ? 0.0
                                            : ((dataMap.values
                                                            .elementAt(i)
                                                            .length *
                                                        100) /
                                                    shift.length)
                                                .round()
                                      },
                                    ),
                                  );
                          },
                          error: (error) => const SizedBox.shrink(),
                        );
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 32),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 24.0,
                ),
                decoration: BoxDecoration(
                  color: SaColors.white,
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Facility Request Rate',
                      style: SaTextStyles.title,
                    ),
                    const SizedBox(height: 6.0),
                    InkWell(
                      onTap: () {
                        NavigatorUtils.push(
                          context,
                          const FacilityRequestRate(),
                        );
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'View detail',
                            style: SaTextStyles.subtitleLarge
                                .copyWith(color: SaColors.primary),
                          ),
                          const SizedBox(width: 10.0),
                          SvgPicture.asset(
                            SaIcons.arrowForward,
                            height: 12.0,
                            color: SaColors.primary,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    BlocBuilder<ChartsBloc, ChartsState>(
                      builder: (context, state) {
                        return state.when(
                          initial: () => const SizedBox.shrink(),
                          loading: () => const SizedBox(
                            height: 186.0,
                            child: Loader(),
                          ),
                          success: (shifts, allShift) {
                            List<Shift> shiftList = List.from(shifts);
                            shiftList.removeWhere((element) =>
                                element.startDate.parseDate().day <
                                    _rangeNotifier.value.start.day &&
                                element.startDate.parseDate().month ==
                                    _rangeNotifier.value.start.month);
                            int clnCancel = 0;
                            int facilityCancel = 0;
                            int fillRate = 0;
                            for (Shift shift in shiftList) {
                              if (shift.declinedBy.toLowerCase() ==
                                  'clinician') {
                                clnCancel++;
                              }
                              if (shift.declinedBy.toLowerCase() ==
                                  'facility') {
                                facilityCancel++;
                              }
                              if (shift.status.isCompleted()) {
                                fillRate++;
                              }
                            }
                            int total = clnCancel + facilityCancel + fillRate;
                            return total == 0
                                ? Container(
                                    height: 186.0,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    child: Text(
                                      'No data available',
                                      style: SaTextStyles.title
                                          .copyWith(color: SaColors.gray),
                                    ),
                                  )
                                : _chart(
                                    list: [
                                      {
                                        'name': 'Fill rate',
                                        'color': SaColors.primary,
                                        'value':
                                            ((fillRate * 100) / total).round()
                                      },
                                      {
                                        'name': 'Cancellation\nrate (CLN)',
                                        'color': SaColors.green,
                                        'value':
                                            ((clnCancel * 100) / total).round()
                                      },
                                      {
                                        'name': 'Cancellation\nrate (Facility)',
                                        'color': SaColors.pink,
                                        'value':
                                            ((facilityCancel * 100) / total)
                                                .round()
                                      },
                                    ],
                                  );
                          },
                          error: (error) => const SizedBox.shrink(),
                        );
                      },
                    ),
                  ],
                ),
              ),
              const SafeArea(child: SizedBox.shrink()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _chart({
    required List<Map<String, dynamic>> list,
  }) {
    return Row(
      children: [
        Container(
          height: 186,
          width: 186,
          padding: const EdgeInsets.all(4.0),
          child: SaCircleChart(
            list: list,
          ),
        ),
        const SizedBox(width: 24.0),
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 8.0),
                  Row(
                    children: [
                      Container(
                        height: 12.0,
                        width: 12.0,
                        decoration: BoxDecoration(
                          color: list[index]['color'],
                          shape: BoxShape.circle,
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: Text(
                          list[index]['name'],
                          style: SaTextStyles.caption.copyWith(fontSize: 12.0),
                          maxLines: 2,
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  LineChartData mainData(List<FlSpot> list) {
    return LineChartData(
      borderData: FlBorderData(
        show: true,
      ),
      gridData: FlGridData(
        drawVerticalLine: false,
        drawHorizontalLine: false,
      ),
      titlesData: FlTitlesData(
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
          ),
        ),
      ),
      minX: 0,
      maxX: 4,
      minY: 0,
      maxY: 4,
      lineBarsData: [
        LineChartBarData(
          dotData: FlDotData(show: false),
          spots: list,
          color: SaColors.primary,
          barWidth: 2,
          isStrokeCapRound: true,
          belowBarData: BarAreaData(
            show: true,
            color: SaColors.transparent,
          ),
        ),
      ],
    );
  }
}
