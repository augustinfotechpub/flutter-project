import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/color.dart';

class SaCircleChart extends StatelessWidget {
  final bool showBadge;
  final List list;

  const SaCircleChart({
    super.key,
    this.showBadge = true,
    required this.list,
  });

  @override
  Widget build(BuildContext context) {
    return PieChart(
      PieChartData(
        borderData: FlBorderData(show: false),
        sectionsSpace: 2.0,
        sections: List.generate(list.length, (i) {
          String value = list[i]['value'].toString() == 'NaN'
              ? '0.0'
              : list[i]['value'].toString();
          return PieChartSectionData(
            color: list[i]['color'],
            value: double.parse(value),
            showTitle: false,
            radius: 16.0,
            badgeWidget: showBadge ? _Badge('$value%') : null,
            badgePositionPercentageOffset: 1.25,
          );
        }),
      ),
    );
  }
}

class _Badge extends StatelessWidget {
  final String text;

  const _Badge(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24.0,
      height: 24.0,
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
      alignment: Alignment.center,
      child: Text(
        text,
        maxLines: 1,
        style: const TextStyle(
          fontSize: 8,
          color: SaColors.black,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;

  const Indicator({
    Key? key,
    required this.color,
    required this.text,
    required this.isSquare,
    this.size = 16,
    this.textColor = const Color(0xff505050),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
            color: color,
          ),
        ),
        const SizedBox(
          width: 4,
        ),
        Text(
          text,
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: textColor),
        )
      ],
    );
  }
}
