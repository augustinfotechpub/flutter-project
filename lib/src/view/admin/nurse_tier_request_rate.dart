import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/charts/bloc/charts_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/admin/multiple_bar_chart.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/date_picker.dart';
import 'package:shift_alerts/src/widget/loader.dart';

class NurseTieRRequestRate extends StatefulWidget {
  const NurseTieRRequestRate({super.key});

  @override
  State<NurseTieRRequestRate> createState() => _NurseTieRRequestRateState();
}

class _NurseTieRRequestRateState extends State<NurseTieRRequestRate> {
  DateTimeRange rangedDate = DateTimeRange(
      start: DateTime.now().firstDate(DateTime.now().month),
      end: DateTime.now().lastDate());

  final ChartsBloc _chartsBloc = ChartsBloc();

  @override
  void initState() {
    super.initState();
    _chartsBloc.add(ChartsEvent.getData(
      fromDate: rangedDate.start.dmY(),
      toDate: rangedDate.end.dmY(),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _chartsBloc,
      child: Scaffold(
        appBar: appBar(context, title: SaString.nureseReqRate),
        body: RefreshIndicator(
          onRefresh: () async {
            _chartsBloc.add(ChartsEvent.getData(
              fromDate: rangedDate.start.dmY(),
              toDate: rangedDate.end.dmY(),
            ));
            await Future.delayed(const Duration(seconds: 1));
          },
          child: ListView(
            shrinkWrap: true,
            children: [
              Column(
                children: [
                  const SizedBox(height: 16),
                  BlocBuilder<ChartsBloc, ChartsState>(
                    builder: (context, state) {
                      return SaOutlineButton(
                        width: 217,
                        height: 40,
                        onPressed: () async {
                          await ShowRangeDatePicker()
                              .rangePicker(ctx: context)
                              .then((value) {
                            if (value != null) {
                              rangedDate = value;
                              _chartsBloc.add(ChartsEvent.refreshChart(
                                fromDate: rangedDate.start.dmY(),
                                toDate: rangedDate.end.dmY(),
                              ));
                            }
                          });
                        },
                        text: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                '${rangedDate.start.MdY()} - ${rangedDate.end.MdY()}',
                                style: SaTextStyles.subtitleSmall,
                              ),
                            ),
                            const SizedBox(width: 4.0),
                            SvgPicture.asset(
                              SaIcons.schedule,
                              color: SaColors.black,
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  const SizedBox(height: 32),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _dataList(
                        text1: 'CNA/STNA Request',
                        color1: SaColors.primary,
                        text2: 'Completed LVN/LPN',
                        color2: SaColors.green,
                        text3: 'Completed CNA/STNA',
                        color3: SaColors.pink,
                      ),
                      const SizedBox(width: 35),
                      _dataList(
                        text1: 'RN Request',
                        color1: SaColors.orange,
                        text2: 'Completed RN',
                        color2: SaColors.purple,
                        text3: 'LVN Request',
                        color3: SaColors.yellow,
                      ),
                    ],
                  ),
                  const SizedBox(height: 35),
                  Container(
                    height: 304,
                    width: 340,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 24.0),
                    decoration: BoxDecoration(
                      color: SaColors.white,
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                    child: BlocBuilder<ChartsBloc, ChartsState>(
                      builder: (context, state) {
                        return state.when(
                          initial: () => const SizedBox.shrink(),
                          loading: () => const Loader(),
                          success: (shifts, allShift) {
                            Map<String, dynamic> mapData;
                            List<String> xList = [];
                            List<int> cnaRequestList = [];
                            List<int> rnRequestList = [];
                            List<int> lvnRequestList = [];
                            List<int> cnaCompleteList = [];
                            List<int> rnCompleteList = [];
                            List<int> lvnCompleteList = [];
                            Duration difference =
                                rangedDate.end.difference(rangedDate.start);
                            if (difference.inDays > 4) {
                              mapData = shifts
                                  .groupBy((m) => m.startDate.parseDate().My());
                              List.generate(
                                (rangedDate.end.month -
                                            rangedDate.start.month ==
                                        0
                                    ? 1
                                    : rangedDate.end.month -
                                        rangedDate.start.month),
                                (index) => xList.add(
                                  rangedDate.start
                                      .firstDate(rangedDate.start.month + index)
                                      .My(),
                                ),
                              );
                              for (String key in xList) {
                                int cnaRequest = 0;
                                int rnRequest = 0;
                                int lvnRequest = 0;
                                int cnaComplete = 0;
                                int rnComplete = 0;
                                int lvnComplete = 0;
                                if (mapData.containsKey(key)) {
                                  for (Shift shift in mapData[key]) {
                                    if (shift.status.isOpen() ||
                                        shift.status.isDeclined() ||
                                        shift.status.isApproved() ||
                                        shift.status.isCompleted()) {
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'cna' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'stna') {
                                        cnaRequest++;
                                      }
                                      if (shift.clinicianPosition
                                              .toLowerCase() ==
                                          'lvn') {
                                        lvnRequest++;
                                      }
                                      if (shift.clinicianPosition
                                              .toLowerCase() ==
                                          'rn') {
                                        rnRequest++;
                                      }
                                    }
                                    if (shift.status.isCompleted()) {
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'cna' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'stna') {
                                        cnaComplete++;
                                      }
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lvn' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lpn') {
                                        lvnComplete++;
                                      }
                                      if (shift.clinicianPosition
                                              .toLowerCase() ==
                                          'rn') {
                                        rnComplete++;
                                      }
                                    }
                                  }
                                }
                                cnaRequestList.add(cnaRequest);
                                rnRequestList.add(rnRequest);
                                lvnRequestList.add(lvnRequest);
                                cnaCompleteList.add(cnaComplete);
                                rnCompleteList.add(rnComplete);
                                lvnCompleteList.add(lvnComplete);
                              }
                            } else {
                              mapData = shifts
                                  .groupBy((m) => m.startDate.parseDate().Md());
                              List.generate(
                                  difference.inDays + 1,
                                  (index) => xList.add(rangedDate.start
                                      .add(Duration(days: index))
                                      .Md()));

                              for (String key in xList) {
                                int cnaRequest = 0;
                                int rnRequest = 0;
                                int lvnRequest = 0;
                                int cnaComplete = 0;
                                int rnComplete = 0;
                                int lvnComplete = 0;
                                if (mapData.containsKey(key)) {
                                  for (Shift shift in mapData[key]) {
                                    if (shift.status.isOpen() ||
                                        shift.status.isDeclined() ||
                                        shift.status.isApproved() ||
                                        shift.status.isCompleted()) {
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'cna' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'stna') {
                                        cnaRequest++;
                                      }
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lvn' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lpn') {
                                        lvnRequest++;
                                      }
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'rn' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'rn') {
                                        rnRequest++;
                                      }
                                    }
                                    if (shift.status.isCompleted()) {
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'cna' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'stna') {
                                        cnaComplete++;
                                      }
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lvn' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'lpn') {
                                        lvnComplete++;
                                      }
                                      if (shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'rn' ||
                                          shift.clinicianPosition
                                                  .toLowerCase() ==
                                              'rn') {
                                        rnComplete++;
                                      }
                                    }
                                  }
                                }
                                cnaRequestList.add(cnaRequest);
                                rnRequestList.add(rnRequest);
                                lvnRequestList.add(lvnRequest);
                                cnaCompleteList.add(cnaComplete);
                                rnCompleteList.add(rnComplete);
                                lvnCompleteList.add(lvnComplete);
                              }
                            }
                            return MultipleBarChart(
                              list: List.generate(
                                xList.length,
                                (index) => {
                                  'x': xList[index],
                                  'data': [
                                    {
                                      'value': cnaRequestList[index],
                                      'color': SaColors.primary
                                    },
                                    {
                                      'value': lvnCompleteList[index],
                                      'color': SaColors.green
                                    },
                                    {
                                      'value': cnaCompleteList[index],
                                      'color': SaColors.pink
                                    },
                                    {
                                      'value': rnRequestList[index],
                                      'color': SaColors.orange
                                    },
                                    {
                                      'value': rnCompleteList[index],
                                      'color': SaColors.purple
                                    },
                                    {
                                      'value': lvnRequestList[index],
                                      'color': SaColors.yellow
                                    },
                                  ],
                                },
                              ),
                            );
                          },
                          error: (error) => const SizedBox.shrink(),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _dataList({
    required String text1,
    Color? color1,
    required String text2,
    Color? color2,
    required String text3,
    Color? color3,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _detailsData(text: text1, color: color1),
        const SizedBox(height: 8),
        _detailsData(text: text2, color: color2),
        const SizedBox(height: 8),
        _detailsData(text: text3, color: color3),
      ],
    );
  }

  Widget _detailsData({
    required String text,
    Color? color,
  }) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(2),
          ),
          height: 8,
          width: 24,
        ),
        const SizedBox(width: 8),
        Text(
          text,
          style: SaTextStyles.subtitleSmall,
        ),
      ],
    );
  }
}
