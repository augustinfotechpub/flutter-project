import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shift_alerts/src/provider/admin/notes/create_note/create_note_bloc.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class CreateNote extends StatelessWidget {
  CreateNote({super.key});

  final TextEditingController _controller = TextEditingController();
  final CreateNoteBloc _createNoteBloc = CreateNoteBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _createNoteBloc,
      child: BlocListener<CreateNoteBloc, CreateNoteState>(
        listener: (context, state) {
          state.whenOrNull(
            loading: () => Loader.show(context),
            success: () async {
              FocusScope.of(context).unfocus();
              _controller.clear();
              Loader.hide(context);
              Toast.success(context, 'Note successfully uploaded');
              Navigator.of(context).pop(true);
            },
            error: (error) {
              Loader.hide(context);
              Toast.error(context, error);
            },
          );
        },
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: appBar(context, title: 'New Note'),
            body: Container(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                children: [
                  SaTextField(
                    controller: _controller,
                    maxLine: 20,
                    keyboardType: TextInputType.multiline,
                  ),
                  const SizedBox(height: 24.0),
                  SaButton(
                    onPressed: () {
                      _createNoteBloc.add(CreateNoteEvent.createNote({
                        'user_id': HiveUtils.get(HiveKeys.user).toString(),
                        'note': _controller.text,
                        'created_date':
                            DateTime.now().toString().split(' ').first,
                        'created_time': DateTime.now().toString(),
                      }));
                    },
                    text: 'Add Note',
                    height: 44.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
