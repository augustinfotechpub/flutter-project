import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/notes/model.dart';
import 'package:shift_alerts/src/provider/admin/notes/notes/notes_bloc.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/admin/notes/create_note.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class NotesScreen extends StatefulWidget {
  const NotesScreen({super.key});

  @override
  State<NotesScreen> createState() => _NotesScreenState();
}

class _NotesScreenState extends State<NotesScreen> {
  final NotesBloc _notesBloc = NotesBloc();

  List<Note>? noteList;
  List<bool> isDissmissingList = [];

  @override
  void initState() {
    _notesBloc.add(const NotesEvent.getNotes());
    super.initState();
  }

  @override
  void dispose() {
    _notesBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _notesBloc,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          children: [
            _title(),
            const SizedBox(height: 24),
            Expanded(
              child: BlocConsumer<NotesBloc, NotesState>(
                listener: (context, state) {
                  state.when(
                    initial: () {},
                    loading: () => Loader.show(context),
                    success: (notes) {
                      noteList = List.from(notes);
                      isDissmissingList =
                          List.generate(notes.length, (index) => false);
                      Loader.hide(context);
                    },
                    refreshedList: (notes) {
                      noteList = List.from(notes);
                      isDissmissingList =
                          List.generate(notes.length, (index) => false);
                    },
                    successDelete: () {
                      Loader.hide(context);
                      _notesBloc.add(const NotesEvent.refreshNotes());
                      Toast.success(context, 'note successfully delete');
                    },
                    error: (error) {
                      Loader.hide(context);
                      Toast.error(context, 'something wen\'t wrong');
                    },
                  );
                },
                builder: (context, state) {
                  return RefreshIndicator(
                    onRefresh: () async {
                      _notesBloc.add(const NotesEvent.getNotes());
                      await Future.delayed(const Duration(milliseconds: 500));
                    },
                    child: noteList == null
                        ? const SizedBox.shrink()
                        : noteList!.isEmpty
                            ? ListView(
                                children: [
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.7,
                                    child: const Center(
                                      child: Noshift(message: "no notes found"),
                                    ),
                                  ),
                                ],
                              )
                            : ListView.builder(
                                itemCount: noteList!.length,
                                itemBuilder: (_, index) {
                                  return isDissmissingList[index]
                                      ? const SizedBox.shrink()
                                      : Dismissible(
                                          key: Key('$index'),
                                          confirmDismiss: (direction) async {
                                            if (direction ==
                                                DismissDirection.endToStart) {
                                              final bool res =
                                                  await ShowAlertDialog()
                                                      .showAlertDialog(
                                                context: context,
                                                title: 'Delete Note',
                                                content:
                                                    'Are you sure, You want to delete ?',
                                                showCloseIcon: false,
                                                onYesPressed: () =>
                                                    Navigator.pop(
                                                        context, true),
                                              );
                                              if (res) {
                                                isDissmissingList[index] = true;
                                                _notesBloc.add(
                                                  NotesEvent.deleteNote(
                                                    noteList![index]
                                                        .noteId
                                                        .toString(),
                                                  ),
                                                );
                                              }
                                              return false;
                                            } else {
                                              return false;
                                            }
                                          },
                                          background: Align(
                                            alignment: Alignment.centerRight,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8.0),
                                              child: SvgPicture.asset(
                                                  SaIcons.trash),
                                            ),
                                          ),
                                          direction:
                                              DismissDirection.endToStart,
                                          child: _notesCard(noteList![index]),
                                        );
                                },
                              ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(
          SaString.notes,
          style: SaTextStyles.heading,
        ),
        SaOutlineButton(
          onPressed: () async {
            await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CreateNote()))
                .then((value) {
              if (value == true) {
                _notesBloc.add(const NotesEvent.refreshNotes());
              }
            });
          },
          width: 154,
          borderRadius: 12,
          height: 40,
          borderWidth: 1,
          borderColor: SaColors.primary,
          text: const Text(
            SaString.newNote,
            style: TextStyle(
              fontSize: 14,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w500,
              color: SaColors.primary,
            ),
          ),
          icon: const Icon(
            Icons.add,
            color: SaColors.primary,
          ),
        )
      ],
    );
  }

  Widget _notesCard(Note note) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Card(
        color: SaColors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                note.note.contains(' ') || note.note.contains('\n')
                    ? note.note.split(' ').toList().length > 1
                        ? note.note
                                    .split(' ')
                                    .first
                                    .split('\n')
                                    .toList()
                                    .length >
                                1
                            ? '${note.note.split('\n').first.capitalize()} ${note.note.split('\n')[1].split(' ').first}'
                            : '${note.note.split(' ').first.capitalize()} ${note.note.split(' ')[1]}'
                        : note.note.split('\n').toList().length > 1
                            ? '${note.note.split('\n').first.capitalize()} ${note.note.split('\n')[1].split(' ').first}'
                            : note.note.split(' ').first.capitalize()
                    : note.note.capitalize(),
                style: SaTextStyles.title,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      note.note,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: SaTextStyles.subtitleLarge.copyWith(
                        color: SaColors.hintColor,
                      ),
                    ),
                  ),
                  Text(
                    date(note.createdDate),
                    style: SaTextStyles.subtitleLarge.copyWith(
                      color: SaColors.hintColor,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  String date(String date) {
    DateTime dateTime = date.parseDate();
    if (dateTime.day > 10 && dateTime.day < 21) {
      return '${dateTime.Md()}th';
    }
    String day = dateTime.day.toString().length == 1
        ? dateTime.day.toString()
        : dateTime.day.toString().substring(1, 2);
    if (day == '1') {
      return '${dateTime.Md()}st';
    }
    if (day == '2') {
      return '${dateTime.Md()}nd';
    }
    if (day == '3') {
      return '${dateTime.Md()}rd';
    }
    return '${dateTime.Md()}th';
  }
}
