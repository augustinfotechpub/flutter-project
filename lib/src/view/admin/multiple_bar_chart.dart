import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/text_style.dart';

class MultipleBarChart extends StatefulWidget {
  final List<Map<String, dynamic>> list;
  const MultipleBarChart({Key? key, required this.list}) : super(key: key);

  @override
  State<MultipleBarChart> createState() => _MultipleBarChartState();
}

class _MultipleBarChartState extends State<MultipleBarChart> {
  @override
  Widget build(BuildContext context) {
    return BarChart(
      BarChartData(
        barGroups: List.generate(
          widget.list.length,
          (index) => BarChartGroupData(
            barsSpace: 0.0,
            x: index,
            barRods: List.generate(
              widget.list[index]['data'].length,
              (i) {
                return _barChartRodData(
                  toY: widget.list[index]['data'][i]['value'],
                  color: widget.list[index]['data'][i]['color'],
                );
              },
            ),
          ),
        ),
        titlesData: FlTitlesData(
          leftTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              getTitlesWidget: (value, meta) {
                return SideTitleWidget(
                  axisSide: meta.axisSide,
                  space: 4,
                  child: Text(
                    meta.formattedValue,
                    style: SaTextStyles.caption,
                  ),
                );
              },
            ),
          ),
          rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
          topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
          bottomTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              getTitlesWidget: (value, meta) {
                return SideTitleWidget(
                  axisSide: meta.axisSide,
                  space: 6,
                  child: Text(
                    widget.list[value.toInt()]['x'],
                    style: SaTextStyles.caption,
                  ),
                );
              },
            ),
          ),
        ),
        borderData: FlBorderData(
          border: const Border(
            left: BorderSide(color: Colors.black),
            bottom: BorderSide(color: Colors.black),
          ),
        ),
        gridData: FlGridData(
          drawHorizontalLine: false,
          drawVerticalLine: false,
        ),
      ),
    );
  }
}

BarChartRodData _barChartRodData({
  required int toY,
  required Color color,
}) {
  return BarChartRodData(
    toY: toY.toDouble(),
    width: 8,
    color: color,
    borderRadius: const BorderRadius.only(
      topLeft: Radius.circular(4.0),
      topRight: Radius.circular(4.0),
    ),
  );
}
