// ignore_for_file: no_leading_underscores_for_local_identifiers, use_build_context_synchronously

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/api_provider.dart';
import 'package:shift_alerts/src/provider/admin/charts/bloc/charts_bloc.dart';
import 'package:shift_alerts/src/provider/admin/invoice/bloc/invoice_bloc.dart';
import 'package:shift_alerts/src/provider/admin/invoice/model.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/admin/circle_chart.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/date_picker.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class FinancialSummary extends StatefulWidget {
  const FinancialSummary({super.key});

  @override
  State<FinancialSummary> createState() => _FinancialSummaryState();
}

class _FinancialSummaryState extends State<FinancialSummary> {
  final TextEditingController _search = TextEditingController();
  final InvoiceBloc _invoiceBloc = InvoiceBloc();
  final _workedDate = TextEditingController();
  final _clinicianName = TextEditingController();
  final _position = TextEditingController();
  final _hrsWorked = TextEditingController();
  final _rate = TextEditingController();
  final _amount = TextEditingController();

  final List<Color> _colorList = [
    SaColors.primary,
    SaColors.pink,
    SaColors.purple,
    SaColors.orange,
    SaColors.green,
    SaColors.yellow,
  ];

  final List<String> _list = [
    'Oldest date',
    'Newest date',
  ];

  final _dropValue = ValueNotifier<String>('Oldest date');
  final _isSeach = ValueNotifier<bool>(false);
  final _searchValue = ValueNotifier<String>('');
  List<InvoiceDetails> searchList = [];

  void _initFireBloc() {
    _invoiceBloc.add(const InvoiceEvent.getInvoices());
    if (!chartsBloc.isClosed) {
      chartsBloc.add(ChartsEvent.getData(
        fromDate: DateTime.now().firstDate(DateTime.now().month).dmY(),
        toDate: DateTime.now().lastDate().dmY(),
      ));
    } else {
      chartsBloc = ChartsBloc();
      chartsBloc.add(ChartsEvent.getData(
        fromDate: DateTime.now().firstDate(DateTime.now().month).dmY(),
        toDate: DateTime.now().lastDate().dmY(),
      ));
    }
    notificationsUtils.onMessageListener(context);
  }

  @override
  void initState() {
    _initFireBloc();
    super.initState();
  }

  @override
  void dispose() {
    _workedDate.dispose();
    _clinicianName.dispose();
    _position.dispose();
    _hrsWorked.dispose();
    _rate.dispose();
    _amount.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _isSeach.value = false,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            _isSeach.value = false;
            await _displayTextInputDialog(
                context: context, invoiceDetails: null);
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          backgroundColor: SaColors.primary,
          child: const Icon(
            Icons.add_chart_sharp,
            color: SaColors.white,
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            _initFireBloc();
            await Future.delayed(const Duration(seconds: 1));
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(
                    left: 24.0,
                    right: 24.0,
                    top: 24.0,
                    bottom: 16.0,
                  ),
                  child: Text(
                    'Facility Bird\'s Eye View',
                    style: SaTextStyles.heading,
                  ),
                ),
                _chart(),
                const SizedBox(height: 36.0),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24.0),
                  child: Text(
                    'Outstanding Transactions',
                    style: SaTextStyles.heading,
                  ),
                ),
                const SizedBox(height: 16.0),
                // transaction
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: ValueListenableBuilder(
                    valueListenable: _isSeach,
                    builder: (context, search, child) {
                      return search
                          ? SaTextField(
                              controller: _search,
                              hintText: 'Find invoice no.',
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: 20.0,
                                vertical: 12.0,
                              ),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 8.0),
                                child: SvgPicture.asset(SaIcons.search),
                              ),
                              onChanged: (p0) {
                                _searchValue.value = p0;
                              },
                            )
                          : Row(
                              children: [
                                // SaOutlineButton(
                                //   height: 44.0,
                                //   width: 144.0,
                                //   onPressed: null,
                                //   text: Row(
                                //     mainAxisSize: MainAxisSize.min,
                                //     children: const [
                                //       Text(
                                //         'Oldest date',
                                //         style: TextStyle(
                                //           fontSize: 14.0,
                                //           color: SaColors.primary,
                                //           fontWeight: FontWeight.w500,
                                //         ),
                                //       ),
                                //       SizedBox(width: 12.0),
                                //       Icon(
                                //         CupertinoIcons.arrow_down,
                                //         size: 18.0,
                                //         color: SaColors.primary,
                                //       )
                                //     ],
                                //   ),
                                // ),
                                ValueListenableBuilder(
                                  valueListenable: _dropValue,
                                  builder: (context, value, child) {
                                    return SaDropDown(
                                      buttonHeight: 44.0,
                                      borderWidth: 2.0,
                                      value: _dropValue.value,
                                      icon: Row(
                                        children: const [
                                          SizedBox(width: 12.0),
                                          Icon(
                                            CupertinoIcons.arrow_down,
                                            size: 18.0,
                                            color: SaColors.primary,
                                          ),
                                        ],
                                      ),
                                      valuesList: _list,
                                      onChanged: (result) {
                                        if (result != null &&
                                            _dropValue.value != result) {
                                          _dropValue.value = result;
                                          // _credentialsBloc.add(
                                          //   CredentialsEvent.getCredential(
                                          //     position: value == 'All position' ? '' : value,
                                          //   ),
                                          // );
                                        }
                                      },
                                    );
                                  },
                                ),
                                const Spacer(),
                                InkWell(
                                  onTap: () => _isSeach.value = true,
                                  child: Container(
                                    height: 44.0,
                                    width: 44.0,
                                    padding: const EdgeInsets.all(14.0),
                                    decoration: BoxDecoration(
                                      color: SaColors.white,
                                      borderRadius: BorderRadius.circular(12.0),
                                    ),
                                    child: SvgPicture.asset(SaIcons.search),
                                  ),
                                ),
                              ],
                            );
                    },
                  ),
                ),
                const SizedBox(height: 24.0),
                _transactionsTable(),
                const SizedBox(height: 40.0),
                // invoice title
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        SaIcons.credential,
                        color: SaColors.primary,
                        height: 20.0,
                      ),
                      const SizedBox(width: 16.0),
                      const Text(
                        'Invoice #12398',
                        style: SaTextStyles.heading,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20.0),
                _invoiceTable(),
                const SizedBox(height: 40.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _displayTextInputDialog(
      {required BuildContext context,
      required InvoiceDetails? invoiceDetails}) async {
    DateTime? pickedDate;
    if (invoiceDetails != null) {
      pickedDate = invoiceDetails.serviceDate.parseDate();
      _workedDate.text = pickedDate.dmY();
      _clinicianName.text = invoiceDetails.clinicianName;
      _position.text = invoiceDetails.position;
      _amount.text = invoiceDetails.amount;
      _rate.text = invoiceDetails.rate;
      _hrsWorked.text = invoiceDetails.hrsWorked;
    } else {
      _workedDate.clear();
      _clinicianName.clear();
      _rate.clear();
      _amount.clear();
      _hrsWorked.clear();
      _position.clear();
    }

    Widget _text(String name) => Text(
          name,
          style: SaTextStyles.title,
        );
    BorderSide _borderside() => const BorderSide(color: SaColors.gray);
    EdgeInsets _contentPadding() =>
        const EdgeInsets.symmetric(horizontal: 15, vertical: 10);

    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) {
        return AlertDialog(
          backgroundColor: SaColors.white,
          actionsPadding:
              const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
          titlePadding: const EdgeInsets.symmetric(horizontal: 16.0),
          contentPadding: const EdgeInsets.all(16),
          insetPadding: const EdgeInsets.all(20),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'CREATE INVOICE',
                    style: SaTextStyles.heading.copyWith(fontSize: 17),
                  ),
                  const Spacer(),
                  SizedBox(
                    width: 24.0,
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      splashRadius: 16,
                      onPressed: () => NavigatorUtils.pop(context),
                      icon: const Icon(Icons.clear, color: SaColors.gray),
                    ),
                  ),
                ],
              ),
              const Divider(
                  color: SaColors.scaffold, thickness: 1, height: 0.0),
            ],
          ),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _text('Service Date'),
                const SizedBox(height: 8.0),
                SaTextField(
                  readOnly: true,
                  controller: _workedDate,
                  hintText: 'Service Date',
                  keyboardType: TextInputType.name,
                  onTap: () async {
                    pickedDate =
                        await ShowRangeDatePicker().datePicker(ctx: context);
                    if (pickedDate != null) {
                      _workedDate.text = pickedDate!.dmY();
                    }
                  },
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
                const SizedBox(height: 15.0),
                _text('Clinician Name'),
                const SizedBox(height: 8.0),
                SaTextField(
                  controller: _clinicianName,
                  hintText: 'Clinician Name',
                  keyboardType: TextInputType.name,
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
                const SizedBox(height: 15.0),
                _text('Clinician Position'),
                const SizedBox(height: 8.0),
                SaTextField(
                  controller: _position,
                  hintText: 'Clinician Position',
                  keyboardType: TextInputType.name,
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
                const SizedBox(height: 15.0),
                _text('Worked Houres'),
                const SizedBox(height: 8.0),
                SaTextField(
                  controller: _hrsWorked,
                  hintText: 'Worked Houres',
                  keyboardType: TextInputType.number,
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
                const SizedBox(height: 15.0),
                _text('Rate'),
                const SizedBox(height: 8.0),
                SaTextField(
                  controller: _rate,
                  hintText: 'Rate',
                  keyboardType: TextInputType.number,
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
                const SizedBox(height: 15.0),
                _text('Amount'),
                const SizedBox(height: 8.0),
                SaTextField(
                  controller: _amount,
                  hintText: 'Amount',
                  keyboardType: TextInputType.number,
                  contentPadding: _contentPadding(),
                  borderSide: _borderside(),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              children: [
                Expanded(
                  child: SaOutlineButton(
                    text: const Text(
                      'Cancel',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: SaColors.red,
                      ),
                    ),
                    borderColor: SaColors.red,
                    onPressed: () => NavigatorUtils.pop(context),
                  ),
                ),
                const SizedBox(width: 12),
                Expanded(
                  child: SaButton(
                    text: invoiceDetails != null ? 'Update' : 'Create',
                    textStyle: SaTextStyles.title
                        .copyWith(fontSize: 14, color: SaColors.white),
                    radius: 12,
                    onPressed: () async {
                      if (pickedDate != null) {
                        final Map<String, dynamic> map = {
                          "service_date": pickedDate!.dmyInvoice(),
                          "clinician_name": _clinicianName.text.trim(),
                          "position": _position.text.trim(),
                          "hrs_worked": _hrsWorked.text.trim(),
                          "rate": _rate.text.trim(),
                          "amount": _amount.text.trim(),
                        };
                        if (map.values.any((element) => element == '')) {
                          Toast.error(context, 'please fill all fields');
                        } else {
                          final response = invoiceDetails != null
                              ? await adminApiProvider.editInvoice(
                                  map, invoiceDetails.invoiceId.toString())
                              : await adminApiProvider.createInvoice(map);
                          if (response.statusCode ==
                              (invoiceDetails != null ? 200 : 201)) {
                            _invoiceBloc.add(const InvoiceEvent.getInvoices());
                            NavigatorUtils.pop(context);
                            Toast.success(
                              context,
                              invoiceDetails != null
                                  ? 'Invoice Edit Successfully'
                                  : 'Invoice Created Successfully',
                            );
                          } else {
                            NavigatorUtils.pop(context);
                            Toast.error(context, 'something wen\'t wrong');
                          }
                        }
                      } else {
                        Toast.error(context, 'please select date');
                      }
                    },
                  ),
                ),
              ],
            )
          ],
        );
      },
    );
  }

  Widget _chart() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      child: BlocBuilder<ChartsBloc, ChartsState>(
        builder: (context, state) {
          return state.when(
            initial: () => const SizedBox.shrink(),
            loading: () => const SizedBox(
              height: 186,
              child: Center(
                child: Loader(),
              ),
            ),
            success: (shifts, allShift) {
              List<int> dataList = [];
              Map<String, dynamic> dataMap =
                  shifts.groupBy((m) => m.clinicianPosition);
              dataMap.forEach((key, value) {
                for (Shift shift in value) {
                  int counter = 0;
                  if (shift.status.isOpen() ||
                      shift.status.isApproved() ||
                      shift.status.isDeclined()) {
                    counter++;
                  }
                  dataList.add(counter);
                }
              });
              return dataMap.isEmpty
                  ? Container(
                      height: 186.0,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Text(
                        'No data available',
                        style:
                            SaTextStyles.title.copyWith(color: SaColors.gray),
                      ),
                    )
                  : Row(
                      children: [
                        Container(
                          height: 186,
                          width: 186,
                          padding: const EdgeInsets.all(4.0),
                          child: SaCircleChart(
                            list: List.generate(
                              dataMap.length,
                              (i) => {
                                'color': _colorList[i],
                                'value': ((dataMap.values.elementAt(i).length *
                                            100) /
                                        shifts.length)
                                    .round()
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 24.0),
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: dataMap.length,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  Container(
                                    height: 12.0,
                                    width: 12.0,
                                    decoration: BoxDecoration(
                                      color: _colorList[index],
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Text(
                                    dataMap.keys.elementAt(index),
                                    style: SaTextStyles.caption
                                        .copyWith(fontSize: 16.0),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    );
            },
            error: (error) => const SizedBox.shrink(),
          );
        },
      ),
    );
  }

  Widget _transactionsTable() {
    return BlocProvider(
      create: (context) => _invoiceBloc,
      child: BlocBuilder<InvoiceBloc, InvoiceState>(
        builder: (context, state) {
          return state.when(
            initial: () => const SizedBox.shrink(),
            loading: () => const SizedBox(
              height: 400,
              child: Center(
                child: Loader(),
              ),
            ),
            success: (invoiceData) {
              return ValueListenableBuilder(
                valueListenable: _dropValue,
                builder: (context, value, child) {
                  final List<InvoiceDetails> invoice =
                      List.from(invoiceData.results);
                  if (value == 'Newest date') {
                    invoice.sort((a, b) => b.serviceDate
                        .split('/')
                        .reversed
                        .join("-")
                        .parseDate()
                        .compareTo(a.serviceDate
                            .split('/')
                            .reversed
                            .join("-")
                            .parseDate()));
                  } else {
                    invoice.sort((a, b) => a.serviceDate
                        .split('/')
                        .reversed
                        .join("-")
                        .parseDate()
                        .compareTo(b.serviceDate
                            .split('/')
                            .reversed
                            .join("-")
                            .parseDate()));
                  }
                  return _transactionTable(invoice);
                },
              );
            },
            error: (error) => const SizedBox(
              height: 250,
              child: Center(
                child: Text(
                  'Something wen\'t wrong',
                  style: SaTextStyles.subtitleLarge,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _transactionTable(List<InvoiceDetails> invoiceData) {
    return ValueListenableBuilder(
      valueListenable: _searchValue,
      builder: (context, value, child) {
        searchList = invoiceData;
        if (value != '') {
          searchList = searchList
              .where((e) => e.invoiceId.toString().contains(value))
              .toList();
        }
        return searchList.isEmpty
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 24.0),
                  decoration: BoxDecoration(
                    color: SaColors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Text(
                    'No data found',
                    style: SaTextStyles.title.copyWith(color: SaColors.gray),
                  ),
                ),
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    const SizedBox(width: 24.0),
                    Card(
                      color: SaColors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16)),
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          children: [
                            ConstrainedBox(
                              constraints: const BoxConstraints(maxWidth: 700),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  'Desciption',
                                  'Due date',
                                  'Original amount',
                                  'Balance'
                                ]
                                    .map((e) => Container(
                                          width: 700 / 4,
                                          height: 48,
                                          padding: e == 'Desciption'
                                              ? const EdgeInsets.only(
                                                  left: 45.0)
                                              : EdgeInsets.zero,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            e,
                                            style: SaTextStyles.subtitleLarge
                                                .copyWith(
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ))
                                    .toList(),
                              ),
                            ),
                            const SizedBox(
                                width: 700,
                                child: Divider(
                                    color: SaColors.scaffold, thickness: 3)),
                            for (int i = 0; i < searchList.length; i++)
                              Column(
                                children: [
                                  _transactionTableRow(searchList[i]),
                                  i != searchList.length - 1
                                      ? const SizedBox(
                                          width: 700,
                                          child: Divider(
                                            color: SaColors.scaffold,
                                            thickness: 3,
                                          ))
                                      : const SizedBox.shrink(),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 24.0),
                  ],
                ),
              );
      },
    );
  }

  Widget _transactionTableRow(InvoiceDetails invoiceDetails) {
    final textStyle = SaTextStyles.subtitleLarge.copyWith(
      fontSize: 16,
      color: SaColors.gray,
    );
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 700, minHeight: 66),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 700 / 4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Checkbox(
                  value: false,
                  onChanged: null,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4)),
                ),
                Text('Invoice #${invoiceDetails.invoiceId}', style: textStyle),
              ],
            ),
          ),
          SizedBox(
            width: 700 / 4,
            child: Text(
              invoiceDetails.serviceDate,
              style: textStyle,
            ),
          ),
          SizedBox(
            width: 700 / 4,
            child: Text(invoiceDetails.amount, style: textStyle),
          ),
          SizedBox(
            width: 700 / 4,
            child: Text(invoiceDetails.amount, style: textStyle),
          ),
        ],
      ),
    );
  }

  Widget _invoiceTableRow(InvoiceDetails invoiceDetails, int index) {
    final textStyle = SaTextStyles.subtitleLarge.copyWith(
      fontSize: 16,
      color: SaColors.gray,
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: index.isOdd ? SaColors.scaffold : null,
      ),
      constraints: const BoxConstraints(maxWidth: 1100, minHeight: 64),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 1100 / 16,
              child: Text('${index + 1}', style: textStyle),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(
                invoiceDetails.serviceDate,
                style: textStyle,
              ),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(invoiceDetails.clinicianName, style: textStyle),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(invoiceDetails.position, style: textStyle),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(invoiceDetails.hrsWorked, style: textStyle),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(invoiceDetails.rate, style: textStyle),
            ),
            SizedBox(
              width: 1100 / 8,
              child: Text(invoiceDetails.amount, style: textStyle),
            ),
            SizedBox(
              width: 1100 / 16,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      ShowAlertDialog().showAlertDialog(
                        context: context,
                        title: 'Delete Invoice',
                        content:
                            'Are you sure, You want to delete this invoice?',
                        showCloseIcon: false,
                        onYesPressed: () {
                          _invoiceBloc.add(InvoiceEvent.deleteInvoice(
                              '${invoiceDetails.invoiceId}'));
                          NavigatorUtils.pop(context);
                        },
                      );
                    },
                    child: SvgPicture.asset(SaIcons.trash),
                  ),
                  const SizedBox(width: 20),
                  GestureDetector(
                    onTap: () async {
                      await _displayTextInputDialog(
                          context: context, invoiceDetails: invoiceDetails);
                    },
                    child: const Icon(
                      Icons.edit,
                      color: SaColors.primary,
                      size: 20,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _invoiceTable() {
    return BlocProvider(
      create: (context) => _invoiceBloc,
      child: BlocBuilder<InvoiceBloc, InvoiceState>(
        builder: (context, state) {
          return state.when(
            initial: () => const SizedBox.shrink(),
            loading: () => const SizedBox(
              height: 400,
              child: Center(
                child: Loader(),
              ),
            ),
            success: (invoiceData) {
              return SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Card(
                    color: SaColors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 1100),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 24.0,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  '#',
                                  'Service date',
                                  'Clinician name',
                                  'Position',
                                  'Hrs worked',
                                  'Rate',
                                  'Amount (USD)',
                                  '',
                                ]
                                    .map((e) => Container(
                                          width: e == '#' || e == ''
                                              ? 1100 / 16
                                              : 1100 / 8,
                                          height: 48,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            e,
                                            style: SaTextStyles.subtitleLarge
                                                .copyWith(
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ))
                                    .toList(),
                              ),
                            ),
                          ),
                          const SizedBox(
                              width: 1100,
                              child: Divider(
                                  color: SaColors.scaffold, thickness: 3)),
                          for (int i = 0;
                              i < invoiceData.results.length;
                              i++) ...{
                            _invoiceTableRow(invoiceData.results[i], i)
                          },
                          const SizedBox(height: 40.0),
                          Row(
                            children: [
                              Text(
                                'Subtotal',
                                style: SaTextStyles.caption
                                    .copyWith(fontSize: 16.0),
                              ),
                              Container(
                                width: 166.0,
                                alignment: Alignment.centerRight,
                                child: const Text(
                                  '1,041.65',
                                  style: SaTextStyles.title,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SaDropDown(
                                valuesList: const ['Discount percent'],
                                borderColor: SaColors.primary,
                                value: 'Discount percent',
                                onChanged: (p0) {},
                              ),
                              const SizedBox(width: 8.0),
                              const SizedBox(
                                width: 74.0,
                                child: SaTextField(
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16.0,
                                    vertical: 6.0,
                                  ),
                                  fillColor: SaColors.scaffold,
                                  hintText: 'Enter',
                                ),
                              ),
                              Container(
                                width: 166.0,
                                alignment: Alignment.centerRight,
                                child: const Text(
                                  '0.00',
                                  style: SaTextStyles.title,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            children: [
                              Text(
                                'Total',
                                style: SaTextStyles.caption
                                    .copyWith(fontSize: 16.0),
                              ),
                              Container(
                                width: 166.0,
                                alignment: Alignment.centerRight,
                                child: const Text(
                                  '1,041.65',
                                  style: SaTextStyles.title,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            children: [
                              Text(
                                'Amount received',
                                style: SaTextStyles.caption
                                    .copyWith(fontSize: 16.0),
                              ),
                              Container(
                                width: 166.0,
                                alignment: Alignment.centerRight,
                                child: const Text(
                                  '937.49',
                                  style: SaTextStyles.title,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            children: [
                              Text(
                                'Balance due',
                                style: SaTextStyles.caption
                                    .copyWith(fontSize: 16.0),
                              ),
                              Container(
                                width: 166.0,
                                alignment: Alignment.centerRight,
                                child: const Text(
                                  '104.16',
                                  style: SaTextStyles.title,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
            error: (error) => const SizedBox(
              height: 250,
              child: Center(
                child: Text(
                  'Something wen\'t wrong',
                  style: SaTextStyles.subtitleLarge,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
