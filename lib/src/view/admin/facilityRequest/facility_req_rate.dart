import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/charts/bloc/charts_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/admin/facilityRequest/barchart.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/date_picker.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/percent_graph.dart';

class FacilityRequestRate extends StatefulWidget {
  const FacilityRequestRate({super.key});

  @override
  State<FacilityRequestRate> createState() => _FacilityRequestRateState();
}

class _FacilityRequestRateState extends State<FacilityRequestRate> {
  DateTimeRange rangedDate = DateTimeRange(
      start: DateTime.now().weekFirstDay(), end: DateTime.now().weekLastDay());
  late DateTime _firstDate;
  late DateTime _lastDate;

  final ChartsBloc _chartsBloc = ChartsBloc();

  @override
  void initState() {
    super.initState();
    _firstDate = rangedDate.start;
    _lastDate = rangedDate.end;
    _chartsBloc.add(ChartsEvent.getAllData(
      fromDate: rangedDate.start.dmY(),
      toDate: rangedDate.end.dmY(),
    ));
    // _chartsBloc.add(ChartsEvent.getData(
    //   fromDate: rangedDate.start.dmY(),
    //   toDate: rangedDate.end.dmY(),
    // ));
  }

  @override
  Widget build(BuildContext context) {
    final screenwidth = MediaQuery.of(context).size.width - 48;
    final radius = (screenwidth / 4) - 8;
    return BlocProvider(
      create: (context) => _chartsBloc,
      child: Scaffold(
        appBar: appBar(context, title: SaString.facilityReqRate),
        body: RefreshIndicator(
          onRefresh: () async {
            _chartsBloc.add(ChartsEvent.getAllData(
              fromDate: rangedDate.start.dmY(),
              toDate: rangedDate.end.dmY(),
            ));
            // _chartsBloc.add(ChartsEvent.getData(
            //   fromDate: rangedDate.start.dmY(),
            //   toDate: rangedDate.end.dmY(),
            // ));
            await Future.delayed(const Duration(seconds: 1));
          },
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    const SizedBox(height: 16),
                    BlocBuilder<ChartsBloc, ChartsState>(
                      builder: (context, state) {
                        return SaOutlineButton(
                          width: 217,
                          height: 40,
                          onPressed: () async {
                            await ShowRangeDatePicker()
                                .rangePicker(ctx: context)
                                .then((value) {
                              if (value != null) {
                                rangedDate = value;
                                _firstDate = rangedDate.start;
                                _lastDate = rangedDate.end;
                                _chartsBloc.add(ChartsEvent.refreshAllChart(
                                  fromDate: rangedDate.start.dmY(),
                                  toDate: rangedDate.end.dmY(),
                                ));
                              }
                            });
                          },
                          text: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  '${_firstDate.MdY()} - ${_lastDate.MdY()}',
                                  style: SaTextStyles.subtitleSmall.copyWith(
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              const SizedBox(width: 4.0),
                              SvgPicture.asset(
                                SaIcons.schedule,
                                color: SaColors.black,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    const SizedBox(height: 28),
                    SizedBox(
                      height: 182,
                      width: MediaQuery.of(context).size.width - 48,
                      child: BlocBuilder<ChartsBloc, ChartsState>(
                        builder: (context, state) {
                          return state.when(
                            initial: () => const SizedBox.shrink(),
                            loading: () => const Loader(),
                            success: (shifts, allShift) {
                              List<Shift> allShiftList =
                                  List.from(allShift ?? []);
                              List<Shift> shiftList = List.from(shifts);

                              allShiftList.removeWhere((element) =>
                                  element.startDate.parseDate().day <
                                      rangedDate.start.day &&
                                  element.startDate.parseDate().month ==
                                      rangedDate.start.month);
                              shiftList.removeWhere((element) =>
                                  element.startDate.parseDate().day <
                                      rangedDate.start.day &&
                                  element.startDate.parseDate().month ==
                                      rangedDate.start.month);

                              int totalRequest = 0;
                              int fillRate = 0;
                              int clnCancel = 0;
                              int facilityCancel = 0;
                              for (Shift shift in shiftList) {
                                if (shift.status.isOpen() ||
                                    shift.status.isDeclined() ||
                                    shift.status.isApproved() ||
                                    shift.status.isCompleted()) {
                                  totalRequest++;
                                }
                                if (shift.status.isCompleted()) {
                                  fillRate++;
                                }
                                if (shift.declinedBy.toLowerCase() ==
                                    'clinician') {
                                  clnCancel++;
                                }
                                if (shift.declinedBy.toLowerCase() ==
                                    'facility') {
                                  facilityCancel++;
                                }
                              }
                              return Stack(
                                alignment: Alignment.topCenter,
                                children: [
                                  Positioned(
                                    top: 0,
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: screenwidth / 2,
                                          child: _halfCircleChart(
                                            radius: radius >= 77 ? 77 : radius,
                                            chartColor: SaColors.primary,
                                            chartDetail: 'Total Request',
                                            chartpercentage: totalRequest /
                                                allShiftList.length,
                                            achievedNumber: '$totalRequest',
                                          ),
                                        ),
                                        const SizedBox(width: 8),
                                        SizedBox(
                                          width: screenwidth / 2,
                                          child: _halfCircleChart(
                                            radius: radius >= 77 ? 77 : radius,
                                            chartColor: SaColors.green,
                                            chartDetail: 'Fill rate',
                                            chartpercentage:
                                                fillRate / totalRequest,
                                            achievedNumber: '$fillRate',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    top: 105,
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: screenwidth / 2,
                                          child: _halfCircleChart(
                                            radius: radius >= 77 ? 77 : radius,
                                            chartColor: SaColors.red,
                                            chartDetail:
                                                'Cancellation rate (CLN)',
                                            chartpercentage:
                                                clnCancel / totalRequest,
                                            achievedNumber: '$clnCancel',
                                          ),
                                        ),
                                        const SizedBox(width: 8),
                                        SizedBox(
                                          width: screenwidth / 2,
                                          child: _halfCircleChart(
                                            radius: radius >= 77 ? 77 : radius,
                                            chartColor: SaColors.orange,
                                            chartDetail:
                                                'Cancellation rate (Facility)',
                                            chartpercentage:
                                                facilityCancel / totalRequest,
                                            achievedNumber: '$facilityCancel',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                            error: (error) => const SizedBox.shrink(),
                          );
                        },
                      ),
                    ),
                    const SizedBox(height: 40),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenwidth / 2,
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: SaColors.primary,
                                  borderRadius: BorderRadius.circular(2),
                                ),
                                height: 8,
                                width: 24,
                              ),
                              const SizedBox(width: 8),
                              const Text(
                                'Total Request',
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: SaColors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: screenwidth / 2,
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: SaColors.red,
                                  borderRadius: BorderRadius.circular(2),
                                ),
                                height: 8,
                                width: 24,
                              ),
                              const SizedBox(width: 8),
                              const Text(
                                'Facility Cancelled',
                                style: SaTextStyles.subtitleSmall,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 31),
                    Card(
                      color: SaColors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: BlocBuilder<ChartsBloc, ChartsState>(
                        builder: (context, state) {
                          return state.when(
                            initial: () => const SizedBox.shrink(),
                            loading: () => const SizedBox(
                              height: 400,
                              child: Center(
                                child: Loader(),
                              ),
                            ),
                            success: (shifts, allShift) {
                              List<Shift> shiftList = List.from(shifts);
                              shiftList.removeWhere((element) =>
                                  element.startDate.parseDate().day <
                                      rangedDate.start.day &&
                                  element.startDate.parseDate().month ==
                                      rangedDate.start.month);
                              Map<String, dynamic> mapData;
                              List<int> totalRequestList = [];
                              List<int> completedList = [];
                              List<String> xList = [];
                              Duration difference =
                                  _lastDate.difference(_firstDate);
                              if (difference.inDays > 31 ||
                                  _firstDate.month != _lastDate.month) {
                                mapData = shiftList.groupBy(
                                    (m) => m.startDate.parseDate().My());
                                if (_lastDate.year != _firstDate.year) {
                                  for (var i = 0; i < difference.inDays; i++) {
                                    if (!xList.contains(_firstDate
                                        .add(Duration(days: i))
                                        .My())) {
                                      xList.add(_firstDate
                                          .add(Duration(days: i))
                                          .My());
                                    }
                                  }
                                } else {
                                  List.generate(
                                      (_lastDate.month - _firstDate.month) + 1,
                                      (index) => xList.add(_firstDate
                                          .firstDate(_firstDate.month + index)
                                          .My()));
                                }

                                for (String key in xList) {
                                  int totalRequest = 0;
                                  int completed = 0;
                                  if (mapData.containsKey(key)) {
                                    for (Shift shift in mapData[key]) {
                                      if (shift.status.isOpen() ||
                                          shift.status.isDeclined() ||
                                          shift.status.isApproved() ||
                                          shift.status.isCompleted()) {
                                        totalRequest++;
                                      }
                                      if (shift.status.isCompleted()) {
                                        completed++;
                                      }
                                    }
                                  }
                                  totalRequestList.add(totalRequest);
                                  completedList.add(completed);
                                }
                              } else {
                                mapData = shiftList.groupBy(
                                    (m) => m.startDate.parseDate().Md());
                                List.generate(
                                    difference.inDays + 1,
                                    (index) => xList.add(_firstDate
                                        .add(Duration(days: index))
                                        .Md()));

                                for (String key in xList) {
                                  int totalRequest = 0;
                                  int completed = 0;
                                  if (mapData.containsKey(key)) {
                                    for (Shift shift in mapData[key]) {
                                      if (shift.status.isOpen() ||
                                          shift.status.isDeclined() ||
                                          shift.status.isApproved() ||
                                          shift.status.isCompleted()) {
                                        totalRequest++;
                                      }
                                      if (shift.status.isCompleted()) {
                                        completed++;
                                      }
                                    }
                                  }
                                  totalRequestList.add(totalRequest);
                                  completedList.add(completed);
                                }
                              }

                              return BarChartSample2(
                                list: List.generate(
                                  xList.length,
                                  (index) => {
                                    'x': xList[index],
                                    'requested': totalRequestList[index],
                                    'completed': completedList[index]
                                  },
                                ),
                              );
                            },
                            error: (error) => const SizedBox.shrink(),
                          );
                        },
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _halfCircleChart({
    required String chartDetail,
    required double chartpercentage,
    required Color chartColor,
    required String achievedNumber,
    required double radius,
  }) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          CircularPercentIndicator(
            radius: radius,
            lineWidth: 15,
            percent: 1,
            progressColor: SaColors.white,
            circularStrokeCap: CircularStrokeCap.square,
            arcType: ArcType.HALF,
          ),
          CircularPercentIndicator(
            radius: 77,
            lineWidth: 15,
            percent: chartpercentage,
            backgroundColor: SaColors.white,
            progressColor: chartColor,
            circularStrokeCap: CircularStrokeCap.square,
            arcType: ArcType.HALF,
          ),
          Positioned(
            bottom: (77 / 2) + 56,
            child: SizedBox(
              width: 70,
              child: Text(
                chartDetail,
                textAlign: TextAlign.center,
                style: SaTextStyles.caption.copyWith(
                  color: SaColors.hintColor,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: (77 / 2) + 35,
            child: Text(
              achievedNumber,
              textAlign: TextAlign.center,
              style: SaTextStyles.title.copyWith(
                fontSize: 14,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
