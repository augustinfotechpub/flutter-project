import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final ForgotPasswordBloc _forgotPasswordBloc = ForgotPasswordBloc();
  final TextEditingController _email = TextEditingController();
  @override
  void dispose() {
    _email.dispose();
    _forgotPasswordBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _forgotPasswordBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: appBar(context, title: SaString.forgotPassword),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    SaString.email,
                    style: SaTextStyles.subtitleSmall,
                  ),
                  const SizedBox(height: 8.0),
                  SaTextField(
                    controller: _email,
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 12.0,
                    ),
                    hintText: SaString.emailAddress,
                  ),
                  const Spacer(),
                  BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
                    listener: (context, state) {
                      state.when(
                        initial: () {},
                        loading: () => Loader.show(context),
                        success: (message) {
                          Loader.hide(context);
                          Toast.success(context, message);
                        },
                        error: (error) {
                          Loader.hide(context);
                          Toast.error(context, error);
                        },
                      );
                    },
                    child: SaButton(
                      onPressed: () {
                        _forgotPasswordBloc.add(
                            ForgotPasswordEvent.resetPassword(
                                _email.text.trim()));
                      },
                      text: SaString.resetPassword,
                      height: 48.0,
                      radius: 12.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
