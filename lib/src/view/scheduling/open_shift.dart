import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/open_shifts/bloc/open_shifts_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/calendar.dart';
import 'package:shift_alerts/src/widget/calendar_control.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:table_calendar/table_calendar.dart';

class OpenShift extends StatefulWidget {
  const OpenShift({super.key});

  @override
  State<OpenShift> createState() => _OpenShiftState();
}

class _OpenShiftState extends State<OpenShift> {
  PageController? myPageController;
  String _dropValue = 'Week';
  DateTime _selectedDate = DateTime.now();
  CalendarFormat _calendarFormat = CalendarFormat.week;
  bool isDay() => _calendarFormat == CalendarFormat.twoWeeks;

  TextStyle getTextStyle(bool lightText) {
    return TextStyle(
      fontSize: 12,
      fontWeight: lightText ? FontWeight.w400 : FontWeight.w500,
      color: lightText ? SaColors.hintColor : SaColors.gray,
    );
  }

  void _changePage(bool nextPage, DateTime focusDay) {
    if (myPageController != null &&
        _calendarFormat != CalendarFormat.twoWeeks) {
      if (nextPage) {
        myPageController!.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      } else {
        myPageController!.previousPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      }
    } else if (_calendarFormat == CalendarFormat.twoWeeks) {
      DateTime fromDate =
          DateTime(focusDay.year, focusDay.month + (nextPage ? 1 : -1), 1);
      openShiftsBloc.add(OpenShiftsEvent.getOpenShifts(
          fromDate: fromDate.month == DateTime.now().month
              ? DateTime.now().dmY()
              : focusDay.firstDate(focusDay.month + (nextPage ? 1 : -1)).dmY(),
          toDate: fromDate.lastDate().dmY()));
      _selectedDate =
          fromDate.month == DateTime.now().month ? DateTime.now() : fromDate;
    }
  }

  @override
  void initState() {
    super.initState();
    _callTimer();
  }

  void _callTimer() {
    openShiftTimer = Timer.periodic(const Duration(seconds: 15), (timer) async {
      // print('call api open shift');
      openShiftsBloc.add(OpenShiftsEvent.refreshOpenShifts(
        fromDate: _selectedDate.firstDate(_selectedDate.month).dmY(),
        toDate: _selectedDate.lastDate().dmY(),
      ));
    });
  }

  @override
  void dispose() {
    super.dispose();
    openShiftTimer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        openShiftsBloc.add(
          OpenShiftsEvent.getOpenShifts(
            fromDate: _selectedDate.firstDate(_selectedDate.month).dmY(),
            toDate: _selectedDate.lastDate().dmY(),
          ),
        );
        _dropValue = 'Week';
        _calendarFormat = CalendarFormat.week;
        await Future.delayed(const Duration(seconds: 1));
      },
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: BlocBuilder<OpenShiftsBloc, OpenShiftsState>(
              builder: (context, state) {
                return state.when(
                  initial: () => const SizedBox.shrink(),
                  loading: () => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _calendarControlPanel(),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.8,
                        child: const Center(
                          child: Loader(),
                        ),
                      )
                    ],
                  ),
                  error: (error) => const SizedBox.shrink(),
                  success: (openShifts) {
                    _selectedDate = _selectedDate;
                    _dropValue = _dropValue;
                    _calendarFormat = _calendarFormat;
                    List<Shift> shiftList = List.from(openShifts);
                    shiftList.removeWhere((element) =>
                        element.startTime
                            .parseDate()
                            .difference(DateTime.now())
                            .inMinutes <
                        0);
                    shiftList.sort((a, b) => a.startTime
                        .parseDate()
                        .compareTo(b.startTime.parseDate()));

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (isDay()) ...{_calendarControlPanel()},
                        if (!isDay()) ...{_calendar(shiftList)},
                        if (isDay()) ...{
                          _dayListView(shiftList)
                        } else ...{
                          shiftList.any((e) =>
                                  _selectedDate.dmY() == e.startDate.ydM())
                              ? ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: shiftList.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                    return shiftList[index]
                                                .startDate
                                                .parseDate()
                                                .dmY() !=
                                            _selectedDate.dmY()
                                        ? const SizedBox.shrink()
                                        : Padding(
                                            padding: const EdgeInsets.symmetric(
                                              vertical: 10,
                                            ),
                                            child: _eventCard(
                                              ctx: context,
                                              shift: shiftList[index],
                                            ),
                                          );
                                  },
                                )
                              : SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.5,
                                  child: const Center(
                                    child: Noshift(),
                                  ),
                                )
                        }
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _calendar(List<Shift> shiftList) {
    return Card(
      color: _calendarFormat == CalendarFormat.month
          ? SaColors.white
          : SaColors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: Padding(
        padding: _calendarFormat == CalendarFormat.month
            ? const EdgeInsets.all(16)
            : EdgeInsets.zero,
        child: SaCalendar(
          onCalendarCreated: (p0) => myPageController = p0,
          onFormatChanged: (format) => _calendarFormat = format,
          shifts: shiftList,
          focusDate: _selectedDate,
          calendarFormat: _calendarFormat,
          value: _dropValue,
          onPrevious: () => _changePage(false, _selectedDate),
          onNext: () => _changePage(true, _selectedDate),
          onDaySelected: (selectedDay, newfocusedDay) {
            if (!_selectedDate.isSameDay(selectedDay)) {
              if (selectedDay.compDate().isAfterOrEqual()) {
                if (_selectedDate.month != selectedDay.month) {
                  _selectedDate = selectedDay;
                  openShiftsBloc.add(OpenShiftsEvent.getOpenShifts(
                    toDate: selectedDay.lastDate().dmY(),
                    fromDate: selectedDay.firstDate(selectedDay.month).dmY(),
                  ));
                } else {
                  _selectedDate = selectedDay;
                  openShiftsBloc.add(
                    OpenShiftsEvent.filterOpenShifts(date: selectedDay.dmY()),
                  );
                }
              }
            }
          },
          onDropChanged: (newvalue) {
            if (newvalue == 'Month' &&
                _calendarFormat != CalendarFormat.month) {
              _dropValue = newvalue ?? 'Month';
              _calendarFormat = CalendarFormat.month;
              openShiftsBloc.add(
                OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
              );
            } else if (newvalue == 'Week' &&
                _calendarFormat != CalendarFormat.week) {
              _dropValue = newvalue ?? 'Week';
              _calendarFormat = CalendarFormat.week;
              openShiftsBloc.add(
                OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
              );
            } else if (newvalue == 'Day' &&
                _calendarFormat != CalendarFormat.twoWeeks) {
              _dropValue = newvalue ?? 'Day';
              _calendarFormat = CalendarFormat.twoWeeks;
              openShiftsBloc.add(
                OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
              );
            }
          },
        ),
      ),
    );
  }

  Widget _calendarControlPanel() {
    return CalendarControl(
      focusDate: _selectedDate,
      isonOpenshift: true,
      value: _dropValue,
      onBackPress: () {
        if (_selectedDate.month != DateTime.now().month) {
          _changePage(false, _selectedDate);
        }
      },
      onNextPress: () => _changePage(true, _selectedDate),
      onDropChanged: (newvalue) {
        if (newvalue == 'Month' && !(_calendarFormat == CalendarFormat.month)) {
          _dropValue = newvalue ?? 'Month';
          _calendarFormat = CalendarFormat.month;
          openShiftsBloc.add(
            OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
          );
        } else if (newvalue == 'Week' &&
            _calendarFormat != CalendarFormat.week) {
          _dropValue = newvalue ?? 'Week';
          _calendarFormat = CalendarFormat.week;
          openShiftsBloc.add(
            OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
          );
        } else if (newvalue == 'Day' &&
            _calendarFormat != CalendarFormat.twoWeeks) {
          _dropValue = newvalue ?? 'Day';
          _calendarFormat = CalendarFormat.twoWeeks;
          openShiftsBloc.add(
            OpenShiftsEvent.filterOpenShifts(date: _selectedDate.dmY()),
          );
        }
      },
    );
  }

  Widget _dayListView(List<Shift> shiftList) {
    return ListView.builder(
      itemCount: _selectedDate.lastDate().day - (_selectedDate.day - 1),
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        DateTime date = DateTime.utc(
          _selectedDate.year,
          _selectedDate.month,
          _selectedDate.day + index,
        );
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(date.MMdY(), style: SaTextStyles.title),
              const SizedBox(height: 16),
              shiftList.any((e) =>
                      date.dmY() == e.startDate.ydM() &&
                      e.startTime
                              .parseDate()
                              .difference(DateTime.now())
                              .inMinutes >
                          0)
                  ? ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: shiftList.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return date.dmY() != shiftList[index].startDate.ydM()
                            ? const SizedBox.shrink()
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: _eventCard(
                                  ctx: context,
                                  shift: shiftList[index],
                                ),
                              );
                      },
                    )
                  : const Noshift(),
            ],
          ),
        );
      },
    );
  }

  Widget _eventCard({
    required BuildContext ctx,
    required Shift shift,
  }) {
    List<String> requestedUsers = List.from(shift.requestingUsers ?? []);

    return IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: VerticalDivider(
              color: Color(
                  int.parse("0xFF${shift.shiftColor.replaceFirst('#', '')}")),
              thickness: 4,
            ),
          ),
          const SizedBox(width: 16),
          SizedBox(
            width: MediaQuery.of(ctx).size.width - (16 + 48 + 16),
            child: Card(
              color: SaColors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 12.0,
                  horizontal: 16.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          shift.clinicianPosition,
                          style: SaTextStyles.title,
                        ),
                        const SizedBox(width: 10),
                        if (shift.status.isPending())
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: SaColors.lightOrange,
                              borderRadius: BorderRadius.circular(24),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 4.0,
                              ),
                              child: Text(
                                SaString.pending,
                                style: getTextStyle(true).copyWith(
                                  color: SaColors.orange,
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                    const SizedBox(height: 12),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _cardImages(SaIcons.time),
                        Text(SaString.time, style: getTextStyle(true)),
                        const SizedBox(width: 2),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text.rich(
                                TextSpan(
                                  text: shift.startDate.parseDate().MdY(),
                                  style: getTextStyle(false),
                                  children: [
                                    TextSpan(
                                      text: ' ● ',
                                      style: getTextStyle(true),
                                    ),
                                    TextSpan(
                                      text:
                                          '${shift.startTime.parseDate().hm()} - ${shift.endTime.parseDate().hm()}',
                                      style: getTextStyle(false),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          _cardImages(SaIcons.facility),
                          Text(SaString.facility, style: getTextStyle(true)),
                          const SizedBox(width: 3),
                          Text(shift.facilityName, style: getTextStyle(false))
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        _cardImages(SaIcons.unit),
                        Text(SaString.unit, style: getTextStyle(true)),
                        const SizedBox(width: 3),
                        Text(shift.unit, style: getTextStyle(false))
                      ],
                    ),
                    const SizedBox(height: 16),
                    if (requestedUsers
                        .contains(HiveUtils.get(HiveKeys.clinicianId))) ...{
                      SaOutlineButton(
                        text: const Text(
                          SaString.cancel,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: SaColors.red,
                          ),
                        ),
                        borderColor: SaColors.red,
                        borderWidth: 1.5,
                        borderRadius: 12,
                        onPressed: () {
                          ShowAlertDialog().showAlertDialog(
                            context: ctx,
                            title: SaString.cancelTakingShift,
                            content:
                                'Are you sure you want to cancel this shift?',
                            showCloseIcon: true,
                            onYesPressed: () {
                              requestedUsers.removeWhere((e) =>
                                  e ==
                                  HiveUtils.get(HiveKeys.clinicianId)
                                      .toString());
                              openShiftsBloc.add(
                                OpenShiftsEvent.editShift(
                                  id: shift.scheduleId.toString(),
                                  status: 'Cancel',
                                  selectedDate: _selectedDate,
                                  facilityId: shift.facilityId.toString(),
                                  reqUsers: requestedUsers,
                                ),
                              );
                              NavigatorUtils.pop(context);
                            },
                          );
                        },
                      )
                    } else ...{
                      SaButton(
                        text: SaString.takeSchedule,
                        textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: SaColors.white,
                        ),
                        radius: 12,
                        onPressed: () {
                          ShowAlertDialog().showAlertDialog(
                            context: ctx,
                            title: SaString.confirmTakingShift,
                            content:
                                'Are you sure you want to take this shift ?',
                            showCloseIcon: true,
                            onYesPressed: () async {
                              requestedUsers.add(
                                  HiveUtils.get(HiveKeys.clinicianId)
                                      .toString());
                              openShiftsBloc.add(
                                OpenShiftsEvent.editShift(
                                  id: shift.scheduleId.toString(),
                                  status: 'Take',
                                  selectedDate: _selectedDate,
                                  facilityId: shift.facilityId.toString(),
                                  reqUsers: requestedUsers,
                                ),
                              );
                              NavigatorUtils.pop(context);
                            },
                          );
                        },
                      )
                    }
                  ],
                ),
              ),
            ),
          ),
          const SizedBox.shrink(),
        ],
      ),
    );
  }

  Widget _cardImages(String image) => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: SvgPicture.asset(
          image,
          color: SaColors.hintColor,
          height: 14,
          width: 14,
        ),
      );
}
