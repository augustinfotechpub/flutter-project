import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/provider/my_shifts/bloc/my_shifts_bloc.dart';
import 'package:shift_alerts/src/provider/open_shifts/bloc/open_shifts_bloc.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/scheduling/my_shift.dart';
import 'package:shift_alerts/src/view/scheduling/open_shift.dart';

class Scheduling extends StatefulWidget {
  const Scheduling({super.key});

  @override
  State<Scheduling> createState() => _SchedulingState();
}

class _SchedulingState extends State<Scheduling> with TickerProviderStateMixin {
  late TabController _tabController;

  final DateTime _date = DateTime.now();
  void _tabListener() {
    if (_tabController.indexIsChanging) {
      if (_tabController.index == 0) {
        myShiftsBloc.add(const MyShiftsEvent.refresh());
      } else {
        openShiftsBloc.add(
          OpenShiftsEvent.filterOpenShifts(date: DateTime.now().dmY()),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, initialIndex: 0, vsync: this)
      ..animateTo(0);
    _tabController.addListener(() {
      _tabListener();
    });
    myShiftsBloc = MyShiftsBloc();
    myShiftsBloc.add(MyShiftsEvent.getMyShifts(
      fromDate: _date.firstDate(_date.month).dmY(),
      toDate: _date.lastDate().dmY(),
      focusDay: DateTime.now(),
    ));
    openShiftsBloc.add(OpenShiftsEvent.getOpenShifts(
      fromDate: _date.firstDate(_date.month).dmY(),
      toDate: _date.lastDate().dmY(),
    ));
  }

  @override
  void dispose() {
    _tabController.removeListener(() {
      _tabListener();
    });
    _tabController.dispose();
    myShiftTimer?.cancel();
    openShiftTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => myShiftsBloc),
        BlocProvider(create: (context) => openShiftsBloc),
      ],
      child: Column(
        children: [
          _tabBar(),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              physics: const NeverScrollableScrollPhysics(),
              children: const [MyShifts(), OpenShift()],
            ),
          )
        ],
      ),
    );
  }

  Widget _tabBar() {
    return TabBar(
      controller: _tabController,
      indicatorColor: SaColors.primary,
      isScrollable: true,
      labelStyle: const TextStyle(
        fontSize: 18,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
      ),
      labelColor: SaColors.primary,
      unselectedLabelStyle: const TextStyle(
        fontSize: 18,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
      ),
      unselectedLabelColor: SaColors.gray,
      tabs: const [
        Tab(
          child: Text(
            SaString.myShift,
            textAlign: TextAlign.center,
            maxLines: 1,
            softWrap: false,
          ),
        ),
        Tab(
          child: Text(
            SaString.openShift,
            textAlign: TextAlign.center,
            maxLines: 1,
            softWrap: false,
          ),
        ),
      ],
    );
  }
}
