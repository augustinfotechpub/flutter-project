// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/my_shifts/bloc/my_shifts_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/permission_utils.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/calendar.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/shift_card.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';
import 'package:table_calendar/table_calendar.dart';

class MyShifts extends StatefulWidget {
  const MyShifts({super.key});

  @override
  State<MyShifts> createState() => _MyShiftsState();
}

class _MyShiftsState extends State<MyShifts> {
  PageController? _myPageController;
  final TextEditingController _noteController = TextEditingController();
  final TextEditingController _pocNameController = TextEditingController();
  final TextEditingController _positionController = TextEditingController();
  List<Shift> _upcomingShifts = [];
  List<Shift> _onShifts = [];

  final pocLoader = ValueNotifier<bool>(false);
  bool isLoading = false;
  DateTime _date = DateTime.now();

  TextStyle getTextStyle(bool lightText) {
    return TextStyle(
      fontSize: 12,
      fontFamily: 'Poppins',
      fontWeight: lightText ? FontWeight.w400 : FontWeight.w500,
      color: lightText ? SaColors.hintColor : SaColors.gray,
    );
  }

  @override
  void initState() {
    super.initState();
    _callTimer();
  }

  void _callTimer() {
    myShiftTimer = Timer.periodic(const Duration(seconds: 15), (timer) async {
      // print('call api my shift');
      myShiftsBloc.add(MyShiftsEvent.refreshMyShifts(
        fromDate: _date.firstDate(_date.month).dmY(),
        toDate: _date.lastDate().dmY(),
        focusDay: _date,
      ));
    });
  }

  @override
  void dispose() {
    pocLoader.dispose();
    _noteController.dispose();
    _pocNameController.dispose();
    _positionController.dispose();
    myShiftTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        myShiftsBloc.add(MyShiftsEvent.getMyShifts(
          fromDate: DateTime.now().firstDate(DateTime.now().month).dmY(),
          toDate: DateTime.now().lastDate().dmY(),
          focusDay: DateTime.now(),
        ));
        await Future.delayed(const Duration(seconds: 1));
      },
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: BlocConsumer<MyShiftsBloc, MyShiftsState>(
              listener: (context, state) {
                state.when(
                  initial: () {},
                  loading: () => isLoading = true,
                  filtered: (date) => _date = date.ydM().parseDate(),
                  refreshList: (onShift, upcomingShift, random) {
                    _onShifts = List.from(onShift);
                    _onShifts
                        .removeWhere((element) => element.timesheetId != '');
                    _onShifts
                        .removeWhere((element) => element.status.isDeclined());
                    _onShifts.removeWhere((element) =>
                        element.checkIn == '' &&
                        element.endTime
                                .parseDate()
                                .difference(DateTime.now())
                                .inMinutes <
                            0);
                    _onShifts.removeWhere((e) =>
                        e.startTime
                            .parseDate()
                            .difference(DateTime.now())
                            .inMinutes >
                        5);
                    _onShifts.sort((a, b) => a.startTime
                        .parseDate()
                        .compareTo(b.startTime.parseDate()));
                    _upcomingShifts = List.from(upcomingShift);
                    _upcomingShifts
                        .removeWhere((element) => element.status.isDeclined());
                    _upcomingShifts.removeWhere((e) =>
                        e.startTime
                            .parseDate()
                            .difference(DateTime.now())
                            .inMinutes <=
                        5);
                    _upcomingShifts.sort((a, b) => a.startTime
                        .parseDate()
                        .compareTo(b.startTime.parseDate()));
                  },
                  onShiftSuccess: (shifts) {
                    _onShifts = List.from(shifts);
                    _onShifts
                        .removeWhere((element) => element.timesheetId != '');
                    _onShifts
                        .removeWhere((element) => element.status.isDeclined());
                    _onShifts.removeWhere((element) =>
                        element.checkIn == '' &&
                        element.endTime
                                .parseDate()
                                .difference(DateTime.now())
                                .inMinutes <
                            0);
                    _onShifts.removeWhere((e) =>
                        e.startTime
                            .parseDate()
                            .difference(DateTime.now())
                            .inMinutes >
                        5);
                    _onShifts.sort((a, b) => a.startTime
                        .parseDate()
                        .compareTo(b.startTime.parseDate()));
                    isLoading = false;
                    pocLoader.value = false;
                  },
                  success: (shifts, date) {
                    _date = date.ydM().parseDate();
                    _upcomingShifts = List.from(shifts);
                    _upcomingShifts
                        .removeWhere((element) => element.status.isDeclined());
                    _upcomingShifts.removeWhere((e) =>
                        e.startTime
                            .parseDate()
                            .difference(DateTime.now())
                            .inMinutes <=
                        5);
                    _upcomingShifts.sort((a, b) => a.startTime
                        .parseDate()
                        .compareTo(b.startTime.parseDate()));
                    myShiftsBloc
                        .add(MyShiftsEvent.getOnShifts(DateTime.now().dmY()));
                    isLoading = false;
                  },
                  error: (error) {
                    isLoading = false;
                    Toast.error(context, error);
                  },
                );
              },
              builder: (context, state) {
                return isLoading
                    ? SizedBox(
                        height: MediaQuery.of(context).size.height / 1.2,
                        child: const Center(
                          child: Loader(),
                        ),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          _calendar(),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 16.0),
                            child: Text(SaString.onShift,
                                style: SaTextStyles.title),
                          ),
                          _onShifts.isEmpty
                              ? const Noshift()
                              : SizedBox(
                                  child: ValueListenableBuilder(
                                    valueListenable: pocLoader,
                                    builder: (context, value, child) {
                                      if (value) {
                                        return const SizedBox(
                                          height: 150,
                                          child: Loader(),
                                        );
                                      } else {
                                        return ListView.separated(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: _onShifts.length,
                                          itemBuilder: (context, index) {
                                            return _eventCard(
                                              ctx: context,
                                              inDialogBox: false,
                                              shift: _onShifts[index],
                                            );
                                          },
                                          separatorBuilder: (context, index) =>
                                              const SizedBox(height: 24.0),
                                        );
                                      }
                                    },
                                  ),
                                ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 16.0),
                            child: Text(
                              SaString.upcomingShifts,
                              style: SaTextStyles.title,
                            ),
                          ),
                          _upcomingShifts.isEmpty
                              ? const Noshift()
                              : _upcomingShifts.any(
                                      (e) => _date.dmY() == e.startDate.ydM())
                                  ? _shiftList(_upcomingShifts)
                                  : const Noshift(),
                        ],
                      );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _shiftList(List<Shift> shift) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      itemCount: shift.length,
      shrinkWrap: true,
      itemBuilder: (_, index) {
        return shift[index].startDate.parseDate().isSameDay(_date)
            ? Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: ShiftCard(result: shift[index]),
              )
            : const SizedBox.shrink();
      },
    );
  }

  Widget _calendar() {
    return SaCalendar(
      shifts: const [],
      focusDate: _date,
      availableCalendarFormats: const {CalendarFormat.week: 'Week'},
      onPrevious: () {
        _myPageController!.previousPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      },
      onNext: () {
        _myPageController!.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      },
      onCalendarCreated: (p0) => _myPageController = p0,
      onDaySelected: (selectedDay, newfocusedDay) {
        if (!_date.isSameDay(selectedDay)) {
          if (selectedDay.compDate().isAfterOrEqual()) {
            if (_date.month == selectedDay.month) {
              _date = selectedDay;
              myShiftsBloc
                  .add(MyShiftsEvent.filterMyShifts(date: selectedDay.dmY()));
            } else {
              _date = selectedDay;
              myShiftsBloc.add(MyShiftsEvent.getMyShifts(
                fromDate: selectedDay.firstDate(selectedDay.month).dmY(),
                toDate: selectedDay.lastDate().dmY(),
                focusDay: selectedDay,
              ));
            }
          }
        }
      },
    );
  }

  Widget _eventCard({
    required BuildContext ctx,
    required bool inDialogBox,
    required Shift shift,
  }) {
    return IntrinsicHeight(
      child: Card(
        color: inDialogBox ? SaColors.scaffold : SaColors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: SizedBox(
          width: MediaQuery.of(ctx).size.width - ((inDialogBox ? 72 : 48)),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 12.0,
              horizontal: 16.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  shift.clinicianPosition,
                  style: SaTextStyles.title.copyWith(
                    color: SaColors.orange,
                  ),
                ),
                const SizedBox(height: 12),
                Row(
                  children: [
                    _cardImages(SaIcons.time),
                    Text(SaString.time, style: getTextStyle(true)),
                    const SizedBox(width: 3),
                    Text.rich(
                      TextSpan(
                        text: shift.startDate.parseDate().MdY(),
                        style: getTextStyle(false),
                        children: [
                          TextSpan(
                            text: '  ●  ',
                            style: getTextStyle(true),
                          ),
                          TextSpan(
                            text:
                                '${shift.startTime.parseDate().hm()} - ${shift.endTime.parseDate().hm()}',
                            style: getTextStyle(false),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Row(
                    children: [
                      _cardImages(SaIcons.facility),
                      Text(
                        SaString.facility,
                        style: getTextStyle(true),
                      ),
                      const SizedBox(width: 3),
                      Text(
                        shift.facilityName,
                        style: getTextStyle(false),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    _cardImages(SaIcons.unit),
                    Text(
                      SaString.unit,
                      style: getTextStyle(true),
                    ),
                    const SizedBox(width: 3),
                    Text(
                      shift.unit,
                      style: getTextStyle(false),
                    )
                  ],
                ),
                if (!inDialogBox) const SizedBox(height: 16),
                if (!inDialogBox) _rowButtons(shift),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _rowButtons(Shift shift) {
    void fireEvent(Map<String, dynamic> data) {
      pocLoader.value = true;
      myShiftsBloc.add(
        MyShiftsEvent.editShift(id: shift.scheduleId.toString(), data: data),
      );
    }

    if (shift.checkIn == "") {
      return Row(
        children: [
          _saButton(
            name: SaString.clockIn.replaceFirst(":", ""),
            onPressed: () async {
              Loader.show(context);
              final res = await apiProvider.getLatLong(shift.facilityId);
              if (res != null) {
                final isinRange = await PermissionUtils().determinePosition(
                    double.parse(res['latitude']),
                    double.parse(res['longitude']));
                if (isinRange) {
                  Map<String, dynamic> data = {
                    'clinician_id': '${HiveUtils.get(HiveKeys.clinicianId)}',
                    'check_in': DateTime.now().toString().split('.').first,
                  };
                  if (DateTime.now()
                          .difference(shift.startTime.parseDate())
                          .inMinutes >
                      5) {
                    data.addEntries({'note': 'late for shift'}.entries);
                  }
                  await apiProvider.createActivity(
                    title: 'Clock in',
                    date: data['check_in'].toString().split(' ').first,
                    facilityId: shift.facilityId,
                  );
                  fireEvent(data);
                  Loader.hide(context);
                } else {
                  Loader.hide(context);
                  Toast.error(context, 'You are not within 100 m radius');
                }
              } else {
                Loader.hide(context);
                Toast.error(context, 'Something wen\'t wrong');
              }
            },
          ),
        ],
      );
    }
    if (shift.checkIn != "" &&
        shift.noMealBreak == "" &&
        shift.breakOut == "") {
      return Row(
        children: [
          Expanded(
            child: SaOutlineButton(
              borderColor:
                  shift.breakIn != "" ? SaColors.hintColor : SaColors.primary,
              borderWidth: 1.5,
              borderRadius: 12,
              text: Text(
                SaString.breakIn,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  color: shift.breakIn != ""
                      ? SaColors.hintColor
                      : SaColors.primary,
                ),
              ),
              onPressed: shift.breakIn != ""
                  ? null
                  : () {
                      Map<String, dynamic> data = {
                        'clinician_id':
                            '${HiveUtils.get(HiveKeys.clinicianId)}',
                        'break_in': DateTime.now().toString().split('.').first,
                      };
                      fireEvent(data);
                    },
            ),
          ),
          const SizedBox(width: 12),
          _saButton(
            name:
                shift.breakIn == "" ? SaString.noMealBreak : SaString.breakOut,
            onPressed: () {
              if (shift.breakIn == "") {
                Map<String, dynamic> data = {
                  'clinician_id': '${HiveUtils.get(HiveKeys.clinicianId)}',
                  'no_meal_break': SaString.noMealBreak,
                };
                fireEvent(data);
              } else {
                Map<String, dynamic> data = {
                  'clinician_id': '${HiveUtils.get(HiveKeys.clinicianId)}',
                  'break_out': DateTime.now().toString().split('.').first,
                };
                fireEvent(data);
              }
            },
          ),
        ],
      );
    }
    if (shift.checkIn != "" &&
        shift.checkOut == "" &&
        (shift.noMealBreak != "" || shift.breakOut != "")) {
      return Row(
        children: [
          _saButton(
            name: SaString.clockOut.replaceFirst(":", ""),
            onPressed: () async {
              Map<String, dynamic> data = {
                'clinician_id': '${HiveUtils.get(HiveKeys.clinicianId)}',
                'check_out': DateTime.now().toString().split('.').first,
              };
              await apiProvider.createActivity(
                title: 'Clock out',
                date: data['check_out'].toString().split(' ').first,
                facilityId: shift.facilityId,
              );
              fireEvent(data);
            },
          ),
        ],
      );
    }
    if (shift.checkIn != "" &&
        shift.checkOut != "" &&
        (shift.noMealBreak != "" || shift.breakOut != "")) {
      return Row(
        children: [
          _saButton(
            name: SaString.submit,
            onPressed: () async {
              pocLoader.value = true;
              _displayTextInputDialog(
                context: context,
                shift: shift,
              );
              final Map<String, dynamic>? faciltyDetails =
                  await apiProvider.getFacilty(id: shift.facilityId);
              pocLoader.value = false;
              if (faciltyDetails != null) {
                _pocNameController.text = faciltyDetails["fullname"];
                _positionController.text = faciltyDetails["role"];
              } else {
                Toast.error(context, 'No POC admin available');
              }
            },
          ),
        ],
      );
    }
    return const SizedBox.shrink();
  }

  Widget _saButton({
    required String name,
    required void Function() onPressed,
  }) {
    return Expanded(
      child: SaButton(
        text: name,
        textStyle: const TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w600,
          color: SaColors.white,
        ),
        radius: 12,
        onPressed: onPressed,
      ),
    );
  }

  Widget _cardImages(String image) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: SvgPicture.asset(
        image,
        color: SaColors.lightHintColor,
        height: 16,
        width: 16,
      ),
    );
  }

  Future<void> _displayTextInputDialog({
    required BuildContext context,
    required Shift shift,
  }) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) {
          return ValueListenableBuilder(
            valueListenable: pocLoader,
            builder: (_, value, __) {
              return AlertDialog(
                backgroundColor: SaColors.white,
                actionsPadding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, bottom: 16.0),
                titlePadding: const EdgeInsets.symmetric(horizontal: 16.0),
                contentPadding: const EdgeInsets.all(16),
                insetPadding: const EdgeInsets.all(20),
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                title: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          SaString.submit,
                          style: SaTextStyles.heading.copyWith(fontSize: 17),
                        ),
                        const Spacer(),
                        SizedBox(
                          width: 24.0,
                          child: IconButton(
                            padding: EdgeInsets.zero,
                            splashRadius: 16,
                            onPressed: () => Navigator.pop(context),
                            icon: const Icon(Icons.clear, color: SaColors.gray),
                          ),
                        ),
                      ],
                    ),
                    const Divider(
                        color: SaColors.scaffold, thickness: 1, height: 0.0),
                  ],
                ),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _eventCard(ctx: context, inDialogBox: true, shift: shift),
                      const SizedBox(height: 26),
                      const Text(
                        SaString.nameOfPOC,
                        style: SaTextStyles.subtitleSmall,
                      ),
                      const SizedBox(height: 8),
                      value
                          ? const SizedBox(
                              height: 200,
                              child: Center(
                                child: Loader(),
                              ),
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SaTextField(
                                  readOnly: true,
                                  contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                    vertical: 10,
                                  ),
                                  controller: _pocNameController,
                                  hintStyle: const TextStyle(fontSize: 15),
                                  borderSide:
                                      const BorderSide(color: SaColors.gray),
                                ),
                                const SizedBox(height: 16),
                                const Text(
                                  SaString.position,
                                  style: SaTextStyles.subtitleSmall,
                                ),
                                const SizedBox(height: 8),
                                SaTextField(
                                  readOnly: true,
                                  contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                    vertical: 10,
                                  ),
                                  controller: _positionController,
                                  hintStyle: const TextStyle(fontSize: 15),
                                  borderSide:
                                      const BorderSide(color: SaColors.gray),
                                ),
                                const SizedBox(height: 16),
                                const Text(
                                  SaString.note,
                                  style: SaTextStyles.subtitleSmall,
                                ),
                                const SizedBox(height: 8),
                                SaTextField(
                                  maxLine: 2,
                                  controller: _noteController,
                                  contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                    vertical: 10,
                                  ),
                                  hintText: "Nurse",
                                  hintStyle: const TextStyle(fontSize: 15),
                                  borderSide:
                                      const BorderSide(color: SaColors.gray),
                                ),
                              ],
                            ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  Row(
                    children: [
                      Expanded(
                        child: SaButton(
                          text: SaString.submit,
                          textStyle: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: SaColors.white,
                          ),
                          radius: 12,
                          onPressed: () async {
                            if (!value) {
                              pocLoader.value = true;
                              final Map<String, dynamic> map = {
                                "shift_id": shift.scheduleId,
                                "clinician_id": shift.clinicianId,
                                "clinician_name": shift.clinicianName,
                                "clinician_position": shift.clinicianPosition,
                                "break_in_date": shift.breakIn.split(" ")[0],
                                "check_in_time": shift.checkIn,
                                "break_out_date": shift.breakOut.split(" ")[0],
                                "check_out_time": shift.checkOut,
                                "shift_date": shift.startDate,
                                "clinician_note": _noteController.text.trim(),
                                "facility_name": shift.facilityName,
                                "facility_id": shift.facilityId,
                                "shift_color": shift.shiftColor,
                                "poc_admin": _pocNameController.text.trim(),
                                "status": "Pending"
                              };
                              _submitForSignature(map, shift.facilityId);
                            }
                          },
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          );
        });
  }

  Future<void> _submitForSignature(
      Map<String, dynamic> map, String facilityId) async {
    await apiProvider.submitForSignature(map: map).then(
      (response) {
        if (response.statusCode == 201) {
          sendNotification(facilityId);
          myShiftsBloc.add(
            MyShiftsEvent.editShift(
              id: map['shift_id'].toString(),
              data: {
                'timesheet_id':
                    jsonDecode(response.body)['timesheet_id'].toString(),
              },
            ),
          );

          pocLoader.value = false;
          NavigatorUtils.pop(context);
          Toast.success(context, "Submitted");
        } else {
          pocLoader.value = false;
          NavigatorUtils.pop(context);
          Toast.error(context, response.body);
        }
      },
    );
  }

  Future<void> sendNotification(String facilityId) async {
    List<String> adminId = [];
    final admins = await apiProvider.getAdmins();
    for (var admin in jsonDecode(admins.body)['results']) {
      admin = Admin.fromJson(admin);
      if (admin.role == 'hmc_admin') {
        adminId.add(admin.adminId.toString());
      }
      if (admin.role == 'poc_admin' &&
          admin.facility.toString() == facilityId) {
        adminId.add(admin.adminId.toString());
      }
    }
    for (String id in adminId) {
      await apiProvider.postNotification(map: {
        'user_id': id,
        'notification':
            '${HiveUtils.get(HiveKeys.fullName)} submitted for poc request.',
        'created_time': '${DateTime.now()}',
        'seen': 'active',
      });
    }
  }
}
