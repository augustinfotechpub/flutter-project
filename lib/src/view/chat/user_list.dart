import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/chat/chat_list/bloc/chat_list_bloc.dart';
import 'package:shift_alerts/src/provider/chat/chat_users/bloc/chat_users_bloc.dart';
import 'package:shift_alerts/src/provider/chat/chat_users/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/view/chat/chat.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class ChatUserList extends StatefulWidget {
  final ChatListBloc chatListBloc;
  const ChatUserList({super.key, required this.chatListBloc});

  @override
  State<ChatUserList> createState() => _ChatUserListState();
}

class _ChatUserListState extends State<ChatUserList> {
  final TextEditingController _search = TextEditingController();
  final ChatUsersBloc _chatUsersBloc = ChatUsersBloc();
  final int userId = HiveUtils.get(HiveKeys.user);
  List<Chatuser> _searchUserList = [];
  List<Chatuser> userList = [];
  final _searchValue = ValueNotifier<int>(0);

  @override
  void initState() {
    super.initState();
    _chatUsersBloc.add(const ChatUsersEvent.getChatUsers());
  }

  @override
  void dispose() {
    _search.dispose();
    _searchValue.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _chatUsersBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: appBar(context, title: SaString.allUsers),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              children: [
                const SizedBox(height: 16.0),
                SaTextField(
                  controller: _search,
                  hintText: SaString.search,
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 12.0,
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 8.0),
                    child: SvgPicture.asset(SaIcons.search),
                  ),
                  onChanged: (p0) {
                    if (p0.isNotEmpty) {
                      _searchUserList = userList
                          .where((e) =>
                              e.firstname
                                  .toLowerCase()
                                  .contains(p0.toLowerCase()) ||
                              e.lastname
                                  .toLowerCase()
                                  .contains(p0.toLowerCase()))
                          .toList();
                      _searchValue.value = randomNumber();
                    } else {
                      _searchValue.value = 0;
                      _searchUserList.clear();
                    }
                  },
                ),
                BlocConsumer<ChatUsersBloc, ChatUsersState>(
                  listener: (context, state) {
                    state.whenOrNull(
                      error: (error) => Toast.error(context, error),
                    );
                  },
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => Container(
                        height: MediaQuery.of(context).size.height - 170,
                        alignment: Alignment.center,
                        child: const Loader(),
                      ),
                      success: (users) {
                        userList = List.from(users);
                        userList.removeWhere((element) =>
                            element.userId == HiveUtils.get(HiveKeys.user));
                        return ValueListenableBuilder(
                          valueListenable: _searchValue,
                          builder: (context, value, _) {
                            final finalUserList =
                                value == 0 ? userList : _searchUserList;
                            return Expanded(
                              child: ListView.separated(
                                itemCount: finalUserList.length + 1,
                                itemBuilder: (context, index) =>
                                    const SizedBox(height: 16.0),
                                separatorBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () async {
                                      String channelId;
                                      int peerId = finalUserList[index].userId;
                                      if (userId > peerId) {
                                        channelId = '${userId}_$peerId';
                                      } else {
                                        channelId = '${peerId}_$userId';
                                      }
                                      NavigatorUtils.pushReplacement(
                                          context,
                                          Chat(
                                            image: finalUserList[index]
                                                .avatarImage,
                                            name:
                                                '${finalUserList[index].firstname.capitalize()} ${finalUserList[index].lastname.capitalize()}',
                                            channelId: channelId,
                                            peerId: finalUserList[index]
                                                .userId
                                                .toString(),
                                          )).then((value) {
                                        currentChannel = '';
                                        widget.chatListBloc
                                            .add(const ChatListEvent.refresh());
                                      });
                                    },
                                    child: Container(
                                      height: 80.0,
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 24.0,
                                        vertical: 16.0,
                                      ),
                                      decoration: BoxDecoration(
                                        color: SaColors.white,
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                      ),
                                      child: Row(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(72.0),
                                            child: CachedNetworkImage(
                                              placeholder: (context, url) =>
                                                  const ColoredBox(
                                                      color:
                                                          SaColors.lightPink),
                                              imageUrl: finalUserList[index]
                                                  .avatarImage,
                                              height: 48.0,
                                              width: 48.0,
                                              fit: BoxFit.cover,
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Icon(
                                                Icons.error_outline_rounded,
                                                color: SaColors.red,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 12.0),
                                          Expanded(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '${finalUserList[index].firstname.capitalize()} ${finalUserList[index].lastname.capitalize()}',
                                                  style: SaTextStyles.title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                          },
                        );
                      },
                      error: (error) => const SizedBox.shrink(),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
