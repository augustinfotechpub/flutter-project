// ignore_for_file: use_build_context_synchronously, empty_catches

import 'dart:convert';

import 'package:agora_rtm/agora_rtm.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/utils/rtc_engine.dart';

class VoiceCall extends StatefulWidget {
  final bool joined;
  final String channelId;
  final String peerId;
  final String? channelToken;
  final String image;
  final String peerName;
  const VoiceCall({
    super.key,
    required this.joined,
    required this.channelId,
    required this.peerId,
    this.channelToken,
    required this.image,
    required this.peerName,
  });

  @override
  State<VoiceCall> createState() => _VoiceCallState();
}

class _VoiceCallState extends State<VoiceCall> {
  String? channelToken;
  String userId = '${HiveUtils.get(HiveKeys.user)}';

  @override
  void initState() {
    isJoin.value = widget.joined;
    super.initState();
    initialize();
    rtcEngineUtils.rtcEventHandler(context);
    if (channelToken == null) {
      getToken();
    } else {
      channelToken = widget.channelToken;
    }
  }

  Future<void> initialize() async {
    await Permission.microphone.request();
  }

  void getToken() async {
    final response =
        await apiProvider.getAgoraToken(channelId: widget.channelId);
    Map<String, dynamic> json = jsonDecode(response.body);
    if (json.containsKey('payload')) {
      channelToken = json['payload'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60.0,
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Stack(
          children: [
            Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 24.0, vertical: 40.0),
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: SaColors.primary.withOpacity(0.25),
                          blurRadius: 5.0,
                          spreadRadius: 5.0,
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(72.0),
                      child: CachedNetworkImage(
                        placeholder: (context, url) =>
                            const ColoredBox(color: SaColors.lightPink),
                        imageUrl: widget.image,
                        height: 96.0,
                        width: 96.0,
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) => const Icon(
                          Icons.error_outline_rounded,
                          color: SaColors.red,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 24.0),
                  Text(widget.peerName, style: SaTextStyles.heading),
                  ValueListenableBuilder(
                    valueListenable: callStatus,
                    builder: (context, value, child) {
                      if (value == 'Call Declined') {
                        value = 'Call Decline';
                        rtcEngine.leaveChannel();
                        try {
                          Future.delayed(
                            const Duration(seconds: 1),
                            () => NavigatorUtils.pop(context),
                          );
                        } catch (e) {}
                      }
                      return Text(value, style: SaTextStyles.subtitleSmall);
                    },
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.symmetric(vertical: 48.0),
              child: ValueListenableBuilder(
                valueListenable: isJoin,
                builder: (context, value, child) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: !value
                        ? [
                            RawMaterialButton(
                              onPressed: () async {
                                await rtcEngine.joinChannel(
                                    channelToken, widget.channelId, null, 0);
                                AgoraRtmRemoteInvitation invitation =
                                    AgoraRtmRemoteInvitation(
                                  widget.peerId,
                                  channelId: widget.channelId,
                                );
                                try {
                                  await rtmClient.acceptRemoteInvitation(
                                      invitation.toJson());
                                } catch (e) {
                                  notificationsUtils.sendNotification(
                                    message: 'Remote Call Accepted',
                                    type: 'call',
                                    peerId: widget.peerId,
                                    channelId: widget.channelId,
                                    image: '',
                                  );
                                }
                                isJoin.value = true;
                              },
                              shape: const CircleBorder(),
                              elevation: 2.0,
                              fillColor: SaColors.green,
                              padding: const EdgeInsets.all(15.0),
                              child: const Icon(
                                Icons.call,
                                color: Colors.white,
                                size: 35.0,
                              ),
                            ),
                            RawMaterialButton(
                              onPressed: () async {
                                AgoraRtmRemoteInvitation invitation =
                                    AgoraRtmRemoteInvitation(
                                  widget.peerId,
                                  channelId: widget.channelId,
                                );
                                try {
                                  await rtmClient.refuseRemoteInvitation(
                                      invitation.toJson());
                                } catch (e) {
                                  notificationsUtils.sendNotification(
                                    message: 'Remote Call End',
                                    type: 'call',
                                    peerId: widget.peerId,
                                    channelId: widget.channelId,
                                    image: '',
                                  );
                                  NavigatorUtils.pop(context);
                                }
                              },
                              shape: const CircleBorder(),
                              elevation: 2.0,
                              fillColor: SaColors.red,
                              padding: const EdgeInsets.all(15.0),
                              child: const Icon(
                                Icons.call_end,
                                color: Colors.white,
                                size: 35.0,
                              ),
                            ),
                          ]
                        : [
                            RawMaterialButton(
                              onPressed: () async {
                                try {
                                  AgoraRtmLocalInvitation invitation =
                                      AgoraRtmLocalInvitation(
                                    widget.peerId,
                                    channelId: widget.channelId,
                                  );
                                  await rtmClient.cancelLocalInvitation(
                                      invitation.toJson());
                                } catch (e) {}
                                rtcEngine.leaveChannel();
                                timer?.cancel();
                                callStatus.value = 'Call Ended';
                                Future.delayed(
                                  const Duration(seconds: 1),
                                  () => Navigator.pop(context),
                                );
                              },
                              shape: const CircleBorder(),
                              elevation: 2.0,
                              fillColor: SaColors.red,
                              padding: const EdgeInsets.all(15.0),
                              child: const Icon(
                                Icons.call_end,
                                color: Colors.white,
                                size: 35.0,
                              ),
                            ),
                          ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
