// ignore_for_file: use_build_context_synchronously, empty_catches

import 'dart:convert';
import 'dart:io';

import 'package:agora_rtm/agora_rtm.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/chat/chat_data/bloc/chat_bloc.dart';
import 'package:shift_alerts/src/provider/chat/chat_data/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/chat/call.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class Chat extends StatefulWidget {
  final String image;
  final String name;
  final String peerId;
  final String channelId;
  final String? lastMessageBy;
  const Chat({
    super.key,
    required this.image,
    required this.name,
    required this.peerId,
    required this.channelId,
    this.lastMessageBy,
  });

  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  final TextEditingController _message = TextEditingController();
  final ImagePicker _imagePicker = ImagePicker();

  final _image = ValueNotifier<XFile?>(null);
  String userId = '${HiveUtils.get(HiveKeys.user)}';

  AgoraRtmChannel? _channel;
  List<ChatData>? _chatDataList;
  String? channelToken;

  @override
  void initState() {
    chatBloc = ChatBloc();
    chatBloc.add(ChatEvent.getMessage(widget.channelId));
    super.initState();
    rtmClient.onMessageReceived = (message, peerId) {
      _chatDataList!.add(ChatData(
        uid: widget.peerId,
        text: message.text,
        time: DateTime.fromMillisecondsSinceEpoch(message.ts).toString(),
        msgType: 'text',
      ));
      // if (widget.lastMessageBy != userId) {
      //   try {
      chatBloc.add(ChatEvent.refresh(_chatDataList!));
      //   } catch (e) {}
      // }
    };
    try {
      apiProvider.updateStatus(widget.channelId, widget.peerId);
    } catch (e) {}
    createChannel();
    getToken();
    // _rtcEventHandler();
  }

  void createChannel() async {
    _channel = await rtmClient.createChannel(widget.channelId);
    await _channel!.join();
    currentChannel = widget.channelId;
  }

  void getToken() async {
    final response =
        await apiProvider.getAgoraToken(channelId: widget.channelId);
    Map<String, dynamic> json = jsonDecode(response.body);
    if (json.containsKey('payload')) {
      channelToken = json['payload'];
    }
  }

  void _downloadFile(String url) async {
    try {
      final directory = Platform.isIOS
          ? await getApplicationSupportDirectory()
          : Directory('storage/emulated/0/Download/Shift Alert');
      if (Platform.isAndroid && !(await directory.exists())) {
        await directory.create();
      }
      File file = Platform.isIOS
          ? File('${directory.path}/${url.split('/').last}')
          : File('${directory.path}/${url.split('/').last.split('.').first}');
      final request = await HttpClient().getUrl(Uri.parse(url));
      final response = await request.close();

      if (response.statusCode == 200) {
        final bytes = await consolidateHttpClientResponseBytes(response);
        await file.writeAsBytes(bytes).then(
          (value) async {
            final savedFile =
                await value.copy('${directory.path}/${url.split('/').last}');
            await file.delete();
            await notificationsUtils
                .localNotification(
                    payload: savedFile.path, title: url.split('/').last)
                .then((_) {
              Toast.success(context, "Download successfully");
            });
          },
        );
      }
    } catch (e) {
      Toast.success(context, "something went't wrong");
    }
  }

  @override
  void dispose() {
    try {
      _channel?.leave();
      _channel?.close();
      _message.dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => chatBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: _appBar(),
          body: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: BlocConsumer<ChatBloc, ChatState>(
                      listener: (context, state) {
                        state.whenOrNull(
                          success: (messages) {
                            if (messages.isNotEmpty) {
                              _chatDataList = List.from(messages);
                              _chatDataList!.sort(
                                (a, b) => b.time
                                    .parseDate()
                                    .compareTo(a.time.parseDate()),
                              );
                            } else {
                              _chatDataList = [];
                            }
                          },
                        );
                      },
                      builder: (context, state) {
                        if (_chatDataList == null) {
                          return const Loader();
                        }
                        return ListView.separated(
                          itemCount: _chatDataList!.length + 1,
                          shrinkWrap: true,
                          reverse: true,
                          padding: const EdgeInsets.symmetric(horizontal: 24.0),
                          itemBuilder: (context, index) =>
                              const SizedBox(height: 16.0),
                          separatorBuilder: (context, index) => Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment:
                                _chatDataList![index].uid == userId
                                    ? MainAxisAlignment.end
                                    : MainAxisAlignment.start,
                            children: [
                              _chatDataList![index].uid == userId
                                  ? const SizedBox.shrink()
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(72.0),
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) =>
                                            const ColoredBox(
                                                color: SaColors.lightPink),
                                        imageUrl: widget.image,
                                        height: 40.0,
                                        width: 40.0,
                                        fit: BoxFit.cover,
                                        errorWidget: (context, url, error) =>
                                            const Icon(
                                          Icons.error_outline_rounded,
                                          color: SaColors.red,
                                        ),
                                      ),
                                    ),
                              const SizedBox(width: 16.0),
                              Container(
                                padding: const EdgeInsets.all(16.0),
                                constraints: BoxConstraints(
                                  minWidth: 10.0,
                                  maxWidth:
                                      MediaQuery.of(context).size.width - 136,
                                ),
                                decoration: BoxDecoration(
                                  color: _chatDataList![index].uid == userId
                                      ? SaColors.lightOrange
                                      : SaColors.lightPink,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: const Radius.circular(20.0),
                                    bottomRight: Radius.circular(
                                        _chatDataList![index].uid == userId
                                            ? 0.0
                                            : 20.0),
                                    topLeft: Radius.circular(
                                        _chatDataList![index].uid == userId
                                            ? 20.0
                                            : 0.0),
                                    topRight: const Radius.circular(20.0),
                                  ),
                                ),
                                child: _msgView(
                                  _chatDataList![index].msgType,
                                  _chatDataList![index].text,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const SafeArea(child: SizedBox(height: 64.0)),
                ],
              ),
              Positioned(
                bottom: 0.0,
                child: Container(
                  color: SaColors.white,
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 24.0, vertical: 8.0),
                  child: SafeArea(
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                              backgroundColor: SaColors.transparent,
                              context: context,
                              builder: (context) => _attechmentSheet(),
                            );
                          },
                          child: SvgPicture.asset(SaIcons.clip,
                              height: 20.0, width: 20.0),
                        ),
                        const SizedBox(width: 20.0),
                        Expanded(
                          child: SaTextField(
                            controller: _message,
                            fillColor: SaColors.scaffold,
                            hintText: SaString.chatHere,
                            hintStyle: SaTextStyles.subtitleLarge
                                .copyWith(color: SaColors.gray),
                          ),
                        ),
                        const SizedBox(width: 20.0),
                        InkWell(
                          onTap: _sendMessage,
                          child: Container(
                            height: 48.0,
                            width: 48.0,
                            padding: const EdgeInsets.all(16.0),
                            decoration: const BoxDecoration(
                              color: SaColors.primary,
                              shape: BoxShape.circle,
                            ),
                            child: SvgPicture.asset(
                              SaIcons.send,
                              height: 18.0,
                              width: 18.0,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _msgView(String type, String message) {
    if (type == 'image') {
      return ConstrainedBox(
        constraints: const BoxConstraints(maxHeight: 200),
        child: Image.network(message, fit: BoxFit.cover),
      );
    }
    if (type == 'text') {
      return Text(message);
    }
    return SizedBox(
      height: 24,
      child: Row(
        children: [
          Expanded(child: Text(message.split('/').last)),
          IconButton(
            onPressed: () {
              _downloadFile(message);
            },
            icon: const Icon(Icons.download),
            padding: const EdgeInsets.all(0.0),
            visualDensity: VisualDensity.compact,
            highlightColor: SaColors.transparent,
            splashColor: SaColors.transparent,
          )
        ],
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(72.0),
            child: CachedNetworkImage(
              placeholder: (context, url) =>
                  const ColoredBox(color: SaColors.lightPink),
              imageUrl: widget.image,
              height: 40.0,
              width: 40.0,
              fit: BoxFit.cover,
              errorWidget: (context, url, error) => const Icon(
                Icons.error_outline_rounded,
                color: SaColors.red,
              ),
            ),
          ),
          // Image.asset(widget.image, height: 40.0, width: 40.0),
          const SizedBox(width: 12.0),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.name,
                style: SaTextStyles.subtitleLarge.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                'Active',
                style: SaTextStyles.subtitleSmall
                    .copyWith(color: SaColors.hintColor),
              ),
            ],
          )
        ],
      ),
      leading: Padding(
        padding: const EdgeInsets.only(left: 24.0),
        child: InkWell(
          splashColor: SaColors.transparent,
          highlightColor: SaColors.transparent,
          onTap: () => NavigatorUtils.pop(context),
          child: SvgPicture.asset(
            SaIcons.arrowBack,
            width: 24.0,
            height: 24.0,
          ),
        ),
      ),
      leadingWidth: 48.0,
      automaticallyImplyLeading: false,
      actions: [
        IconButton(
          onPressed: () async {
            await rtcEngine.joinChannel(
                channelToken,
                widget.channelId,
                null,
                int.parse(
                    HiveUtils.get(HiveKeys.user).toString().padLeft(4, '0')));
            try {
              Map<String, dynamic> status = await rtmClient
                  .queryPeersOnlineStatus(
                      [widget.peerId.toString().padLeft(4, '0')]);
              if (status.containsValue(true)) {
                AgoraRtmLocalInvitation invitation = AgoraRtmLocalInvitation(
                  widget.peerId.toString().padLeft(4, '0'),
                  channelId: widget.channelId,
                  content: jsonEncode({
                    'name': HiveUtils.get(HiveKeys.fullName),
                    'image': HiveUtils.get(HiveKeys.avatarImage),
                    'channel': widget.channelId
                  }),
                );
                await rtmClient.sendLocalInvitation(invitation.toJson());
              } else {
                notificationsUtils.sendNotification(
                  message: 'call',
                  type: 'call',
                  peerId: widget.peerId,
                  channelId: widget.channelId,
                  image: widget.image,
                );
              }
            } catch (e) {}
            callStatus.value = 'Calling';
            NavigatorUtils.push(
                context,
                VoiceCall(
                  joined: true,
                  channelId: widget.channelId,
                  peerId: widget.peerId,
                  channelToken: channelToken,
                  image: widget.image,
                  peerName: widget.name,
                ));
          },
          icon: const Icon(
            // Icons.more_vert_rounded,
            Icons.call,
            color: SaColors.black,
          ),
        ),
      ],
      toolbarHeight: 60.0,
    );
  }

  Widget _attechmentSheet() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      decoration: const BoxDecoration(
        color: SaColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () async {
                Navigator.pop(context);
                _image.value =
                    await _imagePicker.pickImage(source: ImageSource.camera);
                if (_image.value != null) {
                  Loader.show(context);
                  File file = File(_image.value!.path);
                  _sendFile(file);
                }
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100.0),
                      color: SaColors.primary,
                    ),
                    padding: const EdgeInsets.all(12.0),
                    child: const Icon(
                      CupertinoIcons.camera,
                      color: SaColors.white,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  const Text('Camera'),
                ],
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.pop(context);
                _image.value =
                    await _imagePicker.pickImage(source: ImageSource.gallery);
                if (_image.value != null) {
                  Loader.show(context);
                  File file = File(_image.value!.path);
                  _sendFile(file);
                }
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100.0),
                      color: SaColors.primary,
                    ),
                    padding: const EdgeInsets.all(12.0),
                    child: const Icon(
                      CupertinoIcons.photo,
                      color: SaColors.white,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  const Text('Gallery'),
                ],
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.pop(context);
                FilePickerResult? files = await FilePicker.platform
                    .pickFiles(allowMultiple: false, type: FileType.any);
                if (files != null) {
                  File file = File(files.files.first.path!);
                  Loader.show(context);
                  _sendFile(file);
                }
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100.0),
                      color: SaColors.primary,
                    ),
                    padding: const EdgeInsets.all(12.0),
                    child: const Icon(
                      CupertinoIcons.doc,
                      color: SaColors.white,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  const Text('Document'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _sendFile(File file) async {
    // String fileType = mime(file.fileName())!;
    String fileType = 'application/${file.filetype()}';
    final base64String = base64.encode(file.readAsBytesSync());
    try {
      final response = await apiProvider.sendFile(
          base64String, fileType, file.path.split('/').last);
      if (response.statusCode == 413) {
        Loader.hide(context);
        Toast.error(context, 'File is larger than 100 MB');
      } else {
        Map<String, dynamic> json = jsonDecode(response.body);
        if (json.containsKey('location')) {
          await _sendMessage(
            fileLink: json['location'],
            fileType:
                mimeFromExtension(fileType.split('/').last)!.contains('image')
                    ? 'image'
                    : 'file',
          );
          Loader.hide(context);
        }
      }
    } catch (e) {
      Loader.hide(context);
      Toast.error(context, 'Something went wrong');
    }
  }

  Future<void> _sendMessage({String? fileLink, String? fileType}) async {
    if (_message.text.isNotEmpty || fileLink != null) {
      String text;
      if (_message.text.isNotEmpty) {
        text = _message.text.trim();
        _message.clear();
      } else {
        text = fileLink!;
      }
      _chatDataList!.add(
        ChatData(
          uid: userId,
          text: text,
          time: DateTime.now().toUtc().toString(),
          msgType: fileType ?? 'text',
        ),
      );
      chatBloc.add(ChatEvent.postMessage(
        channelId: widget.channelId,
        message: text,
        peerId: widget.peerId,
        peerImage: widget.image,
        peerName: widget.name,
        chatData: _chatDataList!
            .map((e) => ChatData(
                  uid: e.uid,
                  text: e.text,
                  time: e.time,
                  msgType: e.msgType,
                ).toJson())
            .toList(growable: true),
      ));
      chatBloc.add(ChatEvent.refresh(_chatDataList!));
      notificationsUtils.sendNotification(
        message: text,
        type: 'msg',
        peerId: widget.peerId,
        channelId: widget.channelId,
        image: widget.image,
      );
      try {
        AgoraRtmMessage message =
            AgoraRtmMessage.fromText(text, ts: 0, offline: false);
        bool isOnline = false;
        Map<String, dynamic> status = await rtmClient
            .queryPeersOnlineStatus([widget.peerId.toString().padLeft(4, '0')]);
        isOnline = status.containsValue(true);
        if (isOnline) {
          await rtmClient.sendMessageToPeer(
              widget.peerId.toString().padLeft(4, '0'), message, false);
          await apiProvider.updateStatus(widget.channelId, widget.peerId);
        }
      } catch (e) {}
    }
  }
}
