import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mime_type/mime_type.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/chat/chat_list/bloc/chat_list_bloc.dart';
import 'package:shift_alerts/src/provider/chat/chat_list/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/view/chat/chat.dart';
import 'package:shift_alerts/src/view/chat/user_list.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class ChatList extends StatefulWidget {
  const ChatList({super.key});

  @override
  State<ChatList> createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  final TextEditingController _search = TextEditingController();
  final ChatListBloc _chatListBloc = ChatListBloc();
  final int userId = HiveUtils.get(HiveKeys.user);
  List<LastMessage> _searchUserList = [];
  List<LastMessage> userList = [];
  final _searchValue = ValueNotifier<int>(0);

  @override
  void initState() {
    super.initState();
    _chatListBloc.add(const ChatListEvent.getChatList());
  }

  @override
  void dispose() {
    _search.dispose();
    _searchValue.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _chatListBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: appBar(context, title: SaString.message),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              children: [
                const SizedBox(height: 16.0),
                SaTextField(
                  controller: _search,
                  hintText: SaString.search,
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 12.0,
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 8.0),
                    child: SvgPicture.asset(SaIcons.search),
                  ),
                  onChanged: (p0) {
                    if (p0.isNotEmpty) {
                      _searchUserList = userList
                          .where((e) => e.peerFullname
                              .toLowerCase()
                              .contains(p0.toLowerCase()))
                          .toList();
                      _searchValue.value = randomNumber();
                    } else {
                      _searchValue.value = 0;
                      _searchUserList.clear();
                    }
                  },
                ),
                BlocConsumer<ChatListBloc, ChatListState>(
                  listener: (context, state) {
                    state.whenOrNull(
                      error: (error) =>
                          Toast.error(context, 'Something went wrong'),
                    );
                  },
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => Container(
                        height: MediaQuery.of(context).size.height - 170,
                        alignment: Alignment.center,
                        child: const Loader(),
                      ),
                      success: (users) {
                        userList = List.from(users);
                        userList.sort((a, b) => b.messageTime
                            .parseDate()
                            .compareTo(a.messageTime.parseDate()));

                        return ValueListenableBuilder(
                          valueListenable: _searchValue,
                          builder: (context, value, _) {
                            final finalUserList =
                                value == 0 ? userList : _searchUserList;
                            return Expanded(
                              child: ListView.separated(
                                itemCount: finalUserList.length + 1,
                                itemBuilder: (context, index) =>
                                    const SizedBox(height: 16.0),
                                separatorBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () async {
                                      String channelId;
                                      int peerId = int.parse(
                                          finalUserList[index].peerId);
                                      if (peerId == userId) {
                                        peerId = int.parse(
                                            finalUserList[index].userId);
                                      }
                                      if (userId > peerId) {
                                        channelId = '${userId}_$peerId';
                                      } else {
                                        channelId = '${peerId}_$userId';
                                      }
                                      NavigatorUtils.push(
                                          context,
                                          Chat(
                                            lastMessageBy:
                                                finalUserList[index].messageBy,
                                            image: finalUserList[index]
                                                .peerAvatarImage,
                                            name: finalUserList[index]
                                                .peerFullname,
                                            channelId: channelId,
                                            peerId: peerId.toString(),
                                          )).then((value) {
                                        currentChannel = '';
                                        _chatListBloc
                                            .add(const ChatListEvent.refresh());
                                      });
                                    },
                                    child: Container(
                                      height: 80.0,
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 24.0,
                                        vertical: 16.0,
                                      ),
                                      decoration: BoxDecoration(
                                        color: SaColors.white,
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                      ),
                                      child: Row(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(72.0),
                                            child: CachedNetworkImage(
                                              placeholder: (context, url) =>
                                                  const ColoredBox(
                                                      color:
                                                          SaColors.lightPink),
                                              imageUrl: finalUserList[index]
                                                  .peerAvatarImage,
                                              height: 48.0,
                                              width: 48.0,
                                              fit: BoxFit.cover,
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Icon(
                                                Icons.error_outline_rounded,
                                                color: SaColors.red,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 12.0),
                                          Expanded(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  finalUserList[index]
                                                      .peerFullname,
                                                  style: SaTextStyles.title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                const SizedBox(height: 2.0),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    ConstrainedBox(
                                                      constraints:
                                                          BoxConstraints(
                                                        maxWidth: finalUserList[
                                                                            index]
                                                                        .displayed ==
                                                                    'false' &&
                                                                finalUserList[
                                                                            index]
                                                                        .messageBy !=
                                                                    userId
                                                                        .toString()
                                                            ? MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width -
                                                                258
                                                            : MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width -
                                                                246,
                                                      ),
                                                      child: _msgView(
                                                        finalUserList[index]
                                                            .lastMessage,
                                                        finalUserList[index]
                                                            .messageBy,
                                                        finalUserList[index]
                                                            .displayed,
                                                      ),
                                                      // child: finalUserList[
                                                      //                 index]
                                                      //             .messageTo ==
                                                      //         userId.toString()
                                                      //     ? Text(
                                                      //         finalUserList[
                                                      //                 index]
                                                      //             .lastMessage,
                                                      //         maxLines: 1,
                                                      //         overflow:
                                                      //             TextOverflow
                                                      //                 .ellipsis,
                                                      //         style: finalUserList[
                                                      //                         index]
                                                      //                     .displayed ==
                                                      //                 'true'
                                                      //             ? SaTextStyles
                                                      //                 .subtitleLarge
                                                      //                 .copyWith(
                                                      //                 color: SaColors
                                                      //                     .hintColor,
                                                      //               )
                                                      //             : const TextStyle(
                                                      //                 fontSize:
                                                      //                     14.0,
                                                      //                 color: SaColors
                                                      //                     .gray,
                                                      //                 fontWeight:
                                                      //                     FontWeight
                                                      //                         .w600,
                                                      //               ),
                                                      //       )
                                                      //     : Text(
                                                      //         'You: ${finalUserList[index].lastMessage}',
                                                      //         maxLines: 1,
                                                      //         overflow:
                                                      //             TextOverflow
                                                      //                 .ellipsis,
                                                      //         style: SaTextStyles
                                                      //             .subtitleLarge
                                                      //             .copyWith(
                                                      //           color: SaColors
                                                      //               .hintColor,
                                                      //         ),
                                                      //       ),
                                                    ),
                                                    Text(
                                                      '  •  ${finalUserList[index].messageTime.parseDate().hm()}',
                                                      style: SaTextStyles
                                                          .subtitleLarge
                                                          .copyWith(
                                                        color:
                                                            SaColors.hintColor,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                              width: finalUserList[index]
                                                              .displayed ==
                                                          'false' &&
                                                      finalUserList[index]
                                                              .messageBy !=
                                                          userId.toString()
                                                  ? 12.0
                                                  : 0.0),
                                          finalUserList[index].displayed ==
                                                      'false' &&
                                                  finalUserList[index]
                                                          .messageBy !=
                                                      userId.toString()
                                              ? const Icon(
                                                  Icons.circle_rounded,
                                                  size: 12.0,
                                                  color: SaColors.red,
                                                )
                                              : const SizedBox.shrink(),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                          },
                        );
                      },
                      error: (error) => const SizedBox.shrink(),
                    );
                  },
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              NavigatorUtils.push(
                  context, ChatUserList(chatListBloc: _chatListBloc));
            },
            backgroundColor: SaColors.primary,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: SvgPicture.asset(SaIcons.chat, color: SaColors.white),
          ),
        ),
      ),
    );
  }

  Widget _msgView(String message, String messageBy, String read) {
    if (message.contains('https://') && message.contains('s3.amazonaws.com/')) {
      String fileType = mimeFromExtension(message.split('.').last)!;
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (messageBy == '$userId')
            Text(
              'You: ',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: SaTextStyles.subtitleLarge.copyWith(
                color: SaColors.hintColor,
              ),
            ),
          Icon(
            fileType.contains('image')
                ? CupertinoIcons.photo
                : CupertinoIcons.doc,
            color: SaColors.hintColor,
            size: 14.0,
          ),
          Text(
            fileType.contains('image') ? ' Image' : ' Document',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: messageBy != '$userId' && read == 'false'
                ? const TextStyle(
                    fontSize: 14.0,
                    color: SaColors.gray,
                    fontWeight: FontWeight.w600,
                  )
                : SaTextStyles.subtitleLarge
                    .copyWith(color: SaColors.hintColor),
          ),
        ],
      );
    }
    return Text(
      messageBy == '$userId' ? 'You: $message' : message,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: messageBy != '$userId' && read == 'false'
          ? const TextStyle(
              fontSize: 14.0,
              color: SaColors.gray,
              fontWeight: FontWeight.w600,
            )
          : SaTextStyles.subtitleLarge.copyWith(
              color: SaColors.hintColor,
            ),
    );
  }
}
