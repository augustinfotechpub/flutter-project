import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/image.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';

class AddPayCard extends StatefulWidget {
  const AddPayCard({super.key});

  @override
  State<AddPayCard> createState() => _AddPayCardState();
}

class _AddPayCardState extends State<AddPayCard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, title: SaString.addPayCard),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
          child: Column(
            children: [
              const SizedBox(height: 16),
              Image.asset(
                SaImages.payCard,
                filterQuality: FilterQuality.high,
                fit: BoxFit.contain,
                width: MediaQuery.of(context).size.width - 48,
              ),
              const SizedBox(height: 20),
              const Text(
                SaString.para1,
                style: SaTextStyles.subtitleLarge,
              ),
              const SizedBox(height: 20),
              SaButton(
                onPressed: () {},
                height: 48,
                radius: 12.0,
                text: SaString.getStarted,
                textStyle: SaTextStyles.subtitleLarge.copyWith(
                    fontWeight: FontWeight.w600, color: SaColors.white),
              ),
              const SizedBox(height: 40),
              const Text(
                SaString.para2,
                style: SaTextStyles.subtitleLarge,
              ),
              const SizedBox(height: 24),
              benefits(SaString.banefit1),
              const SizedBox(height: 16),
              benefits(SaString.banefit2),
              const SizedBox(height: 16),
              benefits(SaString.banefit3),
              const SizedBox(height: 16),
              benefits(SaString.banefit4),
              const SizedBox(height: 16),
              benefits(SaString.banefit5),
              const SizedBox(height: 16),
              benefits(SaString.banefit6),
              const SizedBox(height: 24),
            ],
          ),
        ),
      ),
    );
  }

  Widget benefits(String text) => Row(
        children: [
          SvgPicture.asset(
            SaIcons.checked,
            // color: SaColors.black,
            height: 14,
            width: 24,
          ),
          const SizedBox(width: 8),
          Text(
            text,
            style: SaTextStyles.subtitleLarge.copyWith(color: SaColors.gray),
          ),
        ],
      );
}
