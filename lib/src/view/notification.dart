import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/notification/bloc/notification_bloc.dart';
import 'package:shift_alerts/src/provider/notification/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';

class Notifications extends StatefulWidget {
  const Notifications({super.key});

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  final NotificationBloc _notificationBloc = NotificationBloc();
  List<bool> isDissmissingList = [];

  @override
  void initState() {
    _notificationBloc.add(const NotificationEvent.getNotification());
    super.initState();
  }

  @override
  void dispose() {
    // Loader.hide(context);
    _notificationBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _notificationBloc,
      child: Scaffold(
        appBar: appBar(context, title: SaString.notification),
        body: BlocBuilder<NotificationBloc, NotificationState>(
          // listener: (context, state) {
          //   state.when(
          //     initial: () {},
          //     loading: () => Loader.show(context),
          //     success: (notifications) {},
          //     error: (error) {
          //       Loader.hide(context);
          //       Toast.error(context, 'something wen\'t wrong');
          //     },
          //   );
          // },
          builder: (context, state) {
            return RefreshIndicator(
              onRefresh: () async {
                _notificationBloc
                    .add(const NotificationEvent.getNotification());
                await Future.delayed(const Duration(milliseconds: 500));
              },
              child: state.when(
                initial: () => const SizedBox.shrink(),
                loading: () {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [Loader()],
                  );
                },
                success: (notifications) {
                  if (notifications.isEmpty) {
                    return const Noshift(
                      message: "You Don't Have Any Notifications",
                    );
                  } else {
                    List<NotificationsList> notificationList =
                        List.from(notifications);
                    notificationList
                        .sort((a, b) => b.createdTime.compareTo(a.createdTime));
                    isDissmissingList = List.generate(
                        notificationList.length, (index) => false);
                    return ListView.separated(
                      itemCount: notificationList.length,
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      separatorBuilder: (context, index) =>
                          const SizedBox(height: 16.0),
                      itemBuilder: (context, index) {
                        return isDissmissingList[index]
                            ? const SizedBox.shrink()
                            : Dismissible(
                                key: Key('$index'),
                                confirmDismiss: (direction) async {
                                  if (direction ==
                                      DismissDirection.endToStart) {
                                    final bool res =
                                        await ShowAlertDialog().showAlertDialog(
                                      context: context,
                                      title: 'Delete Notification',
                                      content:
                                          'Are you sure, You want to delete ?',
                                      showCloseIcon: false,
                                      onYesPressed: () =>
                                          Navigator.pop(context, true),
                                    );
                                    if (res) {
                                      isDissmissingList[index] = true;
                                      notificationList.removeWhere((element) =>
                                          element.notificationId ==
                                          notificationList[index]
                                              .notificationId);
                                      _notificationBloc.add(
                                        NotificationEvent.deleteNotification(
                                          id: notificationList[index]
                                              .notificationId
                                              .toString(),
                                          notificationList: notificationList,
                                        ),
                                      );
                                    }
                                    return false;
                                  } else {
                                    return false;
                                  }
                                },
                                background: Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: SvgPicture.asset(SaIcons.trash),
                                  ),
                                ),
                                direction: DismissDirection.endToStart,
                                child: Container(
                                  padding: const EdgeInsets.all(16.0),
                                  decoration: BoxDecoration(
                                    color: SaColors.white,
                                    borderRadius: BorderRadius.circular(12.0),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: SvgPicture.asset(
                                          SaIcons.scheduleFill,
                                          height: 20.0,
                                          width: 20.0,
                                        ),
                                      ),
                                      const SizedBox(width: 12.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                116,
                                            child: Text(
                                              notificationList[index]
                                                  .notification,
                                              softWrap: true,
                                              maxLines: 2,
                                              style: SaTextStyles.subtitleLarge
                                                  .copyWith(
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            notificationList[index]
                                                .createdTime
                                                .hm(),
                                            softWrap: true,
                                            maxLines: 2,
                                            style: SaTextStyles.subtitleSmall
                                                .copyWith(
                                              color: SaColors.hintColor,
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                      },
                    );
                  }
                },
                error: (error) {
                  return Text(error, style: SaTextStyles.title);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
