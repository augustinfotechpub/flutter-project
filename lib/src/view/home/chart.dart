import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/view/admin/circle_chart.dart';

class MyPerformanceChart extends StatelessWidget {
  final int workedShift;
  final int lateShift;
  final int cancelShift;
  final int totalShift;
  final int feedback;
  final double rating;
  const MyPerformanceChart({
    super.key,
    required this.workedShift,
    required this.lateShift,
    required this.cancelShift,
    required this.totalShift,
    required this.feedback,
    required this.rating,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24.0),
      decoration: BoxDecoration(
        color: SaColors.white,
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _performanceTile(
                      SaColors.orange,
                      'Total shift worked',
                      '$workedShift',
                    ),
                    const SizedBox(height: 16.0),
                    _performanceTile(
                      SaColors.green,
                      'Late for shift',
                      '$lateShift',
                    ),
                    const SizedBox(height: 16.0),
                    _performanceTile(
                      SaColors.purple,
                      'Cancelled Shifts',
                      '$cancelShift',
                    ),
                  ],
                ),
                Expanded(
                  child: SizedBox(
                    height: 170,
                    width: 170,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        SaCircleChart(
                          showBadge: false,
                          list: [
                            {'color': SaColors.orange, 'value': workedShift},
                            {'color': SaColors.green, 'value': lateShift},
                            {'color': SaColors.purple, 'value': cancelShift},
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '$totalShift',
                              style: SaTextStyles.title
                                  .copyWith(fontWeight: FontWeight.w500),
                            ),
                            Text(
                              'shifts total',
                              style: SaTextStyles.subtitleSmall
                                  .copyWith(color: SaColors.hintColor),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Divider(color: SaColors.scaffold, height: 0.0),
          // rating
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20.0,
              vertical: 14.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  SaString.ratings,
                  style: SaTextStyles.title.copyWith(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  ' ($feedback ${SaString.feedbakcs})',
                  style: SaTextStyles.subtitleSmall.copyWith(
                    color: SaColors.lightHintColor,
                  ),
                ),
                const Spacer(),
                Text(
                  '${rating.toStringAsFixed(1)} ',
                  style: SaTextStyles.title.copyWith(fontSize: 18.0),
                ),
                SvgPicture.asset(SaIcons.rating),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _performanceTile(Color? color, String title, String counter) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 2.0,
            margin: const EdgeInsets.symmetric(vertical: 4.0),
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(4.0),
            ),
          ),
          const SizedBox(width: 10.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: SaTextStyles.subtitleSmall
                    .copyWith(color: SaColors.hintColor),
              ),
              const SizedBox(height: 4.0),
              Text(
                counter,
                style: SaTextStyles.title.copyWith(fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
