import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/feedback/bloc/feedback_bloc.dart';
import 'package:shift_alerts/src/provider/my_performance/bloc/my_performance_bloc.dart';
import 'package:shift_alerts/src/provider/timesheet/model.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/bloc/upcoming_shift_bloc.dart';
import 'package:shift_alerts/src/provider/upcoming_shifts/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/chat/chat_list.dart';
import 'package:shift_alerts/src/view/home/chart.dart';
import 'package:shift_alerts/src/view/notification.dart';
import 'package:shift_alerts/src/view/profile/edit_profile.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/shift_card.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final UpcomingShiftBloc _upcomingShiftBloc = UpcomingShiftBloc();
  final MyPerformanceBloc _myPerformanceBloc = MyPerformanceBloc();
  final FeedbackBloc _feedbackBloc = FeedbackBloc();

  List<Shift>? _shift;
  List<Shift>? _upcomingShift;
  final List<TimesheetDetail> _feedbacks = [];
  String _dateType = 'This month';
  final DateTime _date = DateTime.now();
  int _workedShift = 0;
  int _lateShift = 0;
  int _cancelShift = 0;

  @override
  void initState() {
    _myPerformanceBloc.add(MyPerformanceEvent.getChartData(
      dateType: 'This month',
      fromDate: _date.firstDate(_date.month).dmY(),
      toDate: _date.lastDate().dmY(),
    ));
    _upcomingShiftBloc.add(UpcomingShiftEvent.getShift(
      dateType: 'This month',
      fromDate: _date.firstDate(_date.month).dmY(),
      toDate: _date
          .firstDate(_date.month + 3)
          .subtract(const Duration(days: 1))
          .dmY(),
    ));
    _feedbackBloc.add(FeedbackEvent.getFeedback(
        position: null, id: HiveUtils.get(HiveKeys.clinicianId)));
    super.initState();
    notificationsUtils.onMessageListener(context);
  }

  @override
  void dispose() {
    super.dispose();
    _feedbackBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => _myPerformanceBloc),
        BlocProvider(create: (context) => _upcomingShiftBloc),
        BlocProvider(create: (context) => _feedbackBloc),
      ],
      child: Column(
        children: [
          _appBar(),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                _myPerformanceBloc.add(MyPerformanceEvent.getChartData(
                  dateType: 'This month',
                  fromDate: _date.firstDate(_date.month).dmY(),
                  toDate: _date.lastDate().dmY(),
                ));
                _upcomingShiftBloc.add(UpcomingShiftEvent.getShift(
                  dateType: 'This month',
                  fromDate: _date.firstDate(_date.month).dmY(),
                  toDate: _date
                      .firstDate(_date.month + 3)
                      .subtract(const Duration(days: 1))
                      .dmY(),
                ));
                _feedbacks.clear();
                _feedbackBloc.add(FeedbackEvent.getFeedback(
                    position: null, id: HiveUtils.get(HiveKeys.clinicianId)));
                await Future.delayed(const Duration(seconds: 1));
              },
              child: ListView(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 16.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: Row(
                          children: [
                            const Text(
                              SaString.myPerformance,
                              style: SaTextStyles.heading,
                            ),
                            const Spacer(),
                            BlocBuilder<MyPerformanceBloc, MyPerformanceState>(
                              builder: (context, state) {
                                return SaDropDown(
                                  value: _dateType,
                                  valuesList: const [
                                    'This month',
                                    'This week',
                                    'This day'
                                  ],
                                  onChanged: (value) {
                                    if (value != _dateType) {
                                      if (value == 'This month') {
                                        _myPerformanceBloc.add(
                                            MyPerformanceEvent.getChartData(
                                          dateType: 'This month',
                                          fromDate: _date
                                              .firstDate(_date.month)
                                              .dmY(),
                                          toDate: _date.lastDate().dmY(),
                                        ));
                                      }
                                      if (value == 'This week') {
                                        _myPerformanceBloc.add(
                                            MyPerformanceEvent.getChartData(
                                          dateType: 'This week',
                                          fromDate: _date.weekFirstDay().dmY(),
                                          toDate: DateTime.now()
                                              .weekLastDay()
                                              .dmY(),
                                        ));
                                      }
                                      if (value == 'This day') {
                                        _myPerformanceBloc.add(
                                            MyPerformanceEvent.getChartData(
                                          dateType: 'This day',
                                          fromDate: DateTime.now().dmY(),
                                          toDate: DateTime.now().dmY(),
                                        ));
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      BlocConsumer<MyPerformanceBloc, MyPerformanceState>(
                        listener: (context, state) {
                          state.whenOrNull(
                            initial: () {},
                            success: (dateType, upcomingShift) {
                              _shift = List.from(upcomingShift.shift);
                              _shift = _shift!
                                  .where((e) =>
                                      e.status.isApproved() ||
                                      e.status.isCompleted() ||
                                      e.status.isDeclined())
                                  .toList();
                              _shift!.sort((a, b) => a.startTime
                                  .parseDate()
                                  .compareTo(b.startTime.parseDate()));
                              _dateType = dateType;
                              _workedShift = _shift!
                                  .where((e) =>
                                      e.startDate
                                          .parseDate()
                                          .isBefore(DateTime.now()) &&
                                      e.checkIn != '' &&
                                      e.checkOut != '')
                                  .toList()
                                  .length;
                              _lateShift = _shift!
                                  .where((e) =>
                                      e.startDate
                                          .parseDate()
                                          .isBefore(DateTime.now()) &&
                                      e.checkIn != '' &&
                                      e.checkOut != '' &&
                                      e.checkIn
                                              .parseDate()
                                              .difference(
                                                  e.startTime.parseDate())
                                              .inMinutes >
                                          5)
                                  .toList()
                                  .length;
                              _cancelShift = _shift!
                                  .where((e) =>
                                      e.startDate
                                          .parseDate()
                                          .isBefore(DateTime.now()) &&
                                      e.declinedBy.toLowerCase() == 'clinician')
                                  .toList()
                                  .length;
                            },
                          );
                        },
                        builder: (context, state) {
                          return _shift == null
                              ? Container(
                                  height: 264,
                                  alignment: Alignment.center,
                                  child: const Loader(),
                                )
                              : BlocBuilder<FeedbackBloc, FeedbackState>(
                                  builder: (context, state) {
                                    return state.when(
                                      initial: () => const SizedBox.shrink(),
                                      loading: () => const SizedBox.shrink(),
                                      success: (feedbacks, position) {
                                        _feedbacks.addAll(List.from(feedbacks));
                                        double rating = 0.0;
                                        for (final feed in _feedbacks) {
                                          rating += int.parse(
                                              feed.ratings.isEmpty
                                                  ? '0'
                                                  : feed.ratings);
                                        }
                                        rating = rating / _feedbacks.length;
                                        _feedbacks.removeWhere((element) =>
                                            element.feedback == '');

                                        return MyPerformanceChart(
                                          workedShift: _workedShift,
                                          lateShift: _lateShift,
                                          cancelShift: _cancelShift,
                                          totalShift: _shift?.length ?? 0,
                                          feedback: _feedbacks.length,
                                          rating: rating.toString() == 'NaN'
                                              ? 0.0
                                              : rating,
                                        );
                                      },
                                      error: (error) => const SizedBox.shrink(),
                                    );
                                  },
                                );
                        },
                      ),

                      // const SizedBox(height: 16.0),
                      // SizedBox(
                      //   height: 56.0,
                      //   child: ListView(
                      //     scrollDirection: Axis.horizontal,
                      //     children: [
                      //       const SizedBox(width: 24.0),
                      //       _cardListTile(
                      //         SaImages.fireWork,
                      //         SaColors.green,
                      //         'Milestones',
                      //         Row(
                      //           children: [
                      //             const Text(
                      //               'Anniversary',
                      //               style: SaTextStyles.caption,
                      //             ),
                      //             Text(
                      //               ' • ',
                      //               style: SaTextStyles.subtitleSmall.copyWith(
                      //                 color: SaColors.lightHintColor,
                      //               ),
                      //             ),
                      //             const Text(
                      //               'Oct 3, 2020',
                      //               style: SaTextStyles.caption,
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //       const SizedBox(width: 8.0),
                      //       _cardListTile(
                      //         SaImages.team,
                      //         SaColors.primary,
                      //         'Shout outs',
                      //         const Text(
                      //           'Team Player',
                      //           style: SaTextStyles.caption,
                      //         ),
                      //       ),
                      //       const SizedBox(width: 8.0),
                      //       _cardListTile(
                      //         SaImages.team,
                      //         SaColors.primary,
                      //         'Shout outs',
                      //         const Text(
                      //           'Team Player',
                      //           style: SaTextStyles.caption,
                      //         ),
                      //       ),
                      //       const SizedBox(width: 24.0),
                      //     ],
                      //   ),
                      // ),
                      const SizedBox(height: 24.0),
                      const Padding(
                        padding: EdgeInsets.only(left: 24.0),
                        child: Text(
                          SaString.upcomingShifts,
                          style: SaTextStyles.heading,
                        ),
                      ),
                      const SizedBox(height: 16.0),
                      BlocConsumer<UpcomingShiftBloc, UpcomingShiftState>(
                        listener: (context, state) {
                          state.whenOrNull(
                            success: (dateType, upcomingShift) {
                              _upcomingShift = List.from(upcomingShift.shift);
                              _upcomingShift!.removeWhere(
                                  (element) => element.status.isDeclined());
                              _upcomingShift = _upcomingShift!
                                  .where((e) => e.status.isApproved())
                                  .toList();
                              _upcomingShift!.sort((a, b) => a.startTime
                                  .parseDate()
                                  .compareTo(b.startTime.parseDate()));
                              _upcomingShift!.removeWhere((element) =>
                                  element.startTime
                                      .parseDate()
                                      .difference(DateTime.now())
                                      .inMinutes <=
                                  5);
                            },
                          );
                        },
                        builder: (context, state) {
                          return state.when(
                            initial: () => const SizedBox.shrink(),
                            loading: () => const SizedBox(
                              height: 104,
                              child: Loader(),
                            ),
                            success: (dateType, upcomingShift) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 24.0),
                                child: _upcomingShift != null ||
                                        (_upcomingShift?.isNotEmpty ?? false)
                                    ? ListView.builder(
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        itemCount: _upcomingShift?.length ?? 0,
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 16),
                                            child: ShiftCard(
                                              result: _upcomingShift![index],
                                            ),
                                          );
                                        },
                                      )
                                    : const Noshift(),
                              );
                            },
                            error: (error) => const SizedBox.shrink(),
                          );
                        },
                      ),
                      const SizedBox(height: 16.0),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      leadingWidth: 72.0,
      leading: GestureDetector(
        onTap: () => NavigatorUtils.push(context, const EditProfile()),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(width: 24.0),
            ClipRRect(
              borderRadius: BorderRadius.circular(72.0),
              child: ValueListenableBuilder<Box>(
                valueListenable: Hive.box(HiveKeys.boxName).listenable(),
                builder: (context, box, widget) {
                  return CachedNetworkImage(
                    placeholder: (context, url) =>
                        const ColoredBox(color: SaColors.lightPink),
                    imageUrl: HiveUtils.get(HiveKeys.avatarImage),
                    height: 48.0,
                    width: 48.0,
                    fit: BoxFit.cover,
                    errorWidget: (context, url, error) => const Icon(
                        Icons.error_outline_rounded,
                        color: SaColors.red),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      titleSpacing: 12.0,
      title: GestureDetector(
        onTap: () => NavigatorUtils.push(context, const EditProfile()),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              SaString.hello,
              style: SaTextStyles.subtitleLarge
                  .copyWith(color: SaColors.hintColor),
            ),
            const SizedBox(height: 4.0),
            ValueListenableBuilder<Box>(
              valueListenable: Hive.box(HiveKeys.boxName).listenable(),
              builder: (context, box, widget) {
                return Text(
                  '${HiveUtils.get(HiveKeys.fullName)}',
                  style: SaTextStyles.title,
                );
              },
            ),
          ],
        ),
      ),
      actions: [
        const SizedBox(width: 16.0),
        Padding(
          padding: const EdgeInsets.only(top: 2.0),
          child: InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () => NavigatorUtils.push(context, const Notifications()),
            child: const Icon(
              CupertinoIcons.bell_fill,
              color: SaColors.black,
              size: 22.0,
            ),
          ),
        ),
        // BouncingWidget(
        //   onPressed: () => NavigatorUtils.push(context, const Notifications()),
        //   icon: SaIcons.notification,
        // ),
        const SizedBox(width: 14.0),
        InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () => NavigatorUtils.push(context, const ChatList()),
          child: SvgPicture.asset(
            SaIcons.chat,
            height: 20.0,
            width: 20.0,
          ),
        ),
        // BouncingWidget(
        //   onPressed: () => NavigatorUtils.push(context, const ChatList()),
        //   icon: SaIcons.chat,
        //   height: 20.0,
        // ),
        const SizedBox(width: 16.0),
        InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () => drawerKey.currentState!.openDrawer(),
          child: SvgPicture.asset(
            SaIcons.drawer,
            height: 16.0,
            width: 16.0,
          ),
        ),
        // BouncingWidget(
        //   onPressed: () => drawerKey.currentState!.openDrawer(),
        //   icon: SaIcons.drawer,
        //   height: 16.0,
        // ),
        const SizedBox(width: 24.0),
      ],
      centerTitle: false,
      toolbarHeight: 60.0,
    );
  }

  // Widget _cardListTile(
  //   String image,
  //   Color imageBackground,
  //   String title,
  //   Widget subtitleLarge,
  // ) {
  //   return Container(
  //     padding: const EdgeInsets.all(8.0),
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(16.0),
  //       border: Border.all(color: SaColors.primary, width: 1.0),
  //     ),
  //     child: Row(
  //       children: [
  //         CircleAvatar(
  //           backgroundColor: imageBackground,
  //           child: Image.asset(image),
  //         ),
  //         const SizedBox(width: 8.0),
  //         Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Text(
  //               title,
  //               style: SaTextStyles.subtitleSmall,
  //             ),
  //             const Spacer(),
  //             subtitleLarge,
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }
}
