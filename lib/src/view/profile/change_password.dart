import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/change_password/bloc/change_password_bloc.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({super.key});

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final ChangePasswordBloc _changePasswordBloc = ChangePasswordBloc();
  final TextEditingController _currentPassword = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();

  @override
  void dispose() {
    _currentPassword.dispose();
    _newPassword.dispose();
    _confirmPassword.dispose();
    _changePasswordBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _changePasswordBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: appBar(context, title: SaString.changePassword),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                children: [
                  _textField(
                    SaString.currentPassword,
                    _currentPassword,
                    SaString.passwordHint,
                  ),
                  const SizedBox(height: 16.0),
                  _textField(
                    SaString.newPassword,
                    _newPassword,
                    SaString.passwordHint,
                  ),
                  const SizedBox(height: 16.0),
                  _textField(
                    SaString.confirmNewpassword,
                    _confirmPassword,
                    SaString.passwordHint,
                  ),
                  const Spacer(),
                  BlocConsumer<ChangePasswordBloc, ChangePasswordState>(
                    listener: (context, state) {
                      state.when(
                        initial: () {},
                        loading: () => Loader.show(context),
                        success: (message) {
                          Loader.hide(context);
                          Toast.success(context, message);
                        },
                        error: (error) {
                          Loader.hide(context);
                          Toast.error(context, error);
                        },
                      );
                    },
                    builder: (context, state) {
                      return SaButton(
                        onPressed: () {
                          _changePasswordBloc
                              .add(ChangePasswordEvent.changePassword(
                            oldPassword: _currentPassword.text.trim(),
                            newPassword: _newPassword.text.trim(),
                            confirmPassword: _confirmPassword.text.trim(),
                          ));
                        },
                        text: SaString.changePassword,
                        height: 48.0,
                        radius: 12.0,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _textField(
    String label,
    TextEditingController? controller,
    String? hintText,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(label, style: SaTextStyles.subtitleSmall),
        const SizedBox(height: 8.0),
        SaTextField(
          controller: controller,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 12.0,
          ),
          hintText: hintText,
          obscureText: true,
        ),
      ],
    );
  }
}
