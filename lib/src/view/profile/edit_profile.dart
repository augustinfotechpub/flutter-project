// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/admin/edit_profile/model.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/provider/edit_profile/bloc/edit_profile_bloc.dart';
import 'package:shift_alerts/src/provider/edit_profile/model.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/mask_format.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/view/profile/change_password.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/dropdown.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class EditProfile extends StatefulWidget {
  final bool isAdmin;
  const EditProfile({super.key, this.isAdmin = false});

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final EditProfileBloc _editProfileBloc = EditProfileBloc();
  final TextEditingController _firstName = TextEditingController();
  final TextEditingController _lastName = TextEditingController();
  final TextEditingController _dateOfBirth = TextEditingController();
  final TextEditingController _contactNumber = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _employeeId = TextEditingController();

  final ImagePicker _imagePicker = ImagePicker();

  final _jobSitesValue = ValueNotifier<String>('');
  final _image = ValueNotifier<XFile?>(null);

  bool enable = false;
  dynamic _profile;
  bool isFirst = true;
  List<String> _jobSite = [];

  @override
  void initState() {
    super.initState();
    _editProfileBloc.add(
      EditProfileEvent.editProfile(false, isAdmin: widget.isAdmin),
    );
  }

  void _setTextFieldValue(Profile data) {
    _firstName.text = data.firstname;
    _lastName.text = data.lastname;
    _dateOfBirth.text = data.dateOfBirth.parseDate().mdY().replaceAll('-', '/');
    _contactNumber.text = data.contactNo;
    _username.text = data.username;
    _email.text = data.email;
    _password.text = data.password;
    _employeeId.text = data.role;
    _jobSitesValue.value = data.jobSites.first;
    _jobSite = List.from(data.jobSites);
  }

  void _setFieldValue(Admin data) {
    _firstName.text = data.firstname;
    _lastName.text = data.lastname;
    _dateOfBirth.text = data.dateOfBirth.parseDate().mdY().replaceAll('-', '/');
    _contactNumber.text = data.contactNo;
    _username.text = data.username;
    _email.text = data.email;
    _password.text = data.password;
    _employeeId.text = data.role;
  }

  @override
  void dispose() {
    _firstName.dispose();
    _lastName.dispose();
    _dateOfBirth.dispose();
    _contactNumber.dispose();
    _username.dispose();
    _email.dispose();
    _password.dispose();
    _employeeId.dispose();
    _jobSitesValue.dispose();
    _editProfileBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _editProfileBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          appBar: appBar(context, title: 'Profile'),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: BlocConsumer<EditProfileBloc, EditProfileState>(
                listener: (context, state) {
                  state.when(
                    initial: () {},
                    loading: () => Loader.show(
                      context,
                      backgroundColor: SaColors.transparent,
                    ),
                    success: (value, response, admin) {
                      enable = value;
                      if (widget.isAdmin) {
                        _profile = admin!;
                        _setFieldValue(admin);
                      } else {
                        _profile = response!;
                        _setTextFieldValue(response);
                      }
                      Loader.hide(context);
                      if (!isFirst) {
                        Toast.success(context, 'Profile updated successfully');
                      }
                    },
                    error: (error) {
                      Loader.hide(context);
                      Toast.error(context, 'something wen\'t wrong');
                    },
                  );
                },
                builder: (context, state) {
                  return _profile == null
                      ? const SizedBox.shrink()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 24.0),
                            Row(
                              children: [
                                ValueListenableBuilder(
                                  valueListenable: _image,
                                  builder: (context, value, _) => ClipRRect(
                                    borderRadius: BorderRadius.circular(72.0),
                                    child: (value != null &&
                                            value.path.isNotEmpty)
                                        ? Image.file(
                                            File(value.path),
                                            height: 72.0,
                                            width: 72.0,
                                            fit: BoxFit.cover,
                                          )
                                        : Image.network(
                                            _profile!.avatarImage,
                                            height: 72.0,
                                            width: 72.0,
                                            fit: BoxFit.cover,
                                            errorBuilder:
                                                (context, error, stackTrace) =>
                                                    const Icon(Icons
                                                        .error_outline_rounded),
                                          ),
                                    // CachedNetworkImage(
                                    //     imageUrl: _profile!.avatarImage,
                                    //     height: 72.0,
                                    //     width: 72.0,
                                    //     fit: BoxFit.cover,
                                    //     errorWidget:
                                    //         (context, url, error) =>
                                    //             const Icon(Icons
                                    //                 .error_outline_rounded),
                                    //   ),
                                  ),
                                ),
                                const SizedBox(width: 20.0),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${_profile!.firstname.toString().capitalize()} ${_profile!.lastname.toString().capitalize()}',
                                      style: SaTextStyles.title,
                                    ),
                                    const SizedBox(height: 10.0),
                                    enable
                                        ? SaOutlineButton(
                                            onPressed: () {
                                              showModalBottomSheet(
                                                backgroundColor:
                                                    SaColors.transparent,
                                                context: context,
                                                builder: (context) =>
                                                    _changeAvatarSheet(),
                                              );
                                            },
                                            width: 144.0,
                                            text: const Text(
                                              SaString.changeAvatar,
                                              style: TextStyle(
                                                color: SaColors.primary,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          )
                                        : const SizedBox.shrink(),
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              children: [
                                Expanded(
                                  child: _textField(
                                    SaString.firstName,
                                    _firstName,
                                    'Alisa',
                                    keyboardType: TextInputType.name,
                                  ),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: _textField(
                                    SaString.lastName,
                                    _lastName,
                                    'Jones',
                                    keyboardType: TextInputType.name,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 12.0),
                            Row(
                              children: [
                                Expanded(
                                    child: _textField(
                                  SaString.dateOfBirth,
                                  _dateOfBirth,
                                  'mm/dd/yyyy',
                                  format: MaskTextInputFormatter(
                                    mask: '##/##/####',
                                    filter: {'#': RegExp(r'[0-9]')},
                                    type: MaskAutoCompletionType.eager,
                                  ),
                                  readOnly: true,
                                  icon: Container(
                                    padding: const EdgeInsets.only(right: 16.0),
                                    child: InkWell(
                                      onTap: () async {
                                        final date = await showDatePicker(
                                          context: context,
                                          initialDate: _profile.dateOfBirth
                                              .toString()
                                              .parseDate(),
                                          firstDate: DateTime(1900),
                                          lastDate: DateTime.now(),
                                        );
                                        if (date != null) {
                                          _dateOfBirth.text =
                                              date.mdY().replaceAll('-', '/');
                                        }
                                      },
                                      child: SvgPicture.asset(
                                        SaIcons.schedule,
                                        color: SaColors.black,
                                      ),
                                    ),
                                  ),
                                )),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: _textField(
                                    SaString.contactNumber,
                                    _contactNumber,
                                    '123456789',
                                    keyboardType: TextInputType.number,
                                    maxLength: 10,
                                  ),
                                ),
                              ],
                            ),
                            const Divider(color: SaColors.white, height: 20.0),
                            _textField(
                                SaString.username, _username, 'alisa123'),
                            const SizedBox(height: 16.0),
                            _textField(
                              SaString.email,
                              _email,
                              'alisa123@ondesk.com',
                              keyboardType: TextInputType.emailAddress,
                            ),
                            const SizedBox(height: 16.0),
                            _textField(
                              SaString.password,
                              _password,
                              SaString.passwordHint,
                              obscureText: true,
                            ),
                            const Divider(color: SaColors.white, height: 20.0),
                            if (!widget.isAdmin)
                              const Text(
                                SaString.jobSites,
                                style: SaTextStyles.subtitleSmall,
                              ),
                            if (!widget.isAdmin) const SizedBox(height: 8.0),
                            if (!widget.isAdmin)
                              ValueListenableBuilder(
                                valueListenable: _jobSitesValue,
                                builder: (context, value, child) {
                                  return SaDropDown(
                                    isExpanded: true,
                                    buttonHeight: 45,
                                    borderColor: SaColors.white,
                                    iconColor: SaColors.black,
                                    buttonColor: SaColors.white,
                                    hintText: Text(
                                      'Bristol Village',
                                      style:
                                          SaTextStyles.subtitleLarge.copyWith(
                                        color: SaColors.hintColor,
                                      ),
                                    ),
                                    value: value,
                                    valuesList: _profile!.jobSites,
                                    onChanged: enable
                                        ? (p0) {
                                            if (p0 != null) {
                                              _jobSitesValue.value = p0;
                                              _jobSite.remove(p0);
                                              _jobSite.insert(0, p0);
                                            }
                                          }
                                        : null,
                                  );
                                },
                              ),
                            const SizedBox(height: 16.0),
                            _textField(
                              SaString.employeeId,
                              _employeeId,
                              'Nurse',
                              keyboardType: TextInputType.text,
                            ),
                            const SizedBox(height: 32.0),
                            SaButton(
                              onPressed: () async {
                                if (enable) {
                                  isFirst = false;
                                  String avtarImage = _profile!.avatarImage;
                                  List<String> date =
                                      _dateOfBirth.text.trim().split('/');

                                  if (_image.value != null &&
                                      _image.value!.path.isNotEmpty) {
                                    Loader.show(context);
                                    File file = File(_image.value!.path);
                                    final base64String =
                                        base64.encode(file.readAsBytesSync());

                                    await apiProvider.uploadImage(
                                      doc: {
                                        "user_id":
                                            "${HiveUtils.get(HiveKeys.user)}",
                                        "data": base64String
                                      },
                                    ).then((response) {
                                      Loader.hide(context);
                                      if (response.statusCode == 200) {
                                        avtarImage = jsonDecode(
                                            response.body)["location"];
                                        _image.value = null;
                                      }
                                    });
                                  }

                                  if (widget.isAdmin) {
                                    if (adminValidate()) {
                                      _editProfileBloc
                                          .add(EditProfileEvent.saveChanges(
                                        Admin(
                                          adminId: int.parse(
                                              HiveUtils.get(HiveKeys.adminId)),
                                          fullname: _profile.fullname,
                                          username: _username.text.trim(),
                                          password: _password.text.trim(),
                                          email: _email.text.trim(),
                                          firstname: _firstName.text.trim(),
                                          lastname: _lastName.text.trim(),
                                          avatarImage: avtarImage,
                                          contactNo: _contactNumber.text.trim(),
                                          address: _profile.address,
                                          dateOfBirth:
                                              '${date.last}-${date.first}-${date[1]}',
                                          allowScheduling:
                                              _profile.allowScheduling,
                                          isActive: _profile!.isActive,
                                          facility: _profile.facility,
                                          role: _employeeId.text.trim(),
                                          user: _profile!.user,
                                        ),
                                        isAdmin: widget.isAdmin,
                                      ));
                                    } else {
                                      Toast.error(
                                          context, 'Please fill all field');
                                    }
                                  } else {
                                    if (clinicianValidate()) {
                                      _editProfileBloc
                                          .add(EditProfileEvent.saveChanges(
                                        Profile(
                                          clinicianId: int.parse(HiveUtils.get(
                                              HiveKeys.clinicianId)),
                                          username: _username.text.trim(),
                                          password: _password.text.trim(),
                                          email: _email.text.trim(),
                                          firstname: _firstName.text.trim(),
                                          lastname: _lastName.text.trim(),
                                          avatarImage: avtarImage,
                                          contactNo: _contactNumber.text.trim(),
                                          dateOfBirth:
                                              '${date.last}-${date.first}-${date[1]}',
                                          jobSites: _jobSite,
                                          clinicianPosition:
                                              _profile!.clinicianPosition,
                                          isActive: _profile!.isActive,
                                          applyShift: _profile!.applyShift,
                                          role: _employeeId.text.trim(),
                                          user: _profile!.user,
                                        ),
                                        isAdmin: widget.isAdmin,
                                      ));
                                    } else {
                                      Toast.error(
                                          context, 'Please fill all field');
                                    }
                                  }
                                } else {
                                  isFirst = true;
                                  _editProfileBloc.add(
                                    EditProfileEvent.editProfile(
                                      true,
                                      isAdmin: widget.isAdmin,
                                    ),
                                  );
                                }
                              },
                              text: enable
                                  ? SaString.saveChange
                                  : SaString.editProfile,
                              height: 48.0,
                              radius: 12.0,
                            ),
                            const SizedBox(height: 12.0),
                            SaOutlineButton(
                              onPressed: () => NavigatorUtils.push(
                                  context, const ChangePassword()),
                              text: const Text(SaString.changePassword),
                              height: 48.0,
                            ),
                            const SizedBox(height: 32.0),
                          ],
                        );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool clinicianValidate() {
    return _firstName.text.trim() != '' &&
        _lastName.text.trim() != '' &&
        _contactNumber.text.trim() != '' &&
        _username.text.trim() != '' &&
        _email.text.trim() != '' &&
        _password.text.trim() != '' &&
        _jobSite.isNotEmpty &&
        _employeeId.text.trim() != '';
  }

  bool adminValidate() {
    return _firstName.text.trim() != '' &&
        _lastName.text.trim() != '' &&
        _contactNumber.text.trim() != '' &&
        _username.text.trim() != '' &&
        _email.text.trim() != '' &&
        _password.text.trim() != '' &&
        _employeeId.text.trim() != '';
  }

  Widget _changeAvatarSheet() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      decoration: const BoxDecoration(
        color: SaColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () async {
                  _image.value =
                      await _imagePicker.pickImage(source: ImageSource.camera);
                  NavigatorUtils.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: const [
                      Icon(CupertinoIcons.camera),
                      SizedBox(width: 16.0),
                      Text('Camera'),
                    ],
                  ),
                ),
              ),
            ),
            Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () async {
                  _image.value =
                      await _imagePicker.pickImage(source: ImageSource.gallery);
                  NavigatorUtils.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: const [
                      Icon(CupertinoIcons.photo),
                      SizedBox(width: 16.0),
                      Text('Gallery'),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textField(
    String label,
    TextEditingController? controller,
    String? hintText, {
    Widget? icon,
    bool obscureText = false,
    TextInputType? keyboardType,
    TextInputFormatter? format,
    int? maxLength,
    bool readOnly = false,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          label,
          style: SaTextStyles.subtitleSmall,
        ),
        const SizedBox(height: 8.0),
        SaTextField(
          controller: controller,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 12.0,
          ),
          hintText: hintText,
          suffixIcon: icon,
          obscureText: obscureText,
          keyboardType: keyboardType,
          inputFormatters: format == null ? [] : [format],
          enabled: enable,
          maxLength: maxLength,
          readOnly: readOnly,
        ),
      ],
    );
  }
}
