// ignore_for_file: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/timesheet/bloc/timesheet_bloc.dart';
import 'package:shift_alerts/src/provider/timesheet/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/widget/calendar.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/read_more.dart';
import 'package:table_calendar/table_calendar.dart';

class TimeSheets extends StatefulWidget {
  const TimeSheets({super.key});

  @override
  State<TimeSheets> createState() => _TimeSheetsState();
}

class _TimeSheetsState extends State<TimeSheets> {
  final TimesheetBloc _timesheetBloc = TimesheetBloc();
  DateTime _date = DateTime.now();
  PageController? _myPageController;
  final _heightNotifier = ValueNotifier<List<double>>([]);

  TextStyle getTextStyle() => SaTextStyles.subtitleSmall
      .copyWith(fontWeight: FontWeight.w500, color: SaColors.gray);

  Color textColor({required String status}) {
    if (status.isDeclined()) {
      return SaColors.red;
    } else if (status.isCompleted() || status.isApproved()) {
      return SaColors.green;
    } else {
      return SaColors.orange;
    }
  }

  Color boxColor({required String status}) {
    if (status.isDeclined()) {
      return SaColors.lightPink;
    } else if (status.isCompleted() || status.isApproved()) {
      return SaColors.lightGreen;
    } else {
      return SaColors.lightOrange;
    }
  }

  @override
  void initState() {
    _timesheetBloc.add(TimesheetEvent.getTimesheet(
      fromDate: DateTime.now().firstDate(DateTime.now().month),
      toDate: DateTime.now().lastDate(),
      focusDay: DateTime.now(),
    ));
    super.initState();
  }

  @override
  void dispose() {
    _timesheetBloc.close();
    _heightNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 24.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Text(
            SaString.myTimeSheets,
            style: SaTextStyles.heading.copyWith(fontSize: 24),
          ),
        ),
        Expanded(
          child: RefreshIndicator(
            onRefresh: () async {
              _timesheetBloc.add(TimesheetEvent.getTimesheet(
                fromDate: DateTime.now().firstDate(DateTime.now().month),
                toDate: DateTime.now().lastDate(),
                focusDay: DateTime.now(),
              ));
              await Future.delayed(const Duration(seconds: 1));
            },
            child: ListView(
              // shrinkWrap: true,
              children: [
                BlocProvider(
                  create: (context) => _timesheetBloc,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        bottom: 24.0, left: 24.0, right: 24.0, top: 14),
                    child: BlocBuilder<TimesheetBloc, TimesheetState>(
                      builder: (context, state) {
                        return state.when(
                          initial: () => const SizedBox.shrink(),
                          loading: () {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _calendar(),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.6,
                                  child: const Center(
                                    child: Loader(),
                                  ),
                                ),
                              ],
                            );
                          },
                          success: (timesheet, focusday) {
                            _date = focusday;
                            _heightNotifier.value = [];
                            _heightNotifier.value =
                                List.generate(timesheet.length, (index) => 0.0);
                            return Column(
                              children: [
                                _calendar(),
                                const SizedBox(height: 25),
                                if (timesheet.isEmpty)
                                  const Noshift(message: "No Timesheet Found")
                                else
                                  timesheet.any((e) =>
                                          e.shiftDate.parseDate().dmY() ==
                                          _date.dmY())
                                      ? ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: timesheet.length,
                                          shrinkWrap: true,
                                          itemBuilder: (context, index) {
                                            final renderToday = timesheet[index]
                                                    .shiftDate
                                                    .parseDate()
                                                    .dmY() ==
                                                _date.dmY();

                                            return renderToday
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      bottom: 20.0,
                                                    ),
                                                    child: _eventCard(
                                                        ctx: context,
                                                        index: index,
                                                        details:
                                                            timesheet[index],
                                                        cardWidgetKey:
                                                            GlobalKey()),
                                                  )
                                                : const SizedBox.shrink();
                                          },
                                        )
                                      : SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.5,
                                          child: const Center(
                                            child: Noshift(
                                                message: "No Timesheet Found"),
                                          ),
                                        ),
                              ],
                            );
                          },
                          error: (error) =>
                              Text(error, style: SaTextStyles.title),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _calendar() {
    return SaCalendar(
      shifts: const [],
      focusDate: _date,
      onPrevious: () {
        _myPageController!.previousPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      },
      onNext: () {
        _myPageController!.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      },
      calendarFormat: CalendarFormat.week,
      onCalendarCreated: (p0) => _myPageController = p0,
      onDaySelected: (selectedDay, newfocusedDay) {
        if (!_date.isSameDay(selectedDay)) {
          if (selectedDay.month == _date.month) {
            _timesheetBloc.add(
              TimesheetEvent.refreshTimesheet(
                focusday: selectedDay,
              ),
            );
          } else {
            _timesheetBloc.add(
              TimesheetEvent.getTimesheet(
                fromDate: selectedDay.firstDate(selectedDay.month),
                toDate: selectedDay.lastDate(),
                focusDay: selectedDay,
              ),
            );
          }
        }
      },
    );
  }

  Widget _eventCard({
    required BuildContext ctx,
    required TimesheetDetail details,
    required GlobalKey cardWidgetKey,
    required int index,
  }) {
    bool isApproved = details.status.isApproved();
    bool pressMoreButton = true;

    double originalHeight = 0.0;

    void postFrameCallback(_) {
      final context = cardWidgetKey.currentContext;

      if (context != null) {
        if (context.size != null) {
          _heightNotifier.value[index] = context.size!.height - 40;
          if (pressMoreButton) {
            originalHeight = context.size!.height - 40;
          }

          _heightNotifier.notifyListeners();
        }
      }
    }

    WidgetsBinding.instance.addPostFrameCallback(postFrameCallback);

    return Row(
      key: cardWidgetKey,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ValueListenableBuilder(
          valueListenable: _heightNotifier,
          builder: (context, value, child) {
            return Container(
              width: 4,
              height: (_heightNotifier.value[index]),
              color: Color(
                  int.parse("0xFF${details.shiftColor.replaceFirst('#', '')}")),
            );
          },
        ),
        const SizedBox(width: 16),
        Card(
          color: SaColors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: SizedBox(
            width: MediaQuery.of(ctx).size.width - (80),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 12.0,
                horizontal: 16.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        details.clinicianPosition,
                        style: SaTextStyles.title,
                      ),
                      const SizedBox(width: 8),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: boxColor(status: details.status),
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16.0,
                            vertical: 4.0,
                          ),
                          child: Text(
                            details.status,
                            style: SaTextStyles.subtitleSmall.copyWith(
                              color: textColor(status: details.status),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 12),
                  Row(
                    children: [
                      _cardWidget(SaIcons.time),
                      Text(
                        SaString.clockIn,
                        style: SaTextStyles.subtitleSmall
                            .copyWith(color: SaColors.hintColor),
                      ),
                      const SizedBox(width: 4),
                      Text(details.checkInTime.parseDate().hm(),
                          style: getTextStyle()),
                      Text(
                        '  ●  ',
                        style: SaTextStyles.subtitleSmall
                            .copyWith(color: SaColors.hintColor),
                      ),
                      Text(
                        SaString.clockOut,
                        style: SaTextStyles.subtitleSmall
                            .copyWith(color: SaColors.hintColor),
                      ),
                      const SizedBox(width: 4),
                      Text(
                        details.checkOutTime.parseDate().hm(),
                        style: getTextStyle().copyWith(
                          color:
                              isApproved ? SaColors.hintColor : SaColors.black,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      children: [
                        _cardWidget(SaIcons.facility),
                        Text(
                          SaString.facility,
                          style: SaTextStyles.subtitleSmall
                              .copyWith(color: SaColors.hintColor),
                        ),
                        const SizedBox(width: 4),
                        Text(
                          details.facilityName,
                          style: getTextStyle(),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      children: [
                        _cardWidget(SaIcons.person),
                        Text(
                          SaString.supervisor,
                          style: SaTextStyles.subtitleSmall
                              .copyWith(color: SaColors.hintColor),
                        ),
                        const SizedBox(width: 4),
                        Text(details.pocAdmin, style: getTextStyle())
                      ],
                    ),
                  ),
                  if (isApproved)
                    Row(
                      children: [
                        _cardWidget(SaIcons.star),
                        Text(
                          SaString.ratingsTitle,
                          style: SaTextStyles.subtitleSmall
                              .copyWith(color: SaColors.hintColor),
                        ),
                        const SizedBox(width: 4),
                        Text('${details.ratings} stars', style: getTextStyle())
                      ],
                    ),
                  if (isApproved)
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _cardWidget(SaIcons.chatBubble),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ExpandableText(
                                  text: details.feedback,
                                  onMoreorLessTap: () {
                                    if (pressMoreButton) {
                                      pressMoreButton = false;
                                      postFrameCallback('');
                                    } else {
                                      pressMoreButton = true;
                                      _heightNotifier.value[index] =
                                          originalHeight;
                                      _heightNotifier.notifyListeners();
                                    }
                                  },
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _cardWidget(String image) => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: SvgPicture.asset(
          image,
          color: SaColors.hintColor,
          height: 14,
          width: 14,
        ),
      );
}
