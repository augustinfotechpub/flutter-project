// ignore_for_file: empty_catches, use_build_context_synchronously

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/login/bloc/login_bloc.dart';
import 'package:shift_alerts/src/provider/remember_me/bloc/remember_me_bloc.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/utils/third_party_login_utils.dart';
import 'package:shift_alerts/src/view/admin/admin_portal.dart';
import 'package:shift_alerts/src/view/bottombar.dart';
import 'package:shift_alerts/src/view/forgot_password/forgot_password.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shift_alerts/src/widget/toast.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final LoginBloc _loginBloc = LoginBloc();
  final RememberMeBloc _rememberMeBloc = RememberMeBloc();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void newFocus() {
    return FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void initState() {
    super.initState();
    _rememberMeBloc.add(
      RememberMeEvent.rememberMe(HiveUtils.isContainKey(HiveKeys.rememberMe)
          ? HiveUtils.get(HiveKeys.rememberMe)
          : false),
    );

    if (HiveUtils.isContainKey(HiveKeys.rememberMe)) {
      if (HiveUtils.get(HiveKeys.rememberMe)) {
        _emailController.text = HiveUtils.get(HiveKeys.email);
        _passwordController.text = HiveUtils.get(HiveKeys.password);
      }
    }
  }

  @override
  void dispose() {
    _loginBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => _loginBloc),
        BlocProvider(create: (context) => _rememberMeBloc),
      ],
      child: GestureDetector(
        onTap: () => newFocus(),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: SaColors.primary,
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0.r),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Image.asset(SaImages.logo, width: 200.0.w),
                  Card(
                    color: SaColors.scaffold,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0.r),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(20.0.r),
                      child: BlocConsumer<LoginBloc, LoginState>(
                        listener: (context, state) {
                          state.when(
                            initial: () {},
                            loading: () => Loader.show(context),
                            success: (response) {
                              Loader.hide(context);
                              if (!response) {
                                HiveUtils.set(HiveKeys.userType, 'clinitian');
                                userType = UserType.clinician;
                                NavigatorUtils.pushReplacement(
                                    context, const BottomBar());
                              } else {
                                HiveUtils.set(HiveKeys.userType, 'admin');
                                userType = UserType.admin;
                                NavigatorUtils.pushReplacement(
                                    context, const AdminPortal());
                              }
                            },
                            error: (error) {
                              signOut();
                              Loader.hide(context);
                              Toast.error(context, error);
                            },
                          );
                        },
                        builder: (context, state) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                SaString.logIn,
                                style: SaTextStyles.heading
                                    .copyWith(fontSize: 28.0.sp),
                              ),
                              SizedBox(height: 32.0.h),
                              SaTextField(
                                controller: _emailController,
                                hintText:
                                    '${SaString.username}/${SaString.emailAddress}',
                                contentPadding: EdgeInsets.symmetric(
                                  horizontal: 18.0.w,
                                  vertical: 10.0.h,
                                ),
                                keyboardType: TextInputType.emailAddress,
                              ),
                              SizedBox(height: 16.0.h),
                              SaTextField(
                                controller: _passwordController,
                                hintText: SaString.yourPassword,
                                contentPadding: EdgeInsets.symmetric(
                                  horizontal: 18.0.w,
                                  vertical: 10.0.h,
                                ),
                                obscureText: true,
                              ),
                              SizedBox(height: 16.0.h),
                              Row(
                                children: [
                                  SizedBox(
                                    height: 20.0.h,
                                    width: 20.0.w,
                                    child: BlocBuilder<RememberMeBloc,
                                        RememberMeState>(
                                      builder: (context, state) {
                                        return state.when(
                                          initial: (value) => Checkbox(
                                            value: value,
                                            onChanged: (value) {
                                              _rememberMeBloc.add(
                                                  RememberMeEvent.rememberMe(
                                                      value!));
                                            },
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(4.0.r),
                                            ),
                                            side: BorderSide(
                                              color: SaColors.hintColor,
                                              width: 2.0.w,
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 8.0.w),
                                  const Text(
                                    SaString.rememberMe,
                                    style: TextStyle(
                                      fontSize: 12.0,
                                      color: SaColors.black,
                                    ),
                                  ),
                                  const Spacer(),
                                  InkWell(
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const ForgotPassword())),
                                    child: const Text(
                                      SaString.forgotPassword,
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: SaColors.primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 40.0.h),
                              SaButton(
                                onPressed: () {
                                  newFocus();
                                  if (_emailController.text.trim() != '' &&
                                      _passwordController.text.trim() != '') {
                                    _loginBloc.add(LoginEvent.login(
                                      email: _emailController.text.trim(),
                                      password: _passwordController.text.trim(),
                                    ));
                                  } else {
                                    Toast.error(
                                        context, 'Please fill all fields');
                                  }
                                },
                                text: SaString.logIn,
                                height: 44.0.h,
                              ),
                              // BlocConsumer<LoginBloc, LoginState>(
                              //   listener: (context, state) {
                              //     state.when(
                              //       initial: () {},
                              //       loading: () => Loader.show(context),
                              //       success: (response) {
                              //         Loader.hide(context);
                              //         if (!response) {
                              //           HiveUtils.set(
                              //               HiveKeys.userType, 'clinitian');
                              //           userType = UserType.clinician;
                              //           NavigatorUtils.pushReplacement(
                              //               context, const BottomBar());
                              //         } else {
                              //           HiveUtils.set(HiveKeys.userType, 'admin');
                              //           userType = UserType.admin;
                              //           NavigatorUtils.pushReplacement(
                              //               context, const AdminPortal());
                              //         }
                              //       },
                              //       error: (error) {
                              //         Loader.hide(context);
                              //         Toast.error(context, error);
                              //       },
                              //     );
                              //   },
                              //   builder: (context, state) {
                              //     return SaButton(
                              //       onPressed: () {
                              //         newFocus();
                              //         if (_emailController.text.trim() != '' &&
                              //             _passwordController.text.trim() != '') {
                              //           _loginBloc.add(LoginEvent.login(
                              //             email: _emailController.text.trim(),
                              //             password: _passwordController.text.trim(),
                              //           ));
                              //         } else {
                              //           Toast.error(
                              //               context, 'Please fill all fields');
                              //         }
                              //       },
                              //       text: SaString.logIn,
                              //       height: 44.0.h,
                              //     );
                              //   },
                              // ),
                              SizedBox(height: 28.0.h),
                              Text(
                                SaString.continueWith,
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: SaColors.hintColor,
                                ),
                              ),
                              SizedBox(height: 16.0.h),
                              _socialButton(
                                () => signInWithGoogle(),
                                SaString.continueWithGoogle,
                                SvgPicture.asset(
                                  SaIcons.google,
                                  height: 20.0.h,
                                  width: 20.0.w,
                                ),
                              ),
                              SizedBox(height: 12.0.h),
                              _socialButton(
                                () => signInWithFaceBook(),
                                SaString.continueWithFacebook,
                                SvgPicture.asset(
                                  SaIcons.facebook,
                                  height: 20.0.h,
                                  width: 20.0.w,
                                ),
                              ),
                              SizedBox(height: Platform.isIOS ? 12.0.h : 0.0),
                              Platform.isIOS
                                  ? _socialButton(
                                      () => signInWithApple(),
                                      SaString.continueWithApple,
                                      const Icon(
                                        Icons.apple_rounded,
                                        color: SaColors.black,
                                      ),
                                    )
                                  : const SizedBox.shrink(),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                  // SizedBox(height: 24.0.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future signInWithGoogle() async {
    try {
      await loginUtils.googleSignIn.signIn().then((value) {
        _loginBloc.add(LoginEvent.socialLogin(email: value!.email));
      });
    } on PlatformException catch (error) {
      Toast.error(context, error.message ?? 'Something went wrong');
    }
  }

  Future signInWithFaceBook() async {
    try {
      await loginUtils.facebookLogin
          .logIn(permissions: loginUtils.permissions)
          .then((value) async {
        if (value.status == FacebookLoginStatus.success) {
          String? email = await loginUtils.facebookLogin.getUserEmail();
          _loginBloc.add(LoginEvent.socialLogin(email: email));
        }
        if (value.status == FacebookLoginStatus.error) {
          Toast.error(context, 'Something went wrong');
        }
      });
    } on PlatformException catch (e) {
      Toast.error(context, e.message ?? 'Something went wrong');
    }
  }

  Future signInWithApple() async {
    try {
      final credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
        ],
      );
      _loginBloc.add(LoginEvent.socialLogin(
          email: credential.email, identifier: credential.userIdentifier));
    } on PlatformException catch (e) {
      Toast.error(context, e.message ?? 'Something went wrong');
    }
  }

  Future<void> signOut() async {
    try {
      if (await loginUtils.googleSignIn.isSignedIn()) {
        loginUtils.googleSignIn.signOut();
      }
      loginUtils.facebookLogin.logOut();
    } catch (e) {}
  }

  Widget _socialButton(void Function() onPressed, String text, Widget icon) {
    return SaOutlineButton(
      onPressed: () {
        newFocus();
        onPressed();
      },
      text: Text(
        text,
        style: SaTextStyles.subtitleLarge.copyWith(fontWeight: FontWeight.w500),
      ),
      icon: icon,
      height: 44.0.h,
    );
  }
}
