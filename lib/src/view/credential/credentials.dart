import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/document/view_document/bloc/view_document_bloc.dart';
import 'package:shift_alerts/src/provider/document/view_document/model.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/notifications.dart';
import 'package:shift_alerts/src/view/credential/upload_credential.dart';
import 'package:shift_alerts/src/widget/alert_dialog.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/no_shift.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class Credentials extends StatefulWidget {
  const Credentials({super.key});

  @override
  State<Credentials> createState() => _CredentialsState();
}

class _CredentialsState extends State<Credentials> {
  final ViewDocumentBloc _viewDocumentBloc = ViewDocumentBloc();

  Color textColor({required String status}) {
    if (status.isDeclined()) {
      return SaColors.red;
    } else if (status.isCompleted() || status.isApproved()) {
      return SaColors.green;
    } else {
      return SaColors.orange;
    }
  }

  Color boxColor({required String status}) {
    if (status.isDeclined()) {
      return SaColors.lightPink;
    } else if (status.isCompleted() || status.isApproved()) {
      return SaColors.lightGreen;
    } else {
      return SaColors.lightOrange;
    }
  }

  List<bool> isDissmissingList = [];

  @override
  void initState() {
    _viewDocumentBloc.add(const ViewDocumentEvent.getDocument());
    super.initState();
  }

  @override
  void dispose() {
    _viewDocumentBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _viewDocumentBloc,
      child: Column(
        children: [
          _appBar(),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                _viewDocumentBloc.add(const ViewDocumentEvent.getDocument());
                await Future.delayed(const Duration(seconds: 1));
              },
              child: ListView(
                children: [
                  BlocBuilder<ViewDocumentBloc, ViewDocumentState>(
                    builder: (context, state) {
                      return state.when(
                        initial: () => const SizedBox.shrink(),
                        loading: () => SizedBox(
                          height: MediaQuery.of(context).size.height * 0.8,
                          child: const Center(
                            child: Loader(),
                          ),
                        ),
                        success: (viewdocs) {
                          isDissmissingList = List.generate(
                              viewdocs.results.length, (index) => false);
                          return viewdocs.results.isEmpty
                              ? SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.8,
                                  child: const Noshift(
                                      message: "No Documents Found"),
                                )
                              : ListView.separated(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 24.0),
                                  itemCount: viewdocs.results.length,
                                  itemBuilder: (context, index) {
                                    return isDissmissingList[index]
                                        ? const SizedBox.shrink()
                                        : Dismissible(
                                            key: Key('$index'),
                                            confirmDismiss: (direction) async {
                                              if (direction ==
                                                  DismissDirection.endToStart) {
                                                final bool res =
                                                    await ShowAlertDialog()
                                                        .showAlertDialog(
                                                  context: context,
                                                  title: 'Delete credential',
                                                  content:
                                                      'Are you sure, You want to delete ?',
                                                  showCloseIcon: false,
                                                  onYesPressed: () =>
                                                      Navigator.pop(
                                                          context, true),
                                                );
                                                if (res) {
                                                  isDissmissingList[index] =
                                                      true;
                                                  _viewDocumentBloc.add(
                                                    ViewDocumentEvent
                                                        .deleteDocument(
                                                      id: viewdocs
                                                          .results[index]
                                                          .documentId,
                                                    ),
                                                  );
                                                }
                                                return false;
                                              } else {
                                                return false;
                                              }
                                            },
                                            background: Align(
                                              alignment: Alignment.centerRight,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 8.0),
                                                child: SvgPicture.asset(
                                                    SaIcons.trash),
                                              ),
                                            ),
                                            direction:
                                                DismissDirection.endToStart,
                                            child: Container(
                                                height: 80.0,
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 16.0,
                                                  vertical: 12.0,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: SaColors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12.0),
                                                ),
                                                child: _child(
                                                    viewdocs.results[index])),
                                          );
                                  },
                                  separatorBuilder: (context, index) =>
                                      const SizedBox(height: 16.0),
                                );
                        },
                        error: (error) {
                          return SizedBox(
                            height: MediaQuery.of(context).size.height * 0.8,
                            child: const Center(
                              child: Text(
                                'something wen\'t wrong',
                                style: SaTextStyles.title,
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                  const SizedBox(height: 24)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _child(Document doc) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            SvgPicture.asset(SaIcons.credential),
            const SizedBox(width: 8.0),
            Text(
              doc.documentName,
              style: SaTextStyles.subtitleLarge.copyWith(
                decoration: TextDecoration.underline,
              ),
            ),
            const Spacer(),
            GestureDetector(
              onTap: () async {
                Loader.show(context);

                await downloadFileandNotification(
                  doc.documentPath,
                  doc.documentName,
                ).then((_) => Loader.hide(context));
              },
              child: const Icon(Icons.download),
            ),
          ],
        ),
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 4.0,
          ),
          decoration: BoxDecoration(
            color: boxColor(status: doc.documentStatus),
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Text(
            doc.documentStatus,
            style: SaTextStyles.subtitleSmall.copyWith(
              color: textColor(status: doc.documentStatus),
            ),
          ),
        ),
      ],
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      leadingWidth: 0.0,
      title: const Text(
        SaString.credentials,
        style: TextStyle(
          fontSize: 24.0,
          color: SaColors.black,
          fontWeight: FontWeight.w600,
        ),
      ),
      titleSpacing: 24.0,
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 12.0,
            horizontal: 24.0,
          ),
          child: SaOutlineButton(
            onPressed: () async {
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const UploadCredential(),
                  )).then((value) {
                if (value == true) {
                  _viewDocumentBloc.add(const ViewDocumentEvent.getDocument());
                }
              });
            },
            icon: const Icon(Icons.add_sharp),
            text: const Text('Add'),
            width: 80.0,
          ),
        ),
      ],
      centerTitle: false,
      toolbarHeight: 60.0,
    );
  }

  Future<void> downloadFileandNotification(String url, String name) async {
    try {
      final directory = Platform.isIOS
          ? await getApplicationSupportDirectory()
          : Directory('storage/emulated/0/Download/Shift Alert');
      if (Platform.isAndroid && !(await directory.exists())) {
        await directory.create();
      }
      File file = Platform.isIOS
          ? File('${directory.path}/$name.pdf')
          : File('${directory.path}/$name');

      await _downloadPDF(url, name, file).then(
        (newFile) async {
          if (newFile != null) {
            final savedFile = await newFile
                .copy('${directory.path}/${name}_${randomNumber()}.pdf');
            await file.delete();
            await notificationsUtils
                .localNotification(payload: savedFile.path, title: '$name.pdf')
                .then((_) {
              Toast.success(context, "Download successfully");
            });
          }
        },
      );
    } catch (e) {
      Toast.error(context, 'Storage Permission Denied');
    }
  }

  Future<File?> _downloadPDF(String url, String name, File file) async {
    try {
      final request = await HttpClient().getUrl(Uri.parse(url));
      final response = await request.close();

      if (response.statusCode == 200) {
        final bytes = await consolidateHttpClientResponseBytes(response);
        return await file.writeAsBytes(bytes);
      }
    } catch (e) {
      Toast.success(context, "something went't wrong");
    }
    return null;
  }
}
