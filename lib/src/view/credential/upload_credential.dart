import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/icons.dart';
import 'package:shift_alerts/src/constant/string.dart';
import 'package:shift_alerts/src/constant/text_style.dart';
import 'package:shift_alerts/src/provider/document/upload_document/bloc/upload_document_bloc.dart';
import 'package:shift_alerts/src/utils/extension.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/widget/appbar.dart';
import 'package:shift_alerts/src/widget/button.dart';
import 'package:shift_alerts/src/widget/loader.dart';
import 'package:shift_alerts/src/widget/textfield.dart';
import 'package:shift_alerts/src/widget/toast.dart';

class UploadCredential extends StatefulWidget {
  const UploadCredential({super.key});

  @override
  State<UploadCredential> createState() => _UploadCredentialState();
}

class _UploadCredentialState extends State<UploadCredential> {
  final TextEditingController _name = TextEditingController();

  final UploadDocumentBloc _uploadDocumentBloc = UploadDocumentBloc();
  final pickedFile = ValueNotifier<File>(File(''));

  @override
  void dispose() {
    _name.dispose();
    pickedFile.dispose();
    _uploadDocumentBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _uploadDocumentBloc,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: appBar(context, title: SaString.uploadCredential),
          body: BlocListener<UploadDocumentBloc, UploadDocumentState>(
            listener: (context, state) {
              state.when(
                initial: () {},
                loading: () => Loader.show(context),
                success: () {
                  Loader.hide(context);
                  Toast.success(context, 'Document Uploaded successfully');
                  Navigator.pop(context, true);
                },
                error: (error) {
                  Loader.hide(context);
                  Toast.error(context, error);
                },
              );
            },
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  DottedBorder(
                    radius: const Radius.circular(12.0),
                    borderType: BorderType.RRect,
                    color: SaColors.hintColor,
                    padding: EdgeInsets.zero,
                    child: InkWell(
                      splashColor: SaColors.transparent,
                      highlightColor: SaColors.transparent,
                      onTap: () async {
                        FilePickerResult? files =
                            await FilePicker.platform.pickFiles(
                          allowMultiple: false,
                          type: FileType.custom,
                          allowedExtensions: ['pdf'],
                        );
                        if (files != null) {
                          pickedFile.value = File(files.files.first.path!);
                        }
                      },
                      child: Container(
                        height: 244.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: SaColors.white,
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        child: ValueListenableBuilder(
                          valueListenable: pickedFile,
                          builder: (context, value, child) => Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(SaIcons.credential),
                              const SizedBox(height: 6.0),
                              Text(
                                value.path.isEmpty
                                    ? SaString.uploadDocument
                                    : value.fileName(),
                                style: SaTextStyles.subtitleSmall.copyWith(
                                  color: SaColors.gray,
                                  fontSize: 15,
                                ),
                              ),
                              Text(
                                value.fileName().isEmpty
                                    ? ""
                                    : '${File(value.path).fileSize().toStringAsFixed(2)} MB',
                                style: SaTextStyles.subtitleSmall.copyWith(
                                  color: SaColors.gray,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 24.0),
                  const Text(
                    SaString.nameDocument,
                    style: SaTextStyles.subtitleSmall,
                  ),
                  const SizedBox(height: 8.0),
                  SaTextField(
                    controller: _name,
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 12.0,
                    ),
                    hintText: SaString.enterName,
                  ),
                  const Spacer(),
                  SaButton(
                    text: SaString.upload,
                    height: 48.0,
                    radius: 12.0,
                    onPressed: () async {
                      if (pickedFile.value.path.isNotEmpty &&
                          _name.text.isNotEmpty) {
                        File file = File(pickedFile.value.path);
                        final base64String =
                            base64.encode(file.readAsBytesSync());
                        final doc = {
                          "clinician_name": HiveUtils.get(HiveKeys.userName),
                          "clinician_position":
                              HiveUtils.get(HiveKeys.position),
                          "clinician_avatar":
                              HiveUtils.get(HiveKeys.avatarImage),
                          "document_name": _name.text.trim(),
                          "document_updated_date":
                              DateTime.now().toString().split(" ")[0],
                          "document_status": "Pending",
                          "clinician_id": HiveUtils.get(HiveKeys.clinicianId)
                        };
                        _uploadDocumentBloc.add(UploadDocumentEvent.upload(
                            doc: doc, base64string: base64String));
                      } else {
                        _name.text.isEmpty
                            ? Toast.error(context, 'Enter Document name')
                            : Toast.error(context, 'Select Document');
                      }
                    },
                  ),
                  const SizedBox(height: 24.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
