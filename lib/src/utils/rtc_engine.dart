// ignore_for_file: empty_catches

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtm/agora_rtm.dart';
import 'package:flutter/material.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/constant/string.dart';

class RtcEngineUtils {
  Future<void> initAgora() async {
    rtmClient = await AgoraRtmClient.createInstance(SaString.agoraAppId);
    rtcEngine = await RtcEngine.create(SaString.agoraAppId);
    initRtcEngine();
  }

  void initRtcEngine() async {
    await rtcEngine.disableVideo();
    await rtcEngine.muteLocalVideoStream(true);
    await rtcEngine.muteAllRemoteVideoStreams(true);
    await rtcEngine.enableAudio();
    await rtcEngine.setChannelProfile(ChannelProfile.Communication);
    await rtcEngine.setDefaultAudioRouteToSpeakerphone(false);
  }

  void rtcEventHandler(BuildContext context) async {
    rtcEngine.setEventHandler(RtcEngineEventHandler(
      error: (code) {
        // print('---- $code');
      },
      joinChannelSuccess: (channel, uid, elapsed) async {
        // print('--- $channel  $uid');
      },
      leaveChannel: (stats) {},
      userJoined: (uid, elapsed) {
        // print('---- Join');
        // callStatus.value = '00:00';
        callDuration();
      },
      userOffline: (uid, elapsed) {
        rtcEngine.leaveChannel();
        timer?.cancel();
        callStatus.value = 'Call Ended';
        Future.delayed(
          const Duration(seconds: 1),
          () => Navigator.pop(context),
        );
      },
    ));
  }
}

final RtcEngineUtils rtcEngineUtils = RtcEngineUtils();
