// ignore_for_file: non_constant_identifier_names

import 'dart:io';

import 'package:intl/intl.dart';

extension Flutter on String {
  String capitalize() =>
      substring(0, 1).toUpperCase() + substring(1).toLowerCase();
  DateTime parseDate() => DateTime.parse(this);
  String ydM() => split('-').reversed.join("-");

  bool isCompleted() => toLowerCase() == 'completed';
  bool isDeclined() => toLowerCase() == 'declined';
  bool isApproved() => toLowerCase() == 'approved';
  bool isPending() => toLowerCase() == 'pending';
  bool isOpen() => toLowerCase() == 'open';
}

extension Date on DateTime {
  String dmY() => DateFormat('dd-MM-yyyy').format(this);
  String mdY() => DateFormat('MM-dd-yyyy').format(this);
  String dmyInvoice() => DateFormat('dd/MM/yyyy').format(this);
  String d() => DateFormat('dd').format(this);
  String M() => DateFormat.MMM().format(this);
  String MM() => DateFormat.MMMM().format(this);
  String MMdY() => DateFormat.yMMMMd('en_US').format(this);
  String MdY() => DateFormat.yMMMd('en_US').format(this);
  String Md() => DateFormat.MMMd('en_US').format(this);
  String My() => DateFormat.yMMM('en_US').format(this);

  String hm() => DateFormat.jm().format(this);
  String h() => DateFormat.j().format(this);

  DateTime weekFirstDay() => subtract(Duration(days: weekday - 1));
  DateTime weekLastDay() => add(Duration(days: 7 - weekday));

  DateTime firstDate(int month) => DateTime(year, month, 1);
  DateTime lastDate() => DateTime(year, month + 1, 0);

  DateTime compDate() => toString().split(" ")[0].parseDate();

  bool isSameDay(DateTime date) =>
      year == date.year && month == date.month && day == date.day;

  bool isAfterOrEqual() => (isAfter(DateTime.now().compDate()) ||
      isSameDay(DateTime.now().compDate()));
}

extension FileUtils on File {
  double fileSize() {
    int sizeInBytes = lengthSync();
    double sizeInMb = sizeInBytes / (1024 * 1024);
    return sizeInMb;
  }

  String fileName() => path.split('/').last;
  String filetype() => path.split('.').last;
}

extension Iterables<E> on Iterable<E> {
  Map<K, List<E>> groupBy<K>(K Function(E) keyFunction) => fold(
      <K, List<E>>{},
      (Map<K, List<E>> map, E element) =>
          map..putIfAbsent(keyFunction(element), () => <E>[]).add(element));
}
