import 'package:flutter/material.dart';

class NavigatorUtils {
  static Future<dynamic> push(BuildContext context, Widget widget) async {
    FocusScope.of(context).unfocus();
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
  }

  static Future<void> pushRemoveUntil(
      BuildContext context, Widget widget) async {
    await Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
      (route) => false,
    );
  }

  static Future<void> pushReplacement(
      BuildContext context, Widget widget) async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
  }


  static void pop(BuildContext context) {
    Navigator.pop(context);
  }
}
