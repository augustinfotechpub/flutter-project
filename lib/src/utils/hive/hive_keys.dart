class HiveKeys {
  static const String boxName = 'shiftAlert';
  static const String accessToken = 'accessToken';
  static const String refreshToken = 'refreshToken';
  static const String email = 'email';
  static const String password = 'password';
  static const String rememberMe = 'rememberMe';
  static const String adminId = 'adminId';
  static const String clinicianId = 'clinicianId';
  static const String user = 'user';
  static const String userName = 'userName';
  // static const String clinicianName = 'clinicianName';
  static const String position = 'position';
  static const String avatarImage = 'avatarImage';
  static const String userType = 'userType';
  static const String fullName = 'fullName';
  static const String facilityId = 'facilityId';
  static const String deviceToken = 'deviceToken';
}
