import 'dart:io';

import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginUtils {
  final FacebookLogin facebookLogin = FacebookLogin();
  final List<FacebookPermission> permissions = [FacebookPermission.email];

  final GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: ['https://www.googleapis.com/auth/userinfo.email'],
    clientId: (Platform.isIOS
        ? '987373618168-qkr198tq8tj6q0i1kt28ocve4iq540oi.apps.googleusercontent.com'
        : ''),
    serverClientId:
        '987373618168-ia5bl0abbpaorbqucp9d2e0f396njtti.apps.googleusercontent.com',
  );
}

final LoginUtils loginUtils = LoginUtils();
