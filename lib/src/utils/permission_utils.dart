import 'package:geolocator/geolocator.dart';

class PermissionUtils {
  Future<bool> determinePosition(double lat, double lon) async {
    LocationPermission permission = await Geolocator.checkPermission();

    // to get And Measure Distance
    Future<bool> isIn100MeterRadius() async {
      final pos = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      // print(pos);
      // print(Geolocator.distanceBetween(pos.latitude, pos.longitude, lat, lon));
      return Geolocator.distanceBetween(pos.latitude, pos.longitude, lat, lon) <
          100;
    }

    // if permission Granted
    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      // Test if location services are enabled.
      bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        // Location services are not enabled don't continue
        // accessing the position and request users of the
        // App to enable the location services.
        final locationEnbled = await Geolocator.openLocationSettings();
        if (locationEnbled) {
          return await isIn100MeterRadius();
        } else {
          return Future.error('Location services are disabled.');
        }
      } else {
        return await isIn100MeterRadius();
      }
    } else {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        return Future.error('Location permissions are denied');
      } else {
        // if Permission granted now
        return await isIn100MeterRadius();
      }
    }
  }
}
