// ignore_for_file: use_build_context_synchronously, empty_catches

import 'dart:convert';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:open_filex/open_filex.dart';
import 'package:shift_alerts/src/constant/color.dart';
import 'package:shift_alerts/src/constant/const.dart';
import 'package:shift_alerts/src/provider/api_provider.dart';
import 'package:shift_alerts/src/utils/hive/hive.dart';
import 'package:shift_alerts/src/utils/navigation_utils.dart';
import 'package:shift_alerts/src/view/chat/call.dart';
import 'package:shift_alerts/src/view/chat/chat.dart';

class NotificationsUtils {
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  final AwesomeNotifications _notifications = AwesomeNotifications();

  void generateToken() async {
    await _messaging.requestPermission(
      sound: true,
      badge: true,
      alert: true,
      announcement: true,
      criticalAlert: true,
    );
    final token = await _messaging.getToken();
    HiveUtils.set(HiveKeys.deviceToken, token);
    final response = await apiProvider.updateToken(token!);
    if (response.statusCode != 200) {
      apiProvider.createToken(token);
    }
  }

  void sendNotification({
    required String message,
    required String type,
    required String peerId,
    required String channelId,
    required String image,
  }) async {
    try {
      await apiProvider.sendNotification(
        message: message,
        type: type,
        peerId: peerId,
        channelId: channelId,
        image: image,
      );
    } catch (e) {}
  }

  void initNotifications() {
    _notifications.initialize(
      'resource://drawable/notification_icon',
      [
        NotificationChannel(
          channelGroupKey: 'shift_alerts_group',
          channelKey: 'shift_alerts',
          channelName: 'Shift Alerts',
          channelDescription: 'Shift Alerts',
          importance: NotificationImportance.High,
          enableVibration: true,
          playSound: true,
          defaultColor: SaColors.primary,
          soundSource: 'resource://raw/cool',
        ),
      ],
      channelGroups: [
        NotificationChannelGroup(
          channelGroupkey: 'shift_alerts_group',
          channelGroupName: 'Shift Alerts',
        ),
      ],
    );
    FirebaseMessaging.onBackgroundMessage(_messageHandler);
  }

  void messageListener() {
    FirebaseMessaging.onMessage.listen((event) async {
      showNotification(event);
    });
  }

  void onMessageListener(BuildContext context) {
    try {
      _notifications.actionStream.listen((event) async {
        if (event.body == 'Downloaded' &&
            event.payload!['message_type'] == 'path') {
          await OpenFilex.open(event.payload!['file_location']);
        } else if (event.buttonKeyPressed == 'decline') {
          await apiProvider.sendNotification(
            message: 'Remote Call End',
            type: 'call',
            peerId: event.payload!['peerId']!,
            channelId: event.payload!['channelId']!,
            image: '',
          );
        } else if (event.buttonKeyPressed == 'answer') {
          callerDeviceType = event.payload!['devide_type']!;
          NavigatorUtils.push(
              context,
              VoiceCall(
                joined: true,
                channelId: event.payload!['channelId']!,
                peerId: event.payload!['peerId']!,
                peerName: event.payload!['peerName']!,
                image: event.payload!['image']!,
              ));

          final response = await apiProvider.getAgoraToken(
              channelId: event.payload!['channelId']!);
          Map<String, dynamic> json = jsonDecode(response.body);
          if (json.containsKey('payload')) {
            await rtcEngine.joinChannel(
                json['payload'], event.payload!['channelId']!, null, 0);
          } else {
            await rtcEngine.joinChannel(
                null, event.payload!['channelId']!, null, 0);
          }
          callStatus.value = 'Calling';
          callDuration();
        } else {
          if (event.payload!['message_type'] == 'call') {
            callStatus.value = 'Ringing';
            NavigatorUtils.push(
              context,
              VoiceCall(
                joined: false,
                channelId: event.payload!['channelId']!,
                peerId: event.payload!['peerId']!,
                peerName: event.payload!['peerName']!,
                image: event.payload!['image']!,
              ),
            );
          } else if (currentChannel != event.payload!['channelId']!) {
            currentChannel = event.payload!['channelId']!;
            NavigatorUtils.push(
              context,
              Chat(
                image: event.payload!['image']!,
                name: event.payload!['peerName']!,
                peerId: event.payload!['peerId']!,
                channelId: currentChannel,
              ),
            ).then((value) => currentChannel = '');
          }
        }
      });
    } catch (e) {}
  }

  void showNotification(RemoteMessage message) async {
    Map<String, dynamic> data = jsonDecode(message.data['content']);
    Map<String, String>? payload =
        Map<String, String>.from(jsonDecode(message.data['payload']));
    if (data['body'] == 'Remote Call End' &&
        payload['message_type'] == 'call') {
      callStatus.value = 'Call Declined';
    } else if (data['body'] == 'Remote Call Accepted' &&
        payload['message_type'] == 'call') {
      isJoin.value = true;
    } else {
      if (payload['channelId'] != currentChannel) {
        await AwesomeNotifications().createNotification(
          content: NotificationContent(
            id: 1,
            channelKey: data['channelKey'],
            title: data['title'],
            body: data['body'] == 'call' ? null : data['body'],
            groupKey: 'shift_alerts',
            payload: payload,
            category: data['body'] == 'call' ? NotificationCategory.Call : null,
            wakeUpScreen: data['body'] == 'call' ? true : null,
            // autoDismissible: data['body'] == 'call' ? false : null,
          ),
          actionButtons: data['body'] == 'call'
              ? [
                  NotificationActionButton(
                    key: 'answer',
                    label: 'Answer',
                    showInCompactView: true,
                    isDangerousOption: false,
                    enabled: true,
                    color: SaColors.green,
                    buttonType: ActionButtonType.Default,
                  ),
                  NotificationActionButton(
                    key: 'decline',
                    label: 'Decline',
                    showInCompactView: true,
                    isDangerousOption: true,
                    enabled: true,
                    color: SaColors.red,
                    buttonType: ActionButtonType.Default,
                  ),
                ]
              : null,
        );
      }
    }
  }

  Future<void> localNotification({
    required String title,
    required String payload,
  }) async {
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 1,
        channelKey: 'shift_alerts',
        title: title,
        body: 'Downloaded',
        groupKey: 'shift_alerts',
        payload: {"file_location": payload, 'type': 'path'},
      ),
    );
  }
}

@pragma('vm:entry-point')
Future<void> _messageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  notificationsUtils.showNotification(message);
}

final NotificationsUtils notificationsUtils = NotificationsUtils();
